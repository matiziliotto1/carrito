<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variantes_producto extends Model
{
    protected $fillable = [
        'stock',
        'color',
        'talle_ropa',
        'talle_calzado',
        'talle_anzuelo',
        'mano_habil',
        'longitud',
        'fuerza',
        'accion',
        'diametro',
        'habilitado',
        'id_producto',
    ];
    protected $table = 'variantes_producto';
    protected $primaryKey = 'id_variante_producto';
    public $timestamps = false;
}
