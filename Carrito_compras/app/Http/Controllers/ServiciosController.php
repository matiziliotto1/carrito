<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Servicio;
use App\Categoria;
use App\Subcategoria;
use App\Imagenes_producto;
use Illuminate\Support\Facades\DB;
use Exception;

class ServiciosController extends Controller
{
    /*
        Esta funcion busca el servicio seleccionado por el cliente, asocia su categoria
        e imagenes y tambien retorna una lista de servicios que le podrian interesar
        al cliente.
    */
    public function verServicioC($id_servicio)
    {
        $servicio = Servicio::findOrFail($id_servicio);

        $categoria = DB::table('categorias')
                ->join('subcategorias', 'subcategorias.id_categoria', '=', 'categorias.id_categoria')
                ->where("subcategorias.id_subcategoria", $servicio->id_subcategoria)
                ->select('categorias.*')
                ->first();

        $servicio->categoria = $categoria;

        $imagenes_controller = new Imagenes_servicioController();
        $servicio->imagenes = $imagenes_controller->getImagenesDeServicio($id_servicio);

        $servicios_que_pueden_gustar = $this->servicios_random();

        return view("clientes.verServicioIndividual", compact("servicio", "servicios_que_pueden_gustar"));
    }

    /*
        Esta funcion busca todos los servicios asociados a la categoria seleccionada y le
        asocia la primer imagen que posee almacenada.
    */
    public function getServiciosXCategoria(Request $request)
    {
        if (Categoria::where('id_categoria', $request->id_categoria)->exists()) {
            try {
                $servicios = DB::table('servicios')
                        ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                        ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                        ->where([
                            ['servicios.habilitado', '=', 1],
                            ['servicios.eliminado', '=', 0],
                            ['categorias.id_categoria', '=', $request->id_categoria],
                        ])
                        ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                        ->orderBy('servicios.precio', 'ASC')
                        ->paginate(20);

                $imagenes_controller = new Imagenes_servicioController();
                foreach ($servicios as $servicio) {
                    if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                        $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
                    } else {
                        $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
                    }
                }

                $categorias_controller = new CategoriaController();
                $categorias = $categorias_controller->getCategoriasConSubcategorias(0);

                //Hay que devolver todo esto para poder filtrar ya que usamos verservicios.php para todas
                $precio_min = Servicio::min('precio');

                $precio_max = Servicio::max('precio');

                $id_categoria = $request->id_categoria;

                $id_subcategoria = '';

                $texto = '';

                $ordenar_por = 0;

                return view("clientes.verServicios", compact("servicios", "categorias", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por"));
            } catch (Exception $e) {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    /*
        Esta funcion busca todos los servicios asociados a la subcategoria seleccionada y le
        asocia la primer imagen que posee almacenada.
    */
    public function getServiciosXSubcategoria(Request $request)
    {
        try {
            if (Subcategoria::where('id_subcategoria', $request->id_subcategoria)->exists()) {
                $servicios = DB::table('servicios')
                        ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                        ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                        ->where([
                            ['servicios.habilitado', '=', 1],
                            ['servicios.eliminado', '=', 0],
                            ['servicios.id_subcategoria', '=', $request->id_subcategoria],
                        ])
                        ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                        ->orderBy('servicios.precio', 'ASC')
                        ->paginate(20);

                $imagenes_controller = new Imagenes_servicioController();
                foreach ($servicios as $servicio) {
                    if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                        $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
                    } else {
                        $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
                    }
                }

                $categorias_controller = new CategoriaController();
                $categorias = $categorias_controller->getCategoriasConSubcategorias(0);

                //Hay que devolver todo esto para poder filtrar ya que usamos verservicios.php para todas
                $precio_min = Servicio::min('precio');

                $precio_max = Servicio::max('precio');

                $id_subcategoria = $request->id_subcategoria;

                $id_categoria = '';

                $texto = '';

                $ordenar_por = 0;

                return view("clientes.verServicios", compact("servicios", "categorias", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por"));
            } else {
                abort(404);
            }
        } catch (Exception $e) {
            abort(404);
        }
    }

    /*
        Esta funcion busca todos los servicios que haya almacenados y le
        asocia la primer imagen que posee almacenada.
    */
    public function getServiciosC()
    {
        $servicios = DB::table('servicios')
                ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                ->where([["servicios.habilitado", 1], ['servicios.eliminado', 0]])
                ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                ->orderBy('servicios.precio', 'ASC')
                ->paginate(20);

        $imagenes_controller = new Imagenes_servicioController();
        foreach ($servicios as $servicio) {
            if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
            } else {
                $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
            }
        }

        $categorias_controller = new CategoriaController();
        $categorias = $categorias_controller->getCategoriasConSubcategorias(0);
        
        //Hay que devolver todo esto para poder filtrar ya que usamos verservicios.php para todas
        $precio_min = Servicio::min('precio');

        $precio_max = Servicio::max('precio');

        $id_categoria = '';

        $id_subcategoria = '';
        
        $texto = '';

        $ordenar_por = 0;

        return view("clientes.verServicios", compact("servicios", "categorias", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por"));
    }

    /*
        Esta funcion busca 8 servicios random que haya almacenados y le asocia
        la primer imagen que posee almacenada. Solo retorna informacion, no retorna
        a una vista
    */
    public function servicios_random()
    {
        //TODO: por ahora no tenemos en cuenta los servicios destacados
        //$servicios_destacados = Servicio::where('destacado', 1)->get();

        $servicios_destacados = Servicio::where([['habilitado', 1], ['eliminado', 0]])->get();

        $servicios = array();

        for ($i=0; $i < count($servicios_destacados) && $i <5; $i++) {
            array_push($servicios, $servicios_destacados[$i]);
        }
        
        $imagenes_controller = new Imagenes_servicioController();
        foreach ($servicios as $servicio) {
            if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
            } else {
                $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
            }
            $categoria = DB::table('categorias')
                ->join('subcategorias', 'subcategorias.id_categoria', '=', 'categorias.id_categoria')
                ->where("subcategorias.id_subcategoria", $servicio->id_subcategoria)
                ->select('categorias.*')
                ->first();

            $servicio->categoria = $categoria;
        }
        return $servicios;
    }

    public function buscadorServicios(Request $request)
    {
        $texto = $request->texto;

        $servicios = DB::table('servicios')
                ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                ->where([["servicios.habilitado", 1],['servicios.titulo','LIKE','%'.$texto.'%'], ['servicios.eliminado', 0]])
                ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                ->orderBy('servicios.precio', 'ASC')
                ->paginate(20);

        $imagenes_controller = new Imagenes_servicioController();
        foreach ($servicios as $servicio) {
            if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
            } else {
                $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
            }
        }

        $categorias_controller = new CategoriaController();
        $categorias = $categorias_controller->getCategoriasConSubcategorias(0);

        //Hay que devolver todo esto para poder filtrar ya que usamos verservicios.php para todas
        $precio_min = Servicio::min('precio');

        $precio_max = Servicio::max('precio');

        $id_categoria = '';

        $id_subcategoria = '';

        $ordenar_por = 0;

        return view("clientes.verservicios", compact("servicios", "categorias", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por"));
    }

    /*
        Funcion para filtrar los servicios segun precio y marca (tiene en cuenta si se envia una
        categoria o subcategoria) y ordenarlos por nombre o precio (ASC y DESC)
    */

    public function filtrarservicios(Request $request)
    {
        try {
            //Tenemos en cuenta si viene desde getservicior por categoria, subcategoria o el buscador
            $query=[
                ['servicios.habilitado', '=', 1],
                ['servicios.eliminado', 0]
            ];
            if ($request->id_categoria!='') {
                array_push($query, ['categorias.id_categoria', '=', $request->id_categoria]);
            } elseif ($request->id_subcategoria!='') {
                array_push($query, ['servicios.id_subcategoria', '=', $request->id_subcategoria]);
            } elseif ($request->texto!='') {
                array_push($query, ['servicios.titulo','LIKE','%'.$request->texto.'%']);
            }
            //Nos fijamos si filtro por orden, sino por precio de menor a mayor por defecto
            switch ($request->ordenar_por) {
                case '1':
                    $param1 = 'servicios.precio';
                    $param2 = 'ASC';
                    break;
                case '2':
                    $param1 = 'servicios.precio';
                    $param2 = 'DESC';
                    break;
                case '3':
                    $param1 = 'servicios.titulo';
                    $param2 = 'ASC';
                    break;
                case '4':
                    $param1 = 'servicios.titulo';
                    $param2 = 'DESC';
                    break;
                default:
                    $param1 = 'servicios.precio';
                    $param2 = 'ASC';
                    break;
            }

            $servicios = DB::table('servicios')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                    ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                    ->where($query)
                    ->whereBetween('servicios.precio', [$request->precio_min, $request->precio_max])
                    ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                    ->orderBy($param1, $param2)
                    ->paginate(20);

            //Obtener las imagenes
            $imagenes_controller = new Imagenes_servicioController();
            $marcas = array();
            foreach ($servicios as $servicio) {
                if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                    $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
                } else {
                    $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
                }
            }

            //Obtener las categorias
            $categorias_controller = new CategoriaController();
            $categorias = $categorias_controller->getCategoriasConSubcategorias(0);

            //Hay que devolver todo esto para poder filtrar ya que usamos verservicios.php para todas
            $precio_min = $request->precio_min;

            $precio_max = $request->precio_max;

            $id_categoria = $request->id_categoria;

            $id_subcategoria = $request->id_subcategoria;

            $texto = $request->texto;

            $ordenar_por = $request->ordenar_por;

            return view("clientes.verServicios", compact("servicios", "categorias", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por"));
        } catch (Exception $e) {
            abort(404);
        }
    }


    //------------------------------------Funciones para el administrador------------------------------------INICIO
    public function getservicios()
    {
        $servicios = Servicio::where([["habilitado", 1], ['eliminado', 0]])->orderby('created_at', 'DESC')->paginate(50);
        $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo', 0)->get();
        return view("admin.servicios.verServicios", compact("servicios", "categorias"));
    }

    public function buscadorAdminServicios(Request $request)
    {
        $servicios = DB::table('servicios')
                ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                ->where([['servicios.titulo', 'LIKE', '%'.$request->texto.'%'], ['servicios.eliminado', 0]])
                ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                ->orderBy('servicios.precio', 'ASC')
                ->paginate(20);
        $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo','0')->get();
        return view("admin.servicios.verServicios", compact("servicios", "categorias"));
    }

    public function getserviciosDeshabilitados()
    {
        $servicios = Servicio::where([["habilitado", 0], ['eliminado', 0]])->paginate(50);
        $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo', 0)->get();
        return view("admin.servicios.verServicios", compact("servicios", "categorias"));
    }

    public function editarservicio($id_servicio)
    {
        $servicio = Servicio::find($id_servicio);
        $subcategorias = Subcategoria::where('tipo', 0)->get();
        return view("admin.servicios.editarServicio", compact("servicio", "subcategorias"));
    }

    public function updateServicio(Request $request)
    {
        try {
            $servicio = Servicio::find($request->id_servicio);

            $servicio->titulo = $request->titulo;
            $servicio->descripcion = $request->descripcion;
            $servicio->precio = $request->precio;
            $servicio->id_subcategoria = $request->id_subcategoria;
            $servicio->descripcion_resumida = $request->descripcion_resumida;
            $servicio->destacado = $request->destacado;
            $servicio->habilitado = $request->habilitado;

            $servicio->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Servicio editado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al editar servicio.']);
        }
    }

    public function deshabilitarservicio(Request $request)
    {
        try {
            $servicio = Servicio::find($request->id_servicio);
            $servicio->habilitado = 0; // 0 = FALSE
            $servicio->destacado = 0; // 0 = FALSE
            $servicio->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'servicio deshabilitado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al deshabilitar el servicio.']);
        }
    }

    public function habilitarservicio(Request $request)
    {
        try {
            $servicio = Servicio::find($request->id_servicio);
            $servicio->habilitado = 1; // 1 = TRUE
            $servicio->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'servicio habilitado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al habilitar el servicio.']);
        }
    }

    public function createservicio(Request $request)
    {
        try {
            $id_servicio = Servicio::create([
                'titulo' => $request->titulo,
                'precio' => $request->precio,
                'descripcion_resumida' => $request->descripcion_resumida,
                'descripcion' => $request->descripcion,
                'id_subcategoria' => $request->id_subcategoria,
                'destacado' => $request->destacado,
            ])->id_servicio;

            return response()->json(['success'=>'true' , 'mensaje'=>'servicio creado correctamente.' , 'id_servicio'=>$id_servicio]);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al crear servicio.']);
        }
    }

    /*
        Esta funcion busca todos los servicios asociados a la categoria seleccionada y los muestra
        en la vista de los servicios para que los pueda editar o eliminar.
    */
    public function getServiciosXCategoriaAdmin($id_categoria)
    {
        if (Categoria::where('id_categoria', $id_categoria)->exists()) {
            $servicios = DB::table('servicios')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                    ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                    ->where([
                        ['categorias.id_categoria', '=', $id_categoria],
                        ['servicios.eliminado', 0]
                    ])
                    ->select('servicios.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                    ->paginate(50);

            $nombre_categoria = Categoria::find($id_categoria)->nombre_categoria;

            $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo','0')->get();
            return view("admin.servicios.verServicios", compact("servicios", "categorias", "nombre_categoria"));
        } else {
            abort(404);
        }
    }

    /*
        Esta funcion busca todos los servicios asociados a la subcategoria seleccionada y los muestra
        en la vista de los servicios para que los pueda editar o eliminar.
    */
    public function getserviciosXSubcategoriaAdmin($id_subcategoria)
    {
        if (Subcategoria::where('id_subcategoria', $id_subcategoria)->exists()) {
            $servicios = DB::table('servicios')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'servicios.id_subcategoria')
                    ->where([
                        ['subcategorias.id_subcategoria', '=', $id_subcategoria],
                        ['servicios.eliminado', 0]
                    ])
                    ->select('servicios.*')
                    ->paginate(50);

            $subcategoria = Subcategoria::find($id_subcategoria);
            $nombre_subcategoria = $subcategoria->nombre_subcategoria;
            $nombre_categoria = Categoria::find($subcategoria->id_categoria)->nombre_categoria;

            $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo','0')->get();
            return view("admin.servicios.verServicios", compact("servicios", "categorias", "nombre_subcategoria", "nombre_categoria"));
        } else {
            abort(404);
        }
    }

    public function deleteServicio(Request $request)
    {
        try {
            $servicio = Servicio::find($request->id_servicio);
            $servicio->eliminado = 1;
            $servicio->save();

            $eliminado = DB::table('servicios_x_carritos')->where("id_servicio", $request->id_servicio)->delete();

            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha eliminado correctamente el servicio.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al eliminar el servicio.']);
        }
    }
    //------------------------------------Funciones para el administrador------------------------------------FIN
}
