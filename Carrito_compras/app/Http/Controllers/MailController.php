<?php

namespace App\Http\Controllers;

use App\Compras;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Contacto;

use App\Imagenes_producto;
use App\Productos;
use App\Servicio;
use App\User;
use App\Variantes_producto;

class MailController extends Controller {

    public function enviar_email_basico() {
        $data = array('name'=>"Virat Gandhi");
    
        Mail::send(['text'=>'mail'], $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
                ('Laravel Basic Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
    }

    public function enviar_mail_compra($data, $id_compra) {
        $usuario = User::find($data['id_usuario']);
        $email = $usuario->email;

        $tipo_compra = $data['tipo_compra'];

        $metodo_envio = "";
        $costo_envio = "";

        if(isset($data['metodo_envio'])){
            $metodo_envio = $data['metodo_envio'];
        }
        
        if(isset($data['costo_envio'])){
            $costo_envio = $data['costo_envio'];
        }

        $items_aux = $data['items'];

        $items = array();

        if($tipo_compra == 1){
            
            $imagenes_producto_controller = new Imagenes_productoController();
            foreach($items_aux as $item){
                $id_producto = Variantes_producto::find($item['id_variante_producto'])->id_producto;
                $item['nombre'] = Productos::find($id_producto)->nombre;

                //Busco la primera imagen si es que tiene.
                if (Imagenes_producto::where('id_producto', $id_producto)->exists()) {
                    $item['imagen'] = $imagenes_producto_controller->getPrimeraImagenDeProducto($id_producto);
                } else {
                    $item['imagen'] = '/img/imagenes_productos/producto_sin_imagen.jpg';
                }

                array_push($items, $item);

                $total = $data['total'];
            }
        }
        else if($tipo_compra == 0){

            $imagenes_servicio_controller = new Imagenes_servicioController();
            foreach($items_aux as $item){
                $item['nombre'] = Servicio::find($item['id_servicio'])->titulo;

                //Busco la primera imagen si es que tiene.
                if (Imagenes_producto::where('id_servicio', $item['id_servicio'])->exists()) {
                    $item['imagen'] = $imagenes_servicio_controller->getPrimeraImagenDeServicio($item['id_servicio']);
                } else {
                    $item['imagen'] = '/img/imagenes_servicios/servicio_sin_imagen.png';
                }

                array_push($items, $item);
            }

            $total = $data['total_servicios'];
        }

        $data = array('num_compra'=>$id_compra, 'total'=>$total, 'items'=>$items, 'tipo_compra'=>$tipo_compra, 'metodo_envio'=>$metodo_envio, 'costo_envio'=>$costo_envio, 'usuario'=>$usuario);

        Mail::send('mail.compra', $data, function ($message) use ($email) {
            $message->to($email, 'Compra realizada en Mercado Fishing')->subject
                ('Compra realizada en Mercado Fishing');
            $message->from('xyz@gmail.com', 'Mercado Fishing');
        });
    }

    public function enviar_mail_estado($id_compra) {
        $compra = Compras::find($id_compra);

        $usuario = User::find($compra->id_usuario);
        $email = $usuario->email;

        $contacto = Contacto::find(1);

        $data = array(
            //Estos siempre tienen que estar
            'usuario'=>$usuario,
            'estado' => $compra->estado,
            'contacto' => $contacto->email_info,
            //Estos pueden no estar
            'codigo_seguimiento' => $compra->codigo_seguimiento, //Este deberia estar si el estado es En camino
            'contacto_reclamos' => $contacto->email_reclamos //Este deberia estar si el estado es Finalizada
        );

        Mail::send('mail.estado', $data, function ($message) use ($email) {
            $message->to($email, 'Compra realizada en Mercado Fishing')->subject
                ('Compra realizada en Mercado Fishing');
            $message->from('pepez@mailinator.com', 'Mercado Fishing');
        });
    }

    public function enviar_mail_con_documentos() {
        $data = array('name'=>"Virat Gandhi");

        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
                ('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
    }
}