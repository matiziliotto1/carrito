<?php

namespace App\Http\Controllers;

use App\Compras_x_servicios;
use App\Servicio;
use Illuminate\Http\Request;

class Compras_x_ServiciosController extends Controller
{
    public function agregarServicio($id_compra, $id_servicio, $precio)
    {
        $compras_x_servicio = new Compras_x_Servicios();

        $compras_x_servicio->id_compra = $id_compra;
        $compras_x_servicio->id_servicio = $id_servicio;
        $compras_x_servicio->precio = $precio;

        $compras_x_servicio->save();
    }

    /*
        Esta funcion recibe el id de la compra, consulta todos los servicios asociados a esa compra.
        Para cada servicio consultado, se busca su titulo y se le agrega a los datos y luego retorna
        los servicios asociados a la compra.
    */
    public function getServiciosXCompra($id_compra)
    {
        $servicios_comprados = Compras_x_servicios::where("id_compra", $id_compra)->select("id_servicio", "precio")->get();

        foreach($servicios_comprados as $servicio){
            $servicio_aux = Servicio::find($servicio->id_servicio)->select("titulo")->first();
            $servicio->titulo = $servicio_aux->titulo;
        }

        return $servicios_comprados;
    }
}
