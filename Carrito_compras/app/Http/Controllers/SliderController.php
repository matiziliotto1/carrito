<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use Illuminate\Support\Facades\Storage;
use Exception;

class SliderController extends Controller
{
    
    public function verSlidersView(Request $request)
    {
        try {
            $sliders = Slider::orderby('orden')->get();
            return view('admin.slider.verSliders', compact('sliders'));
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al obtener sliders de la base de datos: ".$e->getMessage()
            ]);
        }
    }

    public function editarSliderView(Request $request)
    {
        try {
            $slider = Slider::find($request->id_imagen);
            $ordenes= Slider::select('orden')->orderby('orden', 'ASC')->get();
            return view('admin.slider.editarSlider', compact('slider', 'ordenes'));
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al obtener slider de la base de datos: ".$e->getMessage()
            ]);
        }
    }
    
    public function updateSlider(Request $request)
    {
        try {
            $slider = Slider::find($request->id_imagen);

            //Hago el intercambio de orden con el otro slider
            $sliderOrden = Slider::where('orden', $request->orden)->first();

            $ordenAux=$slider->orden;
            $slider->orden=$sliderOrden->orden;
            $sliderOrden->orden=$ordenAux;
            $sliderOrden->save();

            $slider->texto = $request->texto;
            $slider->mostrar = $request->mostrar;

            //Guardo el slider para obtener el id y poder usarlo en la imagen
            $slider->save();
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al cargar slider a la base de datos: ".$e->getMessage()
            ]);
        }
        try {
            //Guardo la imagen con el id del slider para evitar sobrescritura
            if (!is_null($request->file('archivo_imagen'))) {
                $archivo=$request->file('archivo_imagen');
                $nombre_archivo=$archivo->getClientOriginalName();
                $archivo->storeAs(
                    '/img/slider/',
                    $slider->id_imagen.'_'.$nombre_archivo,
                    'public'
                );

                if (file_exists( public_path() . $slider->url)) {
                    //Elimino la imagen anterior
                    Storage::disk('public')->delete($slider->url);
                }

                //Guardo el nuevo url en la BD
                $slider->url = 'img/slider/'.$slider->id_imagen.'_'.$nombre_archivo;
                $slider->save();
            }
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al subir imagen al servidor: ".$e->getMessage()
            ]);
        }
        //Si no ocurrio ningún problema se avisa que el slider se creo correctamente
        return response([
            "success"=>"true",
            "mensaje"=>"Slider editado correctamente"
        ]);
    }
}
