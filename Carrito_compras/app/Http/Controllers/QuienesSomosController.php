<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quienes_somos;

class QuienesSomosController extends Controller
{
    public function verQuienesSomosUsuario()
    {
        $textos = Quienes_somos::first();
        return view('clientes.quienes_somos', compact('textos'));
    }

    public function verQuienesSomosAdmin()
    {
        $textos = Quienes_somos::first();
        return view('admin.quienes_somos.verQuienes_somos', compact('textos'));
    }

    public function updateQuienesSomos(Request $request)
    {
        try {
            $textos = Quienes_somos::first();
            $textos->review = $request->review;
            $textos->objetivos = $request->objetivos;
            $textos->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Quienes somos actualizado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al actualizar quienes somos.']);
        }
    }
}
