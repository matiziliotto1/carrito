<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Categoria;
use App\Menu;
use App\Subcategoria;
use Exception;

class CategoriaController extends Controller
{
    public function getCategoriasConSubcategorias($tipo)
    {
        $categorias = Categoria::where([['habilitado',1],['tipo',$tipo]])->get();


        foreach($categorias as $categoria){

            $subcategorias = DB::table('subcategorias')->where('id_categoria',$categoria->id_categoria)->get();
            
            $categoria->subcategorias = $subcategorias;
        }

        return $categorias;
    }

    public function verCategoriasView(Request $request)
    {
        try {
            $categorias = Categoria::paginate(10);
            return view('admin.categorias.verCategorias', compact('categorias'));
        } catch (Exception $e) {
            return back()->with('mensaje', 'Error al obtener categorias de la base de datos');
        }
    }

    public function createCategoria(Request $request)
    {
        try {
            $categoria = new Categoria;
            
            $categoria->nombre_categoria = $request->nombre_categoria;
            $categoria->link = '';
            $categoria->tipo = $request->tipo;

            $categoria->save();

            $categoria->link = '/categoria/verCategoria/'.$categoria->id_categoria;

            $categoria->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha agregado correctamente una nueva categoria.', 'id_categoria' => $categoria->id_categoria]);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al agregar categoria.']);
        }
    }
    
    public function updateCategoria(Request $request)
    {
        try {
            $categoria = Categoria::find($request->id_categoria);

            $categoria->nombre_categoria = $request->nombre_categoria;
            $categoria->habilitado = $request->habilitado;
            
            $categoria->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Categoria editada correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al editar categoria.']);
        }
    }
}
