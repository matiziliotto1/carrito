<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imagenes_producto;
use Exception;
use Illuminate\Support\Facades\Storage;

class Imagenes_productoController extends Controller
{

    public function getPrimeraImagenDeProducto($id_producto)
    {
        return Imagenes_Producto::where('id_producto', $id_producto)->first()->url;
    }

    public function getImagenesDeProducto($id_producto)
    {
        return Imagenes_producto::where('id_producto', $id_producto)->get();
    }

    //------------------------------------Funciones para el administrador------------------------------------INICIO
    public function verImagenesProductoView(Request $request)
    {
        $id_producto = $request->id_producto;
        $imagenes=Imagenes_producto::where('id_producto', $request->id_producto)->orderBy('orden', 'ASC')->paginate(10);
        $ordenes=Imagenes_producto::select('orden')->where('id_producto', $request->id_producto)->get();
        return view('admin.productos.Imagenes_producto.verImagenesProducto', compact('imagenes', 'ordenes', 'id_producto'));
    }

    public function editarImagenProductoView(Request $request)
    {
        $imagen=Imagenes_producto::find($request->id_imagen);
        $ordenes=Imagenes_producto::select('orden')->where('id_producto', $imagen->id_producto)->get();
        return view('admin.productos.Imagenes_producto.editarImagen', compact('imagen', 'ordenes'));
    }

    public function agregarImagen(Request $request)
    {
        try {
            $imagen = new Imagenes_producto;

            //Seteo el orden al final
            if (Imagenes_producto::where('id_producto', $request->id_producto)->count()>0) {
                $ordenes=Imagenes_producto::select('orden')->where('id_producto', $request->id_producto)->orderBy('orden', 'desc')->first();
                $imagen->orden=$ordenes->orden+1;
            } else {
                $imagen->orden = 1;
            }
            $imagen->url = '';
            $imagen->id_producto=$request->id_producto;
            $imagen->mostrar = $request->mostrar;

            //Guardo el imagen para obtener el id y poder usarlo en la imagen
            $imagen->save();
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al cargar imagen a la base de datos: ".$e->getMessage()
            ]);
        }
        try {
            //Guardo la imagen con el id del imagen para evitar sobrrescritura
            if (!is_null($request->file('archivo_imagen'))) {
                $archivo=$request->file('archivo_imagen');
                $archivo->storeAs(
                    '/img/imagenes_productos/producto'.$request->id_producto.'/',
                    $imagen->id_imagen.'_imagen_'.$imagen->id_imagen.'.'.$archivo->getClientOriginalExtension(),
                    'public'
                );
                //Guardo el nuevo url en la BD
                $imagen->url = 'img/imagenes_productos/producto'.$request->id_producto.'/'.$imagen->id_imagen.'_imagen_'.$imagen->id_imagen.'.'.$archivo->getClientOriginalExtension();
                $imagen->save();
            }
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al subir imagen al servidor: ".$e->getMessage()
            ]);
        }
        //Si no ocurrio ningún problema se avisa que el imagen se creo correctamente
        return response([
            "success"=>"true",
            "mensaje"=>"Imagen agregada correctamente",
            "id_imagen"=>$imagen->id_imagen,
            "url"=>$imagen->url,
            "orden"=>$imagen->orden,
            "mostrar"=>$imagen->mostrar
        ]);
    }

    public function editarImagen(Request $request)
    {
        try {
            $imagen = Imagenes_producto::find($request->id_imagen);

            //Hago el intercambio de orden con el otro imagen
            if (Imagenes_producto::where('orden', $request->orden)->exists()) {
                $imagenOrden = Imagenes_producto::where('orden', $request->orden)->first();

                $ordenAux=$imagen->orden;
                $imagen->orden=$imagenOrden->orden;
                $imagenOrden->orden=$ordenAux;

                $imagenOrden->save();
            } else {
                $imagen->orden = $request->orden;
            }

            $imagen->mostrar = $request->mostrar;

            //Guardo el imagen para obtener el id y poder usarlo en la imagen
            $imagen->save();
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al cargar imagen a la base de datos: ".$e->getMessage()
            ]);
        }
        try {
            //Guardo la imagen con el id del imagen para evitar sobrrescritura
            if (!is_null($request->file('archivo_imagen'))) {
                $archivo=$request->file('archivo_imagen');
                $archivo->storeAs(
                    '/img/imagenes_productos/producto'.$request->id_producto.'/',
                    $imagen->id_imagen.'_imagen_'.$imagen->id_imagen.'.'.$archivo->getClientOriginalExtension(),
                    'public'
                );

                if (file_exists( public_path() . $imagen->url)) {
                    //Elimino la imagen anterior
                    Storage::disk('public')->delete($imagen->url);
                }

                //Guardo el nuevo url en la BD
                $imagen->url = 'img/imagenes_productos/producto'.$request->id_producto.'/'.$imagen->id_imagen.'_imagen_'.$imagen->id_imagen.'.'.$archivo->getClientOriginalExtension();
                $imagen->save();
            }
        } catch (Exception $e) {
            return response([
                "success"=>'false',
                "mensaje"=>"Error al subir imagen al servidor."
            ]);
        }
        //Si no ocurrio ningún problema se avisa que el imagen se creo correctamente
        return response([
            "success"=>"true",
            "mensaje"=>"Imagen editada correctamente"
        ]);
    }

    public function deleteImagen(Request $request)
    {
        try {
            $imagen = Imagenes_producto::find($request->id_imagen);
            if (file_exists( public_path() . $imagen->url)) {
                //Elimino la imagen anterior
                Storage::disk('public')->delete($imagen->url);
            }
            $id = $imagen->id_producto;
            $imagen->delete();

            $imagenes=Imagenes_producto::where('id_producto', $id)->orderBy('orden', 'ASC')->get();
            $orden = 1;
            foreach ($imagenes as $img) {
                $img_aux = Imagenes_producto::find($img->id_imagen);
                $img_aux->orden=$orden;
                $img_aux->save();
                $orden++;
            }

            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha eliminado correctamente la imagen del producto.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrio un error al eliminar la imagen del producto.']);
        }
    }
    
    //------------------------------------Funciones para el administrador------------------------------------FIN
}
