<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use DOMDocument;
use DOMXPath;

class OcaController extends Controller
{
    /*
        Esta funcion consulta el web service de oca para tarifar los envios corporativos.
    */
    public function cotizarEnvio($PesoTotal, $VolumenTotal, $CantidadPaquetes, $CodigoPostalDestino=6300)
    {
        $url = "http://webservice.oca.com.ar/oep_tracking/Oep_Track.asmx/Tarifar_Envio_Corporativo";

        $_query_string = array(	
            'PesoTotal' => $PesoTotal,
            'VolumenTotal' => $VolumenTotal,
            'CodigoPostalOrigen' => config("envios.oca.CodigoPostalOrigen"), //TODO: este hay que pedirle el codigo postal al cliente
            'CodigoPostalDestino' => $CodigoPostalDestino,
            'CantidadPaquetes' => $CantidadPaquetes,
            'Cuit' => config("envios.oca.Cuit"),
            'Operativa' => config("envios.oca.Operativa")
        );

		$ch = curl_init();

		$curl_opt_arr = array(	
                CURLOPT_RETURNTRANSFER	=> TRUE,
                CURLOPT_HEADER			=> FALSE,
                CURLOPT_CONNECTTIMEOUT	=> 5,
                CURLOPT_FOLLOWLOCATION	=> TRUE
            );
		$curl_opt_arr[CURLOPT_POST] 		= true;
		$curl_opt_arr[CURLOPT_POSTFIELDS] 	= http_build_query($_query_string);
		$curl_opt_arr[CURLOPT_URL] 			= $url;

        curl_setopt_array($ch, $curl_opt_arr);

		$dom = new DOMDocument();
		@$dom->loadXML(curl_exec($ch));
		$xpath = new DOMXpath($dom);

		$e_corp = array();
		foreach (@$xpath->query("//NewDataSet/Table") as $envio_corporativo)
		{
			$e_corp[] = array(	
                'Tarifador'		=> $envio_corporativo->getElementsByTagName('Tarifador')->item(0)->nodeValue,
                'Precio'		=> $envio_corporativo->getElementsByTagName('Precio')->item(0)->nodeValue,
                'Ambito'		=> $envio_corporativo->getElementsByTagName('Ambito')->item(0)->nodeValue,
                'PlazoEntrega'	=> $envio_corporativo->getElementsByTagName('PlazoEntrega')->item(0)->nodeValue,
                'Adicional'		=> $envio_corporativo->getElementsByTagName('Adicional')->item(0)->nodeValue,
                'Total'			=> $envio_corporativo->getElementsByTagName('Total')->item(0)->nodeValue,
            );
		}

		return $e_corp;
    }

    //TODO: esta se fleta
    public function cotizarEnvio2(Request $request)
    {
        $url = "http://webservice.oca.com.ar/oep_tracking/Oep_Track.asmx/Tarifar_Envio_Corporativo";

        $_query_string = array(	
            'PesoTotal' => $request->peso_total,
            'VolumenTotal' => $request->volumen_total,
            'CodigoPostalOrigen' => config("envios.oca.CodigoPostalOrigen"), //TODO: este hay que pedirle el codigo postal al cliente
            'CodigoPostalDestino' => $request->codigo_postal_destino,
            'CantidadPaquetes' => $request->cant_paquetes,
            'Cuit' => config("envios.oca.Cuit"),
            'Operativa' => config("envios.oca.Operativa")
        );

		$ch = curl_init();

		$curl_opt_arr = array(	
                CURLOPT_RETURNTRANSFER	=> TRUE,
                CURLOPT_HEADER			=> FALSE,
                CURLOPT_CONNECTTIMEOUT	=> 5,
                CURLOPT_FOLLOWLOCATION	=> TRUE
            );
		$curl_opt_arr[CURLOPT_POST] 		= true;
		$curl_opt_arr[CURLOPT_POSTFIELDS] 	= http_build_query($_query_string);
		$curl_opt_arr[CURLOPT_URL] 			= $url;

        curl_setopt_array($ch, $curl_opt_arr);

		$dom = new DOMDocument();
		@$dom->loadXML(curl_exec($ch));
		$xpath = new DOMXpath($dom);

		$e_corp = array();
		foreach (@$xpath->query("//NewDataSet/Table") as $envio_corporativo)
		{
			$e_corp[] = array(	
                'Tarifador'		=> $envio_corporativo->getElementsByTagName('Tarifador')->item(0)->nodeValue,
                'Precio'		=> $envio_corporativo->getElementsByTagName('Precio')->item(0)->nodeValue,
                'Ambito'		=> $envio_corporativo->getElementsByTagName('Ambito')->item(0)->nodeValue,
                'PlazoEntrega'	=> $envio_corporativo->getElementsByTagName('PlazoEntrega')->item(0)->nodeValue,
                'Adicional'		=> $envio_corporativo->getElementsByTagName('Adicional')->item(0)->nodeValue,
                'Total'			=> $envio_corporativo->getElementsByTagName('Total')->item(0)->nodeValue,
            );
		}

		return $e_corp;
    }
}
