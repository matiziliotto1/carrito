<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    protected function authenticated(Request $request, $user)
    {
        /*
            Si las cookies estan seteadas es porque el usuario se logeo porque
            queria agregar un producto al carrito, entonces despues de logearse
            lo envio de nuevo a la ruta para agregar el producto.
        */
        $id_variante_producto = $request->session()->get('id_variante_producto');
        $id_servicio = $request->session()->get('id_servicio');
        $cantidad = $request->session()->get('cantidad');
        
        if($id_variante_producto != null && $cantidad != null){
            $this->redirectTo = route('enviarDatosAddProductoCarrito');
        } elseif($id_servicio!=null && $cantidad!=null){
            $this->redirectTo = route('enviarDatosAddServicioCarrito');
        }
    }

    /*
        Sobreescribo el metodo para que al cerrar sesión, solo lo haga con el cliente
        y no cierre sesion al administrador tambien.
    */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
