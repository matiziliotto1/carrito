<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CREAR_CARRITO;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre' => ['required', 'string', 'max:255'],
            'apellido' => ['required', 'string', 'max:255'],
            'tipo_documento' => ['required', 'string', 'max:255'],
            'numero_documento' => ['required', 'integer', 'digits_between:7,10'],
            'provincia' => ['required', 'integer'],
            'ciudad' => ['required', 'integer'],
            'codigo_postal' => ['required', 'integer'],
            'domicilio_real' => ['required', 'string', 'max:255'],
            'telefono_fijo' => ['required', 'string', 'max:20'],
            'telefono_celular' => ['required', 'string', 'max:20'],
            'provincia_recepcion' => ['required', 'integer'],
            'ciudad_recepcion' => ['required', 'integer'],
            'codigo_postal_recepcion' => ['required', 'integer'],
            'domicilio_recepcion' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'nombre' => $data['nombre'],
            'apellido' => $data['apellido'],
            'tipo_documento' => $data['tipo_documento'],
            'numero_documento' => $data['numero_documento'],
            'provincia' => $data['provincia'],
            'ciudad' => $data['ciudad'],
            'codigo_postal' => $data['codigo_postal'],
            'domicilio_real' => $data['domicilio_real'],
            'telefono_fijo' => $data['telefono_fijo'],
            'telefono_celular' => $data['telefono_celular'],
            'provincia_recepcion' => $data['provincia_recepcion'],
            'ciudad_recepcion' => $data['ciudad_recepcion'],
            'codigo_postal_recepcion' => $data['codigo_postal_recepcion'],
            'domicilio_recepcion' => $data['domicilio_recepcion'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
