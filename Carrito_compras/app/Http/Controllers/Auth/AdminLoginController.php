<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;


class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /*
        Retorno la vista para que el administrador ingrese.
    */
    public function login_view()
    {
        if(auth()->guard("administrador")->check()) {
            return redirect(route('admin.home'));
        }
        else{
            return view('admin.auth.login');
        }
    }

    /*
        Si las credenciales coinciden para los datos de ingreso de un administrador
        Lo envio al home, sino lo envio a la funcion login failed.
    */
    public function login(Request $request)
    {
        $this->validator($request);
        
        if(Auth::guard('administrador')->attempt($request->only('nombre_usuario','password'),$request->filled('remember'))){
            //Authentication passed...
            return redirect()->intended(route('admin.home'));
        }
        //Authentication failed...
        return $this->loginFailed();
    }

    /*
        Cierro la sesión del administrador solamente.
    */
    public function logout()
    {
        Auth::guard('administrador')->logout();
        return redirect()->route('admin.home');
    }

    /*
        Valido los datos ingresados por el usuario para iniciar sesión
        en el panel de administrador.
    */
    private function validator(Request $request)
    {
        //validation rules.
        $rules = [
            'nombre_usuario'    => 'required|string|exists:administradores',
            'password' => 'required|string|min:8|max:255',
        ];

        //custom validation error messages.
        $messages = [
            'nombre_usuario.exists' => 'El nombre de usuario no coincide.',
        ];

        //validate the request.
        $request->validate($rules,$messages);
    }

    /*
        Si la autenticacion fallo, le muestro un error al usuario.
    */

    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Ingreso fallido! Por favor, intente de nuevo');
    }
}
