<?php

namespace App\Http\Controllers;

use Exception;
use App\Carrito_compra;
use App\Servicios_x_carrito;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Servicios_x_CarritoController extends Controller
{
    public function agregarServicio($id_servicio, $id_carrito)
    {
        try {
            $servicios_x_carrito = new Servicios_x_carrito();

            $servicios_x_carrito->id_servicio = $id_servicio;
            $servicios_x_carrito->id_carrito = $id_carrito;

            $servicios_x_carrito->save();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function eliminarServicio($id_servicio)
    {
        try {
            $id_usuario = Auth::user()->id;
            $id_carrito = Carrito_compra::where("id_usuario", $id_usuario)->first()->id_carrito;

            Servicios_x_carrito::where([['id_servicio', $id_servicio],['id_carrito',$id_carrito]])->delete();
            
            return true;
        } catch (Exception $e) {
            die($e);
            return false;
        }
    }

    public function eliminarTodosLosServicios()
    {
        try {
            $id_usuario = Auth::user()->id;
            $id_carrito = Carrito_compra::where("id_usuario", $id_usuario)->first()->id_carrito;

            Servicios_x_carrito::where('id_carrito', $id_carrito)->delete();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
