<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

use App\Compras;
use App\Compras_x_productos;
use App\Imagenes_producto;
use App\User;
use App\Pedido;
use App\Variantes_producto;
use Exception;

class ComprasController extends Controller
{
    private function createCompra($data, $tipo_compra, $id_usuario)
    {
        if ($tipo_compra == 1) {
            $total = $data['total'];
            $costo_envio = $data['costo_envio'];
            if($data['metodo_envio'] == 1){
                $metodo_envio = "Envio a domicilio";
            }
            else if($data['metodo_envio'] == 0){
                $metodo_envio = "Retiro por sucursal";
            }
            
            $beneficio = $data['beneficio'];
            $descuento = $data['descuento'];
        }
        else if ($tipo_compra == 0) {
            $total = $data['total_servicios'];
        }

        $puntaje_cada = Config::get('puntajes.puntaje_compra.cada');
        $puntaje_gana = Config::get('puntajes.puntaje_compra.gana');

        //Hago regla de 3 simple y calculo el puntaje total de la compra.
        $puntaje_compra = (int) ($total * $puntaje_gana) / $puntaje_cada;

        if ($tipo_compra == 1) {
            $compra = Compras::create([
                'id_usuario' => $id_usuario,
                'fecha_compra' =>  date("Y-m-d"),
                'puntaje_compra' => $puntaje_compra,
                'monto_total' => $total,
                'monto_envio' => $costo_envio,
                'metodo_envio' => $metodo_envio,
                'estado' => "Procesando",
                'tipo_compra' => $tipo_compra,
                'beneficio' => $beneficio,
                'descuento' => $descuento,
            ]);
        }
        else if ($tipo_compra == 0) {
            $compra = Compras::create([
                'id_usuario' => $id_usuario,
                'fecha_compra' =>  date("Y-m-d"),
                'puntaje_compra' => $puntaje_compra,
                'monto_total' => $total,
                'estado' => "Procesando",
                'tipo_compra' => $tipo_compra,
            ]);
        }

        return $compra->id_compra;
    }

    private function descontarStock($id_variante_producto, $cantidad)
    {
        $variante = Variantes_producto::find($id_variante_producto);

        $variante->stock = $variante->stock - $cantidad;
        
        $variante->save();
    }

    /*
        Esta funcion recibe la informacion de la compra, crea una nueva compra y 
        agrega las variantes de productos compradas a compras_productos
    */
    public function nuevaCompra($id_pedido)
    {
        try{

            //Paso a json el string con toda la informacion almacenada en la session.
            $pedido = Pedido::findOrFail($id_pedido);
            $data = json_decode($pedido->data, true);
    
            $id_usuario = $data['id_usuario'];
            $tipo_compra = $data['tipo_compra'];
    
            $id_compra = $this->createCompra($data, $tipo_compra, $id_usuario);
    
            if ($tipo_compra == 1) {
                $compras_x_productosController = new Compras_x_ProductosController();
                $variantes_productoController = new Variantes_ProductoController();
    
                $carrito_controller = new CarritoController();
                $request = new Request();
    
                //Para cada item almaceno un objeto que tiene el id del producto, precio y cantidad comprada.
                foreach($data['items'] as $item){
                    $id_variante_producto = $item['id_variante_producto'];
                    $precio = $item['precio'];
                    $cantidad = $item['cantidad'];
    
                    $compras_x_productosController->agregarProducto($id_compra, $id_variante_producto, $precio, $cantidad);
                    
                    $this->descontarStock($id_variante_producto, $cantidad);
                    
                    //Una vez agregado el producto comprado a la compra, lo elimino del carrito
                    //Elimino los productos comprados del carrito de compras. (primero creo un nuevo Request y le asigno la informacion necesaria)
                    $request->id_variante_producto = $item['id_variante_producto'];
                    $request->return = false;
    
                    $carrito_controller->deleteProductoCarrito($request);
                }
            }
            else if ($tipo_compra == 0) {
                $compras_x_serviciosController = new Compras_x_ServiciosController();
                $serviciosController = new ServiciosController();
    
                $carrito_controller = new CarritoController();
                $request = new Request();
    
                //Para cada item almaceno un objeto que tiene el id del producto, precio y cantidad comprada.
                foreach($data['items'] as $item){
                    $id_servicio = $item['id_servicio'];
                    $precio = $item['precio'];
    
                    $compras_x_serviciosController->agregarServicio($id_compra, $id_servicio, $precio);
                
                    //Una vez agregado el servicio comprado a la compra, lo elimino del carrito
                    //Elimino los productos comprados del carrito de compras. (primero creo un nuevo Request y le asigno la informacion necesaria)
                    $request->id_servicio = $item['id_servicio'];
                    $request->return = false;
    
                    $carrito_controller->deleteServicioCarrito($request);
                }
            }
    
            $mail_controller = new MailController();
            $mail_controller->enviar_mail_compra($data, $id_compra);
    
            return true;
        } catch(Exception $e) {
            return false;
        }
    }

    /*
        Esta funcion retorna las compras realizadas por el cliente en la vista donde se muestran
        todas las compras realizadas.
    */
    public function getCompras()
    {
        $compras = Compras::where('id_usuario', Auth::user()->id)->orderby('fecha_compra', 'DESC')->orderby('id_compra', 'DESC')->paginate(10);
        
        return view('clientes.perfil.misCompras', compact('compras'));
    }

    /*
        Esta funcion se utiliza con AJAX para consultar la informacion de la compra que el cliente
        presiono para ver su informacion.
    */
    public function getInfoCompra(Request $request)
    {
        $compra = Compras::find($request->id_compra);
        if ($compra->tipo_compra==1) {
            $detalles_productos = DB::table('compras_x_productos')
            ->join('variantes_producto', 'compras_x_productos.id_variante_producto', '=', 'variantes_producto.id_variante_producto')
            ->join('productos', 'variantes_producto.id_producto', '=', 'productos.id_producto')
            ->where('compras_x_productos.id_compra', $request->id_compra)
            ->select('compras_x_productos.cantidad', 'compras_x_productos.precio', 'productos.nombre', 'productos.id_producto', 'variantes_producto.*')
            ->get();

            foreach ($detalles_productos as $detalle) {
                $imagen = Imagenes_producto::where('id_producto', $detalle->id_producto)->select('url')->first();
                $detalle->imagen = $imagen['url'];
            }

            return response()->json(['producto' => true, 'success'=>'true' , 'compra'=>$compra, 'detalles_productos' => $detalles_productos]);
        }
        if ($compra->tipo_compra==0) {
            $detalles_servicios = DB::table('compras_x_servicios')
            ->join('servicios', 'compras_x_servicios.id_servicio', '=', 'servicios.id_servicio')
            ->where('compras_x_servicios.id_compra', $request->id_compra)
            ->select('compras_x_servicios.precio', 'servicios.*')
            ->get();

            foreach ($detalles_servicios as $detalle) {
                $imagen = Imagenes_producto::where('id_servicio', $detalle->id_servicio)->select('url')->first();
                $detalle->imagen = $imagen['url'];
            }

            return response()->json(['producto' => false, 'success'=>'true' , 'compra'=>$compra, 'detalles_servicios' => $detalles_servicios]);
        }
    }

    public function cantidadDeComprasDeUsuario()
    {
        return count(Compras::where('id_usuario', Auth::user()->id)->get());
    }

    public function updateEstadoPagoCompra($id_compra, $estado)
    {
        try {
            $compra = Compras::find($id_compra);
            $compra->estado_pago = $estado;
            $compra->save();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    //------------------------------------Funciones para el administrador------------------------------------INICIO
    public function verComprasProductos()
    {
        $compras = Compras::where('tipo_compra', 1)->orderby('id_compra', 'desc')->paginate(25);
        return view("admin.compras.verCompras", compact("compras"));
    }

    public function verComprasServicios()
    {
        $compras = Compras::where('tipo_compra', 0)->orderby('id_compra', 'desc')->paginate(25);
        return view("admin.compras.verCompras", compact("compras"));
    }
    
    public function getComprasXCliente($id_usuario)
    {
        $compras = Compras::where("id_usuario", $id_usuario)->paginate(25);

        $email_usuario = User::find($id_usuario)->email;

        return view("admin.compras.verCompras", compact("compras", "email_usuario"));
    }

    /*
        Esta funcion se utiliza con AJAX para actualizar el estado y/o codigo de seguimiento de una
        compra y luego se notifica al cliente, a traves de un mail, el cambio ocurrido.
        Los parametros que se reciben son:
            -Id de la compra.
            -Nuevo estado, o el mismo que ya tenia.
            -Nuevo codigo de seguimiento o el que ya tenia.
    */
    public function updateCompra(Request $request)
    {
        try {
            $compra = Compras::find($request->id_compra);

            if($request->estado == "Finalizada"){
                //Sumo el puntaje ganado por la compra al usuario que la realizo.
                $user_controller = new UserController();
                $user_controller->sumarPuntaje($compra->id_usuario, $compra->puntaje_compra);
            }

            $compra->estado = $request->estado;
            $compra->codigo_seguimiento = $request->codigo_seguimiento;

            $compra->save();

            $mail_controller = new MailController();
            $mail_controller->enviar_mail_estado($request->id_compra);

            return response()->json(['success'=>'true' , 'mensaje'=>'Se actualizo correctamente la compra y se notificó al cliente sobre el cambio.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al actualizar la compra.']);
        }
    }

    /*
        Esta funcion recibe el id de la compra, consulta la informacion de la misma:
            -Usuario que realizo la compra.
            -Productos y servicios comprados.
        Y retorna todo a la vista para que el administrador vea la informacion.
    */
    public function verCompra($id_compra)
    {
        $compra = Compras::find($id_compra);
        $usuario = User::where("id", $compra->id_usuario)->select("nombre", "apellido", "email")->first();

        if($compra->tipo_compra == 1){
            $compras_x_productosController = new Compras_x_ProductosController();
            $productos_comprados = $compras_x_productosController->getProductosXCompra($id_compra);
            
            $compra->productos = $productos_comprados;
        }
        else if($compra->tipo_compra == 0){
            $compras_x_serviciosController = new Compras_x_ServiciosController;
            $servicios_comprados = $compras_x_serviciosController->getServiciosXCompra($id_compra);
            
            $compra->servicios = $servicios_comprados;
        }


        return view("admin.compras.verCompra", compact("compra", "usuario"));
    }

    public function buscadorCompras(Request $request)
    {
        if(isset($request->id_compra) && isset($request->fecha_compra)){
            $compras = Compras::where([["tipo_compra", $request->tipo_compra], ['id_compra','LIKE','%'.$request->id_compra.'%'], ['fecha_compra', $request->fecha_compra]])->get();
        }
        else if(isset($request->id_compra)){
            $compras = Compras::where([["tipo_compra", $request->tipo_compra], ['id_compra','LIKE','%'.$request->id_compra.'%']])->get();
        }
        else if(isset($request->fecha_compra)){
            $compras = Compras::where([["tipo_compra", $request->tipo_compra], ['fecha_compra',$request->fecha_compra]])->get();
        }
        else if(!isset($request->fecha_compra) || !!isset($request->fecha_compra)){
            $compras = Compras::where([["tipo_compra", $request->tipo_compra]])->paginate(25);
        }
        //TODO: Hay que acomodar el pagination para cuando entra al ultimo elseif.
        return view("admin.compras.verComprasAJAX", compact("compras"));
    }
    //------------------------------------Funciones para el administrador------------------------------------FIN
}
