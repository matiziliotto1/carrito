<?php

namespace App\Http\Controllers;

use App\Imagenes_producto;
use App\Productos;
use App\Servicio;
use App\Variantes_producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use MercadoPago\Item;
use MercadoPago\MerchantOrder;
use MercadoPago\Payer;
use MercadoPago\Payment;
use MercadoPago\Preference;
use MercadoPago\SDK;
use MercadoPago\Shipments;

use App\mercadopago\lib\MP;
use App\Pedido;
use App\User;
use Exception;

class MercadoPagoController extends Controller
{
    public function crearPreferencia($id_pedido, $costo_envio = 0, $descuento)
    {
        $pedido = Pedido::findOrFail($id_pedido);
        $data = json_decode($pedido->data, true);

        SDK::setAccessToken(config("payment-methods.mercadopago.access_token"));

        # Create a preference object
        $preference = new Preference();

        $items =  array();

        $tipo_compra = $data['tipo_compra'];

        if($tipo_compra == 1){
            foreach($data['items'] as $item){
                $variante_producto = Variantes_producto::where('id_variante_producto', $item['id_variante_producto'])->first();
                $producto = Productos::where('id_producto', $variante_producto->id_producto)->select('nombre','descripcion_resumida')->first();
                
                # Create an item object
                $item_aux = new Item();
                $item_aux->id = $item['id_variante_producto'];
                $item_aux->title = $producto->nombre;
                $item_aux->quantity = $item['cantidad'];
                $item_aux->currency_id = 'ARS';
                $item_aux->unit_price = $item['precio'] * (1 - $descuento / 100);
                $item_aux->description = $producto->descripcion_resumida;
                if (Imagenes_producto::where('id_producto', $variante_producto->id_producto)->exists()) {
                    $imagenes_controller = new Imagenes_productoController();
                    $item_aux->picture_url = env('APP_URL').$imagenes_controller->getPrimeraImagenDeProducto($variante_producto->id_producto);
                } else {
                    $item_aux->picture_url = env('APP_URL').'img/imagenes_productos/producto_sin_imagen.jpg';
                }
                array_push($items, $item_aux);
            }
        }
        else if($tipo_compra == 0){
            foreach($data['items'] as $item){
                $servicio = Servicio::where('id_servicio', $item['id_servicio'])->first();
                
                # Create an item object
                $item_aux = new Item();
                $item_aux->id = $item['id_servicio'];
                $item_aux->title = $servicio->titulo;
                $item_aux->quantity = 1;
                $item_aux->currency_id = 'ARS';
                $item_aux->unit_price = $item['precio'];
                $item_aux->description = $servicio->descripcion_resumida;
                if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                    $imagenes_controller = new Imagenes_servicioController();
                    $item_aux->picture_url = env('APP_URL').$imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
                } else {
                    $item_aux->picture_url = env('APP_URL').'img/imagenes_servicios/servicio_sin_imagen.png';
                }
                array_push($items,$item_aux);
            }
        }

        //Si el costo de envio es mayor a 0 entonces lo sumo al total para que abone a traves de mercado pago.
        if($costo_envio > 0){
            $item_aux = new Item();
            $item_aux->title = "Costo de envío";
            $item_aux->quantity = 1;
            $item_aux->currency_id = 'ARS';
            $item_aux->unit_price = $costo_envio;
            array_push($items, $item_aux);
        }

        $usuario = User::find($data['id_usuario']);

        # Create a payer object
        $payer = new Payer();
        $payer->name = $usuario->nombre;
        $payer->surname = $usuario->apellido;
        $payer->email = $usuario->email;

        # Setting preference properties
        $preference->items = $items;
        $preference->payer = $payer;

        $preference->external_reference = "Pedido#".$id_pedido;

        $preference->notification_url = route('webhook').'?source_news=webhooks';

        return $preference;
    }

    public function procesarPago(Request $request)
    {
        $estado = $request->payment_status;
        $id_pedido =  intval(substr($request->external_reference, 7));

        switch ($estado){
            case "approved":
                    if( $this->pagoExitoso($id_pedido) ){
                        return view('clientes.pagos.aprobado');
                    }
                break;

            case "failure":
                    return view('clientes.pagos.fallido');
                break;
            
            case "pending":
                    return view('clientes.pagos.pendiente');
                break;
        }
    }

    private function pagoExitoso($id_pedido)
    {
        if( !isset($id_pedido) ){
            return abort(401);
        }

        $compras_controller = new ComprasController();
        $respuestaOk = $compras_controller->nuevaCompra($id_pedido);

        if($respuestaOk){
            return true;
        }
        else{
            abort(500);
        }
    }

    public function webhook(Request $request)
    {
        Log::info($request->getContent());
        // Obtengo en formato json el body del request
        $body = json_decode($request->getContent());

        $myfile = file_put_contents('webhookBODY.txt', $request->getContent().PHP_EOL , FILE_APPEND | LOCK_EX);

        SDK::setAccessToken(config("payment-methods.mercadopago.access_token"));
        
        switch($body->type) {
            case "payment":
                $payment_info = SDK::get("/v1/payments/".$body->data->id);
                $myfile = file_put_contents('webhookPAYMENT.txt', json_encode($payment_info).PHP_EOL , FILE_APPEND | LOCK_EX);
                
                if($payment_info['code'] == 200){
                    $id_pedido = intval(substr($payment_info['body']['external_reference'], 7));

                    $merchant_order = SDK::get("/merchant_orders/".$payment_info['body']['order']['id']);
                    
                    $myfile = file_put_contents('webhookMERCHANT_ORDER.txt', json_encode($merchant_order).PHP_EOL , FILE_APPEND | LOCK_EX);
                    
                    $paid_amount = 0;
                    foreach ($merchant_order['body']['payments'] as $payment) {
                        if ($payment['status'] == 'approved') {
                            $paid_amount += $payment['transaction_amount'];
                        }
                    }
                
                    // If the payment's transaction amount is equal (or bigger) than the merchant_order's amount you can release your items
                    if ($paid_amount >= $merchant_order['body']['total_amount']) {
                        
                        $myfile = file_put_contents('webhookTOTALPAGADO.txt', "TOTAL PAGADO".PHP_EOL , FILE_APPEND | LOCK_EX);

                        $compras_controller = new ComprasController();
                        $compras_controller->updateEstadoPagoCompra($id_pedido, 'Pagado');

                    } else {
                        //Avisar al cliente y admin que ingreso el pago pero no es suficiente
                        //print_r("Not paid yet. Do not release your item.");
                        $myfile = file_put_contents('webhookINGRESOELPAGO.txt', "INGRESO EL PAGO".PHP_EOL , FILE_APPEND | LOCK_EX);
                    }
                }
                break;

            default:
                return response(200);
                break;
        }
        return response(200);
    }
}