<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use DateTime;
use Exception;

class UserController extends Controller
{
    public function getPerfil()
    {
        try {
            $usuario = User::where("id",Auth::user()->id)->first();

            return view("clientes.perfil.miPerfil", compact("usuario"));
        } catch (Exception $e) {
            return back()->with("error", "Ocurrió un error al ver sus datos.");
        }
    }

    public function updatePerfil(Request $request)
    {
        try {
            $usuario = User::find(Auth::user()->id);

            $usuario->email = $request->email;
            $usuario->nombre = $request->nombre;
            $usuario->apellido = $request->apellido;
            $usuario->tipo_documento = $request->tipo_documento;
            $usuario->numero_documento = $request->numero_documento;
            $usuario->provincia = $request->provincia;
            $usuario->ciudad = $request->ciudad;
            $usuario->codigo_postal = $request->codigo_postal;
            $usuario->domicilio_real = $request->domicilio_real;
            $usuario->telefono_fijo = $request->telefono_fijo;
            $usuario->telefono_celular = $request->telefono_celular;
            $usuario->provincia_recepcion = $request->provincia_recepcion;
            $usuario->ciudad_recepcion = $request->ciudad_recepcion;
            $usuario->codigo_postal_recepcion = $request->codigo_postal_recepcion;
            $usuario->domicilio_recepcion = $request->domicilio_recepcion;

            $usuario->save();

            return back()->with("mensaje_ok", "Perfil actualizado correctamente.");
        } catch (Exception $e) {
            return back()->with("mensaje_error", "Ocurrió un error al actualizar su perfil.");
        }
    }

    public function editarPassword()
    {
        return view("clientes.perfil.editarPassword");
    }

    public function updatePassword(Request $request)
    {
        try {
            $usuario = User::find(Auth::User()->id);
            $nueva_password = $request->nueva_password;
            $usuario->password = Hash::make($nueva_password);
            $usuario->save();

            return redirect()->route('getPerfil')->with('mensaje_ok','Contraseña actualizada correctamente.');
        } catch (Exception $e) {
            return redirect()->route('getPerfil')->with('mensaje_error','Ocurrió un error al actualizar su contraseña.');
        }
    }

    /*
        Esta funcion le suma al usuario puntaje acumulado, el puntaje obtenido por una compra.
    */
    public function sumarPuntaje($id_usuario, $puntaje)
    {
        $usuario = User::find($id_usuario);
        $usuario->puntaje_acumulado = $usuario->puntaje_acumulado + $puntaje;
        $usuario->save();
    }


    // ------------------------Funciones para el administrador------------------------INICIO
    public function getUsuarios()
    {
        try {
            $usuarios = User::where([["eliminado",0],["habilitado",1]])->paginate(25);

            return view("admin.usuarios.listar_usuarios", compact("usuarios"));
        } catch (Exception $e) {
            return back()->with("error", "Ocurrió un error al ver los usuarios.");
        }
    }

    public function getUsuariosEliminados()
    {
        try {
            $usuarios = User::where("eliminado",1)->paginate(25);

            return view("admin.usuarios.listar_usuarios", compact("usuarios"));
        } catch (Exception $e) {
            return back()->with("error", "Ocurrió un error al ver los usuarios eliminados.");
        }
    }

    public function getUsuariosDeshabilitados()
    {
        try {
            $usuarios = User::where("habilitado",0)->paginate(25);

            return view("admin.usuarios.listar_usuarios", compact("usuarios"));
        } catch (Exception $e) {
            return back()->with("error", "Ocurrió un error al ver los usuarios deshabilitados.");
        }
    }

    public function deleteUsuario(Request $request)
    {
        try {
            $usuario = User::find($request->id_usuario);
            $usuario->eliminado = 1; // 1 = TRUE
            $usuario->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Usuario eliminado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al eliminar el usuario.']);
        }
    }

    public function deshabilitarUsuario(Request $request)
    {
        try {
            $usuario = User::find($request->id_usuario);
            $usuario->habilitado = 0; // 0 = FALSE
            $usuario->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Usuario deshabilitado correctamente.']);
        }
        catch(Exception $e)
        {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al deshabilitar el usuario.']);
        }
    }

    public function restaurarUsuario(Request $request)
    {
        try {
            $usuario = User::find($request->id_usuario);
            $usuario->eliminado = 0; // 0 = FALSE
            $usuario->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Usuario restaurado correctamente.']);
        }
        catch(Exception $e)
        {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al restaurar el usuario.']);
        }
    }

    public function habilitarUsuario(Request $request)
    {
        try {
            $usuario = User::find($request->id_usuario);
            $usuario->habilitado = 1; // 1 = TRUE
            $usuario->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Usuario habilitado correctamente.']);
        }
        catch(Exception $e)
        {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al habilitar el usuario.']);
        }
    }

    public function buscadorUsuario(Request $request)
    {
        $query= array();
        if (isset($request->nombre)) {
            array_push($query, ['nombre','LIKE','%'.$request->nombre.'%']);
        }
        if (isset($request->apellido)) {
            array_push($query, ['apellido','LIKE','%'.$request->apellido.'%']);
        }
        if (isset($request->email)) {
            array_push($query, ['email','LIKE','%'.$request->email.'%']);
        }
        $usuarios = User::where($query)->get();
        //TODO: Hay que acomodar el pagination para cuando entra al ultimo elseif.
        return view("admin.usuarios.verUsuariosAJAX", compact("usuarios"));
    }

    public function verInfoUsuario(Request $request)
    {
        $usuario = User::where('id', $request->id_user)
                    ->select('id', 'nombre', 'apellido', 'tipo_documento', 'numero_documento', 'provincia', 'ciudad', 'domicilio_real', 'codigo_postal', 'telefono_fijo', 'telefono_celular', 'provincia_recepcion', 'ciudad_recepcion', 'domicilio_recepcion', 'codigo_postal_recepcion', 'email')
                    ->first();

        return $usuario;
    }

    public function updateInfoUsuario(Request $request)
    {
        try {
            $usuario = User::find($request->id_user);

            $usuario->email = $request->email;
            $usuario->nombre = $request->nombre;
            $usuario->apellido = $request->apellido;
            $usuario->tipo_documento = $request->tipo_documento;
            $usuario->numero_documento = $request->numero_documento;
            $usuario->provincia = $request->provincia;
            $usuario->ciudad = $request->ciudad;
            $usuario->codigo_postal = $request->codigo_postal;
            $usuario->domicilio_real = $request->domicilio_real;
            $usuario->telefono_fijo = $request->telefono_fijo;
            $usuario->telefono_celular = $request->telefono_celular;
            $usuario->provincia_recepcion = $request->provincia_recepcion;
            $usuario->ciudad_recepcion = $request->ciudad_recepcion;
            $usuario->codigo_postal_recepcion = $request->codigo_postal_recepcion;
            $usuario->domicilio_recepcion = $request->domicilio_recepcion;

            $usuario->domicilio_recepcion = $request->domicilio_recepcion;

            if($request->password){
                $usuario->password = bcrypt($request->password);
            }

            $usuario->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Usuario actualizado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al actualizar el usuario. '.$e->getMessage()]);
        }
    }
    // ------------------------Funciones para el administrador------------------------ FIN
}
