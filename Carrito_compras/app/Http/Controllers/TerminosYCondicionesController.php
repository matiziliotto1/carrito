<?php

namespace App\Http\Controllers;

use App\Terminos_y_condiciones;
use Illuminate\Http\Request;
use Exception;

class TerminosYCondicionesController extends Controller
{
    public function verTerminosYCondicionesUsuario()
    {
        $terminos = Terminos_y_condiciones::all();
        return view('clientes.terminos_y_condiciones', compact('terminos'));
    }

    public function verTerminosYCondicionesAdmin()
    {
        $terminos = Terminos_y_condiciones::all();
        return view('admin.terminos_y_condiciones.verTerminos_y_condiciones', compact('terminos'));
    }

    public function addTerminosYCondiciones(Request $request)
    {
        try {
            $termino = new Terminos_y_condiciones;
            $termino->titulo = $request->titulo;
            $termino->texto = $request->texto;
            $termino->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha agregado correctamente nuevos terminos y condiciones.', 'id_termino_condiciones' => $termino->id_termino_condiciones]);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al agregar terminos y condiciones.']);
        }
    }

    public function editarTerminosYCondicionesView($id_termino)
    {
        $termino = Terminos_y_condiciones::find($id_termino);
        return view('admin.terminos_y_condiciones.editarTerminos_y_Condiciones', compact('termino'));
    }

    public function updateTerminosYCondiciones(Request $request)
    {
        try {
            $termino = Terminos_y_condiciones::find($request->id_termino_condiciones);
            $termino->titulo = $request->titulo;
            $termino->texto = $request->texto;
            $termino->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Terminos y condiciones actualizados correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al actualizar los terminos y condiciones.']);
        }
    }
    
    public function deleteTerminosYCondiciones(Request $request)
    {
        try {
            Terminos_y_condiciones::destroy($request->id_termino_condiciones);
            return response()->json(['success'=>'true' , 'mensaje'=>'Texto eliminado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al eliminar texto.']);
        }
    }
}
