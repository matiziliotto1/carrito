<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imagenes_producto;
use App\Slider;
use App\Preguntas_frecuentes;
use App\Contacto;
use App\Terminos_y_condiciones;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $sliders = Slider::where('mostrar', 1)->orderby('orden')->get();

        $productos_destacados = DB::table('productos')
            ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
            ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
            ->where([['productos.habilitado',1],['productos.destacado',1]])
            ->select('productos.*', 'categorias.nombre_categoria as categoria')
            ->get();

        foreach ($productos_destacados as $producto) {
            if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = Imagenes_Producto::where('id_producto', $producto->id_producto)->first()->url;
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
            $categoria = DB::table('categorias')
                ->join('subcategorias', 'subcategorias.id_categoria', '=', 'categorias.id_categoria')
                ->where("subcategorias.id_subcategoria", $producto->id_subcategoria)
                ->select('categorias.*')
                ->first();

            $producto->categoria = $categoria;
        }

        return view('home', compact("sliders", "productos_destacados"));
    }

    public function ayuda()
    {
        $preguntas = Preguntas_frecuentes::where('mostrar', 1)->get();
        return view('clientes.ayuda', compact('preguntas'));
    }

    public function contacto()
    {
        $contacto = Contacto::first();
        return view('clientes.contacto', compact('contacto'));
    }
}
