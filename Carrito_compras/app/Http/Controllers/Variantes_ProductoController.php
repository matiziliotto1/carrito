<?php

namespace App\Http\Controllers;

use App\Variantes_producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Productos;
use Exception;

class Variantes_ProductoController extends Controller
{
    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos los colores (sin que haya repetidos), que esten seteados y que
        esten habilitados para mostrar.
    */
    public function getColoresDeProducto($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto],['color','<>',null],['habilitado',1]])->select('color')->distinct()->orderby('color')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos los talles de ropa (sin que haya repetidos), que esten seteados y que
        esten habilitados para mostrar.
    */
    public function getTallesRopa($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['talle_ropa',"<>", null]])->select('talle_ropa')->distinct()->orderby('talle_ropa')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos los talles de calzado (sin que haya repetidos), que esten seteados y que
        esten habilitados para mostrar.
    */
    public function getTallesCalzado($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['talle_calzado',"<>", null]])->select('talle_calzado')->distinct()->orderby('talle_calzado')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos los talles de anzuelos (sin que haya repetidos), que esten seteados y que
        esten habilitados para mostrar.
    */
    public function getTallesAnzuelo($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['talle_anzuelo',"<>", null]])->select('talle_anzuelo')->distinct()->orderby('talle_anzuelo')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos las manos habiles (sin que haya repetidos), que esten seteados y que
        esten habilitados para mostrar.
    */
    public function getManosHabiles($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['mano_habil',"<>", null]])->select('mano_habil')->distinct()->orderby('mano_habil')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos las longitudes (sin que haya repetidas), que esten seteadas y que
        esten habilitadas para mostrar.
    */
    public function getLongitudes($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto] ,['habilitado',1], ['longitud',"<>", null]])->select('longitud')->distinct()->orderby('longitud')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos las fuerzas (sin que haya repetidas), que esten seteadas y que
        esten habilitadas para mostrar.
    */
    public function getFuerzas($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['fuerza',"<>", null]])->select('fuerza')->distinct()->orderby('fuerza')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos las acciones (sin que haya repetidas), que esten seteadas y que
        esten habilitadas para mostrar.
    */
    public function getAcciones($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['accion',"<>", null]])->select('accion')->distinct()->orderby('accion')->get();
    }

    /*
        Esta funcion se usa para consultar la informacion para enviar a la vista del producto individual.
        Consulta todos los diametros (sin que haya repetidos), que esten seteados y que
        esten habilitados para mostrar.
    */
    public function getDiametros($id_producto)
    {
        return Variantes_producto::where([['id_producto', $id_producto], ['habilitado',1], ['diametro',"<>", null]])->select('diametro')->distinct()->orderby('diametro')->get();
    }
    
    /*
        Esta funcion se usa con AJAX cuando se esta viendo el producto individual.
        Se setean todas las variables enviadas por POST para luego ver si tienen algun valor,
        es decir, que sean diferente de null y armar una consulta que retorne todos las variantes
        disponibles para las opciones que se van seleccionando.
    */
    public function filtrarVariantes(Request $request)
    {
        $id_producto = $request->id_producto;
        $color = $request->color;
        $talle_ropa = $request->talle_ropa;
        $talle_calzado = $request->talle_calzado;
        $talle_anzuelo = $request->talle_anzuelo;
        $mano_habil = $request->mano_habil;
        $longitud = $request->longitud;
        $fuerza = $request->fuerza;
        $accion = $request->accion;
        $diametro = $request->diametro;

        $query = [
            ['habilitado', '=', 1],
            ['id_producto', '=', $id_producto],
        ];

        if($color){
            $query_color = ['color', '=', $color];
            array_push($query, $query_color);
        }
        if($talle_ropa){
            $query_talle_ropa = ['talle_ropa', '=', $talle_ropa];
            array_push($query, $query_talle_ropa);
        }
        if($talle_calzado){
            $query_talle_calzado = ['talle_calzado', '=', $talle_calzado];
            array_push($query, $query_talle_calzado);
        }
        if($talle_anzuelo){
            $query_talle_anzuelo = ['talle_anzuelo', '=', $talle_anzuelo];
            array_push($query, $query_talle_anzuelo);
        }
        if($mano_habil){
            $query_mano_habil = ['mano_habil', '=', $mano_habil];
            array_push($query, $query_mano_habil);
        }
        if($longitud){
            $query_longitud = ['longitud', '=', $longitud];
            array_push($query, $query_longitud);
        }
        if($fuerza){
            $query_fuerza = ['fuerza', '=', $fuerza];
            array_push($query, $query_fuerza);
        }
        if($accion){
            $query_accion = ['accion', '=', $accion];
            array_push($query, $query_accion);
        }
        if($diametro){
            $query_diametro = ['diametro', '=', $diametro];
            array_push($query, $query_diametro);
        }

        $variantes = Variantes_producto::where($query)->get();

        return $variantes;
    }

    /*
        Esta funcion se usa con AJAX cuando se esta viendo el producto individual.
        Se setean todas las variables enviadas por POST para luego ver si tienen algun valor,
        es decir, que sean diferente de null y armar una consulta que retorne la unica variante que
        coincide con las opciones seleccionadas.
    */
    public function getVariante(Request $request)
    {
        $id_producto = $request->id_producto;
        $color = $request->color;
        $talle_ropa = $request->talle_ropa;
        $talle_calzado = $request->talle_calzado;
        $talle_anzuelo = $request->talle_anzuelo;
        $mano_habil = $request->mano_habil;
        $longitud = $request->longitud;
        $fuerza = $request->fuerza;
        $accion = $request->accion;
        $diametro = $request->diametro;

        $query = [
            ['habilitado', '=', 1],
            ['id_producto', '=', $id_producto],
        ];

        if($color){
            $query_color = ['color', '=', $color];
            array_push($query, $query_color);
        }
        if($talle_ropa){
            $query_talle_ropa = ['talle_ropa', '=', $talle_ropa];
            array_push($query, $query_talle_ropa);
        }
        if($talle_calzado){
            $query_talle_calzado = ['talle_calzado', '=', $talle_calzado];
            array_push($query, $query_talle_calzado);
        }
        if($talle_anzuelo){
            $query_talle_anzuelo = ['talle_anzuelo', '=', $talle_anzuelo];
            array_push($query, $query_talle_anzuelo);
        }
        if($mano_habil){
            $query_mano_habil = ['mano_habil', '=', $mano_habil];
            array_push($query, $query_mano_habil);
        }
        if($longitud){
            $query_longitud = ['longitud', '=', $longitud];
            array_push($query, $query_longitud);
        }
        if($fuerza){
            $query_fuerza = ['fuerza', '=', $fuerza];
            array_push($query, $query_fuerza);
        }
        if($accion){
            $query_accion = ['accion', '=', $accion];
            array_push($query, $query_accion);
        }
        if($diametro){
            $query_diametro = ['diametro', '=', $diametro];
            array_push($query, $query_diametro);
        }

        $variante = Variantes_producto::where($query)->select('id_variante_producto', 'stock')->first();

        return $variante;
    }

    //------------------------------------Funciones para el administrador------------------------------------INICIO
    public function getVariantesProducto($id_producto)
    {
        $variantes_producto = Variantes_producto::where('id_producto', $id_producto)->get();
        return view("admin.productos.variantes.verVariantesProducto", compact("variantes_producto", "id_producto"));
    }

    public function createVarianteProducto(Request $request)
    {
        try {
            $id_variante_producto= Variantes_producto::create([
                'stock' => $request->stock,
                'color' => $request->color,
                'talle_ropa' => $request->talle_ropa,
                'talle_calzado' => $request->talle_calzado,
                'talle_anzuelo' => $request->talle_anzuelo,
                'mano_habil' => $request->mano_habil,
                'longitud' => $request->longitud,
                'fuerza' => $request->fuerza,
                'accion' => $request->accion,
                'diametro' => $request->diametro,
                'id_producto' => $request->id_producto,
            ])->id_variante_producto;
            
            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha agregado correctamente una nueva variante del producto.' , 'id_variante_producto' => $id_variante_producto]);
        } catch(Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al agregar una nueva variante del producto.']);
        }
    }

    public function editVarianteProducto(Request $request)
    {
        try {
            $id_variante_producto = $request->id_variante_producto;
            $variante_producto = Variantes_producto::find($id_variante_producto);
            
            $variante_producto->stock = $request->stock;
            $variante_producto->color = $request->color;
            $variante_producto->talle_ropa = $request->talle_ropa;
            $variante_producto->talle_calzado = $request->talle_calzado;
            $variante_producto->talle_anzuelo = $request->talle_anzuelo;
            $variante_producto->mano_habil = $request->mano_habil;
            $variante_producto->longitud = $request->longitud;
            $variante_producto->fuerza = $request->fuerza;
            $variante_producto->accion = $request->accion;
            $variante_producto->diametro = $request->diametro;
            $variante_producto->habilitado = $request->habilitado;

            $variante_producto->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha editado correctamente la variante del producto.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al editar la variante del producto.']);
        }
    }
    //------------------------------------Funciones para el administrador------------------------------------FIN
}
