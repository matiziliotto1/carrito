<?php

namespace App\Http\Controllers;

use App\Administrador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Exception;

class AdminController extends Controller
{
    public function editarDatos()
    {
        $administrador = Administrador::find(Auth::guard('administrador')->user()->id_administrador);
        return view("admin.auth.editar_datos",compact('administrador'));
    }

    public function updateDatos(Request $request)
    {
        try {
            $administrador = Administrador::find(Auth::guard('administrador')->user()->id_administrador);

            $administrador->nombre = $request->nombre;
            $administrador->apellido = $request->apellido;

            if(Auth::guard('administrador')->user()->nombre_usuario != $request->nombre_usuario){
                $administrador->nombre_usuario = $request->nombre_usuario;
            }

            if(Auth::guard('administrador')->user()->password != $request->password){
                $administrador->password = Hash::make($request->password);
            }

            $administrador->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Se actualizaron correctamente sus datos.']);
        } catch (Exception $e) {
            return back()->with("actualizacion_erronea", "Ocurrió un error al actualizar sus datos.");
        }
    }

    public function validarPassword(Request $request)
    {
        try {
            $administrador = Administrador::find(Auth::guard('administrador')->user()->id_administrador);

            $validada = Hash::check($request->validar_password, $administrador->password);

            if($validada){
                return response()->json(['validada' => true]);
            }
            else{
                return response()->json(['validada' => false , 'mensaje' => "La contraseña es incorrecta."]);
            }            
        } catch (Exception $e) {
            return response()->json(['validada' => false , 'mensaje' => 'Ocurrió un error al validar su contraseña.']);
        }
    }

    public function updatePassword(Request $request)
    {
        try {
            $usuario = Administrador::find(Auth::guard('administrador')->user()->id_administrador);
            $nueva_password = $request->nueva_password;
            $usuario->password = Hash::make($nueva_password);
            $usuario->save();

            return back()->with("actualizacion_exitosa", "Contraseña actualizada correctamente.");
        } catch (Exception $e) {
            return back()->with("actualizacion_erronea", "Ocurrió un error al actualizar su contraseña.");
        }
    }
}
