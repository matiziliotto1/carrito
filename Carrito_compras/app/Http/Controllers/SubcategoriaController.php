<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use App\Subcategoria;
use Exception;

class SubcategoriaController extends Controller
{
    public function verSubcategoriasView(Request $request)
    {
        try {
            $categoria = Categoria::findOrFail($request->id_categoria);
            $subcategorias = Subcategoria::where('id_categoria', $request->id_categoria)->paginate(10);
            return view('admin.subcategorias.verSubcategorias', compact('subcategorias', 'categoria'));
        } catch (Exception $e) {
            return back()->with('mensaje', 'Error al obtener subcategorias de la base de datos');
        }
    }
    
    public function createSubcategoria(Request $request)
    {
        try {
            $subcategoria = new Subcategoria;

            $subcategoria->id_categoria = $request->id_categoria;
            $subcategoria->nombre_subcategoria = $request->nombre_subcategoria;
            $subcategoria->link = '';
            $subcategoria->tipo = $request->tipo;

            $subcategoria->save();

            $subcategoria->link = 'subcategoria/verSubategoria/'.$subcategoria->id_subcategoria;

            $subcategoria->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Subcategoria agregada correctamente.', 'id_subcategoria' => $subcategoria->id_subcategoria]);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al agregar subcategoria.']);
        }
    }
    
    public function updateSubcategoria(Request $request)
    {
        try {
            $subcategoria = Subcategoria::find($request->id_subcategoria);

            $subcategoria->nombre_subcategoria = $request->nombre_subcategoria;
            $subcategoria->link = 'subcategoria/verSubcategoria/'.$request->id_subcategoria;
            $subcategoria->habilitado = $request->habilitado;
            $subcategoria->tipo = $request->tipo;

            $subcategoria->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Subcategoria editada correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al editar subcategoria.']);
        }
    }

    public function getSubcategorias($id_categoria)
    {
        try {
            $subcategorias = Subcategoria::where('id_categoria', $id_categoria)->get();
            return response()->json(['success'=>'true' , 'subcategorias' => $subcategorias]);
        } catch (Exception $e) {            
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al obtener las subcategorias.']);
        }
    }
}
