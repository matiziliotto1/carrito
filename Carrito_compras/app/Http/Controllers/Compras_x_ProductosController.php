<?php

namespace App\Http\Controllers;

use App\Compras;
use App\Compras_x_productos;
use App\Imagenes_producto;
use App\Productos;
use App\Variantes_producto;
use Illuminate\Http\Request;
use Exception;

class Compras_x_ProductosController extends Controller
{
    public function agregarProducto($id_compra, $id_variante_producto, $precio, $cantidad)
    {
        $compras_x_producto = new Compras_x_productos();

        $compras_x_producto->id_compra = $id_compra;
        $compras_x_producto->id_variante_producto = $id_variante_producto;
        $compras_x_producto->precio = $precio;
        $compras_x_producto->cantidad = $cantidad;

        $compras_x_producto->save();
    }

    /*
        Esta funcion recibe el id de la compra, consulta todos los productos asociados a esa compra.
        Para cada producto consultado, se busca su nombre y se le agrega a los datos y luego retorna
        los productos asociados a la compra.
    */
    public function getProductosXCompra($id_compra)
    {
        $productos_comprados = Compras_x_productos::where("id_compra", $id_compra)->select("id_variante_producto", "cantidad", "precio")->get();

        foreach($productos_comprados as $producto){
            $variante = Variantes_producto::find($producto->id_variante_producto);
            $nombre_producto = Productos::find($variante->id_producto)->select("nombre")->first();
            $producto->nombre = $nombre_producto->nombre;
        }

        return $productos_comprados;
    }
}
