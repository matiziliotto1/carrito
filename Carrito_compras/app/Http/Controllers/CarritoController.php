<?php

namespace App\Http\Controllers;

use App\Providers\RouteServiceProvider;
use App\Carrito_compra;
use App\Imagenes_producto;
use App\Productos;
use App\Productos_x_carrito;
use App\Servicios_x_carrito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Exception;

class CarritoController extends Controller
{
    /*
        A esta funcion se la llama luego de que un usuario se registre, asi se le asocia
        su carrito de compras. Ya tiene controles con midlewares, primero chequeamos que
        para ingresar a esta ruta este logeado y en caso de que ya este logueado, miramos
        si tiene ya un carrito creado.
    */
    public function createCarrito()
    {
        $id_usuario = Auth::user()->id;
        $carrito = Carrito_compra::where("id_usuario",$id_usuario)->first();

        if($carrito == null){
            Carrito_compra::create([
                'id_usuario' => $id_usuario,
            ]);
        }

        return redirect()->route('home');
    }

    /*
        Esta funcion solo se usa como puente para que al cargar esa vista, se 
        envie un formulario con informacion necesaria para agregar un producto al
        carrito luego de que se logueo intentando agregar un producto al carrito.
    */
    public function enviarDatosAddProductoCarrito()
    {
        return view("clientes.enviarDatosAddProductoCarrito");
    }

    public function enviarDatosAddServicioCarrito()
    {
        return view("clientes.enviarDatosAddServicioCarrito");
    }

    /*
        Esta funcion se encarga de organizar o implementar todo para almacenar
        un producto en el carrito, buscando el usuario que ingreso, su carrito
        y una vez encontrados, manda a guardar el producto en el carrito.
    */
    public function addProductoCarrito(Request $request)
    {

        if (Auth::check()) {
            //Elimino las variables de sesion para cuando quiso agregar un producto y no estaba logeado
            $request->session()->forget(['id_variante_producto', 'cantidad']);

            $id_usuario = Auth::user()->id;
            $id_carrito = Carrito_compra::where("id_usuario", $id_usuario)->first()->id_carrito;

            $id_variante_producto = $request->id_variante_producto;
            $cantidad = $request->cantidad;

            $controlador_producto_x_carrito = new Productos_x_CarritoController();
            $agregado = $controlador_producto_x_carrito->agregarProducto($id_variante_producto, $id_carrito, $cantidad);
            
            
            if ($agregado) {
                if ($request->compra == 'true'){
                    session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Agregamos el producto a tu carrito!"]);
                    return redirect()->route("verCarritoDeCompras");
                } else {
                    session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Agregamos el producto a tu carrito!"]);
                    return redirect()->back();
                }
            } else {
                session(['mensaje_alerta_ok' =>false, 'mensaje'=> "No pudimos agregar el producto a tu carrito"]);
                return redirect()->route("verCarritoDeCompras");
            }
        } else {
            /*
                Si el usuario no esta logueado, guardo en las variables de sesion el producto que
                queria agregar al carrito y su cantidad, asi despues de ingresar se agrega al carrito
            */
            $request->session()->put('id_variante_producto', $request->id_variante_producto);
            $request->session()->put('cantidad', $request->cantidad);

            return redirect('login');
        }
    }

    public function addServicioCarrito(Request $request)
    {
        if (Auth::check()) {
            //Elimino las variables de sesion para cuando quiso agregar un servicio y no estaba logeado
            $request->session()->forget(['id_servicio', 'cantidad']);

            $id_usuario = Auth::user()->id;
            $id_carrito = Carrito_compra::where("id_usuario", $id_usuario)->first()->id_carrito;
            
            $id_servicio = $request->id_servicio;
            
            $controlador_servicio_x_carrito = new Servicios_x_CarritoController();
            $agregado = $controlador_servicio_x_carrito->agregarServicio($id_servicio, $id_carrito);
            if ($agregado) {
                session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Agregamos el servicio a tu carrito!"]);
                return redirect()->route("verCarritoDeCompras");
            } else {
                session(['mensaje_alerta_ok' =>false, 'mensaje'=> "No pudimos agregar el servicio a tu carrito"]);
                return redirect()->route("verCarritoDeCompras");
            }
        } else {
            /*
                Si el usuario no esta logueado, guardo en las variables de sesion el servicio que
                queria agregar al carrito y su cantidad, asi despues de ingresar se agrega al carrito
            */
            $request->session()->put('id_servicio', $request->id_servicio);
            $request->session()->put('cantidad', $request->cantidad);

            return redirect('login');
        }
    }

    /*
        Esta funcion se encarga de enviar los datos para eliminar un producto del carrito.
    */
    public function deleteProductoCarrito(Request $request)
    {
        $id_variante_producto = $request->id_variante_producto;

        $controlador_producto_x_carrito = new Productos_x_CarritoController();
        $eliminado= $controlador_producto_x_carrito->eliminarProducto($id_variante_producto);

        //Si se estan eliminando productos desde el carrito y se necesita retornar a la pagina anterior. El otro caso es cuando ya hizo la compra y solo se usa la funcion sin retorno.
        if(!isset($request->return)){
            if($eliminado){
                session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Eliminamos el producto de tu carrito!"]);
                return redirect()->route("verCarritoDeCompras");
            }
            else{
                session(['mensaje_alerta_ok' =>false, 'mensaje'=> "No pudimos eliminar el producto de tu carrito"]);
                return redirect()->route("verCarritoDeCompras");
            }
        }
    }

    /*
        Esta funcion se encarga de enviar los datos para eliminar un servicio del carrito.
    */
    public function deleteServicioCarrito(Request $request)
    {
        $id_servicio = $request->id_servicio;

        $controlador_servicio_x_carrito = new Servicios_x_CarritoController();
        $eliminado= $controlador_servicio_x_carrito->eliminarServicio($id_servicio);

        //Si se estan eliminando servicios desde el carrito y se necesita retornar a la pagina anterior. El otro caso es cuando ya hizo la compra y solo se usa la funcion sin retorno.
        if (!isset($request->return)) {
            if ($eliminado) {
                session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Eliminamos el servicio de tu carrito!"]);
                return redirect()->route("verCarritoDeCompras");
            } else {
                session(['mensaje_alerta_ok' =>false, 'mensaje'=> "No pudimos eliminar el servicio de tu carrito"]);
                return redirect()->route("verCarritoDeCompras");
            }
        }
    }

    /*
        Esta funcion busca el carrito asociado al usuario logueado y retorna todos
        los productos que tenga guardados en el carrito con la primer imagen
        que se posee almacenada y tambien retorna una lista de productos que le 
        podrian interesar al cliente.
    */
    public function verCarritoDeCompras()
    {
        $id_usuario = Auth::user()->id;
        $id_carrito = Carrito_compra::where("id_usuario",$id_usuario)->first()->id_carrito;

        $productos_en_carrito = DB::table('productos')
                ->join('variantes_producto', 'variantes_producto.id_producto', '=', 'productos.id_producto')
                ->join('productos_x_carritos', 'productos_x_carritos.id_variante_producto', '=', 'variantes_producto.id_variante_producto')
                ->where('productos_x_carritos.id_carrito',$id_carrito)
                ->select('productos.*', 'productos_x_carritos.cantidad','productos_x_carritos.id_carrito','variantes_producto.*')
                ->get();

        $servicios_en_carrito = DB::table('servicios')
                ->join('servicios_x_carritos', 'servicios_x_carritos.id_servicio', '=', 'servicios.id_servicio')
                ->where('servicios_x_carritos.id_carrito', $id_carrito)
                ->select('servicios.*', 'servicios_x_carritos.id_carrito')
                ->get();

        $imagenes_producto_controller = new Imagenes_productoController();
        foreach ($productos_en_carrito as $producto) {
            if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = $imagenes_producto_controller->getPrimeraImagenDeProducto($producto->id_producto);
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
        }

        $imagenes_servicio_controller = new Imagenes_servicioController();
        foreach ($servicios_en_carrito as $servicio) {
            if (Imagenes_Producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                $servicio->imagen = $imagenes_servicio_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
            } else {
                $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
            }
        }

        $productos_controller = new ProductosController();
        $productos_que_pueden_gustar = $productos_controller->productos_random();

        $cantidad_productos = count(Productos_x_carrito::where('id_carrito',$id_carrito)->get());
        $cantidad_servicios = count(Servicios_x_carrito::where('id_carrito',$id_carrito)->get());

        return view('clientes.verCarritoDeCompra', compact("productos_en_carrito", "servicios_en_carrito", "productos_que_pueden_gustar", "cantidad_productos", "cantidad_servicios"));
    }

    /*
        Esta funcion se encarga de llamar a la funcion que elimina todos los 
        productos del carrito de compras.
    */
    public function vaciarCarritoProductos()
    {
        $controlador_producto_x_carrito = new Productos_x_CarritoController();
        $vaciado = $controlador_producto_x_carrito->eliminarTodosLosProductos();

        if($vaciado){
            session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Vaciamos tu carrito!"]);
            return redirect()->route("verCarritoDeCompras");
        }
        else{
            session(['mensaje_alerta_ok' =>false, 'mensaje'=> "No pudimos vaciar tu carrito"]);
            return redirect()->route("verCarritoDeCompras");
        }
    }

    /*
        Esta funcion se encarga de llamar a la funcion que elimina todos los 
        productos del carrito de compras.
    */
    public function vaciarCarritoServicios()
    {
        $controlador_servicio_x_carrito = new Servicios_x_CarritoController();
        $vaciado = $controlador_servicio_x_carrito->eliminarTodosLosServicios();

        if($vaciado){
            session(['mensaje_alerta_ok' =>true, 'mensaje'=> "Vaciamos tu carrito!"]);
            return redirect()->route("verCarritoDeCompras");
        }
        else{
            session(['mensaje_alerta_ok' =>false, 'mensaje'=> "No pudimos vaciar tu carrito"]);
            return redirect()->route("verCarritoDeCompras");
        }
    }
}
