<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Collection;
use App\Variantes_producto;
use App\Pedido;
use App\Imagenes_producto;
use App\Productos;
use App\Servicio;

class PedidosController extends Controller
{
    /*
        Esta funcion recibe la informacion del pedido, y guarda en sesion el string que 
        almacenara
    */
    public function nuevoPedido(Request $request)
    {   
        $pedido=new Pedido();

        //Guardo el id del usuario que quiere realizar el pedido.
        $data = json_decode($request->data, true);
        $data['id_usuario'] = Auth::user()->id;
        $data = json_encode($data);

        $pedido->data=$data;
        $pedido->save();

        if ($request->tipo_compra == 1) {
            return redirect()->route("seleccionarMetodoEnvio", $pedido->id_pedido);
        } else if ($request->tipo_compra == 0) {
            return redirect()->route("confirmarPedido", $pedido->id_pedido);
        }
    }

    public function seleccionarMetodoEnvio($id_pedido)
    {
        $pedido = Pedido::findOrFail($id_pedido);
        $data = json_decode($pedido->data, true);

        //Si el id de usuario del pedido no coincide con el logeado, no le permito el acceso.
        //Esto se puede dar cuando cambie el id del pedido manualmente por URL.
        if($data['id_usuario'] == Auth::user()->id){
            $puntaje_acumulado = Auth::user()->puntaje_acumulado;

            $total_sin_envio = $data['total_sin_envio'];

            $oca_controller = new OcaController();
            $resultado = $this->calcularPesoYVolumen($data['items']);
            //TODO: tiene que ir el codigo postal del usuario...
            $metodos_de_envio = $oca_controller->cotizarEnvio($resultado['peso'], $resultado['volumen'], $resultado['paquetes'], 6300); /* En 6300: Auth::user()->codigo_postal_recepcion */
    
            return view("clientes.pedidos.seleccionarMetodoDeEnvio", compact("puntaje_acumulado", "metodos_de_envio", "id_pedido", "total_sin_envio"));
        }
        else{
            abort(401);
        }
    }

    /*
        Esta funcion se usa para calcular el volumen total y peso total de los productos
        que se van a comprar.
    */
    private function calcularPesoYVolumen($items){
        $resultado = array();
        $peso = 0;
        $volumen = 0;
        $paquetes = 1;

        foreach ($items as $item) {
            $producto = Variantes_producto::where('id_variante_producto', $item['id_variante_producto'])->first();
            $producto = Productos::where('id_producto', $producto->id_producto)->first();

            $peso += $producto->peso * $item['cantidad'];
            $volumen += ($producto->alto * $producto->ancho * $producto->largo) * $item['cantidad'];
        }

        $peso = $peso / 1000; // Paso de Gramos a Kilogramos.
        $volumen = $volumen / 1000000; // Paso de Centimetros a Metros cubicos;

        $resultado['peso'] = $peso;
        $resultado['volumen'] = $volumen;
        $resultado['paquetes'] = $paquetes;

        /*
            DIMENSIONES MAXIMAS DE OCA (segun mercado libre):
            Hasta 40 kg
            210 cm en total, sumando alto, ancho y largo
        */
        return $resultado;
    }

    /*
        Esta funcion se llama cuando se esta finalizando el pedido.
        Se muestran los detalles de la compra: total, beneficios y descuentos(si es que tiene), metodo y
        costo de envio (si los tiene) y los productos o servicios que se van a comprar.

        En esta funcion se retorna la vista que da la opcion de pagar a traves de Mercado Pago.
    */
    public function confirmarPedido(Request $request, $id_pedido)
    {
        //Paso a json el string con toda la informacion almacenada en la session.
        $pedido = Pedido::findOrFail($id_pedido);
        $data = json_decode($pedido->data, true);
        
        //Si el id de usuario del pedido no coincide con el logeado, no le permito el acceso.
        //Esto se puede dar cuando cambie el id del pedido manualmente por URL.
        if($data['id_usuario'] == Auth::user()->id){
            $total = 0;
            $respuesta = null;

            //Si es una compra de productos, calculo el total y analizo los beneficios o descuentos que se le pueden aplicar.
            if($data['tipo_compra'] == 1){
                $respuesta = $this->chequearBeneficios(Auth::user()->puntaje_acumulado, $data['total_sin_envio'], $request->metodo_envio, $request->costo_envio);
                $beneficio = $respuesta['beneficio'];
                $descuento = $respuesta['descuento'];
            }
            else if($data['tipo_compra'] == 0){
                $respuesta['total'] = $data['total_servicios'];
            }

            //Este total ya tiene incluido los descuentos, o bien el monto del envio ya incluido.
            $total = $respuesta['total'];

            $tipo_compra = $data['tipo_compra'];

            if ($tipo_compra == 1) {
                $data['metodo_envio'] = $request->metodo_envio;
                $data['costo_envio'] = $request->costo_envio;
                $data['total'] = $total;

                $data['beneficio'] = $beneficio;
                $data['descuento'] = $descuento;

                $pedido->data = json_encode($data);
                $pedido->save();

                $productos = collect();

                // Recorro los items del pedido para armar una coleccion que se pueda retornar a la vista con
                // los atributos que quiero del producto y su primer imagen que se encuentre en la base de datos.
                foreach ($data['items'] as $item) {
                    $producto = Variantes_producto::where('id_variante_producto', $item['id_variante_producto'])->first();
                    $producto->nombre = Productos::where('id_producto', $producto->id_producto)->first()->nombre;
                    $producto->precio = $item['precio'];
                    $producto->cantidad = $item['cantidad'];
                    if (Imagenes_producto::where('id_producto', $producto->id_producto)->exists()) {
                        $imagenes_controller = new Imagenes_productoController();
                        $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
                    } else {
                        $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
                    }

                    $productos->push($producto);
                }

                $metodo_envio = $request->metodo_envio;
                $costo_envio = $request->costo_envio;

                return view("clientes.pedidos.confirmarPedido", compact("total", "productos", "metodo_envio", "costo_envio", "beneficio", "tipo_compra", "descuento"));
            } 
            else if ($data['tipo_compra'] == 0) {
                $servicios = collect();

                $data['total_servicios'] = $total;

                // Recorro los items del pedido para armar una coleccion que se pueda retornar a la vista con
                // los atributos que quiero del servicio y su primer imagen que se encuentre en la base de datos.
                foreach ($data['items'] as $item) {
                    $servicio = Servicio::where('id_servicio', $item['id_servicio'])->first();
                    $servicio->titulo = Servicio::where('id_servicio', $servicio->id_servicio)->first()->titulo;
                    $servicio->precio = $item['precio'];
                    if (Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                        $imagenes_controller = new Imagenes_servicioController();
                        $servicio->imagen = $imagenes_controller->getPrimeraImagenDeServicio($servicio->id_servicio);
                    } else {
                        $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
                    }

                    $servicios->push($servicio);
                }

                return view("clientes.pedidos.confirmarPedido", compact("total", "servicios", "tipo_compra"));
            }
        }
        else{
            abort(401);
        }
    }

    /*
		Esta funcion recibe como parametro el puntaje del cliente que va a realizar la compra, el
		total (en caso de compra de productos, recibe el total sin el envio incluido), el tipo de
		compra(0: servicios y 1:productos), el metodo de envio y costo de envios los cuales solo
		son recibidos cuando la compra va a ser de productos.
		Lo que hace la funcion es consultar el sistema de puntajes y ver en que nivel esta
		el cliente para asignarle el beneficio que corresponda y retorno el beneficio con el
		total ya con descuentos aplicados.
    */
    private function chequearBeneficios($puntaje_cliente, $total, $metodo_envio, $costo_envio)
    {
        //Consulto todos los datos para identificar los niveles
        $niv1_10_off = Config::get('puntajes.beneficios.nivel_1');
        $niv2_envio_gratis = Config::get('puntajes.beneficios.nivel_2');
        $niv3_envio_gratis_y_5_off = Config::get('puntajes.beneficios.nivel_3');
        $niv4_envio_gratis_y_10_off = Config::get('puntajes.beneficios.nivel_4');
        $niv5_15_off = Config::get('puntajes.beneficios.nivel_5');

        $niv1_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_1');
        $niv2_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_2');
        $niv3_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_3');
        $niv4_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_4');
        $niv5_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_5');

        $beneficio = "";
        $descuento = 0;

        //Si esta en el nivel 0
        if($puntaje_cliente <= $niv1_10_off ){
            $total = $total + $costo_envio; //Solo sumo monto del envio.

        }//Si esta en el nivel 1
        else if($puntaje_cliente >= $niv1_10_off && $puntaje_cliente < $niv2_envio_gratis){
            //Si el monto de la compra supera lo esperado para obtener el beneficio
            if($total >= $niv1_monto_compra){
                $beneficio = "descuento de 10%";
                $descuento = 10;
                $total = $total * 0.90 + $costo_envio; //10% de descuento + monto del envio.
            }
            else{
                $total = $total + $costo_envio; //Solo sumo monto del envio.
            }

        }//Si esta en el nivel 2
        else if($puntaje_cliente >= $niv2_envio_gratis && $puntaje_cliente < $niv3_envio_gratis_y_5_off){
            //Si el monto de la compra supera lo esperado para obtener el beneficio
            if($total >= $niv2_monto_compra){
                $beneficio = "envío gratis";
                $total = $total;//No sumo el costo de envio ya que es gratis.
            }
            else{
                $total = $total + $costo_envio; //Solo sumo monto del envio.
            }

        }//Si esta en el nivel 3
        else if($puntaje_cliente >= $niv3_envio_gratis_y_5_off && $puntaje_cliente < $niv4_envio_gratis_y_10_off){
            //Si el monto de la compra supera lo esperado para obtener el beneficio
            if($total >= $niv3_monto_compra){
                if($metodo_envio == 1){
                    $beneficio= "envío gratis y descuento de $".number_format($total*0.05, 2, ",", ".")." (5%)";
                }
                else{
                    $beneficio= "descuento de 5%";
                }
                $descuento = 5;
                $total = $total * 0.95; //5% de descuento y no sumo el costo del envio.
            }
            else{
                $total = $total + $costo_envio; //Solo sumo monto del envio y no le hago descuento.
            }

        }//Si esta en el nivel 4
        else if($puntaje_cliente >= $niv4_envio_gratis_y_10_off && $puntaje_cliente < $niv5_15_off){
            //Si el monto de la compra supera lo esperado para obtener el beneficio
            if($total >= $niv4_monto_compra){
                if($metodo_envio == 1){
                    $beneficio= "envío gratis y descuento de $".number_format($total*0.10, 2, ",", ".")." (10%)";
                }
                else{
                    $beneficio= "descuento de 10%";
                }
                $descuento = 10;
                $total = $total * 0.90; //10% de descuento y no sumo el costo del envio
            }
            else{
                $total = $total + $costo_envio; //Solo sumo monto del envio y no le hago descuento.
            }

        }//Si esta en el nivel 5
        else if($puntaje_cliente >= $niv5_15_off &&  $total>= $niv5_monto_compra){
            //Si el monto de la compra supera lo esperado para obtener el beneficio
            if($total >= $niv5_monto_compra){
                $beneficio= "descuento de $".number_format($total*0.15, 2, ",", ".")." (15%)";
                $descuento = 15;

                $total = $total * 0.85 + $costo_envio; //15% de descuento
            }
            else{
                $total = $total + $costo_envio; //Solo sumo monto del envio.
            }
        }

        $respuesta['beneficio'] = $beneficio;
        $respuesta['descuento'] = $descuento;
        $respuesta['total'] = $total;

        return $respuesta;
    }
}
