<?php

namespace App\Http\Controllers;

use Exception;
use App\Carrito_compra;
use App\Productos_x_carrito;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Productos_x_CarritoController extends Controller
{
    public function agregarProducto($id_variante_producto, $id_carrito, $cantidad)
    {
        try{
            $productos_x_carrito = new Productos_x_carrito();

            $productos_x_carrito->id_variante_producto = $id_variante_producto;
            $productos_x_carrito->id_carrito = $id_carrito;
            $productos_x_carrito->cantidad = $cantidad;

            $productos_x_carrito->save();

            return true; 
        }catch(Exception $e){
            return false;
        }
    }

    public function eliminarProducto($id_variante_producto)
    {
        try{
            $id_usuario = Auth::user()->id;
            $id_carrito = Carrito_compra::where("id_usuario",$id_usuario)->first()->id_carrito;

            Productos_x_carrito::where([['id_variante_producto', $id_variante_producto],['id_carrito',$id_carrito]])->delete();
            
            return true;
        }catch(Exception $e){
            die($e);
            return false;
        }
    }

    public function eliminarTodosLosProductos()
    {
        try{
            $id_usuario = Auth::user()->id;
            $id_carrito = Carrito_compra::where("id_usuario",$id_usuario)->first()->id_carrito;

            Productos_x_carrito::where('id_carrito',$id_carrito)->delete();

            return true; 
        }catch(Exception $e){
            return false;
        }
    }
}
