<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Productos;
use App\Subcategoria;
use App\Imagenes_producto;
use App\Variantes_producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Exception;
use Laravel\Ui\Presets\React;

class ProductosController extends Controller
{
    /*
        Esta funcion busca el producto seleccionado por el cliente, asocia su categoria
        e imagenes y tambien retorna una lista de productos que le podrian interesar
        al cliente.
    */
    public function verProductoC($id_producto)
    {
        $producto = Productos::findOrFail($id_producto);

        $categoria = DB::table('categorias')
                ->join('subcategorias', 'subcategorias.id_categoria', '=', 'categorias.id_categoria')
                ->where("subcategorias.id_subcategoria", $producto->id_subcategoria)
                ->select('categorias.*')
                ->first();

        $producto->categoria = $categoria;

        $imagenes_controller = new Imagenes_productoController();
        $producto->imagenes = $imagenes_controller->getImagenesDeProducto($id_producto);

        //Consulto todas las variantes del producto y las envio a la vista
        $variantes_controller = new Variantes_ProductoController();
        $producto->colores = $variantes_controller->getColoresDeProducto($id_producto);
        $producto->talles_ropa = $variantes_controller->getTallesRopa($id_producto);
        $producto->talles_calzado = $variantes_controller->getTallesCalzado($id_producto);
        $producto->talles_anzuelo = $variantes_controller->getTallesAnzuelo($id_producto);
        $producto->manos_habiles = $variantes_controller->getManosHabiles($id_producto);
        $producto->longitudes = $variantes_controller->getLongitudes($id_producto);
        $producto->fuerzas = $variantes_controller->getFuerzas($id_producto);
        $producto->acciones = $variantes_controller->getAcciones($id_producto);
        $producto->diametros = $variantes_controller->getDiametros($id_producto);

        $productos_que_pueden_gustar = $this->productos_random();

        return view("clientes.verProductoIndividual", compact("producto", "productos_que_pueden_gustar"));
    }

    /*
        Esta funcion busca todos los productos asociados a la categoria seleccionada y le
        asocia la primer imagen que posee almacenada.
    */
    public function getProductosXCategoria(Request $request)
    {
        if (Categoria::where('id_categoria', $request->id_categoria)->exists()) {
            try {
                $productos = DB::table('productos')
                        ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                        ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                        ->where([
                            ['productos.habilitado', '=', 1],
                            ['productos.eliminado', '=', 0],
                            ['categorias.id_categoria', '=', $request->id_categoria],
                        ])
                        ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                        ->orderBy('productos.precio', 'ASC')
                        ->paginate(20);

                $imagenes_controller = new Imagenes_productoController();
                $marcas = array();
                foreach ($productos as $producto) {
                    if (!(in_array($producto->marca, $marcas))) {
                        array_push($marcas, $producto->marca);
                    }
                    if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                        $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
                    } else {
                        $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
                    }
                }

                $categorias_controller = new CategoriaController();
                $categorias = $categorias_controller->getCategoriasConSubcategorias(1);

                //Hay que devolver todo esto para poder filtrar ya que usamos verProductos.php para todas
                $precio_min = Productos::min('precio');

                $precio_max = Productos::max('precio');

                $id_categoria = $request->id_categoria;

                $id_subcategoria = '';

                $texto = '';

                $ordenar_por = 0;

                $marcas_filtro = '';

                return view("clientes.verProductos", compact("productos", "categorias", "marcas", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por", "marcas_filtro"));
            } catch (Exception $e) {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    /*
        Esta funcion busca todos los productos asociados a la subcategoria seleccionada y le
        asocia la primer imagen que posee almacenada.
    */
    public function getProductosXSubcategoria(Request $request)
    {
        try {
            if (Subcategoria::where('id_subcategoria', $request->id_subcategoria)->exists()) {
                $productos = DB::table('productos')
                        ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                        ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                        ->where([
                            ['productos.habilitado', '=', 1],
                            ['productos.eliminado', '=', 0],
                            ['productos.id_subcategoria', '=', $request->id_subcategoria],
                        ])
                        ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                        ->orderBy('productos.precio', 'ASC')
                        ->paginate(20);

                $imagenes_controller = new Imagenes_productoController();
                $marcas = array();
                foreach ($productos as $producto) {
                    if (!(in_array($producto->marca, $marcas))) {
                        array_push($marcas, $producto->marca);
                    }
                    if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                        $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
                    } else {
                        $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
                    }
                }

                $categorias_controller = new CategoriaController();
                $categorias = $categorias_controller->getCategoriasConSubcategorias(1);

                //Hay que devolver todo esto para poder filtrar ya que usamos verProductos.php para todas
                $precio_min = Productos::min('precio');

                $precio_max = Productos::max('precio');

                $id_subcategoria = $request->id_subcategoria;

                $id_categoria = '';

                $texto = '';

                $ordenar_por = 0;

                $marcas_filtro = '';

                return view("clientes.verProductos", compact("productos", "categorias", "marcas", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por", "marcas_filtro"));
            } else {
                abort(404);
            }
        } catch (Exception $e) {
            abort(404);
        }
    }

    /*
        Esta funcion busca todos los productos que haya almacenados y le
        asocia la primer imagen que posee almacenada.
    */
    public function getProductosC()
    {
        $productos = DB::table('productos')
                ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                ->where([["productos.habilitado", 1], ['productos.eliminado', 0]])
                ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                ->orderBy('productos.precio', 'ASC')
                ->paginate(20);

        $imagenes_controller = new Imagenes_productoController();
        $marcas = array();
        foreach ($productos as $producto) {
            if (!(in_array($producto->marca, $marcas))) {
                array_push($marcas, $producto->marca);
            }
            if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
        }

        $categorias_controller = new CategoriaController();
        $categorias = $categorias_controller->getCategoriasConSubcategorias(1);
        
        //Hay que devolver todo esto para poder filtrar ya que usamos verProductos.php para todas
        $precio_min = Productos::min('precio');

        $precio_max = Productos::max('precio');

        $id_categoria = '';

        $id_subcategoria = '';
        
        $texto = '';

        $ordenar_por = 0;

        $marcas_filtro = '';

        return view("clientes.verProductos", compact("productos", "categorias", "marcas", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por", "marcas_filtro"));
    }

    /*
        Esta funcion busca 8 productos random que haya almacenados y le asocia
        la primer imagen que posee almacenada. Solo retorna informacion, no retorna
        a una vista
    */
    public function productos_random()
    {
        $productos_destacados = Productos::where([['destacado', 1], ['habilitado', 1], ['eliminado', 0]])->get();

        $productos = array();

        for ($i=0; $i < count($productos_destacados) && $i <5; $i++) {
            array_push($productos, $productos_destacados[$i]);
        }
        
        $imagenes_controller = new Imagenes_productoController();
        foreach ($productos as $producto) {
            if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
            $categoria = DB::table('categorias')
                ->join('subcategorias', 'subcategorias.id_categoria', '=', 'categorias.id_categoria')
                ->where("subcategorias.id_subcategoria", $producto->id_subcategoria)
                ->select('categorias.*')
                ->first();

            $producto->categoria = $categoria;
        }
        return $productos;
    }

    public function buscador(Request $request)
    {
        $texto = $request->texto;

        $productos = DB::table('productos')
                ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                ->where([["productos.habilitado", 1],['productos.nombre','LIKE','%'.$texto.'%'], ['productos.eliminado', 0]])
                ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                ->orderBy('productos.precio', 'ASC')
                ->paginate(20);

        $imagenes_controller = new Imagenes_productoController();
        $marcas = array();
        foreach ($productos as $producto) {
            if (!(in_array($producto->marca, $marcas))) {
                array_push($marcas, $producto->marca);
            }
            if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
        }

        $categorias_controller = new CategoriaController();
        $categorias = $categorias_controller->getCategoriasConSubcategorias(1);
        //TODO: hay que fijarse bien que precios min y max hay que devolver en cada caso
        //Hay que devolver todo esto para poder filtrar ya que usamos verProductos.php para todas
        $precio_min = Productos::min('precio');

        $precio_max = Productos::max('precio');

        $id_categoria = '';

        $id_subcategoria = '';

        $ordenar_por = 0;

        $marcas_filtro = '';

        return view("clientes.verProductos", compact("productos", "categorias", "marcas", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por", "marcas_filtro"));
    }

    /*
        Funcion para filtrar los productos segun precio y marca (tiene en cuenta si se envia una
        categoria o subcategoria) y ordenarlos por nombre o precio (ASC y DESC)
    */

    public function filtrarProductos(Request $request)
    {
        try {
            //Tenemos en cuenta si viene desde getProductor por categoria, subcategoria o el buscador
            $query=[
                ['productos.habilitado', '=', 1],
                ['productos.eliminado', 0]
            ];

            if ($request->id_categoria!='') {
                array_push($query, ['categorias.id_categoria', '=', $request->id_categoria]);
            } elseif ($request->id_subcategoria!='') {
                array_push($query, ['productos.id_subcategoria', '=', $request->id_subcategoria]);
            } elseif ($request->texto!='') {
                array_push($query, ['productos.nombre','LIKE','%'.$request->texto.'%']);
            }
            //Nos fijamos si filtro por orden, sino por precio de menor a mayor por defecto
            switch ($request->ordenar_por) {
                case '1':
                    $param1 = 'productos.precio';
                    $param2 = 'ASC';
                    break;
                case '2':
                    $param1 = 'productos.precio';
                    $param2 = 'DESC';
                    break;
                case '3':
                    $param1 = 'productos.nombre';
                    $param2 = 'ASC';
                    break;
                case '4':
                    $param1 = 'productos.nombre';
                    $param2 = 'DESC';
                    break;
                default:
                    $param1 = 'productos.precio';
                    $param2 = 'ASC';
                    break;
            }
            //Para filtrar por las marcas que eligio
            if ($request->marcas!=null) {
                $productos = DB::table('productos')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                    ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                    ->where($query)
                    ->whereIn('productos.marca', $request->marcas)
                    ->whereBetween('productos.precio', [$request->precio_min, $request->precio_max])
                    ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                    ->orderBy($param1, $param2)
                    ->paginate(20);
            } else {
                $productos = DB::table('productos')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                    ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                    ->where($query)
                    ->whereBetween('productos.precio', [$request->precio_min, $request->precio_max])
                    ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                    ->orderBy($param1, $param2)
                    ->paginate(20);
            }
            
            //Obtener las imagenes
            $imagenes_controller = new Imagenes_productoController();
            $marcas = array();
            foreach ($productos as $producto) {
                if (!(in_array($producto->marca, $marcas))) {
                    array_push($marcas, $producto->marca);
                }
                if (Imagenes_Producto::where('id_producto', $producto->id_producto)->exists()) {
                    $producto->imagen = $imagenes_controller->getPrimeraImagenDeProducto($producto->id_producto);
                } else {
                    $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
                }
            }

            //Obtener las categorias
            $categorias_controller = new CategoriaController();
            $categorias = $categorias_controller->getCategoriasConSubcategorias(1);

            //Hay que devolver todo esto para poder filtrar ya que usamos verProductos.php para todas
            $precio_min = $request->precio_min;

            $precio_max = $request->precio_max;

            $id_categoria = $request->id_categoria;

            $id_subcategoria = $request->id_subcategoria;

            $texto = $request->texto;

            $ordenar_por = $request->ordenar_por;

            $marcas_filtro = $request->marcas;

            return view("clientes.verProductos", compact("productos", "categorias", "marcas", "precio_min", "precio_max", "id_categoria", "id_subcategoria", "texto", "ordenar_por", "marcas_filtro"));
        } catch (Exception $e) {
            abort(404);
        }
    }


    //------------------------------------Funciones para el administrador------------------------------------INICIO
    public function getProductos()
    {
        $productos = Productos::where([["habilitado", 1], ['eliminado', 0]])->orderby('created_at', 'DESC')->paginate(50);
        $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo', 1)->get();
        return view("admin.productos.verProductos", compact("productos", "categorias"));
    }

    public function buscadorAdmin(Request $request)
    {
        $productos = DB::table('productos')
                ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                ->where([['productos.nombre','LIKE','%'.$request->texto.'%'], ['productos.eliminado', 0]])
                ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                ->orderBy('productos.precio', 'ASC')
                ->paginate(20);
        $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo','1')->get();
        return view("admin.productos.verProductos", compact("productos", "categorias"));
    }

    public function getProductosDeshabilitados()
    {
        $productos = Productos::where([["habilitado", 0], ['eliminado', 0]])->paginate(50);
        $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo', 1)->get();
        return view("admin.productos.verProductos", compact("productos", "categorias"));
    }

    public function editarProducto($id_producto)
    {
        $producto = Productos::find($id_producto);
        $subcategorias = Subcategoria::where('tipo', 1)->get();
        return view("admin.productos.editarProducto", compact("producto", "subcategorias"));
    }

    public function updateProducto(Request $request)
    {
        try {
            $producto = Productos::find($request->id_producto);

            $producto->nombre = $request->nombre;
            $producto->descripcion = $request->descripcion;
            $producto->precio = $request->precio;
            $producto->id_subcategoria = $request->id_subcategoria;
            $producto->descripcion_resumida = $request->descripcion_resumida;
            $producto->marca = $request->marca;
            $producto->alto = $request->alto;
            $producto->ancho = $request->ancho;
            $producto->largo = $request->largo;
            $producto->peso = $request->peso;
            $producto->destacado = $request->destacado;
            $producto->habilitado = $request->habilitado;

            $producto->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Producto editado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al editar producto.']);
        }
    }

    public function deshabilitarProducto(Request $request)
    {
        try {
            $producto = Productos::find($request->id_producto);
            $producto->habilitado = 0; // 0 = FALSE
            $producto->destacado = 0; // 0 = FALSE
            $producto->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Producto deshabilitado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al deshabilitar el producto.']);
        }
    }

    public function habilitarProducto(Request $request)
    {
        try {
            $producto = Productos::find($request->id_producto);
            $producto->habilitado = 1; // 1 = TRUE
            $producto->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Producto habilitado correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al habilitar el producto.']);
        }
    }

    public function createProducto(Request $request)
    {
        try {
            $id_producto = Productos::create([
                'nombre' => $request->nombre,
                'marca' => $request->marca,
                'precio' => $request->precio,
                'descripcion_resumida' => $request->descripcion_resumida,
                'descripcion' => $request->descripcion,
                'alto' => $request->alto,
                'ancho' => $request->ancho,
                'largo' => $request->largo,
                'peso' => $request->peso,
                'id_subcategoria' => $request->id_subcategoria,
                'destacado' => $request->destacado,
            ])->id_producto;

            return response()->json(['success'=>'true' , 'mensaje'=>'Producto creado correctamente.' , 'id_producto'=>$id_producto]);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Error al crear producto: ', $e->getMessage()]);
        }
    }

    /*
        Esta funcion busca todos los productos asociados a la categoria seleccionada y los muestra
        en la vista de los productos para que los pueda editar o eliminar.
    */
    public function getProductosXCategoriaAdmin($id_categoria)
    {
        if (Categoria::where('id_categoria', $id_categoria)->exists()) {
            $productos = DB::table('productos')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                    ->join('categorias', 'categorias.id_categoria', '=', 'subcategorias.id_categoria')
                    ->where([
                        ['categorias.id_categoria', '=', $id_categoria],
                        ['productos.eliminado', 0]
                    ])
                    ->select('productos.*', 'categorias.nombre_categoria', 'categorias.id_categoria')
                    ->paginate(50);

            $nombre_categoria = Categoria::find($id_categoria)->nombre_categoria;

            $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo','1')->get();
            return view("admin.productos.verProductos", compact("productos", "categorias","nombre_categoria"));
        } else {
            abort(404);
        }
    }

    /*
        Esta funcion busca todos los productos asociados a la subcategoria seleccionada y los muestra
        en la vista de los productos para que los pueda editar o eliminar.
    */
    public function getProductosXSubcategoriaAdmin($id_subcategoria)
    {
        if (Subcategoria::where('id_categoria', $id_subcategoria)->exists()) {
            $productos = DB::table('productos')
                    ->join('subcategorias', 'subcategorias.id_subcategoria', '=', 'productos.id_subcategoria')
                    ->where([
                        ['subcategorias.id_subcategoria', '=', $id_subcategoria],
                        ['productos.eliminado', 0]
                    ])
                    ->select('productos.*')
                    ->paginate(50);

            $subcategoria = Subcategoria::find($id_subcategoria);
            $nombre_subcategoria = $subcategoria->nombre_subcategoria;
            $nombre_categoria = Categoria::find($subcategoria->id_categoria)->nombre_categoria;

            $categorias = Categoria::select("id_categoria", "nombre_categoria")->where('tipo','1')->get();
            return view("admin.productos.verProductos", compact("productos", "categorias", "nombre_subcategoria", "nombre_categoria"));
        } else {
            abort(404);
        }
    }

    public function deleteProducto(Request $request)
    {
        try {
            $producto = Productos::find($request->id_producto);
            $producto->eliminado = 1;
            $producto->save();
            
            $variantes_aux = DB::table('variantes_producto')->where('id_producto', $request->id_producto)->select('id_variante_producto')->get();
            
            $ids_variantes = array();
            foreach ($variantes_aux as $variante) {
                array_push($ids_variantes, $variante->id_variante_producto);
            }

            $eliminado = DB::table('productos_x_carritos')->whereIn("id_variante_producto", $ids_variantes)->delete();

            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha eliminado correctamente el producto.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al eliminar el producto.']);
        }
    }
    //------------------------------------Funciones para el administrador------------------------------------FIN
}
