<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacto;
use Exception;

class ContactoController extends Controller
{
    public function editarContactoView()
    {
        $contacto = Contacto::find(1);
        return view("admin.contacto.editar_contacto", compact("contacto"));
    }

    public function editarContacto(Request $request)
    {
        try{
            $contacto = Contacto::find(1);
            
            $contacto->telefono_1 = $request->telefono_1;
            $contacto->telefono_2 = $request->telefono_2;
            $contacto->telefono_3 = $request->telefono_3;
            $contacto->facebook = $request->facebook;
            $contacto->instagram = $request->instagram;
            $contacto->twitter = $request->twitter;
            $contacto->youtube = $request->youtube;
            $contacto->email_info = $request->email_info;
            $contacto->email_comprobantes = $request->email_comprobantes;
            $contacto->email_reclamos = $request->email_reclamos;
    
            $contacto->save();

            return response()->json(['success'=>'true' , 'mensaje'=>'Información de contacto actualizada correctamente.']);
        }
        catch(Exception $e){
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al actualizar la informacion de contacto.']);
        }
    }
}
