<?php

namespace App\Http\Controllers;

use App\Preguntas_frecuentes;
use Illuminate\Http\Request;
use Exception;

class PreguntasFrecuentesController extends Controller
{
    public function getPreguntasFrecuentes()
    {
        $preguntas_frecuentes = Preguntas_frecuentes::get();

        return view("admin.preguntas_frecuentes.verPreguntasFrecuentes", compact("preguntas_frecuentes"));
    }

    public function createPreguntaFrecuente(Request $request)
    {
        try {
            $id_pregunta = Preguntas_frecuentes::create([
                'texto_pregunta' => $request->texto_pregunta,
                'texto_respuesta' => $request->texto_respuesta,
            ])->id_pregunta;
            
            return response()->json(['success'=>'true' , 'mensaje'=>'Pregunta creada correctamente.' , 'id_pregunta' => $id_pregunta]);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al crear la pregunta.']);
        }
    }

    public function editarPreguntaFrecuente($id_pregunta)
    {
        $pregunta_frecuente = Preguntas_frecuentes::find($id_pregunta);

        return view("admin.preguntas_frecuentes.editarPreguntaFrecuente", compact("pregunta_frecuente"));
    }

    public function updatePreguntaFrecuente(Request $request)
    {
        try {
            $pregunta_frecuente = Preguntas_frecuentes::find($request->id_pregunta);
    
            $pregunta_frecuente->texto_pregunta = $request->texto_pregunta;
            $pregunta_frecuente->texto_respuesta = $request->texto_respuesta;
            $pregunta_frecuente->mostrar = $request->mostrar;
    
            $pregunta_frecuente->save();
            return response()->json(['success'=>'true' , 'mensaje'=>'Pregunta actualizada correctamente.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al actualizar la pregunta.']);
        }
    }

    public function deletePreguntaFrecuente(Request $request)
    {
        try {
            $pregunta = Preguntas_frecuentes::destroy($request->id_pregunta);

            return response()->json(['success'=>'true' , 'mensaje'=>'Se ha eliminado correctamente la pregunta.']);
        } catch (Exception $e) {
            return response()->json(['success'=>'false' , 'mensaje'=>'Ocurrió un error al eliminar la pregunta.']);
        }
    }
}
