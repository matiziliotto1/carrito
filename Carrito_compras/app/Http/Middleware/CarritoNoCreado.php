<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Carrito_compra;

class CarritoNoCreado
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id_usuario = Auth::user()->id;
        $carrito_compra = Carrito_compra::where('id_usuario',$id_usuario)->first();

        if ($carrito_compra == null) {
            return $next($request);
        }
        else{
            return redirect('/');
        }
    }
}
