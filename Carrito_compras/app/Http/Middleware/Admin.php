<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard="administrador")
    {
        if(!auth()->guard($guard)->check()) {
            return redirect(route('administrador.login'));
        }
        return $next($request);
    }
}
