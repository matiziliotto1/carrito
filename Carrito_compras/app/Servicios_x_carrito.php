<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicios_x_carrito extends Model
{
    protected $fillable = [
        'id_servicio',
        'id_carrito',
    ];
    protected $table = 'servicios_x_carritos';
}
