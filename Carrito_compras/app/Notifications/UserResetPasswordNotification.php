<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserResetPasswordNotification extends Notification
{
    use Queueable;

    protected $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Reinicio de contraseña')
                    ->greeting('Hola!')
                    ->line('Hemos recibido una peticion para reinicar tu contraseña.')
                    ->action('Reiniciar contraseña', url('/password/reset/'.$this->token))
                    ->line('No compartas este enlace con nadie, recuerda que ningun administrador de la pagina nunca te pedira ningún tipo de código o contraseña.')
                    ->line('Si tu no haz hecho esta peticion, puedes ignorar este correo.')
                    ->salutation('Muchas gracias.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
