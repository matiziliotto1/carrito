<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrito_compra extends Model
{
    protected $fillable = [
        'id_usuario',
    ];
    protected $table = 'carrito_compras';
}
