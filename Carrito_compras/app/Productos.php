<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    protected $fillable = [
        'nombre',
        'descripcion',
        'precio',
        'id_subcategoria',
        'descripcion_resumida',
        'marca',
        'alto',
        'ancho',
        'largo',
        'peso',
        'destacado',
        'habilitado',
    ];
    protected $table = 'productos';
    protected $primaryKey = 'id_producto';
}
