<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras_x_productos extends Model
{
    protected $fillable = [
        'id_compra',
        'id_variante_producto',
        'cantidad',
        'precio',
    ];
    protected $table = 'compras_x_productos';
}
