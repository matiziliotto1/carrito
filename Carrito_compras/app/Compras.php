<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras extends Model
{
    protected $fillable = [
        'id_usuario',
        'fecha_compra',
        'puntaje_compra',
        'monto_total',
        'codigo_seguimiento',
        'monto_envio',
        'metodo_envio',
        'estado',
        'tipo_compra',
        'beneficio',
        'descuento',
    ];
    protected $table = 'compras';
    protected $primaryKey = 'id_compra';
}
