<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    public $timestamps = false;
    protected $primaryKey = 'id_contacto';
    
    protected $fillable = [
        'telefono_1',
        'telefono_2',
        'telefono_3',
        'facebook',
        'instagram',
        'twitter',
        'youtube',
        'email_info',
        'email_comprobantes',
        'email_reclamos',
    ];
    protected $table = 'contacto';
}
