<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas_frecuentes extends Model
{
    protected $fillable = [
        'texto_pregunta',
        'texto_respuesta',
        'mostrar',
    ];
    protected $table = 'preguntas_frecuentes';
    protected $primaryKey = 'id_pregunta';
}
