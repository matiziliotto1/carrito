<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = [
        'id_menu',
        'nombre_categoria',
        'link',
        'habilitado',
        'tipo'
    ];
    protected $table = 'categorias';
    protected $primaryKey = 'id_categoria';
}
