<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    // En servidor
    // public function register()
    // {
    //     $this->app->bind('path.public',function(){
    //         return '/home/quedateencasa/public_html/mercadofishing/';
    //         //return base_path().'/../';
    //        //return 'http://209.133.206.33/~quedateencasa/mercadofishing'; 
    //     });
    //     $this->app->bind('path.storage',function(){
    //         return '/home/quedateencasa/public_html/mercadofishing/mercado_fishing/storage/';
    //         //return base_path().'/'.'mercado_fishing';
    //        //return 'http://209.133.206.33/~quedateencasa/mercadofishing/mercado_fishing'; 
    //     });
    // }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
