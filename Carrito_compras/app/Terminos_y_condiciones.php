<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminos_y_condiciones extends Model
{
    protected $fillable = [
        'titulo',
        'texto',
    ];
    protected $table = 'terminos_y_condiciones';
    protected $primaryKey = 'id_termino_condiciones';
}
