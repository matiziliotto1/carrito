<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $casts = [
        'id_pedido' => 'int',
        'data' => 'array'
    ];
    protected $fillable = [
        'data',
    ];
    protected $table = 'pedidos';
    protected $primaryKey = 'id_pedido';
    public $timestamps = false;
}
