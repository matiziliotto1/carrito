<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos_x_carrito extends Model
{
    protected $fillable = [
        'id_variante_producto',
        'id_carrito',
        'cantidad',
    ];
    protected $table = 'productos_x_carritos';
}
