<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Administrador extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = "id_administrador";

    protected $table = 'administradores';

    protected $fillable = ['nombre_usuario', 'nombre' , 'apellido' , 'password'];

    protected $hidden = ['password',  'remember_token'];
}
