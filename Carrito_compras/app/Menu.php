<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'id_menu',
        'etiqueta',
        'link',
        'orden',
    ];
    protected $table = 'menu';
}
