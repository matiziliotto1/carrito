<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategoria extends Model
{
    protected $fillable = [
        'id_categoria',
        'nombre_subcategoria',
        'link',
        'habilitado',
        'tipo'
    ];
    protected $table = 'subcategorias';
    protected $primaryKey = 'id_subcategoria';
}
