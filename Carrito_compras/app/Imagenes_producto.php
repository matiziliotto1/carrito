<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagenes_producto extends Model
{
    protected $fillable = [
        'url',
        'orden',
        'mostrar',
        'id_producto',
    ];
    protected $table = 'imagenes_productos';
    protected $primaryKey = 'id_imagen';
    public $timestamps = false;
}
