<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras_x_servicios extends Model
{
    protected $fillable = [
        'id_compra',
        'id_servicio',
        'precio',
    ];
    protected $table = 'compras_x_servicios';
}
