<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        'url',
        'orden',
        'mostrar',
    ];
    protected $table = 'slider';
    protected $primaryKey = 'id_imagen';
}
