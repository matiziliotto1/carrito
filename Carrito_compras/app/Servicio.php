<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $fillable = [
        'titulo',
        'descripcion',
        'precio',
        'id_subcategoria',
        'descripcion_resumida',
        'destacado',
        'habilitado',
    ];
    protected $table = 'servicios';
    protected $primaryKey = 'id_servicio';
}
