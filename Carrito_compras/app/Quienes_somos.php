<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quienes_somos extends Model
{
    protected $fillable = [
        'review',
        'objetivos',
    ];
    protected $table = 'quienes_somos';
    protected $primaryKey = 'id_quienes_somos';
}
