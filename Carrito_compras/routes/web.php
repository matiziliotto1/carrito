<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//========================================= INICIO RUTAS DE PRUEBA =========================================
Route::get('/obteniendo-provincias', function(){
    // Create a client with a base URI
    $client = new GuzzleHttp\Client(['base_uri' => 'http://webservice.oca.com.ar/oep_tracking/Oep_Track.asmx/GetProvincias']);

    // Send a request to http://webservice.oca.com.ar/oep_tracking/Oep_Track.asmx/GetProvincias
    $response = $client->request('GET');

    // $response contains the data you are trying to get, you can do whatever u want with that data now. However to get the content add the line
    $contents = $response->getBody()->getContents();

    $xml = simplexml_load_string($contents);
    //Convert the result to Json or Array
    $json = json_encode($xml);
    $array = json_decode($json,TRUE);

    foreach($array['Provincia'] as $prov){
        echo $prov['IdProvincia']."--".$prov['Descripcion'];
        echo "<br>";
    }

    // die($json);
});
//========================================= FIN RUTAS DE PRUEBA =========================================



Auth::routes(['verify' => true]);

//Rutas para el manejo de sesiones del administrador
Route::get('/administracion/login', 'Auth\AdminLoginController@login_view')->name('administrador.login');
Route::post('/administracion/login', 'Auth\AdminLoginController@login')->name('administrador.login.post');
Route::post('/administracion/logout', 'Auth\AdminLoginController@logout')->name('administrador.logout');

//Rutas para el inicio, ver productos, categorias, subcategorias y buscar productos
Route::get('/', 'HomeController@index')->name('home');
Route::get('/productos/ver/filtros', 'ProductosController@filtrarProductos')->name('filtrarProductos');
Route::get('/productos/ver/todos', 'ProductosController@getProductosC')->name('getProductosC');
Route::get('/productos/ver/ordenados', 'ProductosController@ordenarProductos')->name('ordenarProductos');
Route::get('/productos/ver/{id_producto}', 'ProductosController@verProductoC')->name('verProductoC');
Route::get('/productos/ver/categoria/{id_categoria}', 'ProductosController@getProductosXCategoria')->name('verProductosXCategoria');
Route::get('/productos/ver/subcategoria/{id_subcategoria}', 'ProductosController@getProductosXSubcategoria')->name('verProductosXSubcategoria');
Route::get('/productos/buscar', 'ProductosController@buscador')->name('buscadorProductos');

//Ruta para agregar producto al carrito (no importa si esta o no logeado, se controla desde las funciones)
Route::post('/add/producto/carrito', 'CarritoController@addProductoCarrito')->name('addProductoCarrito');

//Rutas para el inicio, ver productos, categorias, subcategorias y buscar productos
Route::get('/servicios/ver/todos', 'ServiciosController@getServiciosC')->name('getServiciosC');
Route::get('/servicios/ver/filtros', 'ServiciosController@filtrarServicios')->name('filtrarServicios');
Route::get('/servicios/ver/{id_servicio}', 'ServiciosController@verServicioC')->name('verServicioC');
Route::get('/servicios/ver/categoria/{id_categoria}', 'ServiciosController@getServiciosXCategoria')->name('verServiciosXCategoria');
Route::get('/servicios/ver/subcategoria/{id_subcategoria}', 'ServiciosController@getServiciosXSubcategoria')->name('verServiciosXSubcategoria');
Route::get('/servicios/buscar', 'ServiciosController@buscadorServicios')->name('buscadorServicios');

//Ruta para agregar servicio al carrito (no importa si esta o no logeado, se controla desde las funciones)
Route::post('/add/servicio/carrito', 'CarritoController@addServicioCarrito')->name('addServicioCarrito');

//Ruta para la ayuda, contacto, terminos y condiciones.
Route::get('/ayuda', 'HomeController@ayuda')->name('ayuda');
Route::get('/contacto', 'HomeController@contacto')->name('contacto');
Route::get('/terminos/condiciones', 'TerminosYCondicionesController@verTerminosYCondicionesUsuario')->name('terminosYCondiciones');
Route::get('/quienesSomos', 'QuienesSomosController@verQuienesSomosUsuario')->name('quienesSomos');

//Rutas para peticiones AJAX en la vista individual de un producto para filtrar/descartar variantes
Route::post('/filtrar/variantes', 'Variantes_ProductoController@filtrarVariantes')->name('filtrarVariantes');

Route::post('/get/variante', 'Variantes_ProductoController@getVariante')->name('getVariante');




//===================================RUTAS PARA LOS CLIENTES LOGUEADOS===================================INICIO
Route::middleware(['auth'])->group(function () {
    Route::post('/procesar-pago', 'MercadoPagoController@procesarPago')->name('procesarPago');

    //Rutas para el control del carrito de compras del cliente
    Route::get('/create/carrito_compras', 'CarritoController@createCarrito')->name('createCarrito')->middleware('carritoNoCreado');
    Route::post('/delete/producto/carrito', 'CarritoController@deleteProductoCarrito')->name('deleteProductoCarrito');
    Route::post('/vaciar/carrito/compras/productos', 'CarritoController@vaciarCarritoProductos')->name('vaciarCarritoProductos');
    Route::post('/vaciar/carrito/compras/servicios', 'CarritoController@vaciarCarritoServicios')->name('vaciarCarritoServicios');
    Route::get('/ver/carrito/compras', 'CarritoController@verCarritoDeCompras')->name('verCarritoDeCompras');

    Route::get('/add/datos/producto/carrito', 'CarritoController@enviarDatosAddProductoCarrito')->name('enviarDatosAddProductoCarrito');
    Route::get('/add/datos/servicio/carrito', 'CarritoController@enviarDatosAddServicioCarrito')->name('enviarDatosAddServicioCarrito');
    Route::post('/delete/servicio/carrito', 'CarritoController@deleteServicioCarrito')->name('deleteServicioCarrito');

    //Rutas para el manejo de informacion del cliente
    Route::get('/perfil/cliente', 'UserController@getPerfil')->name('getPerfil');
    Route::post('/update/perfil/cliente', 'UserController@updatePerfil')->name('updatePerfil');
    Route::get('/cambio/password/cliente', 'UserController@editarPassword')->name('editarPassword')->middleware('password.confirm');
    Route::post('/update/password/cliente', 'UserController@updatePassword')->name('updatePassword');
    Route::get('/compras/cliente', 'ComprasController@getCompras')->name('getCompras');
    Route::get('/compra/cliente/get/info/{id_compra}', 'ComprasController@getInfoCompra')->name('getInfoCompra');

    Route::get('/realizar/compra/{id_pedido}', 'ComprasController@nuevaCompra')->name('nuevaCompra');
    
    Route::post('/nuevo/pedido', 'PedidosController@nuevoPedido')->name('nuevoPedido');
    Route::get('/seleccionar/metodo/envio/{id_pedido}', 'PedidosController@seleccionarMetodoEnvio')->name('seleccionarMetodoEnvio');
    Route::get('/confirmar/pedido/{id_pedido}/{metodo_envio?}/{costo_envio?}', 'PedidosController@confirmarPedido')->name('confirmarPedido');
});
//===================================RUTAS PARA LOS CLIENTES LOGUEADOS===================================FIN


//===================================RUTAS PARA LA ADMINISTRACION===================================INICIO
Route::middleware(['admin'])->group(function () {

    Route::get('/administracion', 'HomeAdminController@index')->name('admin.home');

    //Rutas para ver y editar terminos y condiciones
    Route::get('/administracion/ver/terminos/condiciones', 'TerminosYCondicionesController@verTerminosYCondicionesAdmin')->name('admin.verTerminosYCondiciones');
    Route::post('/administracion/add/terminos/condiciones', 'TerminosYCondicionesController@addTerminosYCondiciones')->name('admin.addTerminosYCondiciones');
    Route::get('/administracion/editar/terminos/condiciones/{id_termino}', 'TerminosYCondicionesController@editarTerminosYCondicionesView')->name('admin.editarTerminosYCondicionesView');
    Route::post('/administracion/update/terminos/condiciones', 'TerminosYCondicionesController@updateTerminosYCondiciones')->name('admin.updateTerminosYCondiciones');
    Route::post('/administracion/delete/terminos/condiciones', 'TerminosYCondicionesController@deleteTerminosYCondiciones')->name('admin.deleteTerminosYCondiciones');

    //Rutas para ver y editar quienes somos
    Route::get('/administracion/ver/quienes/somos', 'QuienesSomosController@verQuienesSomosAdmin')->name('admin.verQuienesSomos');
    Route::post('/administracion/update/quienes/somos', 'QuienesSomosController@updateQuienesSomos')->name('admin.updateQuienesSomos');

    //Rutas para editar informacion de contacto
    Route::get('/administracion/editar/contacto', 'ContactoController@editarContactoView')->name('editarContactoView');
    Route::post('/administracion/update/contacto', 'ContactoController@editarContacto')->name('editarContacto');
    
    //Rutas para ver, editar, agregar y eliminar imagenes de productos
    Route::get('/administracion/ver/imagenes/{id_producto}', 'Imagenes_productoController@verImagenesProductoView')->name('verImagenesProductoView');
    Route::post('/administracion/add/imagen', 'Imagenes_productoController@agregarImagen')->name('addImagenProducto');
    Route::get('/administracion/editar/imagen/{id_imagen}', 'Imagenes_productoController@editarImagenProductoView')->name('editarImagenProductoView');
    Route::put('/administracion/update/imagen', 'Imagenes_productoController@editarImagen')->name('updateImagenProducto');
    Route::delete('/administracion/delete/imagen', 'Imagenes_productoController@deleteImagen')->name('deleteImagenProducto');
    
    //Rutas para ver, editar y actualizar el slider
    Route::get('/administracion/ver/sliders/', 'SliderController@verSlidersView')->name('verSlidersView');
    Route::get('/administracion/editar/slider/{id_imagen}', 'SliderController@editarSliderView')->name('editarSliderView');
    Route::put('/administracion/update/slider', 'SliderController@updateSlider')->name('updateSlider');

    //Rutas para la gestion de productos
    Route::get('/administracion/ver/productos', 'ProductosController@getProductos')->name('admin.getProductos');
    Route::get('/administracion/ver/productos/deshabilitados', 'ProductosController@getProductosDeshabilitados')->name('admin.getProductosDeshabilitados');
    Route::get('/administracion/editar/producto/{id_producto}', 'ProductosController@editarProducto')->name('admin.editarProducto');
    Route::post('/administracion/update/producto', 'ProductosController@updateProducto')->name('admin.updateProducto');
    Route::post('/administracion/deshabilitar/producto', 'ProductosController@deshabilitarProducto')->name('admin.deshabilitarProducto');
    Route::post('/administracion/habilitar/producto', 'ProductosController@habilitarProducto')->name('admin.habilitarProducto');
    Route::post('/administracion/create/producto', 'ProductosController@createProducto')->name('admin.createProducto');
    Route::get('/administracion/ver/productos/buscador', 'ProductosController@buscadorAdmin')->name('admin.buscadorAdminProductos');
    Route::delete('/administracion/productos/delete', 'ProductosController@deleteProducto')->name('admin.deleteProducto');

    //Rutas para la gestion de servicios
    Route::get('/administracion/ver/servicios', 'ServiciosController@getServicios')->name('admin.getServicios');
    Route::get('/administracion/ver/servicios/deshabilitados', 'ServiciosController@getServiciosDeshabilitados')->name('admin.getServiciosDeshabilitados');
    Route::post('/administracion/deshabilitar/servicio', 'ServiciosController@deshabilitarServicio')->name('admin.deshabilitarServicio');
    Route::post('/administracion/habilitar/servicio', 'ServiciosController@habilitarServicio')->name('admin.habilitarServicio');
    Route::post('/administracion/create/servicio', 'ServiciosController@createServicio')->name('admin.createServicio');
    Route::get('/administracion/editar/servicio/{id_servicio}', 'ServiciosController@editarServicio')->name('admin.editarServicio');
    Route::post('/administracion/update/servicio', 'ServiciosController@updateServicio')->name('admin.updateServicio');
    Route::delete('/administracion/servicio/delete', 'ServiciosController@deleteServicio')->name('admin.deleteServicio');
    
    Route::get('/administracion/ver/servicios/buscador', 'ServiciosController@buscadorAdminServicios')->name('admin.buscadorAdminServicios');

    //Rutas para ver, editar, agregar y eliminar imagenes de servicios
    Route::get('/administracion/servicio/ver/imagenes/{id_servicio}', 'Imagenes_servicioController@verImagenesServicioView')->name('verImagenesServicioView');
    Route::post('/administracion/servicio/add/imagen', 'Imagenes_servicioController@agregarImagen')->name('addImagenServicio');
    Route::get('/administracion/servicio/editar/imagen/{id_imagen}', 'Imagenes_servicioController@editarImagenServicioView')->name('editarImagenServicioView');
    Route::put('/administracion/servicio/update/imagen', 'Imagenes_servicioController@editarImagen')->name('updateImagenServicio');
    Route::delete('/administracion/servicio/delete/imagen', 'Imagenes_servicioController@deleteImagen')->name('deleteImagenServicio');

    //Rutas para la gestion de categorias
    Route::get('/administracion/ver/categorias/', 'CategoriaController@verCategoriasView')->name('verCategoriasView');
    Route::post('/administracion/create/categoria', 'CategoriaController@createCategoria')->name('createCategoria');
    Route::post('/administracion/update/categoria', 'CategoriaController@updateCategoria')->name('updateCategoria');
    Route::get('/administracion/ver/productos/en/categoria/{id_categoria}', 'ProductosController@getProductosXCategoriaAdmin')->name('admin.verProductosXCategoria');
    Route::get('/administracion/ver/productos/en/subcategoria/{id_subcategoria}', 'ProductosController@getProductosXSubcategoriaAdmin')->name('admin.verProductosXSubcategoria');

    Route::get('/administracion/ver/servicios/en/categoria/{id_categoria}', 'ServiciosController@getServiciosXCategoriaAdmin')->name('admin.verServiciosXCategoria');
    Route::get('/administracion/ver/servicios/en/subcategoria/{id_subcategoria}', 'ServiciosController@getServiciosXSubcategoriaAdmin')->name('admin.verServiciosXSubcategoria');
    
    //Rutas para la gestion de subcategorias
    Route::get('/administracion/ver/subcategorias/{id_categoria}', 'SubcategoriaController@verSubcategoriasView')->name('verSubcategoriasView');
    Route::post('/administracion/create/subcategoria', 'SubcategoriaController@createSubcategoria')->name('createSubcategoria');
    Route::post('/administracion/update/subcategoria', 'SubcategoriaController@updateSubcategoria')->name('updateSubcategoria');
    //Esta ruta se usa con AJAX cuando se va a crear un producto y se selecciona una categoria, entonces se consultan las subcategorias.
    Route::get('/administracion/get/subcategorias/{id_categoria}', 'SubcategoriaController@getSubcategorias')->name('getSubcategorias');
    
    //Rutas para la gestion de clientes/usuarios
    Route::get('/administracion/ver/usuarios', 'UserController@getUsuarios')->name('getUsuarios');
    Route::get('/administracion/ver/usuarios/eliminados', 'UserController@getUsuariosEliminados')->name('getUsuariosEliminados');
    Route::get('/administracion/ver/usuarios/deshabilitados', 'UserController@getUsuariosDeshabilitados')->name('getUsuariosDeshabilitados');
    Route::post('/administracion/delete/usuario', 'UserController@deleteUsuario')->name('deleteUsuario');
    Route::post('/administracion/deshabilitar/usuario', 'UserController@deshabilitarUsuario')->name('deshabilitarUsuario');
    Route::post('/administracion/restaurar/usuario', 'UserController@restaurarUsuario')->name('restaurarUsuario');
    Route::post('/administracion/habilitar/usuario', 'UserController@habilitarUsuario')->name('habilitarUsuario');
    Route::get('/administracion/ver/usuarios/buscador', 'UserController@buscadorUsuario')->name('admin.buscadorUsuario');
    Route::get('/administracion/ver/info/usuario', 'UserController@verInfoUsuario')->name('admin.users.info.ver');
    Route::post('/administracion/actualizar/info/usuario', 'UserController@updateInfoUsuario')->name('admin.users.info.save');

    //Rutas para la gestion de las compras
    Route::get('/administracion/ver/compras/productos', 'ComprasController@verComprasProductos')->name('admin.verComprasProductos');
    Route::get('/administracion/ver/compras/servicios', 'ComprasController@verComprasServicios')->name('admin.verComprasServicios');
    Route::get('/administracion/ver/compras/usuario/{id_usuario}', 'ComprasController@getComprasXCliente')->name('admin.getComprasXCliente');
    Route::post('/administracion/update/compra', 'ComprasController@updateCompra')->name('updateCompra');
    Route::get('/administracion/ver/compra/{id_compra}', 'ComprasController@verCompra')->name('admin.verCompra');
    Route::get('/administracion/ver/compras/buscador', 'ComprasController@buscadorCompras')->name('admin.buscadorCompras');

    
    //Rutas para la gestion de las variantes de los productos
    Route::get('/administracion/ver/variantes/{id_producto}', 'Variantes_ProductoController@getVariantesProducto')->name('getVariantesProducto');
    Route::post('/administracion/create/variante', 'Variantes_ProductoController@createVarianteProducto')->name('createVarianteProducto');
    Route::post('/administracion/edit/variante', 'Variantes_ProductoController@editVarianteProducto')->name('editVarianteProducto');
    
    //Rutas para ver y editar los datos del administrador
    Route::get('/administracion/editar/datos', 'AdminController@editarDatos')->name('admin.editarDatos');
    Route::post('/administracion/update/datos', 'AdminController@updateDatos')->name('admin.updateDatos');
    Route::post('/administracion/validar/password', 'AdminController@validarPassword')->name('admin.validarPassword');
    
    //Rutas para ver y editar las preguntas frecuentes
    Route::get('/administracion/ver/preguntas/frecuentes', 'PreguntasFrecuentesController@getPreguntasFrecuentes')->name('admin.getPreguntasFrecuentes');
    Route::get('/administracion/editar/pregunta/frecuente/{id_pregunta}', 'PreguntasFrecuentesController@editarPreguntaFrecuente')->name('admin.editarPreguntaFrecuente');
    Route::post('/administracion/create/pregunta/frecuente', 'PreguntasFrecuentesController@createPreguntaFrecuente')->name('admin.createPreguntaFrecuente');
    Route::post('/administracion/update/pregunta/frecuente', 'PreguntasFrecuentesController@updatePreguntaFrecuente')->name('admin.updatePreguntaFrecuente');
    Route::post('/administracion/delete/pregunta/frecuente', 'PreguntasFrecuentesController@deletePreguntaFrecuente')->name('admin.deletePreguntaFrecuente');
});
//===================================RUTAS PARA LA ADMINISTRACION===================================FIN

use Illuminate\Support\Facades\Artisan;
Route::get('storageLink', function () {
    echo env('APP_URL').'mercado_fishing/storage';
    // Artisan::call('storage:link');
});