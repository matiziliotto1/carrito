<?php

use App\Localidad;
use App\Provincia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

Route::post('/notifications/mercadopago/webhook', 'MercadoPagoController@webhook')->name('webhook');

Route::get('/get/provincias', function(){
    $provincias = Provincia::all();
    return response()->json(['success'=>true , 'provincias'=>$provincias]);
})->name('getProvincias');

Route::get('/get/localidades/{id_provincia}', function($id_provincia){
    $localidades = Localidad::where('id_provincia', $id_provincia)->orderby('nombre', 'ASC')->get();
    return response()->json(['success'=>true , 'localidades'=>$localidades]);
})->name('getLocalidades');