@php
    //$menues = App\Menu::where('etiqueta',"!=",'Categorias')->get();
    $menues = App\Menu::orderBy('orden', 'ASC')->get();
    $categorias_producto = App\Categoria::where([['habilitado',1],['tipo',1]])->get();
    $categorias_servicio = App\Categoria::where([['habilitado',1],['tipo',0]])->get();
    
    foreach($categorias_producto as $categoria_producto){
        $subcategorias_producto = App\Subcategoria::where([['id_categoria',$categoria_producto->id_categoria],['habilitado',1]])->get();
        $categoria_producto->subcategorias = $subcategorias_producto;
    }

    foreach($categorias_servicio as $categoria_servicio){
        $subcategorias_servicio = App\Subcategoria::where([['id_categoria',$categoria_servicio->id_categoria],['habilitado',1]])->get();
        $categoria_servicio->subcategorias = $subcategorias_servicio;
    }

    $contacto = App\Contacto::first();
    
    if(Auth::check()){
        $id_carrito = App\Carrito_compra::where('id_usuario',Auth::user()->id)->first()->id_carrito;

        $productos_en_carrito = DB::table('productos')
            ->join('variantes_producto', 'variantes_producto.id_producto', '=', 'productos.id_producto')
            ->join('productos_x_carritos', 'productos_x_carritos.id_variante_producto', '=', 'variantes_producto.id_variante_producto')
            ->where('productos_x_carritos.id_carrito',$id_carrito)
            ->select('productos.id_producto', 'productos.nombre', 'productos_x_carritos.cantidad','productos.precio', 'variantes_producto.id_variante_producto')
            ->get();

        $servicios_en_carrito = DB::table('servicios')
            ->join('servicios_x_carritos', 'servicios_x_carritos.id_servicio', '=', 'servicios.id_servicio')
            ->where('servicios_x_carritos.id_carrito',$id_carrito)
            ->select('servicios.id_servicio', 'servicios.titulo', 'servicios.precio')
            ->get();

        foreach($productos_en_carrito as $producto){
            if (App\Imagenes_producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = App\Imagenes_Producto::where('id_producto', $producto->id_producto)->first()->url;
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
        }

        foreach($servicios_en_carrito as $servicio){
            if (App\Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                $servicio->imagen = App\Imagenes_Producto::where('id_servicio', $servicio->id_servicio)->first()->url;
            } else {
                $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
            }
        }

        //Guardo la cantidad de productos en el carrito
        $cantidad_carrito = count(App\Productos_x_carrito::where('id_carrito',$id_carrito)->get())+count(App\Servicios_x_carrito::where('id_carrito',$id_carrito)->get());
    }
@endphp

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Mercado Fishing
        </title>
        <!-- SEO Meta Tags-->
        <meta name="description" content="Unishop - Universal E-Commerce Template">
        <meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
        <meta name="author" content="Rokaux">
        <!-- Mobile Specific Meta Tag-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
        <link rel="stylesheet" media="screen" href="{{ asset('css/vendor.min.css') }}">
        <!-- Main Template Styles-->
        <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('css/styles.min.css') }}">
        <!-- Customizer Styles-->
        <link rel="stylesheet" media="screen" href="{{ asset('customizer/customizer.min.css') }}">

        <link rel="icon" href="{{ asset('img/logo/LogoIcono.png') }}">
        <!-- Google Tag Manager-->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-T4DJFPZ');
        </script>
        <!-- Modernizr-->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    </head>
    <body>
        <!-- Google Tag Manager (noscript)-->
        <noscript>
            <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;"></iframe>
        </noscript>
        
        <!-- Header-->
        <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
        <header class="site-header navbar-sticky">
            <!-- Topbar-->
            <div class="topbar d-flex justify-content-between" id="topbar">
                <!-- Logo-->
                <div class="site-branding d-flex">
                    <a class="site-logo align-self-center" href="{{route('home')}}">
                        <img src="{{ asset('img/logo/LogoBig.png') }}" alt="MercadoFishing">
                    </a>
                </div>
                <!-- Search / Categories-->
                <div class="search-box-wrap d-flex">
                    <div class="search-box-inner align-self-center">
                        <div class="search-box d-flex">
                            <form class="input-group" method="get" action="@if(Route::current()->getName()=='buscadorServicios'){{route('buscadorServicios')}}@else{{route('buscadorProductos')}}@endif" id="formBuscador1">
                                <span class="input-group-btn">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="customSwitches_1" @if(Route::current()->getName()=='buscadorServicios' || Route::current()->getName()=='filtrarServicios') checked @endif>
                                        <label class="custom-control-label" for="customSwitches_1" id="label_switch_1">@if(Route::current()->getName()=='buscadorServicios' || Route::current()->getName()=='filtrarServicios') Servicios @else Productos @endif</label>
                                        <button type="submit"><i class="icon-search"></i></button>
                                    </div>
                                </span>
                                <input class="form-control buscador-menu" type="text" name="texto" id="texto1" placeholder="Buscar..." value="@if(request()->get('texto')){{request()->get('texto')}}@endif">
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Toolbar-->
                <div class="toolbar d-flex">
                    <div class="toolbar-item visible-on-mobile mobile-menu-toggle">
                        <a href="#">
                            <div>
                                <i class="icon-menu"></i>
                                <span class="text-label">Menu</span>
                            </div>
                        </a>
                    </div>
                    <div class="toolbar-item hidden-on-mobile">
                        <a href="@guest {{route("login")}} @else {{route("getPerfil")}} @endguest">
                            <div>
                                <i class="icon-user"></i><span class="text-label">@guest Ingresá @else {{ Auth::user()->nombre }} @endguest</span>
                            </div>
                        </a>
                        @guest
                            <div class="toolbar-dropdown text-center px-3">
                                <a class="link-no-style" href="{{route("login")}}">
                                    Ingresá
                                </a>
                                <hr class="mt-1 mb-1">
                                <a class="link-no-style" href="{{route("register")}}">
                                    Registrate
                                </a>
                            </div>
                        @else
                            <div class="toolbar-dropdown text-center px-3">
                                <a class="link-no-style" href="{{route("getPerfil")}}">
                                    Mi perfil
                                </a>
                                <hr class="mt-1 mb-1">
                                <a class="link-no-style" href="{{route("getCompras")}}">
                                    Mis compras
                                </a>
                                <hr class="mt-1 mb-1">
                                <a class="link-no-style" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Salir
                                </a>

                                <form id="logout-form2" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        @endguest
                        
                    </div>
                    <div class="toolbar-item">
                        <a href="{{route("verCarritoDeCompras")}}">
                            <div>
                                <span class="cart-icon">
                                    <i class="icon-shopping-cart"></i>
                                    @if(Auth::check())
                                        <span class="count-label">{{$cantidad_carrito}}</span>
                                    @endif
                                </span>
                                <span class="text-label">Ver carrito</span>
                            </div>
                        </a>
                        @if(Auth::check())
                            <div class="toolbar-dropdown cart-dropdown widget-cart hidden-on-mobile">
                                @if(count($productos_en_carrito) > 0 )
                                    @foreach($productos_en_carrito as $producto)
                                        <!-- Entry-->
                                        <div class="entry">
                                            <div class="entry-thumb">
                                                <a href="{{route('verProductoC',$producto->id_producto)}}">
                                                    <img src="{{asset($producto->imagen)}}" alt="Producto">
                                                </a>
                                            </div>
                                            <div class="entry-content">
                                                <h4 class="entry-title"><a href="{{route('verProductoC',$producto->id_producto)}}">{{$producto->nombre}}</a></h4>
                                                <span class="entry-meta">{{$producto->cantidad}} x ${{number_format($producto->precio, 2, ",", ".")}}</span>
                                            </div>
                                            <div class="entry-delete">
                                                <a class="link-no-style" href="{{route('deleteProductoCarrito')}}" style="color:red" title="Eliminar del carrito"
                                                onclick="event.preventDefault();
                                                document.getElementById('deleteProductoCarrito{{$producto->id_producto}}').submit();">
                                                    <i class="icon-x"></i>
                                                </a>
                                                <form id="deleteProductoCarrito{{$producto->id_producto}}" action="{{ route('deleteProductoCarrito') }}" method="POST" style="display: none;">
                                                    @csrf
                                                    <input type="hidden" name="id_variante_producto" value="{{$producto->id_variante_producto}}">
                                                </form>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @if(count($servicios_en_carrito) > 0 )
                                    @foreach($servicios_en_carrito as $servicio)
                                        <!-- Entry-->
                                        <div class="entry">
                                            <div class="entry-thumb">
                                                <a href="{{route('verServicioC',$servicio->id_servicio)}}">
                                                    <img src="{{asset($servicio->imagen)}}" alt="Servicio">
                                                </a>
                                            </div>
                                            <div class="entry-content">
                                                <h4 class="entry-title"><a href="{{route('verServicioC',$servicio->id_servicio)}}">{{$servicio->titulo}}</a></h4>
                                                <span class="entry-meta">${{number_format($servicio->precio, 2, ",", ".")}}</span>
                                            </div>
                                            <div class="entry-delete">
                                                <a class="link-no-style" href="{{route('deleteServicioCarrito')}}" style="color:red" title="Eliminar del carrito"
                                                onclick="event.preventDefault();
                                                document.getElementById('deleteServicioCarrito{{$servicio->id_servicio}}').submit();">
                                                    <i class="icon-x"></i>
                                                </a>
                                                <form id="deleteServicioCarrito{{$servicio->id_servicio}}" action="{{ route('deleteServicioCarrito') }}" method="POST" style="display: none;">
                                                    @csrf
                                                    <input type="hidden" name="id_servicio" value="{{$servicio->id_servicio}}">
                                                </form>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                @if (count($productos_en_carrito) > 0 || count($servicios_en_carrito) > 0)
                                    <div class="d-flex">
                                        <div class="w-100">
                                            <a class="btn btn-primary btn-sm btn-block mb-0" href="{{route("verCarritoDeCompras")}}">Ver carrito</a>
                                        </div>
                                    </div>
                                @else
                                    <p class="text-xs p-0 m-0">Aún no agregaste ningun producto al carrito de compra. Mira nuestros <a class="navi-link" href="{{route("getProductosC")}}">productos</a></p>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>

                <!-- Mobile Menu-->
                <div class="mobile-menu">
                    <!-- Search Box-->
                    <div class="mobile-search">
                        <form class="input-group" method="get" action="@if(Route::current()->getName()=='buscadorServicios'){{route('buscadorServicios')}}@else{{route('buscadorProductos')}}@endif" id="formBuscador2">
                            <span class="input-group-btn">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" id="customSwitches_2" @if(Route::current()->getName()=='buscadorServicios') checked @endif>
                                    <label class="custom-control-label" for="customSwitches_2" id="label_switch_2">@if(Route::current()->getName()=='buscadorServicios') Servicios @else Productos @endif</label>
                                    <button type="submit"><i class="icon-search"></i></button>
                                </div>
                            </span>
                            <input class="form-control buscador-menu" type="text" name="texto" id="texto2" placeholder="Buscar..." value="@if(request()->get('texto')){{request()->get('texto')}}@endif">
                        </form>
                    </div>
                    <!-- Toolbar-->
                    <div class="toolbar">
                        <nav class="slideable-menu">
                            <ul class="menu" data-initial-height="385">
                                @guest
                                    <li class="has-children">
                                        <span>
                                            <a href="{{route('login')}}">Ingresá</a>
                                        </span>
                                    </li>
                                    <li class="has-children">
                                        <span>
                                            <a href="{{route('register')}}">Registrate</a>
                                        </span>
                                    </li>
                                @else
                                    <li class="has-children">
                                        <span>
                                            <a href="{{route('getPerfil')}}">Mi perfil</a>
                                        </span>
                                    </li>
                                    <li class="has-children">
                                        <span>
                                            <a href="{{route('getCompras')}}">Mis compras</a>
                                        </span>
                                    </li>
                                    <li class="has-children">
                                        <span>
                                            <a href="{{route('logout')}}" 
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form1').submit();">
                                                Salir
                                            </a>
                                            <form id="logout-form1" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </span>    
                                    
                                    </li>
                                @endguest
                            </ul>
                        </nav>
                    </div>
                    <!-- Slideable (Mobile) Menu-->
                    <nav class="slideable-menu">
                        <ul class="menu" data-initial-height="385">
                            <li class="@if(count($categorias_producto) > 0) has-children @endif @if(Route::current()->getName()== 'verProductosXSubcategoria' || Route::current()->getName()== 'verProductosXCategoria' || Route::current()->getName()== 'buscadorProductos') active @endif">
                                <span>
                                    <a style="cursor: default">Productos</a>
                                    <span class="sub-menu-toggle"></span>
                                </span>
                                <ul class="slideable-submenu">
                                    @foreach($categorias_producto as $categoria)
                                        <li class="@if(Request::route('id_categoria') == $categoria->id_categoria) active @endif @if(count($categoria->subcategorias) > 0) has-children @endif">
                                            <span>
                                                <a href="{{route('verProductosXCategoria',$categoria->id_categoria)}}">
                                                    {{$categoria->nombre_categoria}}
                                                </a>
                                                @if(count($categoria->subcategorias) > 0)
                                                    <span class="sub-menu-toggle"></span>
                                                @endif
                                            </span>

                                            @if(count($categoria->subcategorias) > 0)
                                                <ul class="slideable-submenu">
                                                    @foreach($categoria->subcategorias as $subcategoria)
                                                        <li>
                                                            <a href="{{route('verProductosXSubcategoria', $subcategoria->id_subcategoria)}}">
                                                                {{$subcategoria->nombre_subcategoria}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="@if(count($categorias_servicio) > 0) has-children @endif @if(Route::current()->getName()== 'verServiciosXSubcategoria' || Route::current()->getName()== 'verServiciosXCategoria' || Route::current()->getName()== 'buscadorServicios') active @endif">
                                <span>
                                    <a style="cursor: default">Servicios</a>
                                    <span class="sub-menu-toggle"></span>
                                </span>
                                <ul class="slideable-submenu">
                                    @foreach($categorias_servicio as $categoria)
                                        <li class="@if(Request::route('id_categoria') == $categoria->id_categoria) active @endif @if(count($categoria->subcategorias) > 0) has-children @endif">
                                            <span>
                                                <a href="{{route('verServiciosXCategoria',$categoria->id_categoria)}}">
                                                    {{$categoria->nombre_categoria}}
                                                </a>
                                                @if(count($categoria->subcategorias) > 0)
                                                    <span class="sub-menu-toggle"></span>
                                                @endif
                                            </span>

                                            @if(count($categoria->subcategorias) > 0)
                                                <ul class="slideable-submenu">
                                                    @foreach($categoria->subcategorias as $subcategoria)
                                                        <li>
                                                            <a href="{{route('verServiciosXSubcategoria', $subcategoria->id_subcategoria)}}">
                                                                {{$subcategoria->nombre_subcategoria}}
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            @foreach($menues as $menu)
                                <li class="@if(Route::current()->getName()==$menu->link) active @endif">
                                    <span>
                                        <a href="{{route($menu->link)}}">{{$menu->etiqueta}}</a>
                                    </span>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </div>
            </div>
            <!-- Navbar-->
            <div class="navbar">

                <!-- Main Navigation-->
                <nav class="site-menu text-center" id="nav-navbar">
                    <!-- Logo-->
                    <div id="logo-menu-fijo" class="d-none site-branding float-left" style="padding: 11;">
                        <a class="site-logo align-self-center" href="{{route('home')}}">
                            <img src="{{ asset('img/logo/LogoSmall.png') }}" alt="MercadoFishing">
                        </a>
                    </div>
                    <ul id="submenu-fijo">
                        <li class="d-none pt-2" id="buscador-submenu-fijo">
                            <form class="input-group" method="get" action="@if(Route::current()->getName()=='buscadorServicios'){{route('buscadorServicios')}}@else{{route('buscadorProductos')}}@endif" id="formBuscador3">
                                <span class="input-group-btn">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" id="customSwitches_3" @if(Route::current()->getName()=='buscadorServicios') checked @endif>
                                        <label class="custom-control-label" for="customSwitches_3" id="label_switch_3">@if(Route::current()->getName()=='buscadorServicios') Servicios @else Productos @endif</label>
                                        <button type="submit"><i class="icon-search"></i></button>
                                    </div>
                                </span>
                                <input class="form-control buscador-menu" type="text" name="texto" id="texto3" placeholder="Buscar..." value="@if(request()->get('texto')){{request()->get('texto')}}@endif">
                            </form>
                        </li>
                        <li class="has-submenu @if(Route::current()->getName()== 'verProductosXSubcategoria' || Route::current()->getName()== 'verProductosXCategoria' || Route::current()->getName()== 'buscadorProductos') active @endif">
                            <a style="cursor: default">Productos</a>
                            <ul class="sub-menu">
                                @foreach($categorias_producto as $categoria)
                                    <li class="@if(count($categoria->subcategorias) > 0) has-children @endif">
                                        <a href="{{route('verProductosXCategoria', $categoria->id_categoria)}}">
                                            {{$categoria->nombre_categoria}}
                                        </a>
                                        <ul class="sub-menu w-400 p-0 overflow-hidden">

                                            @foreach($categoria->subcategorias as $subcategoria)
                                                <li class="has-submenu">
                                                    <a href="{{route('verProductosXSubcategoria', $subcategoria->id_subcategoria)}}">
                                                        {{$subcategoria->nombre_subcategoria}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="has-submenu @if(Route::current()->getName()== 'verServiciosXSubcategoria' || Route::current()->getName()== 'verServiciosXCategoria' || Route::current()->getName()== 'buscadorServicios') active @endif">
                            <a style="cursor: default">Servicios</a>
                            <ul class="sub-menu">
                                @foreach($categorias_servicio as $categoria)
                                    <li class="@if(count($categoria->subcategorias) > 0) has-children @endif">
                                        <a href="{{route('verServiciosXCategoria', $categoria->id_categoria)}}">
                                            {{$categoria->nombre_categoria}}
                                        </a>
                                        <ul class="sub-menu w-400 p-0 overflow-hidden">

                                            @foreach($categoria->subcategorias as $subcategoria)
                                                <li class="has-submenu">
                                                    <a href="{{route('verServiciosXSubcategoria', $subcategoria->id_subcategoria)}}">
                                                        {{$subcategoria->nombre_subcategoria}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                        @foreach($menues as $menu)
                            <li class="has-submenu @if(Route::current()->getName()==$menu->link) active @endif">
                                <a href="{{route($menu->link)}}">{{$menu->etiqueta}}</a>
                            </li>
                        @endforeach
                    </ul>
                    <!-- Toolbar ( Put toolbar here only if you enable sticky navbar )-->
                    <div class="toolbar float-right">
                        <div class="toolbar-inner">
                            <div class="toolbar-item hidden-on-mobile">
                                <a href="@guest {{route("login")}} @else {{route("getPerfil")}} @endguest">
                                    <div>
                                        <i class="icon-user"></i><span class="text-label">@guest Ingresá @else {{ Auth::user()->nombre }} @endguest</span>
                                    </div>
                                </a>
                                @guest
                                    <div class="toolbar-dropdown text-center px-3">
                                        <a class="link-no-style" href="{{route("login")}}">
                                            Ingresá
                                        </a>
                                        <hr class="mt-1 mb-1">
                                        <a class="link-no-style" href="{{route("register")}}">
                                            Registrate
                                        </a>
                                    </div>
                                @else
                                    <div class="toolbar-dropdown text-center px-3">
                                        <a class="link-no-style" href="{{route("getPerfil")}}">
                                            Mi perfil
                                        </a>
                                        <hr class="mt-1 mb-1">
                                        <a class="link-no-style" href="{{route("getCompras")}}">
                                            Mis compras
                                        </a>
                                        <hr class="mt-1 mb-1">
                                        <a class="link-no-style" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>
        
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                @endguest
                                
                            </div>
                            <div class="toolbar-item">
                                <a href="{{route("verCarritoDeCompras")}}">
                                    <div>
                                        <span class="cart-icon">
                                            <i class="icon-shopping-cart"></i>
                                            @if(Auth::check())
                                                <span class="count-label">{{$cantidad_carrito}}</span>
                                            @endif
                                        </span>
                                        <span class="text-label">Ver carrito</span>
                                    </div>
                                </a>
                                @if(Auth::check())
                                    <div class="toolbar-dropdown cart-dropdown widget-cart">
                                        @if(count($productos_en_carrito) > 0 )
                                            @foreach($productos_en_carrito as $producto)
                                                <!-- Entry-->
                                                <div class="entry">
                                                    <div class="entry-thumb">
                                                        <a href="{{route('verProductoC',$producto->id_producto)}}">
                                                            <img src="{{asset($producto->imagen)}}" alt="Producto">
                                                        </a>
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4 class="entry-title"><a href="{{route('verProductoC',$producto->id_producto)}}">{{$producto->nombre}}</a></h4>
                                                        <span class="entry-meta">{{$producto->cantidad}} x ${{number_format($producto->precio, 2, ",", ".")}}</span>
                                                    </div>
                                                    <div class="entry-delete">
                                                        <a class="link-no-style" href="{{route('deleteProductoCarrito')}}" style="color:red" title="Eliminar del carrito"
                                                        onclick="event.preventDefault();
                                                        document.getElementById('deleteProductoCarrito{{$producto->id_producto}}').submit();">
                                                            <i class="icon-x"></i>
                                                        </a>
                                                        <form id="deleteProductoCarrito{{$producto->id_producto}}" action="{{ route('deleteProductoCarrito') }}" method="POST" style="display: none;">
                                                            @csrf
                                                            <input type="hidden" name="id_variante_producto" value="{{$producto->id_variante_producto}}">
                                                        </form>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        @if(count($servicios_en_carrito) > 0 )
                                            @foreach($servicios_en_carrito as $servicio)
                                                <!-- Entry-->
                                                <div class="entry">
                                                    <div class="entry-thumb">
                                                        <a href="{{route('verServicioC',$servicio->id_servicio)}}">
                                                            <img src="{{asset($servicio->imagen)}}" alt="Servicio">
                                                        </a>
                                                    </div>
                                                    <div class="entry-content">
                                                        <h4 class="entry-title"><a href="{{route('verServicioC',$servicio->id_servicio)}}">{{$servicio->titulo}}</a></h4>
                                                        <span class="entry-meta">${{number_format($servicio->precio, 2, ",", ".")}}</span>
                                                    </div>
                                                    <div class="entry-delete">
                                                        <a class="link-no-style" href="{{route('deleteServicioCarrito')}}" style="color:red" title="Eliminar del carrito"
                                                        onclick="event.preventDefault();
                                                        document.getElementById('deleteServicioCarrito{{$servicio->id_servicio}}').submit();">
                                                            <i class="icon-x"></i>
                                                        </a>
                                                        <form id="deleteServicioCarrito{{$servicio->id_servicio}}" action="{{ route('deleteServicioCarrito') }}" method="POST" style="display: none;">
                                                            @csrf
                                                            <input type="hidden" name="id_servicio" value="{{$servicio->id_servicio}}">
                                                        </form>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                        @if (count($productos_en_carrito) > 0 || count($servicios_en_carrito) > 0)
                                            <div class="d-flex">
                                                <div class="w-100">
                                                    <a class="btn btn-primary btn-sm btn-block mb-0" href="{{route("verCarritoDeCompras")}}">Ver carrito</a>
                                                </div>
                                            </div>
                                        @else
                                            <p class="text-xs p-0 m-0">Aún no agregaste ningun producto al carrito de compra. Mira nuestros <a class="navi-link" href="{{route("getProductosC")}}">productos</a></p>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
        
        <main class="py-4">
            @yield('content')
        </main>

        <!-- Site Footer-->
        <footer class="site-footer" style="background-image: url(img/footer-bg.png);">
            <div class="container">
                <hr class="hr-light margin-bottom-2x">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <!-- Contact Info-->
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">Contacto</h3>
                            <ul class="list-unstyled text-sm text-white">
                                @if(!is_null($contacto->telefono_1))
                                    <li>
                                        <span class="opacity-50">Telefono de atención al cliente:&nbsp;</span><i class="fa fa-whatsapp" style="font-size: 1.2rem !important"></i>&nbsp;{{$contacto->telefono_1}}
                                    </li> 
                                @endif

                                @if(!is_null($contacto->telefono_2))
                                    <li>
                                        <span class="opacity-50">Telefono de atención al cliente:&nbsp;</span>{{$contacto->telefono_2}}
                                    </li>
                                @endif

                                @if(!is_null($contacto->telefono_3))
                                    <li>
                                        <span class="opacity-50">Telefono de atención al cliente:&nbsp;</span>{{$contacto->telefono_3}}
                                    </li>
                                @endif
                            </ul>

                            <ul class="list-unstyled text-sm text-white">
                                @if(!is_null($contacto->email_info))
                                    <li>
                                        <span class="opacity-50">Consultas:&nbsp;</span>{{$contacto->email_info}}
                                    </li>
                                @endif
                                @if(!is_null($contacto->email_reclamos))
                                    <li>
                                        <span class="opacity-50">Reclamos:&nbsp;</span>{{$contacto->email_reclamos}}
                                    </li>
                                @endif
                                @if(!is_null($contacto->email_comprobantes))
                                    <li>
                                        <span class="opacity-50">Comprobantes:&nbsp;</span>{{$contacto->email_comprobantes}}
                                    </li>
                                @endif
                            </ul>

                            <a class="social-button shape-circle sb-facebook sb-light-skin" href="{{$contacto->facebook}}">
                                <i class="socicon-facebook"></i>
                            </a>
                            <a class="social-button shape-circle sb-twitter sb-light-skin" href="{{$contacto->twitter}}">
                                <i class="socicon-twitter"></i>
                            </a>
                            <a class="social-button shape-circle sb-instagram sb-light-skin" href="{{$contacto->instagram}}">
                                <i class="socicon-instagram"></i>
                            </a>
                            <a class="social-button shape-circle sb-google-plus sb-light-skin" href="{{$contacto->youtube}}">
                                <i class="socicon-youtube"></i>
                            </a>
                        </section>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <!-- Mobile App Buttons-->
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">Ayuda</h3>
                            <ul class="list-unstyled text-lg text-white">
                                <li><a class="navi-link-light" href="{{route('ayuda')}}">¿Cómo comprar?</a></li>
                                <li><a class="navi-link-light" href="{{route('ayuda')}}">Métodos de envío</a></li>
                                <li><a class="navi-link-light" href="{{route('ayuda')}}">¿Quiénes somos?</a></li>
                            </ul>
                        </section>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <!-- Mobile App Buttons-->
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">Terminos y Condiciones</h3>
                            <ul class="list-unstyled text-lg text-white">
                                <li><a class="navi-link-light" href="{{route('terminosYCondiciones')}}">Ver Terminos y Condiciones</a></li>
                            </ul>
                        </section>
                    </div>
                </div>

                <!-- Copyright-->
                <p class="footer-copyright">
                    © MERCADO FISHING 2020
                </p>
            </div>
        </footer>

        <!-- Back To Top Button--><a class="scroll-to-top-btn" href="index.html#"><i class="icon-chevron-up"></i></a>
        <!-- Backdrop-->
        <div class="site-backdrop"></div>
        <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
        <script src="{{ asset('js/vendor.min.js') }}"></script>
        <script src="{{ asset('js/scripts.min.js') }}"></script>
        <!-- Customizer scripts-->
        <script src="{{ asset('customizer/customizer.min.js') }}"></script>

        <script type="text/javascript">
        $( document ).ready(function() {
            $( document ).scroll(function(){
                //Si scrolleo hacia abajo y oculto el menu principal, muestro el logo y buscadores del submenu que estan ocultos
                if($(this).scrollTop() >= 100) {
                    $('#submenu-fijo').attr('style', 'margin: 0');
                    //Si es una pantalla chiquita, pero no de celular, pongo para que el menu no se alinee al centro y le saco un poco de padding.
                    if(window.matchMedia("(min-width:1041px) and (max-width: 1090px)").matches){
                        $('#submenu-fijo > li > a').css("padding","20px 5px");
                        $("#buscador-submenu-fijo").css("padding-left","20px");
                        $("#formBuscador3").css("width","280px");
                    }
                    else if (window.matchMedia("(min-width:1091px) and (max-width: 1160px)").matches){
                        $('#submenu-fijo > li > a').css("padding","20px 10px");
                        $("#buscador-submenu-fijo").css("padding-left","20px");
                        $("#formBuscador3").css("width","280px");
                    }
                    else if (window.matchMedia("(min-width:1161px) and (max-width: 1235px)").matches){
                        $('#submenu-fijo > li > a').css("padding","20px 15px");
                        $("#buscador-submenu-fijo").css("padding-left","20px");
                        $("#formBuscador3").css("width","280px");
                    }
                    else if (window.matchMedia("(min-width:1235px)").matches){
                        $('#submenu-fijo > li > a').css("padding","20px 20px");
                        $("#buscador-submenu-fijo").css("padding-left","20px");
                        $("#formBuscador3").css("width","300px");
                    }

                    $('#buscador-submenu-fijo').css("margin-right","10px");

                    $("#logo-menu-fijo").removeClass("d-none");
                    $("#logo-menu-fijo").addClass("d-flex");

                    $("#buscador-submenu-fijo").removeClass("d-none");
                    $("#buscador-submenu-fijo").addClass("d-flex");

                    $('#nav-navbar').attr('style', 'background-color: #FFED00 !important');
                }
                else{
                    //Vuelvo a setear el submenu al centro
                    $('#submenu-fijo').attr('style', 'margin: auto');
                    $('#submenu-fijo > li > a').css("padding","20px 18px");
                    $("#buscador-submenu-fijo").css("padding-left","0px");
                    
                    $("#logo-menu-fijo").removeClass("d-flex");
                    $("#logo-menu-fijo").addClass("d-none");

                    $("#buscador-submenu-fijo").removeClass("d-flex");
                    $("#buscador-submenu-fijo").addClass("d-none");

                    $("#nav-navbar").removeAttr("style");
                }
            });
        });

        function checkboxsServicios() {
            $("#customSwitches_1").prop('checked', true);
            $("#label_switch_1").text("Servicios");
            $("#customSwitches_2").prop('checked', true);
            $("#label_switch_2").text("Servicios");
            $("#customSwitches_3").prop('checked', true);
            $("#label_switch_3").text("Servicios");
        }
        function checkboxsProductos() {
            $("#customSwitches_1").prop('checked', false);
            $("#label_switch_1").text("Productos");
            $("#customSwitches_2").prop('checked', false);
            $("#label_switch_2").text("Productos");
            $("#customSwitches_3").prop('checked', false);
            $("#label_switch_3").text("Productos");
        }
        function setFormBuscadorAction(route){
            switch (route) {
                case 1:
                    $('#formBuscador1').attr('action', "{{route('buscadorProductos')}}");
                    $('#formBuscador2').attr('action', "{{route('buscadorProductos')}}");
                    $('#formBuscador3').attr('action', "{{route('buscadorProductos')}}");
                    break;
                case 2:
                    $('#formBuscador1').attr('action', "{{route('buscadorServicios')}}");
                    $('#formBuscador2').attr('action', "{{route('buscadorServicios')}}");
                    $('#formBuscador3').attr('action', "{{route('buscadorServicios')}}");
                    break;
                default:
                    $('#formBuscador1').attr('action', "{{route('buscadorProductos')}}");
                    $('#formBuscador2').attr('action', "{{route('buscadorProductos')}}");
                    $('#formBuscador3').attr('action', "{{route('buscadorProductos')}}");
                    break;
            }
        }
        $("#customSwitches_1").on('change', function(){
            if($(this).prop('checked')){
                checkboxsServicios();
                setFormBuscadorAction(2);
            }
            if(!$(this).prop('checked')){
                checkboxsProductos();
                setFormBuscadorAction(1);
            }
        });
        $("#customSwitches_2").on('change', function(){
            if($(this).prop('checked')){
                checkboxsServicios();
                setFormBuscadorAction(2);
            }
            if(!$(this).prop('checked')){
                checkboxsProductos();
                setFormBuscadorAction(1);
            }
        });
        $("#customSwitches_3").on('change', function(){
            if($(this).prop('checked')){
                checkboxsServicios();
                setFormBuscadorAction(2);
            }
            if(!$(this).prop('checked')){
                checkboxsProductos();
                setFormBuscadorAction(1);
            }
        });
        $(".alert").on("close.bs.alert", function () {
            $("#alert").addClass('d-none');
            return false;
        });
        </script>
        @yield('scripts')
    </body>
</html>