<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('title') | Mercado Fishing</title>
        <!-- SEO Meta Tags-->
        <meta name="description" content="Unishop - Universal E-Commerce Template">
        <meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
        <meta name="author" content="Rokaux">
        <!-- Mobile Specific Meta Tag-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Favicon and Apple Icons-->
        <link rel="icon" href="{{ asset('img/logo/LogoIcono.png') }}">
        <link rel="apple-touch-icon" href="touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="180x180" href="touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="167x167" href="touch-icon-ipad-retina.png">
        <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
        <link rel="stylesheet" media="screen" href="{{ asset('css/vendor.min.css') }}">
        <!-- Main Template Styles-->
        <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('css/styles.min.css') }}">
        <!-- Customizer Styles-->
        <link rel="stylesheet" media="screen" href="{{ asset('customizer/customizer.min.css') }}">
        <!-- Google Tag Manager-->
        <script>
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-T4DJFPZ');
          
        </script>
        <!-- Modernizr-->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>
    </head>
    <body style='background-image: url("{{ asset('img/fondos/FondoOlas.png') }}");background-repeat: no-repeat;background-attachment: fixed;background-size:100% 100%;'>
        <main>
            @yield('content')
        </main>
    </body>

    <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
    <script src="{{ asset('js/vendor.min.js') }}"></script>
    <script src="{{ asset('js/scripts.min.js') }}"></script>
    <!-- Customizer scripts-->
    <script src="{{ asset('customizer/customizer.min.js') }}"></script>
    <script>
        $(".alert").on("close.bs.alert", function () {
            $("#alert").addClass('d-none');
            return false;
        });
    </script>
    @yield('scripts')
</html>

