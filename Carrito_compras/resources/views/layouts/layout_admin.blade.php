<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    {{-- <link href="{{ asset('admin/img/logo/logo.png') }}" rel="icon"> --}}
    <title>Fishing - Panel</title>
    <link href="{{ asset('admin/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('admin/css/ruang-admin.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet">
    <link rel="icon" href="{{ asset('img/logo/LogoIcono.png') }}">
</head>

<body id="page-top">
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route("admin.home")}}">
                <div class="sidebar-brand-icon">
                    MERCADO FISHING
                </div>
            </a>
            <hr class="sidebar-divider" style="margin-top: 5px;">
            
            <li class="nav-item @if(Route::current()->getName() == 'getUsuarios' || 
                                    Route::current()->getName() == 'getUsuariosEliminados'||
                                    Route::current()->getName() == 'getUsuariosDeshabilitados' ) active-custom @endif">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePage" aria-expanded="true"
                aria-controls="collapsePage">
                <i class="fas fa-fw fa-user"></i>
                <span>Clientes</span>
                </a>
                <div id="collapsePage" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Opciones</h6>
                        <a class="collapse-item" href="{{route("getUsuarios")}}">Ver todos</a>
                        <a class="collapse-item" href="{{route("getUsuariosEliminados")}}">Ver eliminados</a>
                        <a class="collapse-item" href="{{route("getUsuariosDeshabilitados")}}">Ver deshabilitados</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item @if(Route::current()->getName()== 'admin.verCompra' || Route::current()->getName()== 'admin.getComprasXCliente' || Route::current()->getName()== 'admin.verComprasServicios' || Route::current()->getName()== 'admin.verComprasProductos' ) active-custom @endif">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageC" aria-expanded="true"
                aria-controls="collapsePageC">
                <i class="fas fa-shopping-cart"></i>
                <span>Compras</span>
                </a>
                <div id="collapsePageC" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Opciones</h6>
                        <a class="collapse-item" href="{{route("admin.verComprasProductos")}}">Productos</a>
                        <a class="collapse-item" href="{{route("admin.verComprasServicios")}}">Servicios</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider">
            
            <li class="nav-item @if(Route::current()->getName() == 'verCategoriasView' || Route::current()->getName() == 'verSubcategoriasView' ) active-custom @endif">
                <a class="nav-link" href="{{route("verCategoriasView")}}">
                    <i class="fas fa-fw fa-list-ul"></i>
                    <span>Categorias</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            <li class="nav-item @if(Route::current()->getName() == 'admin.getProductos' || 
                                    Route::current()->getName() == 'admin.getProductosDeshabilitados'||
                                    Route::current()->getName() == 'admin.editarProducto' || 
                                    Route::current()->getName() == 'admin.buscadorAdmin'||
                                    Route::current()->getName() == 'admin.verProductosXCategoria' || 
                                    Route::current()->getName() == 'admin.verProductosXSubcategoria'||
                                    Route::current()->getName() == 'verImagenesProductoView' || 
                                    Route::current()->getName() == 'editarImagenProductoView'||
                                    Route::current()->getName() == 'getVariantesProducto' || 
                                    Route::current()->getName() == 'editarVarianteProducto' ) active-custom @endif">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageP" aria-expanded="true"
                aria-controls="collapsePageP">
                <i class="fas fa-fw fa-box-open"></i>
                <span>Productos</span>
                </a>
                <div id="collapsePageP" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Opciones</h6>
                        <a class="collapse-item" href="{{route("admin.getProductos")}}">Ver todos</a>
                        <a class="collapse-item" href="{{route("admin.getProductosDeshabilitados")}}">Ver deshabilitados</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider">
            <li class="nav-item  @if(Route::current()->getName() == 'admin.getServicios' || 
                Route::current()->getName() == 'admin.getServiciossDeshabilitados'||
                Route::current()->getName() == 'admin.editarServicio' || 
                Route::current()->getName() == 'admin.verServiciosXCategoria' || 
                Route::current()->getName() == 'admin.verServiciosXSubcategoria'||
                Route::current()->getName() == 'verImagenesServicioView' || 
                Route::current()->getName() == 'editarImagenServicioView'||
                Route::current()->getName() == 'getVariantesServicio' || 
                Route::current()->getName() == 'editarVarianteServicio' ) active-custom @endif">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageS" aria-expanded="true"
                aria-controls="collapsePageS">
                <i class="fas fa-fw fa-box-open"></i>
                <span>Servicios</span>
                </a>
                <div id="collapsePageS" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Opciones</h6>
                        <a class="collapse-item" href="{{route("admin.getServicios")}}">Ver todos</a>
                        <a class="collapse-item" href="{{route("admin.getServiciosDeshabilitados")}}">Ver deshabilitados</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider">
            <li class="nav-item @if(Route::current()->getName() == 'verSlidersView' || Route::current()->getName() == 'editarSliderView' ) active-custom @endif">
                <a class="nav-link" href="{{route("verSlidersView")}}">
                <i class="far fa-images"></i>
                <span>Slider</span>
                </a>
            </li>
            
            <hr class="sidebar-divider">
            
            <li class="nav-item @if(Route::current()->getName()== 'editarContactoView' ) active-custom @endif">
                <a class="nav-link" href="{{route("editarContactoView")}}">
                <i class="fas fa-fw fa-address-card"></i>
                <span>Contacto</span>
                </a>
            </li>
            <hr class="sidebar-divider">

            <li class="nav-item @if(Route::current()->getName()== 'admin.getPreguntasFrecuentes' || Route::current()->getName()== 'admin.editarPreguntaFrecuente' ) active-custom @endif">
                <a class="nav-link" href="{{route("admin.getPreguntasFrecuentes")}}">
                <i class="fas fa-fw fa-question-circle"></i>
                <span>Preguntas frecuentes</span>
                </a>
            </li>

            <hr class="sidebar-divider">

            <li class="nav-item @if(Route::current()->getName()== 'admin.verTerminosYCondiciones' || Route::current()->getName()== 'admin.editarTerminosYCondicionesView' ) active-custom @endif">
                <a class="nav-link" href="{{route("admin.verTerminosYCondiciones")}}">
                <i class="fas fa-fw fa-question-circle"></i>
                <span>Terminos y condiciones</span>
                </a>
            </li>

            <hr class="sidebar-divider">

            <li class="nav-item @if(Route::current()->getName()== 'admin.verQuienesSomos' ) active-custom @endif">
                <a class="nav-link" href="{{route("admin.verQuienesSomos")}}">
                <i class="fas fa-fw fa-question-circle"></i>
                <span>Quienes somos</span>
                </a>
            </li>
        </ul>
        <!-- Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <!-- TopBar -->
                <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
                    <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <ul class="navbar-nav ml-auto">
                        
                        <div class="topbar-divider d-none d-sm-block"></div>
                        <li class="nav-item dropdown {{-- no-arrow --}}">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="d-none d-lg-inline text-white small">
                                    {{Auth::guard('administrador')->user()->nombre}} {{Auth::guard('administrador')->user()->apellido}}
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <a class="dropdown-item @if(Route::current()->getName() == 'admin.editarDatos' ) active-custom @endif" href="{{route('admin.editarDatos')}}">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Mis datos
                                </a>

                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('administrador.logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Salir
                                </a>

                                <form id="logout-form" action="{{ route('administrador.logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!-- Topbar -->

                <!-- Container Fluid-->
                <div class="container-fluid" id="container-wrapper">

                    <main class="py-4">
                        @yield('content')
                    </main>
                </div>
                <!-- Container Fluid-->
            </div>
        </div>
    </div>

    <!-- Scroll to top -->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('admin/js/ruang-admin.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('admin/js/demo/chart-area-demo.js') }}"></script>  
    <script>
        $(".alert").on("close.bs.alert", function () {
            $("#alert").addClass('d-none');
            return false;
        });
    </script>
    @yield('scripts')
</body>

</html>