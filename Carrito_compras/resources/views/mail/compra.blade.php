@extends('mail.layout')

@section('content')
    <tr>
        <td width="100%" cellpadding="0" cellspacing="0" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#edf2f7;border-bottom:1px solid #edf2f7;border-top:1px solid #edf2f7;margin:0;padding:0;width:100%">
            <table align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#ffffff;border-color:#e8e5ef;border-radius:2px;border-width:1px;margin:0 auto;padding:0;width:570px">
                <tbody>
                    <tr>
                        <td style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100vw;padding:32px">
                            <h1 style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:18px;font-weight:bold;margin-top:0;text-align:left">
                                ¡Hola {{$usuario->nombre}}!
                            </h1>
                            <h1 style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:18px;font-weight:bold;margin-top:0;text-align:left">
                                Se ha realizado con exito la compra N° {{$num_compra}}
                            </h1>
                            <div>
                                <table class="table align-items-center table-flush" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';width:570px;margin-bottom: 0 !important;">
                                    <tbody>
                                        @foreach($items as $item)
                                            <tr>
                                                <td style="margin:0; padding:0;">
                                                    <img src="{{ asset($item['imagen']) }}" alt="Imagen del producto" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100%;border:none;height:50px;width:50px" class="CToWUd">
                                                </td>
                                                <td>
                                                    <p>
                                                        {{$item['nombre']}} @if($tipo_compra == 1 && isset($item['cantidad'])) x <small>{{$item['cantidad']}}  </small> @endif
                                                    </p>
                                                </td>
                                                <td>
                                                    <p >
                                                        ${{number_format($item['precio'], 2, ",", ".")}} @if($tipo_compra == 1) <small> x unidad </small> @endif
                                                    </p>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @if($tipo_compra == 1)
                                @if( (isset($metodo_envio) && $metodo_envio != "") && (isset($costo_envio) && $costo_envio != ""))
                                    <hr style="margin-bottom: 20px;">
                                    <div>
                                        <h3 style="display: inline;box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:15px;margin-top:0;text-align:left">
                                            Método de envío:
                                        </h3>
                                        <p style="display: inline;">
                                            @if($metodo_envio == 1) 
                                                Normal a domicilio
                                            @elseif($metodo_envio == 0)
                                                Retiro por sucursal 
                                            @endif
                                        </p>
                                    </div>

                                    <div>
                                        <h3 style="display: inline;box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:15px;margin-top:0;text-align:left">
                                            Costo de envío:
                                        </h3>
                                        <p style="display: inline;"> 
                                            @if($costo_envio > 0)
                                                ${{number_format($costo_envio, 2, ",", ".")}}
                                            @endif
                                        </p>
                                    </div>
                                @endif
                            @endif
                            <hr style="margin-bottom: 20px;">
                            <h1 style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:18px;font-weight:bold;margin-top:0;text-align:right">
                                Total pagado: ${{number_format($total, 2, ",", ".")}}
                            </h1>
                            <p style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                Saludos.<br>{{config('app.name')}}
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
@endsection