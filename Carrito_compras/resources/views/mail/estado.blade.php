@extends('mail.layout')

@section('content')
    <tr>
        <td width="100%" cellpadding="0" cellspacing="0" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#edf2f7;border-bottom:1px solid #edf2f7;border-top:1px solid #edf2f7;margin:0;padding:0;width:100%">
            <table align="center" width="570" cellpadding="0" cellspacing="0" role="presentation" style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';background-color:#ffffff;border-color:#e8e5ef;border-radius:2px;border-width:1px;margin:0 auto;padding:0;width:570px">
                <tbody>
                    <tr>
                        <td style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';max-width:100vw;padding:32px">
                            <h1 style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';color:#3d4852;font-size:18px;font-weight:bold;margin-top:0;text-align:left">
                                ¡Hola {{$usuario->nombre}}!
                            </h1>
                            @switch($estado)
                                @case('Procesando')
                                    <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                        Te informamos que tu pedido se encuentra en proceso de ser enviado. 
                                    </p>
                                    <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                        Cuando se despache te enviaremos un correo con el codigo de seguimiento. 
                                    </p>
                                    <br>
                                    @break
                                @case('En camino')
                                    <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                        Te informamos que tu pedido se encuentra en camino. 
                                    </p>
                                    @if (isset($codigo_seguimiento))
                                    {{-- TODO: aca se va a poner el link con el codigo a OCA --}}
                                        <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                            Tu codigo de seguimiento es :
                                        </p>
                                        <div style="padding: 3%; background-color: rgba(213, 216, 220, 0.3); width: 80%; text-align: center" >
                                            <h5 style="font-family: Roboto, Arial, Helvetica, sans-serif;margin: auto;">{{$codigo_seguimiento}}</h5>
                                        </div>
                                        <br>
                                    @endif
                                    @break
                                @case('Finalizada')
                                    <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                        Te informamos que tu pedido fue entregado. 
                                    </p>
                                    <br>
                                    @if (isset($contacto_reclamos))
                                        <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                            Si ocurrio algun problema con la entrega no dudes en escribirnos a: <b>{{$contacto_reclamos}}</b> 
                                        </p>
                                        <br>
                                    @endif
                                    @break
                                @default
                                    <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                        Te informamos que tu pedido se encuentra {{$estado}}. 
                                    </p>
                                    <br>
                                @break
                            @endswitch
                            <p style="font-family: Roboto, Arial, Helvetica, sans-serif">
                                Para obtener mas informacion al respecto podes escribirnos a: <b>{{$contacto}}</b>.
                            </p>
                            <br>
                            <p style="box-sizing:border-box;font-family:-apple-system,BlinkMacSystemFont,'Segoe UI',Roboto,Helvetica,Arial,sans-serif,'Apple Color Emoji','Segoe UI Emoji','Segoe UI Symbol';font-size:16px;line-height:1.5em;margin-top:0;text-align:left">
                                Saludos.<br>{{config('app.name')}}
                            </p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
@endsection