@extends('layouts.layout')

@section('content')


  <section class="hero-slider">
      <div class="owl-carousel large-controls dots-inside" data-owl-carousel="{ &quot;nav&quot;: true, &quot;dots&quot;: true, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 3500 }">
        @foreach($sliders as $slider)
          <div class="item">
            <div class="container padding-top-2x">
              <div class="row justify-content-center align-items-center">
                <div class="col-lg-5 col-md-6 padding-bottom-2x text-md-left text-center">
                  <div class="from-bottom">
                    <div class="h2 text-body mb-2 pt-1">{{$slider->texto}}</div>
                  </div>
                  <a class="btn btn-primary scale-up delay-1" href="{{route('getProductosC')}}">
                    Ver productos&nbsp;<i class="icon-arrow-right"></i>
                  </a>
                </div>
                <div class="col-md-6 padding-bottom-2x mb-3">
                  <img class="d-block mx-auto" src="{{asset($slider->url)}}" alt="Imagen no encontrada">
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
  </section>
  <!-- Featured Products-->
  <section class="container padding-bottom-2x mb-2">
    <h2 class="h3 pb-3 text-center mt-5">Productos destacados</h2>
    <div class="row">
      @foreach($productos_destacados as $producto_destacado)
        <div class="col-lg-3 col-md-4 col-sm-6">
          <div class="product-card mb-30">
            <a class="product-thumb" href="{{route('verProductoC',$producto_destacado->id_producto)}}">
              <img src="{{asset($producto_destacado->imagen)}}" alt="Producto">
            </a>
            <div class="product-card-body">
              <div class="product-category">
                <a href="{{route('verProductosXCategoria',$producto_destacado->categoria->id_categoria)}}">{{$producto_destacado->categoria->nombre_categoria}}</a>
              </div>
              <h3 class="product-title"><a href="{{route('verProductoC',$producto_destacado->id_producto)}}">{{$producto_destacado->nombre}}</a></h3>
              <h4 class="product-price">
                ${{number_format($producto_destacado->precio, 2, ",", ".")}}
              </h4>
            </div>
            <div class="product-button-group">
              <a class="product-button" href="{{route('verProductoC',$producto_destacado->id_producto)}}">
                Ver producto
              </a>
            </div>
            
            {{-- <div class="product-button-group">
              <a class="product-button" href="{{route('addProductoCarrito')}}"
                  onclick="event.preventDefault();
                  document.getElementById('add-producto-form-{{$producto_destacado->id_producto}}').submit();">

                  <i class="icon-shopping-cart"></i><span>Añadir al carrito</span>
              </a>
            </div>
            <form id="add-producto-form-{{$producto_destacado->id_producto}}" action="{{route('addProductoCarrito')}}" method="POST" style="display: none;">
                @csrf
                <input type="hidden" name="id_producto" value="{{$producto_destacado->id_producto}}">
                <input type="hidden" name="cantidad" value="1">
            </form> --}}
          </div>
        </div>
      @endforeach
    </div>
    <div class="text-center"><a class="btn btn-outline-secondary" href="{{route('getProductosC')}}">Ver todos los productos</a></div>
  </section>
  <!-- CTA-->
  <section class="fw-section padding-top-4x padding-bottom-8x" style="background-image: url(img/banners/rio.jpg);">
    <span class="overlay" style="opacity: .7;"></span>
    <div class="container text-center">
      <div class="display-4 text-white py-4">Mejorá tu experiencia con nuestros productos</div>
      <div class="pt-5"></div>
  </section>
  <a class="d-block position-relative mx-auto" style="max-width: 682px; margin-top: -180px; z-index: 10;">
    <img class="d-block w-100" src="{{asset('img/banners/cana_anzuelo.png')}}" alt="Printers">
  </a>
  <!-- Popular Brands Carousel-->
  <section class="bg-secondary padding-top-3x padding-bottom-3x">
    <div class="container">
      <h2 class="h3 text-center mb-30 pb-3">Marcas Populares</h2>
      <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: false, &quot;loop&quot;: true, &quot;autoplay&quot;: true, &quot;autoplayTimeout&quot;: 4000, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:2}, &quot;470&quot;:{&quot;items&quot;:3},&quot;630&quot;:{&quot;items&quot;:4},&quot;991&quot;:{&quot;items&quot;:5},&quot;1200&quot;:{&quot;items&quot;:6}} }"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/01.png" alt="IBM"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/02.png" alt="Sony"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/03.png" alt="HP"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/04.png" alt="Canon"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/05.png" alt="Bosh"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/06.png" alt="Dell"><img class="d-block w-110 opacity-75 m-auto" src="img/brands/07.png" alt="Samsung"></div>
    </div>
  </section>
@endsection
