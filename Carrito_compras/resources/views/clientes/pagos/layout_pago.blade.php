@php
    //$menues = App\Menu::where('etiqueta',"!=",'Categorias')->get();
    $menues = App\Menu::orderBy('orden', 'ASC')->get();
    $categorias_producto = App\Categoria::where([['habilitado',1],['tipo',1]])->get();
    $categorias_servicio = App\Categoria::where([['habilitado',1],['tipo',0]])->get();
    
    foreach($categorias_producto as $categoria_producto){
        $subcategorias_producto = App\Subcategoria::where([['id_categoria',$categoria_producto->id_categoria],['habilitado',1]])->get();
        $categoria_producto->subcategorias = $subcategorias_producto;
    }

    foreach($categorias_servicio as $categoria_servicio){
        $subcategorias_servicio = App\Subcategoria::where([['id_categoria',$categoria_servicio->id_categoria],['habilitado',1]])->get();
        $categoria_servicio->subcategorias = $subcategorias_servicio;
    }

    $contacto = App\Contacto::first();
    
    if(Auth::check()){
        $id_carrito = App\Carrito_compra::where('id_usuario',Auth::user()->id)->first()->id_carrito;

        $productos_en_carrito = DB::table('productos')
            ->join('variantes_producto', 'variantes_producto.id_producto', '=', 'productos.id_producto')
            ->join('productos_x_carritos', 'productos_x_carritos.id_variante_producto', '=', 'variantes_producto.id_variante_producto')
            ->where('productos_x_carritos.id_carrito',$id_carrito)
            ->select('productos.id_producto', 'productos.nombre', 'productos_x_carritos.cantidad','productos.precio', 'variantes_producto.id_variante_producto')
            ->get();

        $servicios_en_carrito = DB::table('servicios')
            ->join('servicios_x_carritos', 'servicios_x_carritos.id_servicio', '=', 'servicios.id_servicio')
            ->where('servicios_x_carritos.id_carrito',$id_carrito)
            ->select('servicios.id_servicio', 'servicios.titulo', 'servicios.precio')
            ->get();

        foreach($productos_en_carrito as $producto){
            if (App\Imagenes_producto::where('id_producto', $producto->id_producto)->exists()) {
                $producto->imagen = App\Imagenes_Producto::where('id_producto', $producto->id_producto)->first()->url;
            } else {
                $producto->imagen = '/img/imagenes_productos/producto_sin_imagen.jpg';
            }
        }

        foreach($servicios_en_carrito as $servicio){
            if (App\Imagenes_producto::where('id_servicio', $servicio->id_servicio)->exists()) {
                $servicio->imagen = App\Imagenes_Producto::where('id_servicio', $servicio->id_servicio)->first()->url;
            } else {
                $servicio->imagen = '/img/imagenes_servicios/servicio_sin_imagen.png';
            }
        }

        //Guardo la cantidad de productos en el carrito
        $cantidad_carrito = count(App\Productos_x_carrito::where('id_carrito',$id_carrito)->get())+count(App\Servicios_x_carrito::where('id_carrito',$id_carrito)->get());
    }
@endphp

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Mercado Fishing
        </title>
        <!-- SEO Meta Tags-->
        <meta name="description" content="Unishop - Universal E-Commerce Template">
        <meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
        <meta name="author" content="Rokaux">
        <!-- Mobile Specific Meta Tag-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
        <link rel="stylesheet" media="screen" href="{{ asset('css/vendor.min.css') }}">
        <!-- Main Template Styles-->
        <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('css/styles.min.css') }}">
        <!-- Customizer Styles-->
        <link rel="stylesheet" media="screen" href="{{ asset('customizer/customizer.min.css') }}">

        <link rel="icon" href="{{ asset('img/logo/LogoIcono.png') }}">
        <!-- Google Tag Manager-->
        <script>
            (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-T4DJFPZ');
        </script>
        <!-- Modernizr-->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <style>
            .logo_pago{
                width: 30%;
            }
            @media (max-width: 600px){
                .logo_pago{
                    width: 50%;
                }
            }
        </style>
    </head>
    <body>
        <!-- Google Tag Manager (noscript)-->
        <noscript>
            <iframe src="http://www.googletagmanager.com/ns.html?id=GTM-T4DJFPZ" height="0" width="0" style="display: none; visibility: hidden;"></iframe>
        </noscript>
        
        <!-- Header-->
        <!-- Remove "navbar-sticky" class to make navigation bar scrollable with the page.-->
        <header class="site-header navbar-sticky">
            <!-- Topbar-->
            <div class="topbar d-flex" id="topbar">
                <!-- Logo-->
                <a href="{{route('home')}}" class="d-flex justify-content-center">
                    <img src="{{ asset('img/logo/LL.png') }}" class="logo_pago" alt="Mercado Fishing">
                </a>
            </div>
        </header>
        
        <main class="py-4" style="min-height: 70vh">
            @yield('content')
        </main>

        <!-- Site Footer-->
        <footer class="site-footer" style="background-image: url(img/footer-bg.png);">
            <div class="container">
                <hr class="hr-light margin-bottom-2x">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <!-- Contact Info-->
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">Contacto</h3>
                            <ul class="list-unstyled text-sm text-white">
                                <li><span class="opacity-50">Telefono de atención al cliente:&nbsp;</span>{{$contacto->telefono_1}}</li>
                                <li><span class="opacity-50">Telefono de reclamos:&nbsp;</span>{{$contacto->telefono_3}}</li>
                            </ul>

                            <ul class="list-unstyled text-sm text-white">
                                <li><span class="opacity-50">Consultas:&nbsp;</span>{{$contacto->email_info}}</li>
                                <li><span class="opacity-50">Reclamos:&nbsp;</span>{{$contacto->email_reclamos}}</li>
                                <li><span class="opacity-50">Comprobantes:&nbsp;</span>{{$contacto->email_comprobantes}}</li>
                            </ul>
                            <a class="social-button shape-circle sb-facebook sb-light-skin" href="{{$contacto->facebook}}">
                                <i class="socicon-facebook"></i>
                            </a>
                            <a class="social-button shape-circle sb-twitter sb-light-skin" href="{{$contacto->twitter}}">
                                <i class="socicon-twitter"></i>
                            </a>
                            <a class="social-button shape-circle sb-instagram sb-light-skin" href="{{$contacto->instagram}}">
                                <i class="socicon-instagram"></i>
                            </a>
                            <a class="social-button shape-circle sb-google-plus sb-light-skin" href="{{$contacto->youtube}}">
                                <i class="socicon-youtube"></i>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <!-- Mobile App Buttons-->
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">Ayuda</h3>
                            <ul class="list-unstyled text-lg text-white">
                                <li><a class="navi-link-light" href="">¿Cómo comprar?</a></li>
                                <li><a class="navi-link-light" href="">Métodos de envío</a></li>
                                <li><a class="navi-link-light" href="">¿Quiénes somos?</a></li>
                            </ul>
                        </section>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <!-- Mobile App Buttons-->
                        <section class="widget widget-light-skin">
                            <h3 class="widget-title">Terminos y Condiciones</h3>
                            <ul class="list-unstyled text-lg text-white">
                                <li><a class="navi-link-light" href="{{route('terminosYCondiciones')}}">Ver Terminos y Condiciones</a></li>
                            </ul>
                        </section>
                    </div>
                </div>
                <!-- Copyright-->
                <p class="footer-copyright">
                    © MERCADO FISHING 2020
                </p>
            </div>
        </footer>

        <!-- Back To Top Button--><a class="scroll-to-top-btn" href="index.html#"><i class="icon-chevron-up"></i></a>
        <!-- Backdrop-->
        <div class="site-backdrop"></div>
        <!-- JavaScript (jQuery) libraries, plugins and custom scripts-->
        <script src="{{ asset('js/vendor.min.js') }}"></script>
        <script src="{{ asset('js/scripts.min.js') }}"></script>
        <!-- Customizer scripts-->
        <script src="{{ asset('customizer/customizer.min.js') }}"></script>
    </body>
</html>