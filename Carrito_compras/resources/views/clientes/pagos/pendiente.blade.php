@extends('clientes.pagos.layout_pago')

@section('content')


    <div class="card mx-auto" style="width: 35rem;">
        <div class="card-header text-center" style="background-color: #ffa000;">
            <h2 style="color: white;">¡Tu pago esta pendiente!</h2>
        </div>
        <div class="card-body">
            <h5 class="card-title"><i class="icon-alert-triangle" style="color: #ffa000;"></i> Estamos esperando que tu pago se acredite</h5>
            <p class="card-text">
                Durante el transcurso de estos dias esperaremos a que se acredite el pago. <br>
                En el momento que se acredite te notificaremos mediante un correo electrónico.
            </p>
            <div class="text-center">
                <a href="{{route('getCompras')}}" class="btn btn-primary">Ver mis compras</a>
            </div>
        </div>
    </div>


@endsection