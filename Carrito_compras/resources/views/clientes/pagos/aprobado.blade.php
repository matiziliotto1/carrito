@extends('clientes.pagos.layout_pago')

@section('content')


    <div class="card mx-auto" style="width: 35rem;">
        <div class="card-header text-center" style="background-color: #47a43d;">
            <h2 style="color: white;">¡Se acreditó tu pago!</h2>
        </div>
        <div class="card-body">
            <h5 class="card-title"><i class="icon-check-circle" style="color:#47a43d;"></i> Gracias por tu compra</h5>
            <p class="card-text">
                Desde Mercado Fising queremos agradecerte por esta compra. <br>
                Te enviamos un correo electrónico con los detalles de la compra y durante los siguientes dias 
                te notificaremos por el mismo medio mas información sobre el estado de la misma.
            </p>
            <div class="text-center">
                <a href="{{route('getCompras')}}" class="btn btn-primary">Ver mis compras</a>
            </div>
        </div>
    </div>


@endsection