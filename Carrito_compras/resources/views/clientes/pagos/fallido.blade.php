@extends('clientes.pagos.layout_pago')

@section('content')


    <div class="card mx-auto" style="width: 35rem;">
        <div class="card-header text-center" style="background-color: #d06060;">
            <h2 style="color: white;">¡Tu pago no fue acreditado!</h2>
        </div>
        <div class="card-body">
            <h5 class="card-title"><i class="icon-slash" style="color:#d06060;"></i> Parece que hubo un error al realizar el pago</h5>
            <p class="card-text">
                Intenta realizar la compra nuevamente ingresando al <a class="link-no-style" href="{{route('verCarritoDeCompras')}}">carrito de compras</a>
                y siguiendo los pasos recomendados.
            </p>

            <div class="text-center">
                <a href="{{route('verCarritoDeCompras')}}" class="btn btn-primary">Ir al carrito</a>
            </div>
        </div>
    </div>


@endsection