@extends('layouts.layout')

@section('content')

<!-- Page Title-->
<div class="page-title">
    <div class="container">
      <div class="column">
        <h1>Ayuda</h1>
      </div>
      <div class="column">
        <ul class="breadcrumbs">
          <li><a href="{{route('home')}}">Inicio</a>
          </li>
          <li class="separator">&nbsp;</li>
          <li>Ayuda</li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Page Content-->
  <div class="container padding-bottom-3x">
    <div class="row">
      <!-- Side Menu-->
      <div class="col-lg-3 col-md-4">
        <nav class="list-group">
          <a class="list-group-item active" href="{{route('ayuda')}}">Preguntas frecuentes</a>
          <a class="list-group-item" href="{{route('terminosYCondiciones')}}">Terminos y condiciones</a>
        <div class="padding-bottom-3x hidden-md-up"></div>
      </div>
      <!--Content-->
      <div class="col-lg-9 col-md-8">
        <div class="accordion" id="accordion" role="tablist">
            @foreach ($preguntas as $pregunta)
                <div class="card">
                    <div class="card-header" id="heading{{$pregunta->id_pregunta}}">
                        <h6><a class="collapsed" data-toggle="collapse" data-target="#collapse{{$pregunta->id_pregunta}}" aria-expanded="true" aria-controls="collapse{{$pregunta->id_pregunta}}">{{$pregunta->texto_pregunta}}</a></h6>
                    </div>
                    <div class="collapse" id="collapse{{$pregunta->id_pregunta}}" data-parent="#accordion" role="tabpanel" aria-labelledby="heading{{$pregunta->id_pregunta}}">
                        <div class="card-body">
                            <p>{{$pregunta->texto_respuesta}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
      </div>
    </div>
  </div>

@endsection