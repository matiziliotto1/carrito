@extends('layouts.layout')

@section('content')
<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>Viendo {{$producto->nombre}}</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{route('home')}}">Inicio</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>Producto</li>
            </ul>
        </div>
    </div>
</div>
@if(session('mensaje_alerta_ok'))
        <div class="alert 
            @if(session('mensaje_alerta_ok')) 
                alert-success 
            @else
                alert-danger
            @endif 
            alert-dismissible fade show text-center margin-bottom-1x">

            <span class="alert-close" data-dismiss="alert"></span>
            <i class="icon-check-circle"></i>&nbsp;&nbsp; 
            {{ session('mensaje') }}
        </div>
    @endif
<!-- Page Content-->
<div class="container padding-bottom-3x">
    <div class="row">
        <!-- Poduct Gallery-->
        <div class="col-md-6">
            <div class="product-gallery">
                <div class="product-carousel owl-carousel gallery-wrapper">
                    @php
                        //Esta hash se usaba para cada div de los items de la galeria de imagenes, por lo tanto lo mantuve
                        $data_hash =  array(1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine");
                        $i=1;
                    @endphp

                    @if (count($producto->imagenes)>0)
                        @foreach($producto->imagenes as $imagen)
                            <div class="gallery-item" data-hash="{{$data_hash[$i]}}">
                                <a href="{{asset($imagen->url)}}" data-size="1000x667">
                                    <img src="{{asset($imagen->url)}}" alt="Producto">
                                </a>
                            </div>
                            @php
                                $i = $i + 1;
                            @endphp
                        @endforeach
                    @else
                        <div class="gallery-item" data-hash="{{$data_hash[$i]}}">
                            <a data-size="1000x667">
                                <img src="{{asset('/img/imagenes_productos/producto_sin_imagen.jpg')}}" alt="Producto">
                            </a>
                        </div>
                    @endif
                </div>
                <ul class="product-thumbnails">
                    @php
                        $i = 1;
                    @endphp
                    @if (count($producto->imagenes)>0)
                        @foreach($producto->imagenes as $imagen)
                            <li class="@if ($loop->first)
                                active
                            @endif">
                                <a href="#{{$data_hash[$i]}}">
                                    <img src="{{asset($imagen->url)}}" alt="Product">
                                </a>
                            </li>
                            @php
                                $i = $i + 1;
                            @endphp
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <!-- Product Info-->
        <div class="col-md-6">
            <input type="hidden" id="id_producto" value="{{$producto->id_producto}}">
            <div class="padding-top-2x mt-2 hidden-md-up"></div>
            <div class="sp-categories pb-3">
                <i class="icon-tag"></i>
                <a href="{{route('verProductosXCategoria', $producto->categoria->id_categoria)}}">{{$producto->categoria->nombre_categoria}}</a>
            </div>
            <h2 class="mb-3">{{$producto->nombre}}</h2>
            <span class="d-none" id="precio">${{$producto->precio}}</span>
            <span class="h3 d-block">
                ${{number_format($producto->precio, 2, ",", ".")}}
            </span>
            <p class="text-muted">{{$producto->descripcion_resumida}}
                <a href='#detalles' class='scroll-to'>Mas información</a>
            </p>

            <div class="row margin-top-1x">
                {{-- Select de los colores --}}
                @if(count($producto->colores) > 0)
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="color_producto">Elegir color</label>
                            <select class="form-control" id="color_producto" name="color_producto">
                                <option value="0" disabled selected>Seleccione una opción</option>
                                @foreach($producto->colores as $producto_color)
                                    <option value="{{$producto_color->color}}">{{$producto_color->color}}</option>
                                @endforeach
                            </select>
                            <span class="invalid-feedback" role="alert">
                                <strong id="mensaje_error_color_producto"></strong>
                            </span>
                        </div>
                    </div>
                @endif

                {{-- Select de los talles de ropa --}}
                @if(count($producto->talles_ropa) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="talle_ropa_producto">Elegir talle</label>
                                <select class="form-control" id="talle_ropa_producto" name="talle_ropa_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->talles_ropa as $producto_talle_ropa)
                                        <option value="{{$producto_talle_ropa->talle_ropa}}">{{$producto_talle_ropa->talle_ropa}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_talle_ropa_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de los talles de calzados --}}
                @if(count($producto->talles_calzado) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="talle_calzado_producto">Elegir talle</label>
                                <select class="form-control" id="talle_calzado_producto" name="talle_calzado_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->talles_calzado as $producto_talle_calzado)
                                        <option value="{{$producto_talle_calzado->talle_calzado}}">{{$producto_talle_calzado->talle_calzado}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_talle_calzado_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de los talles de anzuelos --}}
                @if(count($producto->talles_anzuelo) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="talle_anzuelo_producto">Elegir talle</label>
                                <select class="form-control" id="talle_anzuelo_producto" name="talle_anzuelo_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->talles_anzuelo as $producto_talle_anzuelo)
                                        <option value="{{$producto_talle_anzuelo->talle_anzuelo}}">{{$producto_talle_anzuelo->talle_anzuelo}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_talle_anzuelo_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de las manos habiles --}}
                @if(count($producto->manos_habiles) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="mano_habil_producto">Left/Right</label>
                                <select class="form-control" id="mano_habil_producto" name="mano_habil_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->manos_habiles as $producto_mano_habil)
                                        @if($producto_mano_habil->mano_habil == "1")
                                            <option value="1">Derecha</option>
                                        @elseif($producto_mano_habil->mano_habil == "2")
                                            <option value="2">Izquierda</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_mano_habil_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de las longitudes --}}
                @if(count($producto->longitudes) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="longitud_producto">Elegir longitud</label>
                                <select class="form-control" id="longitud_producto" name="longitud_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->longitudes as $producto_longitud)
                                        <option value="{{$producto_longitud->longitud}}">{{$producto_longitud->longitud}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_longitud_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de las fuerzas --}}
                @if(count($producto->fuerzas) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="fuerza_producto">Elegir fuerza</label>
                                <select class="form-control" id="fuerza_producto" name="fuerza_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->fuerzas as $producto_fuerza)
                                        <option value="{{$producto_fuerza->fuerza}}">{{$producto_fuerza->fuerza}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_fuerza_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de las acciones --}}
                @if(count($producto->acciones) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="accion_producto">Elegir accion</label>
                                <select class="form-control" id="accion_producto" name="accion_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->acciones as $producto_accion)
                                        <option value="{{$producto_accion->accion}}">{{$producto_accion->accion}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_accion_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif

                {{-- Select de los diametros --}}
                @if(count($producto->diametros) > 0)
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="diametro_producto">Elegir diametro</label>
                                <select class="form-control" id="diametro_producto" name="diametro_producto">
                                    <option value="0" disabled selected>Seleccione una opción</option>
                                    @foreach($producto->diametros as $producto_diametro)
                                        <option value="{{$producto_diametro->diametro}}">{{$producto_diametro->diametro}}</option>
                                    @endforeach
                                </select>
                                <span class="invalid-feedback" role="alert">
                                    <strong id="mensaje_error_diametro_producto"></strong>
                                </span>
                            </div>
                        </div>
                @endif
            </div>

            <div class="d-none alert alert-danger alert-dismissible fade show text-center margin-bottom-1x" id="div_mensaje_error_no_hay_variantes">
                <span class="alert-close" data-dismiss="alert"></span>
                <i class="icon-slash"></i> 
                <span id="mensaje_error_no_hay_variantes">
    
                </span>
            </div>
            <!-- Cantidad del producto -->
            <div class="row align-items-end pb-4">
                <div class="col-sm-6">
                    <div class="form-group mb-0">
                        <label for="cantidad_input">Cantidad</label>
                        <input type="number" pattern="\d{1,5}" onkeypress="return event.charCode >= 48 && event.charCode <= 57" class="form-control" name="cantidad_input" id="cantidad_input" value="0" disabled>
                    </div>
                </div>
                <div class="col-sm-6">
                        <label id="stock"></label>
                </div>                                            
            </div>
            <!-- Botones para comprar y agregar al carrito -->
            <div class="row align-items-end pb-4">
                <div class="col-sm-6">
                    <div class="pt-4 hidden-sm-up"></div>
                    <a class="btn btn-primary btn-block m-0 btn-addProducto" style="cursor: pointer;" data-id="true">
                        <span>Comprar</span>
                        @php
                            if(Auth::check()){
                                $id_carro = App\Carrito_compra::where('id_usuario',Auth::user()->id)->first()->id_carrito;

                                $productos_en_carro = DB::table('productos')
                                    ->join('variantes_producto', 'variantes_producto.id_producto', '=', 'productos.id_producto')
                                    ->join('productos_x_carritos', 'productos_x_carritos.id_variante_producto', '=', 'variantes_producto.id_variante_producto')
                                    ->where('productos_x_carritos.id_carrito',$id_carro)
                                    ->select('variantes_producto.id_variante_producto')
                                    ->get();
                            }
                        @endphp
                        @if(Auth::check())
                            @if(count($productos_en_carro) > 0 )
                                @foreach($productos_en_carro as $producto_en_carro)
                                    <input type="hidden" name="productos_en_carro[]" id="productos_en_carro" value="{{$producto_en_carro->id_variante_producto}}">
                                @endforeach
                            @else
                                <input type="hidden" name="productos_en_carro[]" id="productos_en_carro">
                            @endif            
                        @else
                            <input type="hidden" name="productos_en_carro[]" id="productos_en_carro">
                        @endif
                    </a>
                </div>
                <div class="col-sm-6">
                    <a class="btn btn-primary btn-block m-0 btn-addProducto" data-id="false"><i class="icon-shopping-cart pr-2"></i>Añadir al carrito</a>
                </div>
                <form id="add-producto" action="{{route('addProductoCarrito')}}" method="POST" style="display: none;">
                    @csrf
                    <input type="hidden" name="compra" id="is_compra" value="true">
                    <input type="hidden" name="id_variante_producto" id="id_variante_producto"  value="0">
                    <input type="hidden" name="cantidad" id="cantidad" value="0">
                </form>
            </div>
        <hr class="mb-2">
        <div class="d-flex flex-wrap justify-content-between">
            <div id="div_carga">
                <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product Details-->
<div class="bg-secondary padding-top-3x padding-bottom-2x mb-3" id="detalles">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="h4">Detalles del producto</h3>
                <p class="mb-4">
                    {!! nl2br(e($producto->descripcion)) !!}
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container padding-bottom-3x mb-1">             
    <!-- Related Products Carousel-->
    <h3 class="text-center padding-top-2x mt-2 padding-bottom-1x">Tambien te puede gustar</h3>
    <!-- Carousel-->
    <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
        @foreach($productos_que_pueden_gustar as $producto)
            <div class="product-card">
                <a class="product-thumb" style="height: 30%; background-image: url('{{asset($producto->imagen)}}'); background-repeat: no-repeat; background-position: center; background-size: 100% 100%;" href="{{route("verProductoC",$producto->id_producto)}}">
                </a>
                <div class="product-card-body">
                    <div class="product-category">
                        <a href="{{route("verProductosXCategoria",$producto->categoria->id_categoria)}}">{{$producto->categoria->nombre_categoria}}</a>
                    </div>
                    <h3 class="product-title"><a href="{{route("verProductoC",$producto->id_producto)}}">{{$producto->nombre}}</a></h3>
                    <h4 class="product-price">
                        ${{number_format($producto->precio, 2, ",", ".")}}
                    </h4>
                </div>
                <div class="product-button-group">
                    <a class="product-button" href="{{route('verProductoC',$producto->id_producto)}}">
                        Ver producto
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<!-- Photoswipe container-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>

<!-- Default Modal-->
<div class="modal fade" id="modal_producto_ya_agregado" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #FFED00 !important">
                <h4 class="modal-title">Producto ya agregado</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Este producto ya se encuentra en el carrito, puedes modificar la cantidad si quieres comprar más.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        //Defino variables globales que voy a necesitar su valor en varias funciones.
        var id_producto = $("#id_producto").val();
        var stock;
        var color_producto;
        var talle_ropa_producto;
        var talle_calzado_producto;
        var talle_anzuelo_producto;
        var mano_habil_producto;
        var longitud_producto;
        var fuerza_producto;
        var accion_producto;
        var diametro_producto;

        /*
            Defino un array con las posibles variantes para que al hacer funciones comunes para todas las variantes, se
            recorra este array y no haya que escribir un llamado a una funcion (por ejemplo) a cada rato.
        */
        var posibles_variantes = ["color_producto", "talle_ropa_producto", "talle_calzado_producto", "talle_anzuelo_producto", "mano_habil_producto", "longitud_producto", "fuerza_producto", "accion_producto", "diametro_producto"];

        //Obtengo la cantidad de selects para despues hacer controles al momento de identificar la variante y permitir agregar al carrito o comprar
        var cantidad_selects = 0;

        /*
            Voy almacenando en esta variable la cantidad de selects que ya fueron seleccionados para luego comparar
            con la cantidad de selects total o sino para hacer controles en los cambios (on change).
        */
        var cantidad_selects_seleccionados = 0;

        //En estas variables voy guardando el ultimo select que se cambio (on change) y las consultas realizadas al servidor.
        var select_previo;
        var responses_previos = [];

        //Cuando se termina de cargar la pagina llamo a esa funcion.
        $( document ).ready(function() {
            contarCantidadDeSelects();

            if(cantidad_selects == 0){
                $("#div_mensaje_error_no_hay_variantes").removeClass("d-none");
                let route = "{{route('contacto')}}";
                $("#mensaje_error_no_hay_variantes").text("Al parece hay un error con el producto que intentas comprar, por favor comunicarlo al soporte. Podra encontrar los datos de contacto en: ");
                $("#mensaje_error_no_hay_variantes").append("<a class='link-no-style' href='"+route+"'>Contacto</a>");
                
            }
        });

        /*
            Esta funcion recorre todos los posibles selects y se fija cuantos de ellos estan en la pagina, es decir cuantos existen
            y actualiza la variable global con la cantidad total de selects encontrados.
        */
        function contarCantidadDeSelects(){
            for(let i = 0; i < posibles_variantes.length; i++ ){
                if($("#"+posibles_variantes[i]).length){
                    cantidad_selects = cantidad_selects + 1;
                }
            }
            
        }

        //Cada vez que ocurre un cambio en los select seleccionados, se resetean los valores del id de la variante y la cantidad.
        function resetearIdVarianteYCantidad(){
            $("#id_variante_producto").val("0");
            $("#cantidad").val("0");
        }

        //Cada vez que ocurre un cambio en los select seleccionados, se limpian todos los mensajes de errores y se eliminan las clases que hace que se muestren los errores.
        function limpiarMensajesDeErrores(){
            for(let i = 0; i < posibles_variantes.length; i++ ){
                $("#"+posibles_variantes[i]).removeClass("is-invalid");
            }

            for(let i = 0; i < posibles_variantes.length; i++ ){
                $("#mensaje_error_"+posibles_variantes[i]).text("");
            }

            //Vacio el texto del stock y vuelvo a setear el color del texto del stock por si se quizo agregar un producto sin stock al carrito.
            $("#stock").text("");
            $("#stock").css("color", "#505050");
        }

        /*
            Esta funcion se llama al final de cada cambio en algun select.
            Lo que hace es chequear si ya se seleccionaron todos los inputs para consultar la variante
            que coincide con lo seleccionado para identificarla (obtener el id exacto) y stock de la misma.
            En base al stock de la variante se habilita o deshabilita el input en el cual ingresa la cantidad a comprar.
        */
        function habilitar_o_deshabilitad_input_cantidad(){
            
            if(cantidad_selects == cantidad_selects_seleccionados){
                var token = '{{csrf_token()}}';
                $.ajax({
                    url: '{{route("getVariante")}}',
                    type: 'post',
                    data: {
                        id_producto: id_producto,
                        color: color_producto,
                        talle_ropa: talle_ropa_producto,
                        talle_calzado: talle_calzado_producto,
                        talle_anzuelo: talle_anzuelo_producto,
                        mano_habil: mano_habil_producto,
                        longitud: longitud_producto,
                        fuerza: fuerza_producto,
                        accion: accion_producto,
                        diametro: diametro_producto,
                        _token: token
                    },
                    dataType: 'json',
                    
                    success: function(response){
                        if(response['stock'] > 0){
                            $("#id_variante_producto").val(response['id_variante_producto']);
                            stock = response['stock'];

                            $("#cantidad_input").removeAttr("disabled");

                            $("#cantidad_input").val("1");
                            $("#cantidad").val("1");

                            $("#cantidad_input").attr("min","1");
                            $("#cantidad_input").attr("max",response['stock']);
                        }
                        else{
                            stock = 0;
                            //Deshabilito el input para ingresar la cantidad, seteo los valores de los input a 0 asi no se envia el formulario y muestro mensaje que no hay stock.
                            $("#cantidad_input").attr("disabled",true);

                            $("#cantidad_input").val("0");
                            $("#cantidad").val("0");

                            $("#stock").text("No hay stock disponible");
                        }
                    },
                });
            }
            else{
                $("#cantidad_input").attr("disabled",true);
                $("#cantidad_input").val("0");
                $("#cantidad").val("0");
            }
        }

        /*
            Esta funcion recibe como parametro el texto de la opcion seleccionada (opcion) junto al id del select (id_elemento) que se modifico.
            Lo que hace es lo siguiente:
            Busco en la opcion el texto 'No disponible' para saber si la opcion que eligio era una de las disponibles o no.
            Si selecciono una variante que no estaba disponible:
                -Reseteo todas las variables globales, poniendolas en undefined y hacer que la consulta que se hace siempre al
                    servidor, se resetee y se consulte por el nuevo campo seleccionado (si ya selecciono mas campos, tambien se
                    consulta en base a esos).
                -Reseteo el array donde guardaba las consultas previas realizadas al servidor.
                -Reseteo todos los select para que tengan la opcion "Seleccionar opcion" seleccionada, excepto el ultimo que se cambio.
                -Reinicio a 1 la cantidad de selects seleccionados.
        */
        function chequearOpcionSeleccionadaNoDisponible(opcion, id_elemento){
            resultado = opcion.search('No disponible'); //Busco el texto 'No disponible': No esta? retorna -1. Esta? retorna la posicion.

            //De todas formas se eliminan todos los no disponible para luego volver a setearlos y hacer las comprobaciones.
            eliminarTodosLosTextoNoDisponible();

            if(resultado != -1){
                stock = undefined;
                color_producto = undefined;
                talle_ropa_producto = undefined;
                talle_calzado_producto = undefined;
                talle_anzuelo_producto = undefined;
                mano_habil_producto = undefined;
                longitud_producto = undefined;
                fuerza_producto = undefined;
                accion_producto = undefined;
                diametro_producto = undefined;

                responses_previos = [];

                reiniciarTodosLosSelect(id_elemento);
                cantidad_selects_seleccionados = 1;
            }
        }

        /*
            Esta funcion recibe el id del select que se cambio (id_elemento).
            Lo que hace es recorrer todas las posibles variantes que haya y si son diferentes del
            select que se cambio, se lo reinicia para que aparezca la opcion "Seleccione una opcion".
        */
        function reiniciarTodosLosSelect(id_elemento){
            for(let i = 0; i < posibles_variantes.length; i++ ){
                if(posibles_variantes[i] != id_elemento){
                    $("#"+posibles_variantes[i]).val("0");
                }
            }
        }

        /*
            Lo que hace esta funcion es recorrer todas las posibles variantes que haya haciendo lo siguiente:
                -Elimina el texto "No disponible" de todas las opciones de los selects.
                -Resetea el color por defecto de todas las opciones de los selects.
        */
        function eliminarTodosLosTextoNoDisponible(){
            for(let i = 0; i < posibles_variantes.length; i++ ){
                eliminarTextoNoDisponible(posibles_variantes[i]);
            }

            for(let i = 0; i < posibles_variantes.length; i++ ){
                $("#"+posibles_variantes[i]).css("color", "#505050");
            }
        }

        /*
            Lo que hace esta funcion es recorrer todas las opciones que tenga el select que coincide con el id_elemento recibido y 
            se fija si tiene el texto "No disponible": 
                -Si lo tiene: se lo saca y se le pone el color normal.
        */
        function eliminarTextoNoDisponible(id_elemento){
            $("#"+id_elemento+" option").each(function(){
                if($(this).text().indexOf("No disponible") != -1){
                    $(this).text($(this).text().replace(' - No disponible', ''));
                    $(this).css("color", "#505050");
                }
            });
        }

        /*
            Se recibe por parametro el id del select (elemento) ya formateado con # para chequear si
            existe el elemento:
                -Si existe retorna true.
                -Si no existe retorna false.
        */
        function existeElElemento(elemento){
            if($(elemento).length){
                return true;
            }
            else{
                return false;
            }
        }

        /*
            Esta funcion recibe como parametro el id del select (id_elemento), el nombre real del campo en la base de datos (campo_db)
            y el response que se obtuvo en la consulta al servidor (response_auxiliar).
            Lo que hace la funcion es:
                -Agrego todos los valores del response (para el campo_db) en un array con todos los valores para ese campo.
                -Recorro todas las opciones del select que coincide con el id_elemento 
                    evitando siempre realizar alguna accion con la primera opcion de todas ("Seleccione una opcion").
                -Analizo si en el array con todos los valores (para el campo consultado) esta el texto de la opcion
                    que actualmente se esta recorriendo:
                        -Si NO esta: me fijo si el texto "No disponible" NO esta para agregarselo y ponerle un color tipo 'disabled'.
        */
        function agregarTextoNoDisponible(id_elemento, campo_db, response_auxiliar){
            var valor_campos_db = [];
            $.each(response_auxiliar, function( index, value ) {
                valor_campos_db.push(response_auxiliar[index][campo_db]);
            });

            $("#"+id_elemento+" option").each(function(){
                //Si la opcion es diferente de la primera ("Seleccione una opcion").
                if($(this).val() !== "0"){

                    //Este control es unicamente para la mano habil
                    if(campo_db == "mano_habil"){
                        //Si en los valores de los campos esta el 1, cambio todos los 1 por "Derecha"
                        if ( !(valor_campos_db.indexOf("1") > -1)) {
                            for(let i = 0; i < valor_campos_db.length; i++){
                                if(valor_campos_db[i] == "1"){
                                    valor_campos_db[i] = "Derecha";
                                }
                            }
                        }
                        //Si en los valores de los campos esta el 2, cambio todos los 2 por "Izquierda"
                        if ( !(valor_campos_db.indexOf("2") > -1)) {
                            for(let i = 0; i < valor_campos_db.length; i++){
                                if(valor_campos_db[i] == "2"){
                                    valor_campos_db[i] = "Izquierda";
                                }
                            }
                        }
                    }
                    //Si la opcion que se esta recorriendo, no esta dentro de las recibidas en la consulta.
                    if ( !(valor_campos_db.indexOf($(this).text()) > -1)) {
                        //Si no tiene el texto No disponible, se lo pongo
                        if( $(this).text().search('No disponible') == -1){
                            $(this).text( $(this).text() + ' - No disponible' );
                            $(this).css("color", "#AAAAAA");
                        }
                    }
                }
            });
        }

        /*
            Esta funcion recorre todos los posibles selects y se fija cuantos de ellos ya tienen 
            una opcion seleccionada, es decir cuantos existen y actualiza la variable global
            con la cantidad de selects que ya tienen la opcion seleccionada.

        */
        function contarCantidadDeSelectsSeleccionados(){
            for(let i = 0; i < posibles_variantes.length; i++ ){
                if($("#"+posibles_variantes[i]+" option:selected").val() != "0"){
                    cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
                }
            }
        }

        /*
            Esta funcion recibe el id del select (id_elemento) y el response actual (response).
            Lo que hace es fijarse si el ultimo select que se cambio no es el mismo:
                -Si NO es el mismo, agrego el response a los response previos.
                -Si es el mismo, elimino el ultimo response (que corresponderia al mismo select cambiado)
                    y agrego el response a los response previos.
        */
        function responses_previos_actualizar(id_elemento, response){
            if( select_previo != id_elemento){
                let item = [];
                item.push(response);
                responses_previos.push(item);
            }
            else{
                responses_previos.pop();
                let item = [];
                item.push(response);
                responses_previos.push(item);
            }
        }

        //Si selecciona un color para el producto que se esta viendo
        $("#color_producto").on("change",function(){
            
            if( select_previo != "color_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }

            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "color_producto");

            //Guardo el color seleccionado en la variable global.
            color_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "color_producto"){
                        agregarTextoNoDisponible("color_producto", "color", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "color_producto"){
                        agregarTextoNoDisponible("color_producto", "color", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("color_producto", response);

                    select_previo = "color_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
            
            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona un talle de ropa para el producto que se esta viendo
        $("#talle_ropa_producto").on("change",function(){

            if( select_previo != "talle_ropa_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }
            
            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "talle_ropa_producto");

            //Guardo el talle de la ropa seleccionado en la variable global.
            talle_ropa_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){                    
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "talle_ropa_producto"){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "talle_ropa_producto"){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("talle_ropa_producto", response);

                    select_previo = "talle_ropa_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona un color para el producto que se esta viendo
        $("#talle_calzado_producto").on("change",function(){

            if( select_previo != "talle_calzado_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }
            
            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "talle_calzado_producto");

            //Guardo el talle del calzado seleccionado en la variable global.
            talle_calzado_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){                    
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "talle_calzado_producto"){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "talle_calzado_producto"){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("talle_calzado_producto", response);

                    select_previo = "talle_calzado_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona un talle de calzado para el producto que se esta viendo
        $("#talle_anzuelo_producto").on("change",function(){

            if( select_previo != "talle_anzuelo_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }
            
            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "talle_anzuelo_producto");

            //Guardo el talle del calzado seleccionado en la variable global.
            talle_anzuelo_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){                    
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "talle_anzuelo_producto"){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "talle_anzuelo_producto"){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("talle_anzuelo_producto", response);

                    select_previo = "talle_anzuelo_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona una mano habil para el producto que se esta viendo
        $("#mano_habil_producto").on("change",function(){

            if( select_previo != "mano_habil_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }
            
            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "mano_habil_producto");

            //Guardo la mano habil seleccionada en la variable global.
            mano_habil_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "mano_habil_producto"){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "mano_habil_producto"){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("mano_habil_producto", response);

                    select_previo = "mano_habil_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona una longitud para el producto que se esta viendo
        $("#longitud_producto").on("change",function(){

            if( select_previo != "longitud_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }
            
            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "longitud_producto");

            //Guardo la longitud seleccionado en la variable global.
            longitud_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "longitud_producto"){
                        agregarTextoNoDisponible("longitud_producto", "longitud", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "longitud_producto"){
                        agregarTextoNoDisponible("longitud_producto", "longitud", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("longitud_producto", response);

                    select_previo = "longitud_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona una fuerza para el producto que se esta viendo
        $("#fuerza_producto").on("change",function(){

            if( select_previo != "fuerza_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }
            
            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "fuerza_producto");

            //Guardo la fuerza seleccionada en la variable global.
            fuerza_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "fuerza_producto"){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "fuerza_producto"){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("fuerza_producto", response);

                    select_previo = "fuerza_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona una accion para el producto que se esta viendo
        $("#accion_producto").on("change",function(){

            if( select_previo != "accion_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }

            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "accion_producto");

            //Guardo la accion seleccionada en la variable global.
            accion_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#diametro_producto') ){
                        agregarTextoNoDisponible("diametro_producto", "diametro", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "accion_producto"){
                        agregarTextoNoDisponible("accion_producto", "accion", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "accion_producto"){
                        agregarTextoNoDisponible("accion_producto", "accion", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("accion_producto", response);

                    select_previo = "accion_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });

        //Si selecciona un diametro para el producto que se esta viendo
        $("#diametro_producto").on("change",function(){

            if( select_previo != "diametro_producto"){
                cantidad_selects_seleccionados = cantidad_selects_seleccionados + 1;
            }

            resetearIdVarianteYCantidad();
            limpiarMensajesDeErrores();
            chequearOpcionSeleccionadaNoDisponible($(this).children("option:selected").text(), "diametro_producto");

            //Guardo el diametro seleccionado en la variable global.
            diametro_producto = $(this).children("option:selected").val();

            var token = '{{csrf_token()}}';
            $.ajax({
                url: '{{route("filtrarVariantes")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    color: color_producto,
                    talle_ropa: talle_ropa_producto,
                    talle_calzado: talle_calzado_producto,
                    talle_anzuelo: talle_anzuelo_producto,
                    mano_habil: mano_habil_producto,
                    longitud: longitud_producto,
                    fuerza: fuerza_producto,
                    accion: accion_producto,
                    diametro: diametro_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    //Chequeo en cada if si existen el select talles de calzado,manos habiles, etc.
                    if( existeElElemento('#color_producto') ){
                        agregarTextoNoDisponible("color_producto", "color", response);
                    }
                    if( existeElElemento('#talle_ropa_producto') ){
                        agregarTextoNoDisponible("talle_ropa_producto", "talle_ropa", response);
                    }
                    if( existeElElemento('#talle_calzado_producto') ){
                        agregarTextoNoDisponible("talle_calzado_producto", "talle_calzado", response);
                    }
                    if( existeElElemento('#talle_anzuelo_producto') ){
                        agregarTextoNoDisponible("talle_anzuelo_producto", "talle_anzuelo", response);
                    }
                    if( existeElElemento('#mano_habil_producto') ){
                        agregarTextoNoDisponible("mano_habil_producto", "mano_habil", response);
                    }
                    if( existeElElemento('#longitud_producto') ){
                        agregarTextoNoDisponible("longitud_producto", "longitud", response);
                    }
                    if( existeElElemento('#fuerza_producto') ){
                        agregarTextoNoDisponible("fuerza_producto", "fuerza", response);
                    }
                    if( existeElElemento('#accion_producto') ){
                        agregarTextoNoDisponible("accion_producto", "accion", response);
                    }

                    if(cantidad_selects_seleccionados > 1 && select_previo != "diametro_producto"){
                        agregarTextoNoDisponible("diametro_producto", "diametro", responses_previos[responses_previos.length-1][0]);
                    }
                    else if(cantidad_selects_seleccionados > 1 && select_previo == "diametro_producto"){
                        agregarTextoNoDisponible("diametro_producto", "diametro", responses_previos[responses_previos.length-2][0]);
                    }

                    responses_previos_actualizar("diametro_producto", response);

                    select_previo = "diametro_producto";
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });

            habilitar_o_deshabilitad_input_cantidad();
        });
        //Cuando se cambia el input donde se ingresa la cantidad a comprar o a agregar al carrito de una variante de producto.
        $("#cantidad_input").on("change",function(){
            //Si el valor ingresado es mayor al maximo permitido, lo dejo seteado en el maximo. (Mas que nada por las entradas del teclado)
            if(Number($(this).val()) > Number($(this).attr('max'))){
                $(this).val($(this).attr('max'));
            }
            if(Number($(this).val()) < Number($(this).attr('min'))){
                $(this).val(1);
            }
            $("#cantidad").val($("#cantidad_input").val());
        });

        //Si presiona el boton de agregar el producto al carrito.
        $(".btn-addProducto").on("click",function(){
            var compra = $(this).attr("data-id");
            //Chequeo si la cantidad es mayor a cero (es decir, hay stock y selecciono al menos cantidad 1). Con la variante miro si se identifico una variante unica.
            if(Number($("#cantidad").val()) > 0 && Number($("#id_variante_producto").val()) > 0){
                if ($("#productos_en_carro").val().includes( Number($("#id_variante_producto").val()) ) ) {
                    $('#modal_producto_ya_agregado').modal('toggle');
                } else {
                    if (compra == "true") {
                        $('#is_compra').val(true);
                        document.getElementById('add-producto').submit();
                    } else {
                        $('#is_compra').val(false);
                        document.getElementById('add-producto').submit();
                    }
                }
            }
            else{
                for(let i = 0; i < posibles_variantes.length; i++ ){
                    //Si no selecciono una opcion en alguno de los selects muestro un mensaje de error.
                    if($("#"+posibles_variantes[i]).children("option:selected").val() == 0){
                        $("#"+posibles_variantes[i]).addClass("is-invalid");
                        $("#mensaje_error_"+posibles_variantes[i]).text("Primero debe seleccionar una opción.");
                    }
                }
                //Si no hay stock muestro el texto del stock en rojo.
                if(stock == 0){
                    $("#stock").css("color", "red");
                }
            }
        });

        $("#btn_realizar_compra").on("click",function(){
            //Creo un json en donde se van a guardar el id de la variante, la cantidad de cada variante y el total
            data = {};
            items = [];

            let id_variante_producto = $("#id_variante_producto").val();
            let cantidad = Number($("#cantidad").val());
            let precio = Number($("#precio").text().substring(1));

            item = {}
            item ["id_variante_producto"] = id_variante_producto;
            item ["precio"] = precio;
            item ["cantidad"] = cantidad;

            //Voy almacenando cada item en el array de items
            items.push(item);

            
            //Guardo los items en el array de toda la informacion de la compra y seteo el subtotal.
            data['items'] = items;
            data['total_sin_envio'] = cantidad * precio;
            data['tipo_compra'] = 1;

            //Paso el Json con la informacion a un string.
            data = JSON.stringify(data);

            //Si la variante es diferente de 0 y la cantidad tambien es porque ya selecciono todo para poder comprar
            if(id_variante_producto != 0 && cantidad != 0){
                //Agrego un input con el id de la variante para usarlo en el backend.
                $("#realizar_compra").append("<input type='hidden' name='data' value='"+data+"'>");

                //Al obtener todos los productos seleccionados para la compra, con su total, lo enviamos al checkout
                document.getElementById('realizar_compra').submit();
            }
            else{
                for(let i = 0; i < posibles_variantes.length; i++ ){
                    //Si no selecciono una opcion en alguno de los selects muestro un mensaje de error.
                    if($("#"+posibles_variantes[i]).children("option:selected").val() == 0){
                        $("#"+posibles_variantes[i]).addClass("is-invalid");
                        $("#mensaje_error_"+posibles_variantes[i]).text("Primero debe seleccionar una opción.");
                    }
                }
                //Si no hay stock muestro el texto del stock en rojo.
                if(stock == 0){
                    $("#stock").css("color", "red");
                }
            }
        });
    </script>
@endsection