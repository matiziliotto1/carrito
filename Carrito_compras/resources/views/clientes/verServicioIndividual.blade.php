@extends('layouts.layout')

@section('content')
<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>Viendo {{$servicio->titulo}}</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{route('home')}}">Inicio</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>Servicio</li>
            </ul>
        </div>
    </div>
</div>
<!-- Page Content-->
<div class="container padding-bottom-3x">
    <div class="row">
        <!-- Poduct Gallery-->
        <div class="col-md-6">
            <div class="product-gallery">
                <div class="product-carousel owl-carousel gallery-wrapper">
                    @php
                        //Esta hash se usaba para cada div de los items de la galeria de imagenes, por lo tanto lo mantuve
                        $data_hash =  array(1 => "one", 2 => "two", 3 => "three", 4 => "four", 5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine");
                        $i=1;
                    @endphp

                    @if (count($servicio->imagenes)>0)
                        @foreach($servicio->imagenes as $imagen)
                            <div class="gallery-item" data-hash="{{$data_hash[$i]}}">
                                <a href="{{asset($imagen->url)}}" data-size="1000x667">
                                    <img src="{{asset($imagen->url)}}" alt="Servicio">
                                </a>
                            </div>
                            @php
                                $i = $i + 1;
                            @endphp
                        @endforeach
                    @else
                        <div class="gallery-item" data-hash="{{$data_hash[$i]}}">
                            <a data-size="1000x667">
                                <img src="{{asset('/img/imagenes_servicios/servicio_sin_imagen.png')}}" alt="Servicio">
                            </a>
                        </div>
                    @endif

                    
                </div>
                <ul class="product-thumbnails">
                    @php
                        $i = 1;
                    @endphp
                    @if (count($servicio->imagenes)>0)
                        @foreach($servicio->imagenes as $imagen)
                            <li class="@if ($loop->first)
                                active
                            @endif">
                                <a href="#{{$data_hash[$i]}}">
                                    <img src="{{asset($imagen->url)}}" alt="Product">
                                </a>
                            </li>
                            @php
                                $i = $i + 1;
                            @endphp
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <!-- Product Info-->
        <div class="col-md-6">
            <div class="padding-top-2x mt-2 hidden-md-up"></div>
            <div class="sp-categories pb-3">
                <i class="icon-tag"></i>
                <a href="{{route('verServiciosXCategoria', $servicio->categoria->id_categoria)}}">{{$servicio->categoria->nombre_categoria}}</a>
            </div>
            <h2 class="mb-3">{{$servicio->titulo}}</h2>
            <span class="d-none" id="precio">${{$servicio->precio}}</span>
            <span class="h3 d-block">
                ${{number_format($servicio->precio, 2, ",", ".")}}
            </span>
            <p class="text-muted">{{$servicio->descripcion_resumida}}
                <a href='#detalles' class='scroll-to'>Mas información</a>
            </p>
            <div class="row align-items-end pb-4">
                <div class="col-sm-6">
                    <a class="btn btn-primary btn-block m-0" id="btn_realizar_compra">Comprar</a>

                    <form id="realizar_compra" action="{{ route('nuevoPedido') }}" method="POST" style="display: none;">
                        @csrf
                            <input type="hidden" name="tipo_compra" value="0">
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="pt-4 hidden-sm-up"></div>
                    <a class="btn btn-primary btn-block m-0" id="btn-addServicio" style="cursor: pointer;">
                        <i class="icon-shopping-cart pr-2"></i><span>Añadir al carrito</span>
                    </a>
                        @php
                            if(Auth::check()){
                                $id_carro = App\Carrito_compra::where('id_usuario',Auth::user()->id)->first()->id_carrito;

                                $servicios_en_carro = DB::table('servicios')
                                    ->join('servicios_x_carritos', 'servicios_x_carritos.id_servicio', '=', 'servicios.id_servicio')
                                    ->where('servicios_x_carritos.id_carrito',$id_carro)
                                    ->select('servicios.id_servicio')
                                    ->get();
                            }
                        @endphp    
                        @if(Auth::check())
                            @if(count($servicios_en_carro) > 0 )
                                @foreach($servicios_en_carro as $servicio_en_carro)
                                    <input type="hidden" name="servicios_en_carro[]" id="servicios_en_carro" value="{{$servicio_en_carro->id_servicio}}">
                                @endforeach
                            @else
                                    <input type="hidden" name="servicios_en_carro[]" id="servicios_en_carro">
                            @endif
                        @else
                                <input type="hidden" name="servicios_en_carro[]" id="servicios_en_carro">
                        @endif
                    <form id="add-servicio" action="{{route('addServicioCarrito')}}" method="POST" style="display: none;">
                        @csrf
                        <input type="hidden" name="id_servicio" id="id_servicio"  value="{{$servicio->id_servicio}}">
                    </form>
                </div>
            </div>
        <hr class="mb-2">
        <div class="d-flex flex-wrap justify-content-between">
            <div id="div_carga">
                <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product Details-->
<div class="bg-secondary padding-top-3x padding-bottom-2x mb-3" id="detalles">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h3 class="h4">Detalles del servicio</h3>
                <p class="mb-4">
                    {!! nl2br(e($servicio->descripcion)) !!}
                </p>
            </div>
        </div>
    </div>
</div>

<div class="container padding-bottom-3x mb-1">             
    <!-- Related Products Carousel-->
    <h3 class="text-center padding-top-2x mt-2 padding-bottom-1x">Tambien te puede gustar</h3>
    <!-- Carousel-->
    <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
        @foreach($servicios_que_pueden_gustar as $servicio)
            <div class="product-card">
                <a class="product-thumb" style="height: 30%; background-image: url('{{asset($servicio->imagen)}}'); background-repeat: no-repeat; background-position: center; background-size: 100% 100%;" href="{{route("verServicioC",$servicio->id_servicio)}}">
                </a>
                <div class="product-card-body">
                    <div class="product-category">
                        <a href="{{route("verServiciosXCategoria",$servicio->categoria->id_categoria)}}">{{$servicio->categoria->nombre_categoria}}</a>
                    </div>
                    <h3 class="product-title"><a href="{{route("verServicioC",$servicio->id_servicio)}}">{{$servicio->Titulo}}</a></h3>
                    <h4 class="product-price">
                        ${{number_format($servicio->precio, 2, ",", ".")}}
                    </h4>
                </div>
                <div class="product-button-group">
                    <a class="product-button" href="{{route('verServicioC',$servicio->id_servicio)}}">
                        Ver servicio
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

<!-- Photoswipe container-->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="pswp__bg"></div>
    <div class="pswp__scroll-wrap">
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
            <div class="pswp__top-bar">
                <div class="pswp__counter"></div>
                <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
                <button class="pswp__button pswp__button--share" title="Share"></button>
                <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
                <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                        <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div>
            </div>
            <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
            <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>
        </div>
    </div>
</div>

<!-- Default Modal-->
<div class="modal fade" id="modal_servicio_ya_agregado" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #FFED00 !important">
                <h4 class="modal-title">Servicio ya agregado</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <p>Este servicio ya se encuentra en el carrito.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-outline-primary btn-sm" type="button" data-dismiss="modal">Aceptar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $("#btn-addServicio").on("click",function(){
            if ($("#servicios_en_carro").val().includes( Number($("#id_servicio").val()) ) ) {
                $('#modal_servicio_ya_agregado').modal('toggle');
            } else {
                document.getElementById('add-servicio').submit();
            }
        });

        $("#btn_realizar_compra").on("click",function(){
            //Creo un json en donde se van a guardar el id del servicio, la cantidad y el total
            data = {};
            items = [];

            let id_servicio = $("#id_servicio").val();
            let precio = Number($("#precio").text().substring(1));

            item = {}
            item ["id_servicio"] = id_servicio;
            item ["precio"] = precio;

            //Almaceno el item en el array de items
            items.push(item);

            data['items'] = items;
            data['total_servicios'] = precio;
            data['tipo_compra'] = 0;

            //Paso el Json con la informacion a un string.
            data = JSON.stringify(data);

            //Agrego un input con el id de la variante para usarlo en el backend.
            $("#realizar_compra").append("<input type='hidden' name='data' value='"+data+"'>");

            //Al obtener todos los servicios seleccionados para la compra, con su total, lo enviamos al checkout
            document.getElementById('realizar_compra').submit();
        });
    </script>
@endsection