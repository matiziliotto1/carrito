@php
    $niv2_envio_gratis = Config::get('puntajes.beneficios.nivel_2');
    $niv3_envio_gratis_y_5_off = Config::get('puntajes.beneficios.nivel_3');
    $niv4_envio_gratis_y_10_off = Config::get('puntajes.beneficios.nivel_4');
    $niv5_15_off = Config::get('puntajes.beneficios.nivel_5');

    $niv2_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_2');
    $niv3_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_3');
    $niv4_monto_compra = Config::get('puntajes.monto_compra_para_beneficios.nivel_4');

    $envio_gratis = false;
    
    //Si esta entre los niveles que se obtienen envios gratis y el monto de la compra cumple con el esperado para otorgar el envio gratis para ese nivel.
    if($puntaje_acumulado >= $niv2_envio_gratis && $puntaje_acumulado < $niv3_envio_gratis_y_5_off  && $total_sin_envio >= $niv2_monto_compra){
        $envio_gratis = true;
    }
    else if($puntaje_acumulado >= $niv3_envio_gratis_y_5_off && $puntaje_acumulado < $niv4_envio_gratis_y_10_off  && $total_sin_envio >= $niv3_monto_compra){
        $envio_gratis = true;
    }
    else if($puntaje_acumulado >= $niv4_envio_gratis_y_10_off && $puntaje_acumulado < $niv5_15_off  && $total_sin_envio >= $niv4_monto_compra){
        $envio_gratis = true;
    }
@endphp
@extends('layouts.layout')

@section('content')
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        <div class="row">
            <!-- Checkout Adress-->
            <div class="col-xl-11 col-lg-11 mx-auto">
                <h4>Seleccionar método de envío</h4>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <tbody>
                            @if($metodos_de_envio[0])
                                <tr>
                                    <td class="align-middle">
                                        <div class="custom-control custom-radio mb-0">
                                        <input class="custom-control-input" type="radio" id="envio_domicilio" name="shipping-method" checked>
                                        <label class="custom-control-label" for="envio_domicilio"></label>
                                        </div>
                                    </td>
                                    <td class="align-middle">
                                        <span class="text-gray-dark">Normal a domicilio</span><small class="text-muted">&nbsp; (Llega en {{$metodos_de_envio[0]['PlazoEntrega']}} días hábiles) </small>
                                        @if($envio_gratis)
                                            <br>
                                            <span class="text-muted text-sm">Beneficio de puntaje acumulado en Mercado Fishing</span>
                                        @endif
                                    </td>
                                    <td class="align-middle text-center">
                                        @if($envio_gratis)
                                            <span style="color:green">Envío gratis</span>
                                        @else
                                            ${{number_format($metodos_de_envio[0]['Total'], 2, ",", ".")}}
                                        @endif
                                    </td>
                                    <input type="hidden" value="{{$metodos_de_envio[0]['Total']}}" id="costo_envio_domicilio">
                                </tr>
                            @endif
                            <tr>
                                <td class="align-middle">
                                    <div class="custom-control custom-radio mb-0">
                                        <input class="custom-control-input" type="radio" id="retiro_sucursal" name="shipping-method">
                                        <label class="custom-control-label" for="retiro_sucursal"></label>
                                    </div>
                                </td>
                                <td class="align-middle">
                                    <span class="text-gray-dark">Retiro por sucursal</span>
                                </td>
                                <td class="align-middle text-center" style="color:green">Sin costo</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="shopping-cart-footer">
                    <div class="column">
                        <a class="btn btn-outline-secondary" href="{{route("verCarritoDeCompras")}}">
                            <i class="icon-shopping-cart"></i>&nbsp;Ir al carrito
                        </a>
                    </div>
                    <div class="column">
                        <form id="btn_nuevo_pedido" action="{{ route('confirmarPedido', $id_pedido) }}" method="GET">
                            <input type="hidden" value="1" id="metodo_envio" name="metodo_envio">
                            {{-- //TODO: el costo del envio debe ser recibido de la funcion que calcula el costo --}}
                            <input type="hidden" 
                                @if($envio_gratis) 
                                    value="0" 
                                @else
                                    value="{{$metodos_de_envio[0]['Total']}}"
                                @endif 
                                id="costo_envio" name="costo_envio">

                            <button type="submit" class="btn btn-primary">Seleccionar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(".custom-control-input").on("click",function(){
            if(this.id == "envio_domicilio"){
                $("#metodo_envio").val("1");
                $("#costo_envio").val($("#costo_envio_domicilio").val());
            }
            else if(this.id == "retiro_sucursal"){
                $("#metodo_envio").val("0");
                $("#costo_envio").val("0");
            }
        });
    </script>
@endsection