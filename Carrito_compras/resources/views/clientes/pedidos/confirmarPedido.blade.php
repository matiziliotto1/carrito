@php
    // Creo la preferencia usando el metodo del controlador de mercado pago.
    $mercadopago_controller = new App\Http\Controllers\MercadoPagoController();
    if(!isset($costo_envio)){
        $costo_envio = 0;
    }
    if(!isset($descuento)){
        $descuento = 0;
    }
    $preference = $mercadopago_controller->crearPreferencia(Request::route('id_pedido'), $costo_envio, $descuento);
    
    # Save and POST preference
    $preference->save();
@endphp

@extends('layouts.layout')

<style>
    #form-mercadopago > button{
        margin-right: 0;
        margin-left: 15px;

        background-color: #05f;
        display: inline-block;
        position: relative;
        height: 46px;
        margin-top: 8px;
        margin-bottom: 8px;
        padding: 0 22px;
        transform: translateZ(0);
        transition: all .4s;
        border: 1px solid transparent;
        border-radius: 5px;
        background-image: none;
        font-family: "Rubik",Helvetica,Arial,sans-serif;
        font-size: 14px;
        font-style: normal;
        font-weight: 400 !important;
        letter-spacing: .025em;
        line-height: 44px;
        white-space: nowrap;
        cursor: pointer;
        vertical-align: middle;
        text-transform: none;
        text-decoration: none;
        text-align: center;
        touch-action: manipulation;
        user-select: none;
    }
</style>

@section('content')
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        <div class="row">
            <!-- Checkout Adress-->
            <div class="col-xl-11 col-lg-11 mx-auto">
                <h4><strong>Revisa tu compra</strong></h4>
                <div class="table-responsive shopping-cart">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td>@if(isset($productos)) Producto/s @elseif(isset($servicios)) Servicio/s @endif</td>
                                <td class="text-center">@if(isset($productos)) Precio x unidad @elseif(isset($servicios)) Precio @endif</td>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($productos))
                                @foreach ($productos as $producto)
                                    <tr>
                                        <td>
                                            <div class="product-item">
                                                <a class="product-thumb">
                                                    <img src="{{ asset($producto->imagen) }}" alt="{{ $producto->nombre }}">
                                                </a>
                                                <div class="product-info">
                                                    <h4 class="product-title">
                                                        <a class="no-hover">
                                                            {{ $producto->nombre }}<small>x {{ $producto->cantidad }}</small>
                                                        </a>
                                                    </h4>
                                                    {{-- TODO: esto se cambia porque ahora hay 9 tipos de variantes, no solo 2. --}}
                                                    <span><em>Color:</em> {{$producto->color}}</span>
                                                    @if($producto->mano_habil == 1 || $producto->mano_habil == 2)
                                                        <span><em>Mano hábil:</em> @if($producto->mano_habil == 1) Derecha @elseif($producto->mano_habil == 2) Izquierda @endif</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center text-lg">${{number_format($producto->precio, 2, ",", ".")}}</td>
                                    </tr>
                                @endforeach
                            @endif

                            @if(isset($servicios))
                                @foreach ($servicios as $servicio)
                                    <tr>
                                        <td>
                                            <div class="product-item">
                                                <a class="product-thumb">
                                                    <img src="{{ asset($servicio->imagen) }}" alt="{{ $servicio->titulo }}">
                                                </a>
                                                <div class="product-info">
                                                    <h4 class="product-title">
                                                        <a class="no-hover">
                                                            {{ $servicio->titulo }}
                                                        </a>
                                                    </h4>
                                                    <span>{{$servicio->descripcion_resumida}}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center text-lg">${{number_format($servicio->precio, 2, ",", ".")}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                <div class="shopping-cart-footer">
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Checkout Adress-->
            <div class="col-xl-11 col-lg-11 mx-auto">
                @if(isset($metodo_envio) && isset($costo_envio))
                    <h4><strong>Método de envío</strong></h4>
                    <div class="table-responsive shopping-cart">
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <td class="align-middle">
                                        @if ($metodo_envio == "1")
                                            <span class="text-gray-dark">
                                                Envío a domicilio
                                            </span>
                                        @elseif ($metodo_envio == "0")
                                            <span class="text-gray-dark">
                                                Retiro en sucursal
                                            </span>
                                        @endif
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                    <td class="align-middle text-center text-lg">
                                        @if ($metodo_envio == "1")
                                            @if($costo_envio == "0")
                                                <span class="" style="color:green">Envío gratis</span>
                                            @else
                                                ${{number_format($costo_envio, 2, ",", ".")}}
                                            @endif
                                        @elseif ($metodo_envio == "0")
                                            <span class="" style="color:green">Sin costo</span>
                                        @endif
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                @endif
                <div class="shopping-cart-footer">
                    <div class="column">
                        @if(isset($beneficio))
                            @if($beneficio != "")
                                <span class="text-sm" style="color:green">
                                    Beneficio de <strong>{{$beneficio}}</strong> por puntaje acumulado en Mercado Fishing
                                </span>
                            @endif
                        @endif
                    </div>
                    <div class="column text-lg">
                        <span class="text-gray-dark"><strong>Total:&nbsp; </strong></span>
                        <span class="text-gray-dark">${{number_format($total, 2, ",", ".")}}</span>
                    </div>
                </div>
                <div class="d-flex justify-content-between paddin-top-1x mt-4">
                    <a class="btn btn-outline-secondary" 
                    @if($tipo_compra == 1) href="{{route('seleccionarMetodoEnvio', Request::route('id_pedido'))}}" @elseif($tipo_compra == 0) href="{{route('verCarritoDeCompras')}}" @endif>
                        <i class="icon-arrow-left"></i><span class="hidden-xs-down">&nbsp;Regresar</span>
                    </a>
                    <form action="{{route('procesarPago')}}" method="POST" id="form-mercadopago">
                        @csrf
                        <script
                            src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
                            data-preference-id="<?php echo $preference->id; ?>" data-button-label="Realizar pago">
                        </script>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection