@php
    $precio_minimo = App\Servicio::min('precio');
    $precio_maximo = App\Servicio::max('precio');    
@endphp
@extends('layouts.layout')

@section('content')

<!-- Page Content-->
<div class="container padding-bottom-3x mb-1">
    <div class="row">
        <!-- Products-->
        <div class="col-lg-9 order-lg-2">  
            <!-- Shop Toolbar-->
            <div class="shop-toolbar padding-bottom-1x mb-2">
                <div class="column">
                    <div class="shop-sorting">
                        <label for="ordenar_servicios">Ordenar por:</label>
                        <select class="form-control" id="ordenar_servicios">
                            <option value="0" disabled @if ($ordenar_por==0) selected @endif>Seleccione una opción</option>
                            <option value="1" @if ($ordenar_por==1) selected @endif>Menor precio</option>
                            <option value="2" @if ($ordenar_por==2) selected @endif>Mayor precio</option>
                            <option value="3" @if ($ordenar_por==3) selected @endif>Orden alfabetico (ascendente)</option>
                            <option value="4" @if ($ordenar_por==4) selected @endif>Orden alfabetico (descendente)</option>
                        </select>
                        <form method="GET" id="form_ordenar_por" action="{{route("filtrarServicios")}}">
                            <input type="hidden" id="ordenar_por" value="{{$ordenar_por}}" name="ordenar_por">
                            <input type="hidden" name="id_categoria" value="{{$id_categoria}}">
                            <input type="hidden" name="id_subcategoria" value="{{$id_subcategoria}}">
                            <input type="hidden" name="texto" value="{{$texto}}">
                            <input type="hidden" name="precio_min" value="{{$precio_min}}">
                            <input type="hidden" name="precio_max" value="{{$precio_max}}">
                        </form>
                    </div>
                </div>
                <div class="column">
                </div>
            </div>
            <!-- Products Grid-->

            @if(count($servicios) > 0)
                @foreach($servicios as $servicio)
                    <div class="product-card product-list mb-30">
                        <a class="product-thumb" style="height: 150px; background-image: url('{{asset($servicio->imagen)}}'); background-repeat: no-repeat; background-position: center; background-size: 100% 100%;" href="{{route('verServicioC', $servicio->id_servicio)}}">
                        </a>
                        <div class="product-card-inner">
                            <div class="product-card-body">
                                <div class="product-category">
                                    <a href="{{route('verServiciosXCategoria', $servicio->id_categoria)}}">{{$servicio->nombre_categoria}}</a>
                                </div>
                                <h3 class="product-title">
                                    <a href="{{route('verServicioC', $servicio->id_servicio)}}">{{$servicio->titulo}}</a>
                                </h3>
                                <h4 class="product-price">
                                    ${{number_format($servicio->precio, 2, ",", ".")}}
                                </h4>
                                <p class="text-sm text-muted hidden-xs-down my-1">{{$servicio->descripcion_resumida}}</p>
                            </div>
                            <div class="product-button-group">
                                <a class="product-button" href="{{route('verServicioC',$servicio->id_servicio)}}">
                                    Ver servicio
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @elseif(count($servicios) == 0 && !request()->get('texto'))
                <h3 class="p-3 mx-auto">Aún no hay servicios.</h3>
            @elseif(count($servicios) == 0 && request()->get('texto'))
                <h3 class="p-3 mx-auto">No encontramos servicios para: <strong>{{request()->get('texto')}}</strong>.</h3>
            @endif

            {{ $servicios->links("clientes.pagination.paginationProductos") }}
        </div>
        <!-- Sidebar-->
        <div class="col-lg-3 order-lg-1">
            <div class="sidebar-toggle position-left">
                <i class="icon-filter"></i>
            </div>
            <aside class="sidebar sidebar-offcanvas position-left">
                <span class="sidebar-close">
                    <i class="icon-x"></i>
                </span>
                @if(request()->get('texto'))
                    <section class="">
                        <h3 class="widget-title">Resultados para: <strong>{{request()->get('texto')}}</strong></h3>
                    </section>
                @endif
                <!-- Widget Categories-->
                <section class="widget widget-categories">
                    <h3 class="widget-title">Categorias</h3>
                    <ul>
                        @foreach($categorias as $categoria)
                            <li class="has-children @if(Request::route('id_categoria') == $categoria->id_categoria || $categoria->subcategorias->contains('id_subcategoria',Request::route('id_subcategoria'))) expanded active @endif">
                                <a href="#">{{$categoria->nombre_categoria}}</a>
                                <ul>
                                    @foreach($categoria->subcategorias as $subcategoria)
                                        <li class="@if(Request::route('id_subcategoria') == $subcategoria->id_subcategoria) active @endif">
                                            <a href="{{route('verServiciosXSubcategoria', $subcategoria->id_subcategoria)}}">{{$subcategoria->nombre_subcategoria}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </section>
                <!-- Widget Price Range-->
                <section class="widget widget-categories">
                    <form class="price-range-slider" action="{{route("filtrarServicios")}}" method="get" data-start-min="{{$precio_min}}" data-start-max="{{$precio_max}}" data-min="{{$precio_minimo}}" data-max="{{$precio_maximo}}" data-step="1">
                            <input type="hidden" name="id_categoria" value="{{$id_categoria}}">
                            <input type="hidden" name="id_subcategoria" value="{{$id_subcategoria}}">
                            <input type="hidden" name="texto" value="{{$texto}}">
                            <input type="hidden" id="ordenar_por" value="{{$ordenar_por}}" name="ordenar_por">
                        <h3 class="widget-title">Rango de precios</h3>
                        <div class="ui-range-slider">
                        </div>
                        <div class="ui-range-slider-footer">
                            <div class="column">
                                <div class="ui-range-values">
                                    <div class="ui-range-value-min">
                                        $<span></span>
                                        <input type="hidden" name="precio_min">
                                    </div>&nbsp;-&nbsp;
                                    <div class="ui-range-value-max">
                                        $<span></span>
                                        <input type="hidden" name="precio_max">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <button class="btn btn-outline-primary btn-sm" type="submit">Filtrar</button>
                        </div>
                    </form>
                </section>
            </aside>
        </div>
    </div>
</div>
    
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#ordenar_servicios").on("change",function(){
            var ordenar_por = $(this).children("option:selected").val();
            $("#ordenar_por").val(""+ordenar_por);
            $("#form_ordenar_por").submit();
        });
    </script>
    <!-- //TODO: si se recarga la pagina da error porque se borran todos los parametros
    <script>
        window.history.replaceState(null, null, window.location.pathname);
    </script>
    -->
@endsection