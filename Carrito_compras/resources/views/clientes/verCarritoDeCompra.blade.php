@extends('layouts.layout')

@section('content')
<!-- Page Content-->
<div class="container padding-bottom-3x mb-1">
    <!-- Alert-->
    @if(session('mensaje_alerta_ok'))
        <div class="alert 
            @if(session('mensaje_alerta_ok')) 
                alert-success 
            @else
                alert-danger
            @endif 
            alert-dismissible fade show text-center margin-bottom-1x">

            <span class="alert-close" data-dismiss="alert"></span>
            <i class="icon-check-circle"></i>&nbsp;&nbsp; 
            {{ session('mensaje') }}
        </div>
    @endif

    @php
        session()->forget('mensaje_alerta_ok');
        session()->forget('mensaje');
    @endphp

    <!-- Page Content-->
    <div class="container padding-bottom-1x">
        <div class="row">
            <div class="col-lg-12 col-md-12 order-md-2">
                <h6 class="text-muted text-lg text-uppercase">Carrito de compras</h6>
                <hr class="margin-bottom-1x pb-2">
                <div class="steps flex-sm-nowrap">
                    <a class="step active" href="#" id="solapa_productos">
                        <h4 class="step-title">Productos <small>({{$cantidad_productos}})</small></h4>
                    </a>
                    <a class="step" href="#" id="solapa_servicios">
                        <h4 class="step-title">Servicios <small>({{$cantidad_servicios}})</small></h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
    *Debe seleccionar los productos que desea comprar (solo puede seleccionar productos hasta completar un unico paquete para el envío).
    <!-- Shopping Cart-->
    <div class="" id="tabla_productos">
        <div class="table-responsive shopping-cart">

            {{--Mensaje de error en caso de que no seleccione ningun producto para comprar --}}
            <div class="d-none alert alert-danger alert-dismissible fade show text-center margin-bottom-1x" id="error_seleccionar_algun_producto">
                <i class="icon-slash"></i> 
                <span id="texto_error_seleccionar_algun_producto">

                </span>
            </div>
            {{--Mensaje de error en caso de que seleccione productos que superen un paquete de envio --}}
            <div class="d-none alert alert-warning alert-dismissible fade show text-center margin-bottom-1x" id="error_seleccionar_menos_productos">
                <i class="icon-slash"></i> 
                <span id="texto_error_seleccionar_menos_productos">

                </span>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Productos</th>
                        <th class="text-center">Cantidad</th>
                        <th class="text-center">Precio <small>x unidad</small></th>
                        <th class="text-center">Comprar</th>
                        <th class="text-center">
                            <a class="btn btn-sm btn-outline-danger" href="{{route('vaciarCarritoProductos')}}" 
                                onclick="event.preventDefault();
                                document.getElementById('vaciarCarritoProductos').submit();">
                                Vaciar carrito
                            </a>
                            <form id="vaciarCarritoProductos" action="{{ route('vaciarCarritoProductos') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total = 0;
                    @endphp
                    @if(count($productos_en_carrito)>0)
                        @foreach($productos_en_carrito as $producto)
                            <tr>
                                <td>
                                    <input type="hidden" id="peso_{{$producto->id_variante_producto}}" value="{{$producto->peso}}">
                                    <input type="hidden" id="alto_{{$producto->id_variante_producto}}" value="{{$producto->alto}}">
                                    <input type="hidden" id="ancho_{{$producto->id_variante_producto}}" value="{{$producto->ancho}}">
                                    <input type="hidden" id="largo_{{$producto->id_variante_producto}}" value="{{$producto->largo}}">

                                    <div class="product-item">
                                        <a class="product-thumb" href="{{route("verProductoC",$producto->id_producto)}}">
                                            <img src="{{asset($producto->imagen)}}" alt="Producto">
                                        </a>
                                        <div class="product-info">
                                            <h4 class="product-title">
                                                <a href="{{route("verProductoC",$producto->id_producto)}}">{{$producto->nombre}}</a>
                                            </h4>
                                            <span>
                                                {{$producto->color}} @if($producto->mano_habil == 1) - Derecha @elseif($producto->mano_habil == 2) - Izquierda @endif
                                            </span>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="count-input">
                                        <div class="form-group mb-0">
                                            <input type="number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" 
                                            class="form-control cantidad" name="cantidad{{$producto->id_variante_producto}}" 
                                            id="cantidad{{$producto->id_variante_producto}}" value="{{$producto->cantidad}}" 
                                            min="1" max="{{$producto->stock}}" data-id="{{$producto->id_variante_producto}}" data-cantidad_previa="{{$producto->cantidad}}">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-lg">
                                    <span class="d-none" id="precio_producto{{$producto->id_variante_producto}}">${{$producto->precio}}</span>
                                    ${{number_format($producto->precio, 2, ",", ".")}}
                                </td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input producto_seleccionado" type="checkbox" name="producto_seleccionado{{$producto->id_variante_producto}}" id="producto_seleccionado{{$producto->id_variante_producto}}" data-id="{{$producto->id_variante_producto}}">
                                        <label class="custom-control-label" for="producto_seleccionado{{$producto->id_variante_producto}}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a class="remove-from-cart" href="{{route('deleteProductoCarrito')}}" data-toggle="tooltip" title="Eliminar del carrito"
                                    onclick="event.preventDefault();
                                    document.getElementById('deleteProductoCarrito{{$producto->id_variante_producto}}').submit();">
                                        <i class="icon-x"></i>
                                    </a>
                                    <form id="deleteProductoCarrito{{$producto->id_variante_producto}}" action="{{ route('deleteProductoCarrito') }}" method="POST" style="display: none;">
                                        @csrf
                                        <input type="hidden" name="id_variante_producto" value="{{$producto->id_variante_producto}}">
                                    </form>
                                </td>
                            </tr>
                            @php
                                $total = $total + $producto->precio * $producto->cantidad;
                            @endphp
                        @endforeach
                    @endif
                    @if(count($productos_en_carrito) < 1)
                        <tr>
                            <td colspan="5">Aún no agregaste ningun producto al carrito. Mira nuestros <a class="navi-link" href="{{route("getProductosC")}}">productos</a> que tenemos para ofrecerte</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(count($productos_en_carrito) > 0 )
            <div class="shopping-cart-footer">
                <div class="column text-lg">
                    <span class="text-muted">Total (sin envío):&nbsp; 
                        <span class="d-none" id="total_sin_envio">$0</span>
                    </span>
                    <span class="text-gray-dark" id="total_sin_envio_formateado">@php echo "$".number_format(0, 2, ",", "."); @endphp</span>
                </div>
            </div>
        @endif
        <div class="shopping-cart-footer">
            <div class="column">
                <a class="btn btn-outline-secondary" href="{{route("getProductosC")}}">
                    <i class="icon-arrow-left"></i>&nbsp;Ver mas productos
                </a>
            </div>
            <div class="column">
                @if(count($productos_en_carrito) > 0)
                    <a class="btn btn-primary" id="btn_realizar_compra_productos">Comprar producto/s</a>

                    <form id="realizar_compra_productos" action="{{ route('nuevoPedido') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endif
            </div>
        </div>
    </div>

    <!-- Shopping Cart-->
    <div class="d-none" id="tabla_servicios">
        <div class="table-responsive shopping-cart">

            {{--Mensaje de error en caso de que no seleccione ningun servicio para comprar --}}
            <div class="d-none alert alert-danger alert-dismissible fade show text-center margin-bottom-1x" id="error_seleccionar_algun_servicio">
                <span class="alert-close" data-dismiss="alert"></span>
                <i class="icon-slash"></i> 
                <span id="texto_error_seleccionar_algun_servicio">

                </span>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Servicios</th>
                        <th class="text-center">Precio</th>
                        <th class="text-center">Comprar</th>
                        <th class="text-center">
                            <a class="btn btn-sm btn-outline-danger" href="{{route('vaciarCarritoServicios')}}" 
                                onclick="event.preventDefault();
                                document.getElementById('vaciarCarritoServicios').submit();">
                                Vaciar carrito
                            </a>
                            <form id="vaciarCarritoServicios" action="{{ route('vaciarCarritoServicios') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $total_servicios = 0;
                    @endphp
                    @if(count($servicios_en_carrito)>0)
                        @foreach($servicios_en_carrito as $servicio)
                            <tr>
                                <td>
                                    <div class="product-item">
                                        <a class="product-thumb" href="{{route("verServicioC",$servicio->id_servicio)}}">
                                            <img src="{{asset($servicio->imagen)}}" alt="Servicio">
                                        </a>
                                        <div class="product-info">
                                            <h4 class="product-title">
                                                <a href="{{route("verServicioC",$servicio->id_servicio)}}">{{$servicio->titulo}}</a>
                                            </h4>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center text-lg">
                                    <span class="d-none" id="precio_servicio{{$servicio->id_servicio}}">${{$servicio->precio}}</span>
                                    ${{number_format($servicio->precio, 2, ",", ".")}}
                                </td>
                                <td class="text-center">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input servicio_seleccionado" type="checkbox" name="servicio_seleccionado{{$servicio->id_servicio}}" id="servicio_seleccionado{{$servicio->id_servicio}}" checked data-id="{{$servicio->id_servicio}}">
                                        <label class="custom-control-label" for="servicio_seleccionado{{$servicio->id_servicio}}"></label>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a class="remove-from-cart" href="{{route('deleteServicioCarrito')}}" data-toggle="tooltip" title="Eliminar del carrito"
                                    onclick="event.preventDefault();
                                    document.getElementById('deleteServicioCarrito{{$servicio->id_servicio}}').submit();">
                                        <i class="icon-x"></i>
                                    </a>
                                    <form id="deleteServicioCarrito{{$servicio->id_servicio}}" action="{{ route('deleteServicioCarrito') }}" method="POST" style="display: none;">
                                        @csrf
                                        <input type="hidden" name="id_servicio" value="{{$servicio->id_servicio}}">
                                    </form>
                                </td>
                            </tr>
                            @php
                                $total_servicios = $total_servicios + $servicio->precio;
                            @endphp
                        @endforeach
                    @endif
                    
                    @if(count($servicios_en_carrito)<1)
                        <tr>
                            <td colspan="5">Aún no agregaste ningun servicio al carrito. Mira nuestros <a class="navi-link" href="{{route("getServiciosC")}}">servicios</a> que tenemos para ofrecerte</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
        @if(count($servicios_en_carrito)>0)
            <div class="shopping-cart-footer">
                <div class="column text-lg">
                    <span class="text-muted">Total:&nbsp; 
                        <span class="d-none" id="total_servicios">${{$total_servicios}}</span>
                    </span>
                    <span class="text-gray-dark" id="total_servicios_formateado">${{number_format($total_servicios, 2, ",", ".")}}</span>
                </div>
            </div>
        @endif
        <div class="shopping-cart-footer">
            <div class="column">
                <a class="btn btn-outline-secondary" href="{{route("getServiciosC")}}">
                    <i class="icon-arrow-left"></i>&nbsp;Ver mas servicios
                </a>
            </div>
            <div class="column">
                @if(count($servicios_en_carrito)>0)
                    <a class="btn btn-primary" id="btn_realizar_compra_servicios">Comprar servicio/s</a>

                    <form id="realizar_compra_servicios" action="{{ route('nuevoPedido') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endif
            </div>
        </div>
    </div>

    <!-- Related Products Carousel-->
    <h3 class="text-center padding-top-2x padding-bottom-1x">Tambien te puede gustar</h3>
    <!-- Carousel-->
    <div class="owl-carousel" data-owl-carousel="{ &quot;nav&quot;: false, &quot;dots&quot;: true, &quot;margin&quot;: 30, &quot;responsive&quot;: {&quot;0&quot;:{&quot;items&quot;:1},&quot;576&quot;:{&quot;items&quot;:2},&quot;768&quot;:{&quot;items&quot;:3},&quot;991&quot;:{&quot;items&quot;:4},&quot;1200&quot;:{&quot;items&quot;:4}} }">
        @foreach($productos_que_pueden_gustar as $producto)
            <div class="product-card">
                <a class="product-thumb" style="height: 30%; background-image: url('{{asset($producto->imagen)}}'); background-repeat: no-repeat; background-position: center; background-size: 100% 100%;" href="{{route("verProductoC",$producto->id_producto)}}">
                </a>
                <div class="product-card-body">
                    <div class="product-category">
                        <a href="{{route("verProductosXCategoria",$producto->categoria->id_categoria)}}">{{$producto->categoria->nombre_categoria}}</a>
                    </div>
                    <h3 class="product-title"><a href="{{route("verProductoC",$producto->id_producto)}}">{{$producto->nombre}}</a></h3>
                    <h4 class="product-price">
                        ${{number_format($producto->precio, 2, ",", ".")}}
                    </h4>
                </div>
                <div class="product-button-group">
                    <a class="product-button" href="{{route('verProductoC',$producto->id_producto)}}">
                        Ver producto
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var peso_total = 0; // Peso en Kilogramos
        var dimensiones_totales = 0; // Dimensiones en centimetros

        $(".cantidad").on("change",function(){
            //Obtengo el id del producto al que se le cambio la cantidad
            var this_cant = $(this);

            var id_variante_producto_cambiado = this_cant.attr("data-id");
            var cant_actual = this_cant.val();
            var cant_previa = this_cant.attr("data-cantidad_previa");

            //Si el valor ingresado es mayor al maximo permitido, lo dejo seteado en el maximo. (Mas que nada por las entradas del taclado)
            if(Number(cant_actual) > Number(this_cant.attr('max'))){
                this_cant.val(this_cant.attr('max'));
            }
            if(Number(cant_actual) < Number(this_cant.attr('min'))){
                this_cant.val(1);
            }

            //Calculo el nuevo subtotal para actualizarlo dinamicamente y mostrar el subtotal que corresponde
            var total_sin_envio = 0;
            peso_total = 0;
            dimensiones_totales = 0;
            $('.cantidad').each(function(i, obj) {
                var id_variante_producto = $(this).attr("data-id");

                //Para sumar al total, el producto debe estar seleccionado para comprar
                if($("#producto_seleccionado"+id_variante_producto).prop("checked") == true){
                    var cantidad_aux = Number($(this).val());
                    var precio_aux = Number($("#precio_producto"+id_variante_producto).text().substring(1));

                    total_sin_envio = total_sin_envio + precio_aux * cantidad_aux;

                    let peso_producto = Number($("#peso_"+id_variante_producto).val());
                    let alto_producto = Number($("#alto_"+id_variante_producto).val());
                    let largo_producto = Number($("#largo_"+id_variante_producto).val());
                    let ancho_producto = Number($("#ancho_"+id_variante_producto).val());

                    peso_total += (peso_producto / 1000) * cantidad_aux;
                    dimensiones_totales += (alto_producto + largo_producto + ancho_producto) * cantidad_aux;
                }
            });

            if(mayorAUnPaquete(peso_total, dimensiones_totales)){
                cant_aux = Math.abs(cant_actual-cant_previa);

                total_sin_envio = total_sin_envio - Number($("#precio_producto"+id_variante_producto_cambiado).text().substring(1)) * cant_aux;

                let peso_producto = Number($("#peso_"+id_variante_producto_cambiado).val());
                let alto_producto = Number($("#alto_"+id_variante_producto_cambiado).val());
                let largo_producto = Number($("#largo_"+id_variante_producto_cambiado).val());
                let ancho_producto = Number($("#ancho_"+id_variante_producto_cambiado).val());

                peso_total -= (peso_producto / 1000) * cant_aux;
                dimensiones_totales -= (alto_producto + largo_producto + ancho_producto) * cant_aux;
                
                this_cant.val(cant_previa);
                errorMayorAUnPaquete(true);
            }
            else{
                if($("#producto_seleccionado"+id_variante_producto_cambiado).prop("checked") == true){
                    errorMayorAUnPaquete(false);
                }
            }
            actualizarTotales(total_sin_envio);
            
            this_cant.attr("data-cantidad_previa", this_cant.val());
        });

        function actualizarTotales(total_sin_envio) {
            let total_sin_envio_formateado = separadorDeMiles(total_sin_envio);

            $("#total_sin_envio").text("$"+total_sin_envio);
            $("#total_sin_envio_formateado").text("$"+total_sin_envio_formateado);
        }

        $(".producto_seleccionado").on("change",function(){
            //Obtengo el id del producto al que se le cambio la cantidad, el subtotal actual, la cantidad y el precio de la fila que ocasiono el change del input check
            var id_variante_producto = $(this).attr("data-id");
            var total_sin_envio = Number($("#total_sin_envio").text().substring(1));
            var cantidad = Number($("#cantidad"+id_variante_producto).val());
            var precio = Number($("#precio_producto"+id_variante_producto).text().substring(1));

            var peso_producto = Number($("#peso_"+id_variante_producto).val());
            var alto_producto = Number($("#alto_"+id_variante_producto).val());
            var largo_producto = Number($("#largo_"+id_variante_producto).val());
            var ancho_producto = Number($("#ancho_"+id_variante_producto).val());

            //Si el checkbox fue tildado sumo al subtotal el precio por la cantidad del producto, y sino lo resto
            if(this.checked) {
                if(!$("#error_seleccionar_algun_producto").hasClass("d-none") ){
                    $("#texto_error_seleccionar_algun_producto").text("");
                    $("#error_seleccionar_algun_producto").addClass("d-none"); 
                }
                
                peso_total += (peso_producto / 1000) * cantidad;
                dimensiones_totales += (alto_producto + largo_producto + ancho_producto) * cantidad;
                
                if(mayorAUnPaquete(peso_total, dimensiones_totales)){
                    peso_total -= (peso_producto / 1000) * cantidad;
                    dimensiones_totales -= (alto_producto + largo_producto + ancho_producto) * cantidad;
                    $(this).prop("checked", false);
                    
                    errorMayorAUnPaquete(true);
                }
                else{
                    total_sin_envio = total_sin_envio + precio * cantidad;
                    errorMayorAUnPaquete(false);
                }
            }
            else{
                total_sin_envio = total_sin_envio - precio * cantidad;

                peso_total -= (peso_producto / 1000) * cantidad;
                dimensiones_totales -= (alto_producto + largo_producto + ancho_producto) * cantidad;

                if(!mayorAUnPaquete(peso_total, dimensiones_totales)){
                    errorMayorAUnPaquete(false);
                }
            }
            
            var total_sin_envio_formateado = separadorDeMiles(total_sin_envio);

            $("#total_sin_envio").text("$"+total_sin_envio);
            $("#total_sin_envio_formateado").text("$"+total_sin_envio_formateado);
        });

        function mayorAUnPaquete(peso, dimensiones) {
            if(peso > 25 || dimensiones > 210){
                return true;
            }
            else{
                return false;
            }
        }

        function errorMayorAUnPaquete(activar) {
            if(activar){
                $("#texto_error_seleccionar_menos_productos").text("Los productos seleccionados superan el tamaño máximo de un único paquete para el envío. Intente seleccionando menos cantidad de productos para comprar.");
                $("#error_seleccionar_menos_productos").removeClass("d-none");
                $("#error_seleccionar_menos_productos").focus();
            }
            else{
                $("#texto_error_seleccionar_menos_productos").text("");
                $("#error_seleccionar_menos_productos").addClass("d-none"); 
            }
        }

        $(".servicio_seleccionado").on("change",function(){
            //Obtengo el id del producto al que se le cambio la cantidad, el subtotal actual, la cantidad y el precio de la fila que ocasiono el change del input check
            var id_servicio = $(this).attr("data-id");
            var total_servicios = Number($("#total_servicios").text().substring(1));
            var precio = Number($("#precio_servicio"+id_servicio).text().substring(1));

            //Si el checkbox fue tildado sumo al subtotal el precio por la cantidad del producto, y sino lo resto
            if(this.checked) {
                total_servicios = total_servicios + precio;

                if(!$("#error_seleccionar_algun_servicio").hasClass("d-none") ){
                    $("#texto_error_seleccionar_algun_servicio").text("");
                    $("#error_seleccionar_algun_servicio").addClass("d-none"); 
                }
            }
            else{
                total_servicios = total_servicios - precio;
            }
            
            var total_servicios_formateado = separadorDeMiles(total_servicios);

            $("#total_servicios").text("$"+total_servicios);
            $("#total_servicios_formateado").text("$"+total_servicios_formateado);
        });

        function separadorDeMiles(numero) {
            num = numero;
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/,'');
            return num;
        }

        $("#btn_realizar_compra_productos").on("click",function(){
            //Creo un json en donde se van a guardar el id de la variante, la cantidad de cada variante y el total
            data = {};
            items = [];

            //Esta variable se usa para chequear que al menos haya seleccionado un producto para comprar.
            var productos_seleccionados = 0;

            //Recorro todos los input checkbox que seleccionan los productos a comprar
            $('.producto_seleccionado').each(function(i, obj) {
                //Si el checkbox esta seleccionado (va a comprar ese producto)
                if($(this).is(":checked")){
                    productos_seleccionados++;

                    var id_variante_producto = $(this).attr('data-id');
                    var cantidad = Number($("#cantidad"+id_variante_producto).val());
                    var precio = Number($("#precio_producto"+id_variante_producto).text().substring(1));

                    item = {}
                    item ["id_variante_producto"] = id_variante_producto;
                    item ["precio"] = precio;
                    item ["cantidad"] = cantidad;

                    //Voy almacenando cada item en el array de items
                    items.push(item);
                }
            });

            //Guardo los items en el array de toda la informacion de la compra y seteo el subtotal.
            data['items'] = items;
            data['total_sin_envio'] = Number($("#total_sin_envio").text().substring(1));
            data['tipo_compra'] = 1;

            //Paso el Json con la informacion a un string.
            data = JSON.stringify(data);

            //Si selecciono al menos un producto para comprar
            if(productos_seleccionados >0 ){
                //Agrego un input con el id de la variante para usarlo en el backend.
                $("#realizar_compra_productos").append("<input type='hidden' name='data' value='"+data+"'>");
                $("#realizar_compra_productos").append("<input type='hidden' name='tipo_compra' value='1'>");

                //Al obtener todos los productos y servicios seleccionados para la compra, con su total, lo enviamos al checkout
                document.getElementById('realizar_compra_productos').submit();
            }
            else{
                $("#texto_error_seleccionar_algun_producto").text("Debe seleccionar al menos un producto para comprar");
                $("#error_seleccionar_algun_producto").removeClass("d-none");
                $("#error_seleccionar_algun_producto").focus();
            }
        });

        $("#btn_realizar_compra_servicios").on("click",function(){
            //Creo un json en donde se van a guardar el id de la variante, la cantidad de cada variante y el total
            data = {};
            items = [];

            //Esta variable se usa para chequear que al menos haya seleccionado un producto para comprar.
            var servicios_seleccionados = 0;

            //Recorro todos los input checkbox que seleccionan los servicios a comprar
            $('.servicio_seleccionado').each(function(i, obj) {
                //Si el checkbox esta seleccionado (va a comprar ese servicio)
                if($(this).is(":checked")){
                    servicios_seleccionados++;

                    var id_servicio = $(this).attr('data-id');
                    var precio = Number($("#precio_servicio"+id_servicio).text().substring(1));

                    item = {}
                    item ["id_servicio"] = id_servicio;
                    item ["precio"] = precio;

                    //Voy almacenando cada item en el array de items
                    items.push(item);
                }
            });

            //Guardo los items en el array de toda la informacion de la compra y seteo el subtotal.
            data['items'] = items;
            data['total_servicios'] = Number($("#total_servicios").text().substring(1));
            data['tipo_compra'] = 0;

            //Paso el Json con la informacion a un string.
            data = JSON.stringify(data);

            //Si selecciono al menos un servicio para comprar
            if(servicios_seleccionados >0 ){
                //Agrego un input con el id de la variante para usarlo en el backend.
                $("#realizar_compra_servicios").append("<input type='hidden' name='data' value='"+data+"'>");
                $("#realizar_compra_servicios").append("<input type='hidden' name='tipo_compra' value='0'>");

                //Al obtener todos los servicios seleccionados para la compra, con su total, lo enviamos al checkout
                document.getElementById('realizar_compra_servicios').submit();
            }
            else{
                $("#texto_error_seleccionar_algun_servicio").text("Debe seleccionar al menos un servicio para comprar");
                $("#error_seleccionar_algun_servicio").removeClass("d-none");
                $("#error_seleccionar_algun_servicio").focus();
            }
        });

        $("#solapa_productos").on("click",function(){
            if(!$(this).hasClass("active") && $("#tabla_productos").hasClass("d-none")){
                $("#solapa_servicios").removeClass("active");
                $("#tabla_servicios").addClass("d-none");

                $(this).addClass("active");
                $("#tabla_productos").removeClass("d-none");
            }
        });

        $("#solapa_servicios").on("click",function(){
            if(!$(this).hasClass("active") && $("#tabla_servicios").hasClass("d-none")){
                $("#solapa_productos").removeClass("active");
                $("#tabla_productos").addClass("d-none");

                $(this).addClass("active");
                $("#tabla_servicios").removeClass("d-none");
            }
        });
    </script>
@endsection