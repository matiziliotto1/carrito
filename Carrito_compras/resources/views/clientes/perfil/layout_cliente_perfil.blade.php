@extends('layouts.layout')

@php
    $compras_controller = new App\Http\Controllers\ComprasController();
    $cantidad_compras = $compras_controller->cantidadDeComprasDeUsuario();
@endphp

@section('content')
    <!-- Page Content-->
    <div class="container padding-bottom-3x mb-2">
        <div class="row">
            <div class="col-lg-4">
                <aside class="user-info-wrapper">
                    <div class="user-cover" style="background-image: url({{asset('img/account/user-cover-img.jpg')}});">
                        <div class="info-label" data-toggle="tooltip" title="Tienes acumulados {{Auth::user()->puntaje_acumulado}} puntos para utilizar">
                            <i class="icon-award"></i>{{Auth::user()->puntaje_acumulado}} puntos
                        </div>
                        <div class="user-info">
                            <div class="user-data">
                                <h4 class="h5">{{Auth::user()->nombre}} {{Auth::user()->apellido}}</h4>
                                <span>
                                    Registrado 
                                    <?php
                                        $fecha_sql = Auth::user()->created_at;
                                        $fecha = $fecha_sql->format('d-m-y');
                                    ?>
                                    {{$fecha}}
                                </span>
                            </div>
                        </div>
                    </div>
                    
                </aside>
                <nav class="list-group">
                   
                    <a class="list-group-item @if(Route::current()->getName()=='getPerfil') active @endif" href="{{route('getPerfil')}}">
                        <i class="icon-user"></i>Mis datos
                    </a>
                    <a class="list-group-item @if(Route::current()->getName()=='editarPassword') active @endif" href="{{route('editarPassword')}}">
                        <i class="icon-lock"></i>Cambio de contraseña
                    </a>
                    <a class="list-group-item with-badge @if(Route::current()->getName()=='getCompras') active @endif" href="{{route('getCompras')}}">
                        <i class="icon-shopping-bag"></i>Compras
                        <span class="badge badge-default badge-pill">{{$cantidad_compras}}</span>
                    </a>
                </nav>
            </div>
            <div class="col-lg-8 p-2" style="background-color: #f5f5f5;border-radius: 5px;">

                @yield('content-perfil')
                
            </div>
        </div>
    </div>
@endsection