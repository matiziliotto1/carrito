@extends('clientes.perfil.layout_cliente_perfil')

@section('content-perfil')
    <div class="padding-top-2x mt-2 hidden-lg-up"></div>
    <div class="table-responsive">
        <table class="table table-hover mb-0">
            <thead>
                <tr>
                    <th>N° compra</th>
                    <th>Fecha de compra</th>
                    <th>Estado</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @if(count($compras)>0)
                    @foreach($compras as $compra)
                        <tr class="btn-getInfo" style="cursor: pointer" data-toggle="modal" data-target="#detallesCompra" data-id="{{$compra->id_compra}}">
                            <td>
                                <a class="navi-link">
                                    {{$compra->id_compra}}
                                </a>
                            </td>
                            <td>
                                @php
                                    $compra->fecha_compra = date('d-m-Y', strtotime($compra->fecha_compra));
                                @endphp
                                {{$compra->fecha_compra}}
                            </td>
                            <td>
                                <span @if($compra->estado == "Finalizada") class="text-success" @endif>{{$compra->estado}}</span>
                            </td>
                            <td>
                                <span>${{number_format($compra->monto_total, 2, ",", ".")}}</span>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4"> Aún no has realizado ninguna compra. Mira nuestros <a class="navi-link" href="{{route("getProductosC")}}">productos</a></td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <hr>
    {{ $compras->links("clientes.pagination.pagination") }}

    <div class="modal fade" id="detallesCompra" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title"></h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <div class="table-responsive shopping-cart mb-0">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Producto</th>
                                    <th class="text-center">Precio x unidad</th>
                                </tr>
                            </thead>
                            <tbody id="tbody-detalles">
                            </tbody>
                        </table>
                    </div>
                    <hr class="mb-3">
                    <div class="d-flex flex-wrap justify-content-between align-items-center pb-2">
                        <div class="text-lg px-2 py-1"><span class="text-muted">Metodo envio: </span> <span class="text-gray-dark" id="compra-metodo-envio">-</span></div>
                        <div class="text-lg px-2 py-1"><span class="text-muted">Seguimiento: </span> <span class="text-gray-dark" id="compra-codigo-seguimiento">-</span></div>
                        <div class="text-lg px-2 py-1"><span class="text-muted">Costo envio: </span> <span class="text-gray-dark" id="compra-costo-envio">-</span></div>
                        <div class="text-lg px-2 py-1"><span class="text-muted">Total:</span> <span class="text-gray-dark" id="compra-monto-total"></span></div>
                    </div>
                    <hr class="mb-3">
                    <div class="d-none flex-wrap justify-content-between align-items-center pb-2" id="compra-datos-adicionales">
                        <div class="text-lg px-2 py-1"><span class="text-muted">Beneficio: </span> <span class="text-success" id="compra-beneficio">-</span></div>
                        <div class="text-lg px-2 py-1"><span class="text-muted">Descuento: </span> <span class="text-success" id="compra-descuento">-</span></div>
                        <div class="text-lg px-2 py-1"><span class="text-muted">Puntaje obtenido: </span> <span class="text-gray-dark" id="compra-puntaje">-</span></div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">

    $('.btn-getInfo').on("click",function(){
        var id_compra = $(this).attr("data-id");
        var url = "{{ route('getInfoCompra', ":id") }}";
        url = url.replace(':id', id_compra);
        $.ajax({
            url: url,
            type: 'get',
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}',
            },
            dataType: 'json',

            beforeSend: function(){
                $("#tbody-detalles").empty();
                $("#modal-title").empty();
                $("#compra-monto-total").empty();
            },
            success: function(response){
                $("#mensaje").empty();
                $("#alert").removeClass("alert-danger");

                if(response['success']=="true"){
                    if (response['producto']) {
                        var productos="";
                        $.each( response['detalles_productos'], function( key, value ) {
                            var precio = separadorDeMiles(value['precio']);
                            let mano=['Ambidiestro', 'Left', 'Right'];
                            var variante='';
                            //TODO: hay que ver como se guarda lo de mano habil en la BD
                            if(value['mano_habil']){
                                variante += '<span><em>Mano hábil: </em>'+mano[value['mano_habil']]+'</span>';
                            }
                            if(value['color']){
                                variante += '<span><em>Color: </em>'+value['color']+'</span>';
                            }
                            if(value['talle_ropa']){
                                variante += '<span><em>Talle: </em>'+value['talle_ropa']+'</span>';
                            }
                            if(value['talle_calzado']){
                                variante += '<span><em>Talle: </em>'+value['talle_calzado']+'</span>';
                            }
                            if(value['talle_anzuelo']){
                                variante += '<span><em>Talle: </em>'+value['talle_anzuelo']+'</span>';
                            }
                            if(value['longitud']){
                                variante += '<span><em>Longitud: </em>'+value['longitud']+'</span>';
                            }
                            if(value['fuerza']){
                                variante += '<span><em>Fuerza: </em>'+value['fuerza']+'</span>';
                            }
                            if(value['accion']){
                                variante += '<span><em>Accion: </em>'+value['accion']+'</span>';
                            }
                            if(value['diametro']){
                                variante += '<span><em>Diametro: </em>'+value['diametro']+'</span>';
                            }

                            var url_imagen = "{{ asset(":img") }}";
                            url_imagen = url_imagen.replace(':img', value['imagen']);
                            
                            productos+= '<tr>'+
                            '<td>'+
                                '<div class="product-item"><a class="product-thumb"><img src="'+url_imagen+'" alt="Producto"></a>'+
                                    '<div class="product-info">'+
                                        '<h4 class="product-title">'+value['nombre']+'<small>x '+value['cantidad']+'</small></h4>'+variante+
                                    '</div>'+
                                '</div>'+
                            '</td>'+
                            '<td class="text-center text-lg">$'+precio+'</td>'+
                        '</tr>';
                        });

                        $("#tbody-detalles").append(productos);
                        mostrarDatos(response);

                    }
                    if (!response['producto']) {
                        var servicios="";
                        $.each( response['detalles_servicios'], function( key, value ) {
                            var precio = separadorDeMiles(value['precio']);

                            var url_imagen = "{{ asset(":img") }}";
                            url_imagen = url_imagen.replace(':img', value['imagen']);
                            
                            servicios+= '<tr>'+
                                            '<td>'+
                                                '<div class="product-item"><a class="product-thumb"><img src="'+url_imagen+'" alt="Servicio"></a>'+
                                                    '<div class="product-info">'+
                                                        '<h4 class="product-title">'+value['titulo']+'</h4>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</td>'+
                                            '<td class="text-center text-lg">$'+precio+'</td>'+
                                        '</tr>';
                        });

                        $("#tbody-detalles").append(servicios);
                        mostrarDatos(response);
                    }
                    
                }
                else{
                    $("#alert").addClass("alert-danger");
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                }
            },
        });
    });

    function mostrarDatos (response) { 
        $("#modal-title").text("Compra N° "+response['compra']['id_compra']);
        $("#compra-monto-total").text("$"+separadorDeMiles(response['compra']['monto_total']));

        //TODO: hay que ver con que formato se guarda el monto de envio
        if (response['compra']['monto_envio']) {
            $("#compra-costo-envio").text("$"+separadorDeMiles(response['compra']['monto_envio']));
        }
        if (response['compra']['metodo_envio']) {
            $("#compra-metodo-envio").text(response['compra']['metodo_envio']);
        }
        if (response['compra']['codigo_seguimiento']) {
            $("#compra-codigo-seguimiento").text(response['compra']['codigo_seguimiento']);
        }

        if (response['compra']['puntaje_compra'] || response['compra']['beneficio'] || response['compra']['descuento']) {
            $("#compra-datos-adicionales").removeClass("d-none");
            $("#compra-datos-adicionales").addClass("d-flex");
            if (response['compra']['beneficio']) {
                $("#compra-beneficio").text(response['compra']['beneficio']);
            }
            //TODO: hay que ver con que formato se guarda el descuento
            if (response['compra']['descuento']) {
                $("#compra-descuento").text("$"+separadorDeMiles(response['compra']['descuento']));
            }
            if (response['compra']['puntaje_compra']) {
                $("#compra-puntaje").text(response['compra']['puntaje_compra']);
            }
        }
     }

    function separadorDeMiles(numero) {
            num = numero;
            num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
            num = num.split('').reverse().join('').replace(/^[\.]/,'');
            return num;
        }
    
</script>
@endsection