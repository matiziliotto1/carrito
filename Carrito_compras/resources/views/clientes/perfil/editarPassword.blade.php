@extends('clientes.perfil.layout_cliente_perfil')

@section('content-perfil')

<div class="padding-top-2x mt-2 hidden-lg-up"></div>
<form class="row" action="{{route('updatePassword')}}" method="POST" id="form-editarPassword">
    @csrf    
    
    <div class="col-md-12">
        <div class="form-group" id="div-nueva_password">
            <label for="nueva_password">Nueva contraseña</label>
            <input class="form-control" type="text" name="nueva_password" id="nueva_password" required placeholder="Nueva contraseña" autofocus>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group" id="div-confirmar_password">
            <label for="confirmar_password">Confirmar contraseña</label>
            <input class="form-control" type="text" name="confirmar_password" id="confirmar_password" required placeholder="Confirmar contraseña">
        </div>
    </div>
    <div class="col-12">
        <hr class="mt-2 mb-3">
        <div class="d-flex flex-wrap justify-content-between align-items-center">
            <button class="btn btn-primary" type="submit" id="btn-actualizar-password">Actualizar contraseña</button>
        </div>
    </div>
</form>
@endsection

@section('scripts')
<script type="text/javascript">
    $('#form-editarPassword').submit(function(e){

        if($("#nueva_password").hasClass("is-invalid")){
            $("#nueva_password").removeClass("is-invalid");
            $("#error-lenght").remove();
        }
        if($("#confirmar_password").hasClass("is-invalid")){
            $("#confirmar_password").removeClass("is-invalid");
            $("#error-diferentes").remove();
        }

        var nueva_password = $("#nueva_password").val();
        var confirmar_password = $("#confirmar_password").val();
        if(nueva_password.length > 7 && nueva_password == confirmar_password){
            return true;
        }
        else{
            if(nueva_password.length < 8){
                $("#nueva_password").addClass("is-invalid");
                $("#div-nueva_password").append('<span class="invalid-feedback" role="alert" id="error-lenght"><strong>La nueva contraseña debe tener al menos 8 caracteres.</strong></span>');
            }
            else{
                $("#confirmar_password").addClass("is-invalid");
                $("#div-confirmar_password").append('<span class="invalid-feedback" role="alert" id="error-diferentes"><strong>Las contraseñas deben ser iguales.</strong></span>');
            }
            return false;
        }
    });
</script>
@endsection