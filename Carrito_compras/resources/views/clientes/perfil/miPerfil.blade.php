@extends('clientes.perfil.layout_cliente_perfil')

@section('content-perfil')

<style>
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
        -moz-appearance:textfield;
    }
</style>

<div class="padding-top-2x mt-2 hidden-lg-up"></div>
<form class="row" action="{{route('updatePerfil')}}" method="POST">
    @csrf

    <div class="col-md-12">
        @if(session('mensaje_ok'))
            <div class="alert alert-success alert-dismissible fade show text-center margin-bottom-1x">
                <span class="alert-close" data-dismiss="alert"></span>
                <i class="icon-check-circle"></i>&nbsp;&nbsp; 
                {{ session('mensaje_ok') }}
            </div>
        @endif
        @if(session('mensaje_error'))
            <div class="alert alert-danger alert-dismissible fade show text-center margin-bottom-1x">
                <span class="alert-close" data-dismiss="alert"></span>
                <i class="icon-slash"></i>&nbsp;&nbsp;
                {{ session('mensaje_error') }}
            </div>
        @endif
    </div>
    
    <div class="col-md-12">
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" type="email" name="email" id="email" value="{{Auth::user()->email}}" readonly>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input class="form-control" type="text" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" name="nombre" id="nombre" value="{{Auth::user()->nombre}}" required placeholder="Nombre">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="apellido">Apellido</label>
            <input class="form-control" type="text" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" name="apellido" id="apellido" value="{{Auth::user()->apellido}}" required placeholder="Apellido">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="tipo_documento">Tipo de documento</label>
            <select class="form-control" name="tipo_documento" id="tipo_documento" required>
                <option disabled>Tipo de documento</option>
                <option value="DNI" @if (Auth::user()->tipo_documento=='DNI') selected @endif>DNI</option>
                <option value="LE" @if (Auth::user()->tipo_documento=='LE') selected @endif>LE</option>
                <option value="LC" @if (Auth::user()->tipo_documento=='LC') selected @endif>LC</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="numero_documento">Numero de documento</label>
            <input class="form-control" type="number" name="numero_documento" id="numero_documento" value="{{Auth::user()->numero_documento}}" required placeholder="Numero de documento">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="codigo_postal">Provincia</label>
            <select class="form-control" id="provincia" name="provincia" required>
                <option disabled selected>Seleccione la provincia</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="codigo_postal">Ciudad</label>
            <select class="form-control" id="ciudad" name="ciudad" required>
                <option disabled selected>Seleccione la ciudad</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="codigo_postal">Codigo postal</label>
            <input class="form-control" type="number" name="codigo_postal" id="codigo_postal" value="{{Auth::user()->codigo_postal}}" required placeholder="Codigo postal">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="domicilio_real">Domicilio personal</label>
            <input class="form-control" type="text" name="domicilio_real" id="domicilio_real" value="{{Auth::user()->domicilio_real}}" required placeholder="Domicilio">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="telefono_fijo">Teléfono</label>
            <input class="form-control" type="text" name="telefono_fijo" id="telefono_fijo" value="{{Auth::user()->telefono_fijo}}" required placeholder="Teléfono">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="telefono_celular">Celular</label>
            <input class="form-control" type="text" name="telefono_celular" id="telefono_celular" value="{{Auth::user()->telefono_celular}}" required placeholder="Celular">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="codigo_postal">Provincia de recepción</label>
            <select class="form-control" id="provincia_recepcion" name="provincia_recepcion" required>
                <option disabled selected>Seleccione la provincia de recepción</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="codigo_postal">Ciudad de recepción</label>
            <select class="form-control" id="ciudad_recepcion" name="ciudad_recepcion" required>
                <option disabled selected>Seleccione la ciudad de recepción</option>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="codigo_postal">Codigo postal de recepción</label>
            <input class="form-control" type="number" name="codigo_postal_recepcion" id="codigo_postal_recepcion" value="{{Auth::user()->codigo_postal_recepcion}}" required placeholder="Codigo postal de recepción">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="domicilio_recepcion">Domicilio de recepción</label>
            <input class="form-control" type="text" name="domicilio_recepcion" id="domicilio_recepcion" value="{{Auth::user()->domicilio_recepcion}}" required placeholder="Domicilio de recepción">
        </div>
    </div>
    <div class="col-12">
        <hr class="mt-2 mb-3">
        <div class="d-flex flex-wrap justify-content-between align-items-center">
            <button class="btn btn-primary" type="submit">Actualizar perfil</button>
        </div>
    </div>
</form>

@endsection

@section('scripts')
    <script>
        var user=<?php echo \Auth::user(); ?>;

        $( document ).ready(function() {
            $.ajax({
                url: '{{route("getProvincias")}}',
                type: 'get',
                dataType: 'json',

                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['provincias'].length-1; i++) {
                            $("#provincia").append('<option value="'+response['provincias'][i].id_provincia+'">'+response['provincias'][i].nombre+'</option>');
                        }

                        for (var i =  0; i < response['provincias'].length-1; i++) {
                            $("#provincia_recepcion").append('<option value="'+response['provincias'][i].id_provincia+'">'+response['provincias'][i].nombre+'</option>');
                        }

                        $("#provincia").val(user.provincia);
                        $("#provincia_recepcion").val(user.provincia_recepcion);

                        let url = "{{route('getLocalidades', ":id_provincia")}}";
                        url = url.replace(':id_provincia', user.provincia);
                        $.ajax({
                            url: url,
                            type: 'get',
                            dataType: 'json',

                            success: function(response){
                                if(response['success']){
                                    for (var i =  0; i < response['localidades'].length-1; i++) {
                                        $("#ciudad").append('<option value="'+response['localidades'][i].id_localidad+'">'+response['localidades'][i].nombre+'</option>');
                                    }
                                    $("#ciudad").val(user.ciudad);
                                }
                            },
                        });

                        let url2 = "{{route('getLocalidades', ":id_provincia")}}";
                        url2 = url2.replace(':id_provincia', user.provincia_recepcion);
                        $.ajax({
                            url: url2,
                            type: 'get',
                            dataType: 'json',

                            success: function(response){
                                if(response['success']){
                                    for (var i =  0; i < response['localidades'].length-1; i++) {
                                        $("#ciudad_recepcion").append('<option value="'+response['localidades'][i].id_localidad+'">'+response['localidades'][i].nombre+'</option>');
                                    }
                                    $("#ciudad_recepcion").val(user.ciudad_recepcion);
                                }
                            },
                        });
                    }
                },
            });
        });

        $(document).on("change", "#provincia", function() {
            $("#ciudad option").remove();
            let id_provincia = $(this).children("option:selected").val();

            $('#provincia option[value="'+id_provincia+'"]').attr('selected', "selected");

            let url = "{{route('getLocalidades', ":id_provincia")}}";
            url = url.replace(':id_provincia', id_provincia);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',

                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['localidades'].length-1; i++) {
                            $("#ciudad").append('<option value="'+response['localidades'][i].id_localidad+'">'+response['localidades'][i].nombre+'</option>');
                        }
                    }
                },
            });
        });

        $(document).on("change", "#provincia_recepcion", function() {
            $("#ciudad_recepcion option").remove();
            let id_provincia = $(this).children("option:selected").val();

            $('#provincia_recepcion option[value="'+id_provincia+'"]').attr('selected', "selected");

            let url = "{{route('getLocalidades', ":id_provincia")}}";
            url = url.replace(':id_provincia', id_provincia);
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',

                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['localidades'].length-1; i++) {
                            $("#ciudad_recepcion").append('<option value="'+response['localidades'][i].id_localidad+'">'+response['localidades'][i].nombre+'</option>');
                        }
                    }
                },
            });
        });
    </script>
@endsection