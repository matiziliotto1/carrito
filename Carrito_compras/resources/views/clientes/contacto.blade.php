@extends('layouts.layout')

@section('content')
<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>Contacto</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{route('home')}}">Inicio</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>Contacto</li>
            </ul>
        </div>
    </div>
</div>
<!-- Page Content-->
<div class="container padding-bottom-2x mb-2">
    <div class="row">
        <div class="col-md-7">
            <div class="display-3 text-muted opacity-75 mb-30">Telefonos</div>
        </div>
        <div class="col-md-5 text-lg">
            <ul class="list-icon">
                <li> <i class="icon-phone text-muted"></i>{{$contacto->telefono_1}}</li>
                <li> <i class="icon-phone text-muted"></i>{{$contacto->telefono_2}}</li>
                <li> <i class="icon-phone text-muted"></i>{{$contacto->telefono_3}}</li>
            </ul>
        </div>
    </div>
    <hr class="margin-top-2x">
    <div class="row margin-top-2x">
        <div class="col-md-7">
            <div class="display-3 text-muted opacity-75 mb-30">Facebook</div>
        </div>
        <div class="col-md-5 text-lg">
            <ul class="list-icon">
                <li> <i class="icon-facebook text-muted"></i><a class="navi-link" href="{{$contacto->facebook}}">{{$contacto->facebook}}</a></li>
            </ul>
        </div>
    </div>
    <hr class="margin-top-2x">
    <div class="row margin-top-2x">
        <div class="col-md-7">
            <div class="display-3 text-muted opacity-75 mb-30">Instagram</div>
        </div>
        <div class="col-md-5 text-lg">
            <ul class="list-icon">
                <li> <i class="icon-instagram text-muted"></i><a class="navi-link" href="{{$contacto->instagram}}">{{$contacto->instagram}}</a></li>
            </ul>
        </div>
    </div>
    <hr class="margin-top-2x">
    <div class="row margin-top-2x">
        <div class="col-md-7">
            <div class="display-3 text-muted opacity-75 mb-30">Twitter</div>
        </div>
        <div class="col-md-5 text-lg">
            <ul class="list-icon">
                <li> <i class="icon-twitter text-muted"></i><a class="navi-link" href="{{$contacto->twitter}}">{{$contacto->twitter}}</a></li>
            </ul>
        </div>
    </div>
    <hr class="margin-top-2x">
    <div class="row margin-top-2x">
        <div class="col-md-7">
            <div class="display-3 text-muted opacity-75 mb-30">Youtube</div>
        </div>
        <div class="col-md-5 text-lg">
            <ul class="list-icon">
                <li> <i class="icon-youtube text-muted"></i><a class="navi-link" href="{{$contacto->youtube}}">{{$contacto->youtube}}</a></li>
            </ul>
        </div>
    </div>
    <hr class="margin-top-2x">
    <div class="row margin-top-2x">
        <div class="col-md-7">
            <div class="display-3 text-muted opacity-75 mb-30">E-Mail</div>
        </div>
        <div class="col-md-5 text-lg">
            <ul class="list-icon">
                <li> <i class="icon-mail text-muted"></i>{{$contacto->email_info}}</li>
                <li> <i class="icon-mail text-muted"></i>{{$contacto->email_comprobantes}}</li>
                <li> <i class="icon-mail text-muted"></i>{{$contacto->email_reclamos}}</li>
            </ul>
        </div>
    </div>
</div>
@endsection