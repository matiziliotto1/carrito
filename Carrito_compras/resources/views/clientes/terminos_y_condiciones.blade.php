@extends('layouts.layout')

@section('content')

<!-- Page Title-->
<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>Ayuda</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{route('home')}}">Inicio</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>Ayuda</li>
            </ul>
        </div>
    </div>
</div>
<!-- Page Content-->
<div class="container padding-bottom-3x">
    <div class="row">
        <!-- Side Menu-->
        <div class="col-lg-3 col-md-4">
            <nav class="list-group">
            <a class="list-group-item" href="{{route('ayuda')}}">Preguntas frecuentes</a>
            <a class="list-group-item active" href="{{route('terminosYCondiciones')}}">Terminos y condiciones</a>
            <div class="padding-bottom-3x hidden-md-up"></div>
        </div>
        <!--Content-->
        <div class="col-lg-9 col-md-8">
            @foreach ($terminos as $termino)
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <h4 class="mb-2">{{$termino->titulo}}</h4>
                        <p class="text-lg">{{$termino->texto}}</p>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection