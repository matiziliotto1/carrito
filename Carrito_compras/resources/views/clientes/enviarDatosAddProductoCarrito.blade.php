<form method="post" action="{{route("addProductoCarrito")}}" id="form">
    @csrf
    <input name="id_variante_producto" type="hidden" value="{{ Session::get('id_variante_producto') }}">
    <input name="cantidad" type="hidden" value="{{ Session::get('cantidad') }}">
</form>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
    $( document ).ready(function() {
        $("#form").submit();
    });
</script>
