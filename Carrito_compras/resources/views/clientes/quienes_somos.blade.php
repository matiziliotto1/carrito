@extends('layouts.layout')

@section('content')
<div class="page-title">
    <div class="container">
        <div class="column">
            <h1>¿Quienes Somos?</h1>
        </div>
        <div class="column">
            <ul class="breadcrumbs">
                <li><a href="{{route('home')}}">Inicio</a>
                </li>
                <li class="separator">&nbsp;</li>
                <li>¿Quienes Somos?</li>
            </ul>
        </div>
    </div>
</div>
    <!-- Page Content-->
<div class="container padding-bottom-3x mb-1">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <h6 class="text-center">{!! nl2br($textos->review) !!}</h6>
            <h4 class="text-left"><strong>Objetivos</strong></h4>
            <h6 class="text-left">{!! nl2br($textos->objetivos) !!}</h6>
        </div>
    </div>
</div>
@endsection