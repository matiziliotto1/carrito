@extends('errors::minimal')

@section('title', __('Error 404'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/404.jpg') }}">
@endsection

@section('message', __('Ups! Parece que esta página no existe.'))