@extends('errors::minimal')

@section('title', __('Error 419'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/419.jpg') }}">
@endsection

@section('message', __('Ups! Parece que la página expiró.'))