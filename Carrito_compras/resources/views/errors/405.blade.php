@extends('errors::minimal')

@section('title', __('Error 405'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/403.jpg') }}">
@endsection

@section('message', __('Método no permitido.'))