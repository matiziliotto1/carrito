@extends('errors::minimal')

@section('title', __('Error 503'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/500-503.png') }}">
@endsection

@section('message', __($exception->getMessage() ?: 'Ups! Parece que el sitio no esta funcionando en este momento.'))