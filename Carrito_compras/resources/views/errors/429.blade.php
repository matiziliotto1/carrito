@extends('errors::minimal')

@section('title', __('Error 429'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/403.png') }}">
@endsection

@section('message', __('Ups! Parece que has realizado demasiadas peticiones.'))