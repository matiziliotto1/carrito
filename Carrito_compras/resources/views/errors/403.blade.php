@extends('errors::minimal')

@section('title', __('Error 403'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/403.png') }}">
@endsection

@section('message', __($exception->getMessage() ?: 'Ups! Parece que tienes prohibido el acceso.'))