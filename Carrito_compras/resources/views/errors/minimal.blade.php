<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title')</title>

        <meta charset="utf-8">
        <title>Inicia sesión | Fishing.com
        </title>
        <!-- SEO Meta Tags-->
        <meta name="description" content="Unishop - Universal E-Commerce Template">
        <meta name="keywords" content="shop, e-commerce, modern, flat style, responsive, online store, business, mobile, blog, bootstrap 4, html5, css3, jquery, js, gallery, slider, touch, creative, clean">
        <meta name="author" content="Rokaux">
        <!-- Mobile Specific Meta Tag-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Favicon and Apple Icons-->
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <link rel="icon" type="image/png" href="favicon.png">
        <link rel="apple-touch-icon" href="touch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="180x180" href="touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="167x167" href="touch-icon-ipad-retina.png">
        <!-- Vendor Styles including: Bootstrap, Font Icons, Plugins, etc.-->
        <link rel="stylesheet" media="screen" href="{{ asset('css/vendor.min.css') }}">
        <!-- Main Template Styles-->
        <link id="mainStyles" rel="stylesheet" media="screen" href="{{ asset('css/styles.min.css') }}">
        <!-- Customizer Styles-->
        <link rel="stylesheet" media="screen" href="{{ asset('customizer/customizer.min.css') }}">
        <!-- Google Tag Manager-->
        <script>
          (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
          new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
          j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
          '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
          })(window,document,'script','dataLayer','GTM-T4DJFPZ');
          
        </script>
        <!-- Modernizr-->
        <script src="{{ asset('js/modernizr.min.js') }}"></script>

        <!-- Styles -->
        <style>
            .code {
                border-right: 2px solid;
            }
        </style>
    </head>
    <body style='background-image: url("{{ asset('img/fondos/FondoOlas.png') }}");background-repeat: no-repeat;background-attachment: fixed;background-size:100% 100%;'>
        <!-- Page Content-->
        <div class="container h-100">
            <div class="row justify-content-center h-90">
                <div class="col-md-7 my-auto">
                    <div class="text-center mt-4 mb-4">
                        <a href="{{route('home')}}">
                            <img src="{{ asset('img/logo/LogoAuth.png') }}">
                        </a>
                    </div>
                    <div class="card" style="box-shadow: 0 .15rem 1.75rem 0 rgba(39, 37, 2, 0.15)!important;">

                        <div class="text-center">
                            @yield('imagen')
                        </div>
                        <hr>
                        <div class="code">
                            @yield('code')
                        </div>
                        <hr>
                        <div class="message text-center" style="padding: 10px;">
                            @yield('message')
                        </div>
                        <hr>
                        <div class="text-center" style="padding: 10px;">
                            <a href="{{route('home')}}">Volver a la pagina principal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
