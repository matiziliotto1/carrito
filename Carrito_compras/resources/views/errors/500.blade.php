@extends('errors::minimal')

@section('title', __('Error 500'))

@section('imagen')
    <img class="pt-1 pb-1" src="{{ asset('img/error/500-503.png') }}">
@endsection

@section('message', __('Ups! Parece que hubo un error en el servidor.'))