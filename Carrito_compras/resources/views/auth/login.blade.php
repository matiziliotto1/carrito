@extends('layouts.layout_auth')

@section('title', 'Login')

@section('content')

<!-- Page Content-->
<div class="container h-100">
    <div class="row justify-content-center h-90">
        <div class="col-md-7 my-auto">
            <div class="text-center mt-4 mb-4">
                <a href="{{route('home')}}">
                    <img src="{{ asset('img/logo/LogoAuth.png') }}">
                </a>
            </div>
            <form method="POST" action="{{ route('login') }}" class="card" style="box-shadow: 0 .15rem 1.75rem 0 rgba(39, 37, 2, 0.15)!important;">
                @csrf

                <div class="card-body">
                    <h4 class="margin-bottom-1x">Ingresa</h4>
                    @error('email')
                        <span class="text-danger" style="font-size: 12px">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group input-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email"><span class="input-group-addon"><i class="icon-mail"></i></span>
                    </div>
                    @error('password')
                        <span class="text-danger" style="font-size: 12px">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    <div class="form-group input-group">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password"><span class="input-group-addon"><i class="icon-lock"></i></span>
                    </div>
                    <div class="d-flex flex-wrap justify-content-between padding-bottom-1x">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Recordarme</label>
                        </div>

                        @if (Route::has('password.request'))
                            <a class="navi-link" href="{{ route('password.request') }}">Olvidaste tu contraseña?</a>
                        @endif
                    </div>
                    <div class="text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" type="submit">Ingresa</button>
                    </div>
                </div>
                <hr>
                <p></p>
                <p class="text-center">Aun no tienes tu cuenta? <a href="{{ route('register') }}">Regístrate</a></p>
            </form>
        </div>
    </div>
</div>
    
@endsection
