@extends('clientes.perfil.layout_cliente_perfil')

@section('content-perfil')
    <div class="padding-top-2x mt-2 hidden-lg-up"></div>
    
    <div class="card-header">
        <h5 class="h5 text-center">Confirme su contraseña actual para continuar</h5>
    </div>

    <form class="row" method="POST" action="{{ route('password.confirm') }}" enctype="multipart/form-data" id="form-editarPassword">
        @csrf

        <div class="col-md-6 mt-3">
            <div class="form-group">
                <label for="password">Contraseña</label>
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="col-12">
            <hr class="mt-2 mb-3">
            <div class="d-flex flex-wrap justify-content-between align-items-center">
                <button type="submit" class="btn btn-primary">
                    Continuar
                </button>
            </div>
        </div>
    </form>
@endsection