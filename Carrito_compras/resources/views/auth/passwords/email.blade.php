@extends('layouts.layout_auth')

@section('content')
<!-- Page Content-->
<div class="container h-100">
    <div class="row justify-content-center h-90">
        <div class="col-lg-8 col-md-10 my-auto">
            <div class="text-center mt-4 mb-4">
                <a href="{{route('home')}}">
                    <img src="{{ asset('img/logo/LogoAuth.png') }}">
                </a>
            </div>
            <div class="p-3 bg-white card">
                <h2>Olvidaste tu contraseña?</h2>
                <p>Cambia tu contraseña en 3 pasos muy fáciles. Esto ayuda a mantener segura su nueva contraseña.</p>
                <ol class="list-unstyled">
                    <li><span class="text-primary text-medium">1. </span>Complete su dirección de correo electrónico abajo.</li>
                    <li><span class="text-primary text-medium">2. </span>Le enviaremos un link temporal por correo electrónico..</li>
                    <li><span class="text-primary text-medium">3. </span>Use el link para cambiar su contraseña en nuestro sitio web.</li>
                </ol>
                <form method="POST" action="{{ route('password.email') }}" class="card">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="email-for-pass">Ingresa tu direccion de correo electrónico</label>
                            <input class="form-control" type="text" name="email" id="email-for-pass" required placeholder="Email"><small class="form-text text-muted">Escriba la dirección de correo electrónico que utilizó cuando se registró. Luego enviaremos un link por correo electrónico a esta dirección.</small>
                        </div>
                        <button type="submit" class="btn btn-primary">
                            Enviar link
                        </button>
                    </div>
                    
                    
                </form>
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection