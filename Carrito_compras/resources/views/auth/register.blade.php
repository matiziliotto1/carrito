@extends('layouts.layout_auth')

@section('title', 'Registro')

@section('content')
    
<!-- Page Content-->
<div class="container h-100">
    <div class="row justify-content-center h-90">
        <div class="col-md-9 my-auto">
            <div class="text-center mt-4 mb-4">
                <a href="{{route('home')}}">
                    <img src="{{ asset('img/logo/LogoAuth.png') }}">
                </a>
            </div>
            <div class="p-3 bg-white card">
                <h3 class="">Completá con tus datos</h3>
                <form method="POST" action="{{ route('register') }}" class="row">
                    @csrf

                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('nombre') is-invalid @enderror" type="text" id="nombre" name="nombre" value="{{ old('nombre') }}" required autofocus placeholder="Nombre/s">
                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('apellido') is-invalid @enderror" type="text" id="apellido" name="apellido" value="{{ old('apellido') }}" required placeholder="Apellido/s">
                            @error('apellido')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="form-control" id="tipo_documento" name="tipo_documento" value="{{ old('tipo_documento') }}" required>
                                <option disabled selected>Seleccione el tipo de documento</option>
                                <option value="DNI">DNI</option>
                                <option value="LE">LE</option>
                                <option value="LC">LC</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('numero_documento') is-invalid @enderror" type="text" id="numero_documento"  name="numero_documento" value="{{ old('numero_documento') }}" required placeholder="Número de documento">
                            @error('numero_documento')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="form-control" id="provincia" name="provincia" value="{{ old('provincia') }}" required>
                                <option disabled selected>Seleccione la provincia</option>
                            </select>
                            @error('provincia')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="form-control" id="ciudad" name="ciudad" value="{{ old('ciudad') }}" required>
                                <option disabled selected>Seleccione la ciudad</option>
                            </select>
                            @error('ciudad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('codigo_postal') is-invalid @enderror" type="text" id="codigo_postal" name="codigo_postal" value="{{ old('codigo_postal') }}" required placeholder="Codigo postal">
                            @error('codigo_postal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('domicilio_real') is-invalid @enderror" type="text" id="domicilio_real" name="domicilio_real" value="{{ old('domicilio_real') }}" required placeholder="Domicilio personal">
                            @error('domicilio_real')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('telefono_fijo') is-invalid @enderror" type="text" id="telefono_fijo" name="telefono_fijo" value="{{ old('telefono_fijo') }}" required placeholder="Teléfono">
                            @error('telefono_fijo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('telefono_celular') is-invalid @enderror" type="text" id="telefono_celular" name="telefono_celular" value="{{ old('telefono_celular') }}" required placeholder="Celular">
                            @error('telefono_celular')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="form-control" id="provincia_recepcion" name="provincia_recepcion" value="{{ old('provincia_recepcion') }}" required>
                                <option disabled selected>Seleccione la provincia de recepción</option>
                            </select>
                            @error('provincia_recepcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select class="form-control" id="ciudad_recepcion" name="ciudad_recepcion" value="{{ old('ciudad_recepcion') }}" required>
                                <option disabled selected>Seleccione la ciudad de recepción</option>
                            </select>
                            @error('ciudad_recepcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('codigo_postal_recepcion') is-invalid @enderror" type="text" id="codigo_postal_recepcion" name="codigo_postal_recepcion" value="{{ old('codigo_postal_recepcion') }}" required placeholder="Codigo postal de recepción">
                            @error('codigo_postal_recepcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('domicilio_recepcion') is-invalid @enderror" type="text" id="domicilio_recepcion" name="domicilio_recepcion" value="{{ old('domicilio_recepcion') }}" required placeholder="Domicilio de entrega">
                            @error('domicilio_recepcion')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('email') is-invalid @enderror" type="text" id="email" name="email" value="{{ old('email') }}" required placeholder="Email">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control @error('password') is-invalid @enderror" type="password" id="password" name="password" required placeholder="Contraseña">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-6 text-center text-sm-left">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input" type="checkbox" id="checkbox-terminos-y-condiciones">
                            <label class="custom-control-label" for="checkbox-terminos-y-condiciones">He leído y acepto los <a href="{{route('terminosYCondiciones')}}"> Terminos y condiciones</a></label>
                        </div>
                    </div>
                    <div class="col-6 text-center text-sm-right">
                        <button class="btn btn-primary margin-bottom-none" type="submit" disabled id="btn-crear-cuenta">Crear cuenta</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            $.ajax({
                url: '{{route("getProvincias")}}',
                type: 'get',
                dataType: 'json',

                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['provincias'].length-1; i++) {
                            $("#provincia").append('<option value="'+response['provincias'][i].id_provincia+'">'+response['provincias'][i].nombre+'</option>');
                        }

                        for (var i =  0; i < response['provincias'].length-1; i++) {
                            $("#provincia_recepcion").append('<option value="'+response['provincias'][i].id_provincia+'">'+response['provincias'][i].nombre+'</option>');
                        }

                        let provincia = "{{old('provincia') ?? ''}}";
                        let provincia_recepcion = "{{old('provincia_recepcion') ?? ''}}";

                        if (provincia !== "") {
                            let element = document.getElementById('provincia');
                            element.value = provincia;

                            getLocalidades(provincia,'#ciudad');
                        }

                        if (provincia_recepcion !== "") {
                            let element = document.getElementById('provincia_recepcion');
                            element.value = provincia_recepcion;
                            getLocalidades(provincia_recepcion,'#ciudad_recepcion');
                        }
                    }
                },
            });

            let tipo_documento = "{{old('tipo_documento') ?? ''}}";

            if (tipo_documento !== "") {
                let element = document.getElementById('tipo_documento');
                element.value = tipo_documento;
            }
        });

        $(document).on("change", "#provincia", function() {
            $("#ciudad option").remove();
            let id_provincia = $(this).children("option:selected").val();

            $('#provincia option[value="'+id_provincia+'"]').attr('selected', "selected");

            getLocalidades(id_provincia, '#ciudad');
        });

        $(document).on("change", "#provincia_recepcion", function() {
            $("#ciudad_recepcion option").remove();
            let id_provincia = $(this).children("option:selected").val();

            $('#provincia_recepcion option[value="'+id_provincia+'"]').attr('selected', "selected");

            getLocalidades(id_provincia,'#ciudad_recepcion');
        });

        $("#checkbox-terminos-y-condiciones").on('change', function(){
            let val = $(this).is(':checked');
            if(val){
                $("#btn-crear-cuenta").removeAttr('disabled');
            }
            else{
                $("#btn-crear-cuenta").attr('disabled', true);
            }
        });

        function getLocalidades(id_provincia, select){
            let url = "{{route('getLocalidades', ":id_provincia")}}";
            url = url.replace(':id_provincia', id_provincia);

            let ciudad = "{{old('ciudad') ?? ''}}";
            let ciudad_recepcion = "{{old('ciudad_recepcion') ?? ''}}";
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['localidades'].length-1; i++) {
                            $(select).append('<option value="'+response['localidades'][i].id_localidad+'">'+response['localidades'][i].nombre+'</option>');
                        }

                        if (ciudad !== "") {
                            let element = document.getElementById('ciudad');
                            element.value = ciudad;
                        }

                        if (ciudad_recepcion !== "") {
                            let element = document.getElementById('ciudad_recepcion');
                            element.value = ciudad_recepcion;
                        }
                    }
                },
            });
        }
    </script>
@endsection