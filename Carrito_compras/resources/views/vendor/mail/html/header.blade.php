<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Mercado Fishing')
<img src="{{asset('img/logo/LogoIcono.png')}}" class="logo" alt="Mercado Fishing logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
