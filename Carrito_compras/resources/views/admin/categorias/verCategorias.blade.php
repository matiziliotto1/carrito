@extends('layouts.layout_admin')

@section('content')
    <div class="row w-90 mx-auto">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Crear categoria</h6>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead>
                            <tr>
                                <th>
                                    <input type="text" name="nombre_categoria" id="nombre_categoria_nueva" class="form-control" placeholder="Ingrese nombre de la categoria" required>
                                </th>
                                <th>
                                    <select class="form-control" id="tipo_categoria_nueva" name="tipo_categoria">
                                        <option disabled selected>Seleccione el tipo de categoria</option>
                                        <option value="1">Producto</option>
                                        <option value="0">Servicio</option>
                                    </select>
                                </th>
                                <th>
                                    <a href="#" class="btn btn-primary btn-crear text-white">Crear Categoria</a>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>

    <div id="alert_nueva" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje_nueva">
        </div>
    </div>
    
    <div class="row">

        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de categorias</h6>
                </div>
                
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Mostrar categoria</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @if(count($categorias)>0)
                                @foreach ($categorias as $categoria)
                                    <tr id="categoria{{$categoria->id_categoria}}" class="">
                                        <td>
                                            <input type="text" id="nombre_categoria_{{$categoria->id_categoria}}" class="form-control" value="{{$categoria->nombre_categoria}}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" value="@if($categoria->tipo==1) Producto @else Servicio @endif" disabled>
                                        </td>
                                        <td>
                                            <select class="form-control" id="habilitado_{{$categoria->id_categoria}}">
                                                    <option disabled>Mostrar categoria</option>
                                                    <option @if($categoria->habilitado==1) selected @endif value="1">Si</option>
                                                    <option @if($categoria->habilitado==0) selected @endif value="0">No</option>
                                            </select>
                                        </td>
                                        <td class="td-actions">
                                            <a href="#" class="btn btn-sm btn-primary btn-actualizar text-white" id="actualizar" data-id="{{$categoria->id_categoria}}">Actualizar</a>
                                            <a href="{{route('verSubcategoriasView', $categoria->id_categoria)}}" class="btn btn-sm btn-primary text-white"
                                                data-toggle="tooltip" data-placement="top" title="Ver subcategorias"> Ver subcategorías</a>
                                            @if($categoria->tipo == 1)
                                                <a href="{{route('admin.verProductosXCategoria', $categoria->id_categoria)}}" class="btn btn-sm btn-primary text-white mt-2"
                                                    data-toggle="tooltip" data-placement="top" title="Ver productos"> Ver productos</a>
                                            @elseif($categoria->tipo == 0)
                                                <a href="{{route('admin.verServiciosXCategoria', $categoria->id_categoria)}}" class="btn btn-sm btn-primary text-white mt-2"
                                                    data-toggle="tooltip" data-placement="top" title="Ver servicios"> Ver servicios</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td colspan="4">Aun no hay categorias cargadas.</td>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">    
                    <!-- Pagination links -->
                    {{ $categorias->links() }}
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>
    <script type="text/javascript">

        //Si lo uso de esta forma, se reconocen los nuevos botones agregados dinamicamente.
        $(document).on("click", ".btn-actualizar", function() {
            var token = '{{csrf_token()}}';
            var id_categoria = $(this).attr("data-id");
            var nombre_categoria = $("#nombre_categoria_"+id_categoria).val();
            var habilitado = $("#habilitado_"+id_categoria).children("option:selected").val();

            $.ajax({
                url: '{{route("updateCategoria")}}',
                type: 'post',
                data: {
                    id_categoria: id_categoria,
                    nombre_categoria: nombre_categoria,
                    habilitado: habilitado,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
        
        $(".btn-crear").on("click",function(){
            var token = '{{csrf_token()}}';
            var nombre_categoria = $("#nombre_categoria_nueva").val();
            var tipo = $("#tipo_categoria_nueva").children("option:selected").val();

            $.ajax({
                url: '{{route("createCategoria")}}',
                type: 'post',
                data: {
                    nombre_categoria: nombre_categoria,
                    tipo: tipo,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje_nueva").empty();
                    $("#alert_nueva").removeClass("alert-success");
                    $("#alert_nueva").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert_nueva").addClass("alert-success");
                        var id_categoria = response['id_categoria'];
                        let texto_tipo;
                        if (tipo==1) {
                            texto_tipo='Producto';
                        } else {
                            texto_tipo='Servicio';
                        }
                        var nueva_categoria ='<tr id="categoria'+id_categoria+'" class="">'+
                                                '<td>'+
                                                    '<input type="text" id="nombre_categoria_'+id_categoria+'" class="form-control" value="'+nombre_categoria+'">'+
                                                '</td>'+
                                                '<td>'+
                                                    '<input type="text" class="form-control" value="'+texto_tipo+'" disabled>'+
                                                '</td>'+
                                                '<td>'+
                                                    '<select class="form-control" id="habilitado_'+id_categoria+'">'+
                                                            '<option disabled>Mostrar categoria</option>'+
                                                            '<option selected value="1">Si</option>'+
                                                            '<option value="0">No</option>'+
                                                    '</select>'+
                                                '</td>'+
                                                '<td class="td-actions">'+
                                                    '<a href="#" class="btn btn-sm btn-primary btn-actualizar text-white mr-2" id="actualizar" data-id="'+id_categoria+'">Actualizar</a>'+
                                                    '<a href="{{route("verSubcategoriasView", '+id_categoria+')}}" class="btn btn-sm btn-primary text-white"'+
                                                    'data-toggle="tooltip" data-placement="top" title="Ver subcategorias">Ver subcategorías</a>'+
                                                '</td>'+
                                            '</tr>';

                        $("#tbody").append(nueva_categoria);
                    }
                    else{
                        $("#alert_nueva").addClass("alert-danger");
                    }
                    $("#alert_nueva").removeClass("d-none");
                    $("#mensaje_nueva").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

    </script>
@endsection
