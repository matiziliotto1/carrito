@extends('layouts.layout_admin')

@section('content')
<div class="col-lg-12">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Preguntas frecuentes</h6>
    </div>

    <div class="col-sm-12 mb-3">
        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal-crear-pregunta">Crear pregunta</a>
        
    </div>

    <div id="preguntas_frecuentes">
        @foreach ($preguntas_frecuentes as $pregunta_frecuente)
            <div class="card mb-4" id="pregunta{{$pregunta_frecuente->id_pregunta}}">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary m-0 p-0">{{$pregunta_frecuente->texto_pregunta}}</h6>
                </div>
                <div class="card-body m-0 p-0 pl-3">
                    <p>{{$pregunta_frecuente->texto_respuesta}}</p>
                </div>
                <div class="col-sm-12 mb-3">
                    <a href="{{route('admin.editarPreguntaFrecuente', $pregunta_frecuente->id_pregunta)}}" class="btn btn-primary">Editar</a>
                    <a href="#" class="btn btn-primary btn-eliminar" data-id="{{$pregunta_frecuente->id_pregunta}}">Eliminar</a>
                </div>
            </div>
        @endforeach
    </div>

  </div>

  <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-crear-pregunta" tabindex="-1" role="dialog" aria-labelledby="titulo_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal">Complete los campos y presione crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input class="form-control" id="texto_pregunta" name="texto_pregunta" type="text" placeholder="Ingrese la pregunta" required>
                </div>
                <div class="modal-body">
                    <input class="form-control" id="texto_respuesta" name="texto_respuesta" type="text" placeholder="Ingrese la respuesta" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-crear" id="crear-pregunta">Crear</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).on("click", ".btn-eliminar", function() {
            var token = '{{csrf_token()}}';
            var id_pregunta = $(this).attr("data-id");

            bootbox.dialog({
                message: "¿Esta seguro que desea eliminar esta pregunta?",
                title: "<i class='glyphicon glyphicon-trash'></i> Eliminar pregunta",
                buttons: {
                    success: {
                    label: "No",
                    className: "btn-success",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                    },
                    danger: {
                    label: "Si",
                    className: "btn-danger",
                    callback: function() {
                        $.ajax({
                            url: '{{route("admin.deletePreguntaFrecuente")}}',
                            type: 'post',
                            data: {
                                id_pregunta: id_pregunta,
                                _token: token
                            },
                            dataType: 'json',

                            beforeSend: function(){
                                // Show image container
                                $('#div_carga').fadeIn();
                            },
                            success: function(response){
                                $("#mensaje").empty();
                                $("#alert").removeClass("alert-success");
                                $("#alert").removeClass("alert-danger");

                                if(response['success']=="true"){
                                    $("#alert").addClass("alert-success");
                                }
                                else{
                                    $("#alert").addClass("alert-danger");
                                }
                                $("#alert").removeClass("d-none");
                                $("#mensaje").text(response['mensaje']);
                            },
                            complete:function(data){
                                $("#pregunta"+id_pregunta).remove();
                                // Hide image container
                                $('#div_carga').fadeOut();
                            }
                        })			  
                        }
                    }
                }
            });
        });

        $(".btn-crear").on("click",function(){
            var token = '{{csrf_token()}}';
            var texto_pregunta = $('#texto_pregunta').val();
            var texto_respuesta = $('#texto_respuesta').val();

            $.ajax({
                url: '{{route("admin.createPreguntaFrecuente")}}',
                type: 'post',
                data: {
                    texto_pregunta: texto_pregunta,
                    texto_respuesta: texto_respuesta,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");

                        
                        var id_pregunta = response['id_pregunta'];
                        var url = "{{ route('admin.editarServicio', ":id") }}";
                        url = url.replace(':id', id_pregunta);
                        var nueva_pregunta = '<div class="card mb-4" id="pregunta'+id_pregunta+'">'+
                                                '<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">'+
                                                    '<h6 class="m-0 font-weight-bold text-primary m-0 p-0">'+texto_pregunta+'</h6>'+
                                                '</div>'+
                                                '<div class="card-body m-0 p-0 pl-3">'+
                                                    '<p>'+texto_respuesta+'</p>'+
                                                '</div>'+
                                                '<div class="col-sm-12 mb-3">'+
                                                    '<a href="'+url+'" class="btn btn-primary mr-1">Editar</a>'+
                                                    '<a href="#" class="btn btn-primary btn-eliminar"  data-id="'+id_pregunta+'">Eliminar</a>'+
                                                '</div>'+
                                            '</div>';

                        $("#preguntas_frecuentes").append(nueva_pregunta);
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $('#texto_pregunta').val(""),
                    $('#texto_respuesta').val(""),
                    $('#modal-crear-pregunta').modal('toggle');

                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
    </script>
@endsection