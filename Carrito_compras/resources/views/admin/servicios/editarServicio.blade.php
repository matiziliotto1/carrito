@extends('layouts.layout_admin')

@section('content')
<style>
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
        -moz-appearance:textfield;
    }    
</style>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <a href="{{ route('admin.getServicios') }}">Regresar</a>
</div>
<div class="col-lg-12">
    <!-- General Element -->
    <div class="mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Editar servicio</h6>
        </div>
        <div class="card-body">
            <form class="row">
                @csrf

                <input type="hidden" name="id_servicio" id="id_servicio" value="{{$servicio->id_servicio}}">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Titulo</label>
                        <input type="text" class="form-control" name="titulo" id="titulo" value="{{$servicio->titulo}}" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <label for="exampleFormControlInput1">Precio</label>
                        
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text" id="basic-addon1">$</span>
                        </div>

                        <input type="number" class="form-control" name="precio" id="precio" value="{{$servicio->precio}}" required placeholder="0" aria-label="Precio"
                          aria-describedby="basic-addon1">
                      </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="id_subcategoria">Subcategoria</label>
                        <select name="id_subcategoria" id="id_subcategoria" required class="form-control">
                            <option disabled>Elija la subcategoria del servicio</option>
                            @foreach ($subcategorias as $subcategoria)
                                <option value="{{$subcategoria->id_subcategoria}}" @if($subcategoria->id_subcategoria == $servicio->id_subcategoria) selected @endif>{{$subcategoria->nombre_subcategoria}}</option>   
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="descripcion">Descripción resumida</label>
                        <textarea type="text" class="form-control" name="descripcion_resumida" id="descripcion_resumida" required>{{$servicio->descripcion_resumida}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="texto_respuesta">Descripción</label>
                        <textarea type="text" class="form-control" rows="5" name="descripcion" id="descripcion" required>{{$servicio->descripcion}}</textarea>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="destacado" id="destacado" @if($servicio->destacado==1) checked @endif>
                            <label class="custom-control-label" for="destacado">Destacar servicio?</label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="habilitado" id="habilitado" @if($servicio->habilitado==1) checked @endif>
                            <label class="custom-control-label" for="habilitado">Mostrar servicio?</label>
                        </div>
                    </div>
                </div>

                
                <br>
                <div class="col-sm-12 mt-3">
                    <a href="#" class="btn btn-primary btn-actualizar">Actualizar servicio</a>
                    <a href="{{route('verImagenesServicioView', $servicio->id_servicio)}}" class="btn btn-primary float-right mr-2">Ver imagenes</a>
                </div>
                
            </form>
        </div>
    </div>    

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">

    $(".btn-actualizar").on("click",function(){
        var token = '{{csrf_token()}}';

        var destacado;
        if($("#destacado").prop('checked')){
            destacado = 1;
        }else{
            destacado = 0;
        }

        var habilitado;
        if($("#habilitado").prop('checked')){
            habilitado = 1;
        }else{
            habilitado = 0;
        }

        $.ajax({
            url: '{{route("admin.updateServicio")}}',
            type: 'post',
            data: {
                id_servicio: $('#id_servicio').val(),
                id_subcategoria: $('#id_subcategoria').val(),
                titulo: $('#titulo').val(),
                precio: $('#precio').val(),
                descripcion: $('#descripcion').val(),
                descripcion_resumida: $('#descripcion_resumida').val(),
                destacado: destacado,
                habilitado: habilitado,
                _token: token
            },
            dataType: 'json',

            beforeSend: function(){
                // Show image container
                $('#div_carga').fadeIn();
            },
            success: function(response){
                $("#mensaje").empty();
                $("#alert").removeClass("alert-success");
                $("#alert").removeClass("alert-danger");

                if(response['success']=="true"){
                    $("#alert").addClass("alert-success");
                }
                else{
                    $("#alert").addClass("alert-danger");
                }
                $("#alert").removeClass("d-none");
                $("#mensaje").text(response['mensaje']);
            },
            complete:function(data){
                // Hide image container
                $('#div_carga').fadeOut();
            }
        });
    });
    </script>
@endsection
