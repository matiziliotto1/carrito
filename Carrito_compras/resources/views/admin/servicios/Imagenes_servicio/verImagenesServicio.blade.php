@extends('layouts.layout_admin')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="{{ route('admin.editarServicio',Request::route('id_servicio')) }}">Regresar</a>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Agregar imagen</h6>
                </div>
                <div class="form-group w-40 ml-4 mr-4">
                    <label>Imagen</label>
                    <div class="form-group form-width">
                        <div class="custom-file">
                        <input type="file" class="custom-file-input" id="archivo_imagen" aria-describedby="inputGroupFileAddon01" name="archivo_imagen" accept=".jpg,.jpeg,.png">
                        <label class="custom-file-label" for="archivo_imagen">Seleccionar imagen</label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <label>Imagen seleccionada:</label>
                        <br>
                        <img id='imgSalida' class='img-fluid' src='' width='200' height='200'>
                    </div>

                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="mostrar" id="mostrar" checked>
                            <label class="custom-control-label" for="mostrar">Mostrar imagen del servicio?</label>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-agregar" data-id="{{$id_servicio}}">Agregar imagen</button>
                </div>
            </div>
        </div>
        <div id="alert" class="alert alert-dismissible d-none col-lg-12 mb-4" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div id="mensaje">
            </div>
        </div>
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de imagenes</h6>
                </div>
                
                <div class="d-inline" id="imagenes">
                    @foreach ($imagenes as $imagen)
                        <div class="ml-4 d-inline-flex" style="width: 30%" id="imagen{{$imagen->id_imagen}}">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Imagen N° {{$imagen->orden}}</h6>
                                </div>
                                <div class="card-body bg-blue">
                                    <div class="chart-pie">
                                        <img src="{{asset($imagen->url)}}" alt="Error al cargar imagen" width="100%" height="100%"/>
                                    </div>
                                    <hr>
                                        <h6 class="m-0 font-weight-bold text-primary">
                                            Mostrar imagen: @if ($imagen->mostrar==1) Si @else No @endif
                                        </h6>
                                    <hr>
                                    <a href="{{route('editarImagenServicioView', $imagen->id_imagen)}}" class="btn btn-sm btn-primary">Editar</a>
                                    <button class="btn btn-sm btn-primary btn-eliminar" data-id="{{$imagen->id_imagen}}">Eliminar</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="card-footer">    
                    <!-- Pagination links --> 
                     {{ $imagenes->links() }}
                </div>
            </div>
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $('.custom-file-input').on('change', function() { 
            let fileName = $(this).val().split('\\').pop();
            $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
        });
        $('#archivo_imagen').change(function(e) {
            var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
            if ( !allowedExtensions.exec($('#archivo_imagen').val())) {
                $('#archivo_imagen').val('');
            } else {
                addImage(e); 
            }
            });

            function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
            
            if (!file.type.match(imageType))
            return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            }
        
            function fileOnload(e) {
            var result=e.target.result;
            $('#imgSalida').attr("src",result);
            }
            $(".btn-agregar").on("click",function(){
                var token = '{{csrf_token()}}';
                var formData = new FormData();
                var imagen = document.getElementById('archivo_imagen');
                var id_servicio = $(this).attr("data-id");
                var mostrar;
                if($("#mostrar").prop('checked')){
                    mostrar = 1;
                }else{
                    mostrar = 0;
                }

                if (document.getElementById('archivo_imagen').files[0]==null) {
                    $("#alert").addClass("alert-danger");
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text("Debes agregar una imagen con formato .jpg, .jpeg o .png");
                } else {
                    formData.append('archivo_imagen', imagen.files[0]);
                    formData.append('id_servicio', id_servicio);
                    formData.append('mostrar', mostrar);
                    formData.append('_token', token);
                    formData.append('_method', 'POST');

                    $.ajax({
                        url: '{{route("addImagenServicio")}}',
                        type: 'post',
                        data: formData,
                        dataType: 'json',
                        contentType: false,
                        cache: false,
                        processData: false,

                        beforeSend: function(){
                            // Show image container
                            $('#div_carga').fadeIn();
                        },
                        success: function(response){
                            $("#mensaje").empty();
                            $("#alert").removeClass("alert-success");
                            $("#alert").removeClass("alert-danger");

                            if(response['success']=="true"){
                                $("#alert").addClass("alert-success");
                                let id_imagen_nueva = response['id_imagen'];
                                let url_nueva = response['url'];
                                let orden_nueva = response['orden'];
                                let mostrar_nueva = '';
                                if (mostrar==1) {
                                    mostrar_nueva = 'Si';
                                } else {
                                    mostrar_nueva = 'No';
                                }
                                var url_editar = "{{ route('editarImagenServicioView', ":id") }}";
                                url_editar = url_editar.replace(':id', id_imagen_nueva);
                                let nueva_imagen ='<div class="ml-4 d-inline-flex" style="width: 30%" id="imagen'+id_imagen_nueva+'">'+
                                                    '<div class="card shadow mb-4">'+
                                                        '<div class="card-header py-3">'+
                                                            '<h6 class="m-0 font-weight-bold text-primary">Imagen N° '+orden_nueva+'</h6>'+
                                                        '</div>'+
                                                        '<div class="card-body bg-blue">'+
                                                            '<div class="chart-pie">'+
                                                                '<img src="/'+url_nueva+'" alt="Error al cargar imagen" width="100%" height="100%"/>'+
                                                            '</div>'+
                                                            '<hr>'+
                                                                '<h6 class="m-0 font-weight-bold text-primary">'+
                                                                    'Mostrar imagen: '+mostrar_nueva+
                                                                '</h6>'+
                                                            '<hr>'+
                                                            '<a href="'+url_editar+'" class="btn btn-sm btn-primary mr-1">Editar</a>'+
                                                            '<button class="btn btn-sm btn-primary btn-eliminar" data-id="'+id_imagen_nueva+'">Eliminar</button>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>';
                                $("#imagenes").append(nueva_imagen);
                            }
                            else{
                                $("#alert").addClass("alert-danger");
                            }
                            $("#alert").removeClass("d-none");
                            $("#mensaje").text(response['mensaje']);
                        },
                        complete:function(data){
                            // Hide image container
                            $('#div_carga').fadeOut();
                        }
                    });
                }
            });

            $(document).on("click", ".btn-eliminar", function() {
                var token = '{{csrf_token()}}';
                var id_imagen = $(this).attr("data-id");

                bootbox.dialog({
                message: "¿Esta seguro que desea eliminar esta imagen?",
                title: "<i class='glyphicon glyphicon-trash'></i> Eliminar imagen",
                buttons: {
                    success: {
                    label: "No",
                    className: "btn-success",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                    },
                    danger: {
                    label: "Si",
                    className: "btn-danger",
                    callback: function() {
                        $.ajax({
                            url: '{{route("deleteImagenServicio")}}',
                            type: 'delete',
                            data: {
                                id_imagen: id_imagen,
                                _token: token
                            },
                            dataType: 'json',

                            beforeSend: function(){
                                // Show image container
                                $('#div_carga').fadeIn();
                            },
                            success: function(response){
                                $("#mensaje").empty();
                                $("#alert").removeClass("alert-success");
                                $("#alert").removeClass("alert-danger");

                                if(response['success']=="true"){
                                    $("#alert").addClass("alert-success");
                                }
                                else{
                                    $("#alert").addClass("alert-danger");
                                }
                                $("#alert").removeClass("d-none");
                                $("#mensaje").text(response['mensaje']);
                            },
                            complete:function(data){
                                $("#imagen"+id_imagen).remove();
                                // Hide image container
                                $('#div_carga').fadeOut();
                            }
                        })			  
                        }
                    }
                }
                });
            });
    </script>
@endsection
