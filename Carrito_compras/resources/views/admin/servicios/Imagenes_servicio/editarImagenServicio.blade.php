@extends('layouts.layout_admin')

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    @php
        $imagen = App\Imagenes_producto::find(Request::route('id_imagen'));
    @endphp
    <a href="{{ route('verImagenesServicioView', $imagen->id_servicio) }}">Regresar</a>
</div>
<div class="col-lg-12">
    <!-- General Element -->
    <div class="mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Editar imagen del servicio</h6>
        </div>
        <div class="card-body">
            <form class="row">
                @csrf

                <input type="hidden" name="id_imagen" id="id_imagen" value="{{$imagen->id_imagen}}">
                <div class="col-sm-12">
                    <div class="form-group">
                        <h6 class="m-0">Imagen</h6>
                    </div>
                </div>
                
                <div class="col-sm-12">
                    <div class="form-group form-width">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01" name="archivo_imagen" onchange="checkextension()" accept=".jpg,.jpeg,.png">
                            <label class="custom-file-label" for="inputGroupFile01">Seleccionar imagen</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class='form-group'>
                        <label for="imgSalida">Imagen actual</label><br>
                        <img id='imgSalida' class='img-fluid' src='{{asset($imagen->url)}}' width='300' height='300'>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="">
                        <div class="form-group">
                            <label>Orden para mostrar</label>
                            <select name="orden" id="orden" required class="form-control">
                                <option disabled>Elija el orden</option>
                                {{$i=0}}
                                @foreach ($ordenes as $orden)
                                    <option value="{{$orden->orden}}" @if($imagen->orden == $orden->orden) selected @endif>{{$orden->orden}}</option>
                                {{$i=$i+1}}
                                @endforeach
                                    <option value="{{$i}}">Agregar al final ({{$i}})</option>
                            </select>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input type="checkbox" class="custom-control-input" name="mostrar" id="mostrar" @if($imagen->mostrar==1) checked @endif>
                                    <label class="custom-control-label" for="mostrar">Mostrar imagen?</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="col-sm-12 mt-3">
                    <a href="#" class="btn btn-primary btn-editar">Editar imagen</a>
                </div>
            </form>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    @if(session('mensaje'))
        <div class="alert alert-info alert-dismissible fade show m-5 mx-auto" 
            style="width: 50%;" role="alert">
            {{ session('mensaje') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div> 
    @endif

@endsection

@section('scripts')
    <script type="text/javascript">
        $('.custom-file-input').on('change', function() { 
            let fileName = $(this).val().split('\\').pop();
            $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
        });
        $('#inputGroupFile01').change(function(e) {
            var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
            if ( !allowedExtensions.exec($('#inputGroupFile01').val())) {
                $('#inputGroupFile01').val('');
                document.getElementById("div-alert").style.display = "block";
            } else {
                addImage(e); 
            }
            });

            function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
            
            if (!file.type.match(imageType))
            return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            }
        
            function fileOnload(e) {
            var result=e.target.result;
            $('#imgSalida').attr("src",result);
            }

            $(".btn-editar").on("click",function(){
            var token = '{{csrf_token()}}';
            var formData = new FormData();
            var imagen = document.getElementById('inputGroupFile01');
            var id_imagen = $('#id_imagen').val();
            var orden = $('#orden').val();
            var mostrar;
            if($("#mostrar").prop('checked')){
                mostrar = 1;
            }else{
                mostrar = 0;
            }

            formData.append('archivo_imagen', imagen.files[0]);
            formData.append('id_imagen', id_imagen);
            formData.append('orden', orden);
            formData.append('mostrar', mostrar);
            formData.append('_token', token);
            formData.append('_method', 'PUT');

            $.ajax({
                url: '{{route("updateImagenServicio")}}',
                type: 'post',
                data: formData,
                dataType: 'json',
                contentType: false,
                cache: false,
                processData: false,

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
    </script>
@endsection