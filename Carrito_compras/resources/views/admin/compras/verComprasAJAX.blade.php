@if(count($compras)>0)
    @foreach($compras as $compra)
        <tr>
            <input type="hidden" class="form-control" id="tipo_compra_{{$compra->id_compra}}" value="{{$compra->tipo_compra}}" readonly>
            <td>
                <div style="width: 100px;">
                    <input type="text" class="form-control" value="{{$compra->id_compra}}" readonly>
                </div>
            </td>
            <td>
                @php
                    $compra->fecha_compra = date('d-m-Y', strtotime($compra->fecha_compra));
                @endphp
                
                <div style="width: 140px;"><input type="text" class="form-control" value="{{$compra->fecha_compra}}" readonly></div>
            </td>
            <td>
                <select class="form-control" id="estado_{{$compra->id_compra}}">
                    <option value="0" disabled>Seleccione un estado</option>
                    <option value="Procesando" @if($compra->estado == "Procesando") selected @endif>Procesando</option>
                    <option value="En camino" @if($compra->estado == "En camino") selected @endif>En camino</option>
                    <option value="Finalizada" @if($compra->estado == "Finalizada") selected @endif>Finalizada</option>
                </select>
            </td>
            @if($compra->tipo_compra == 1)
                <td>
                    <input type="text" class="form-control" id="codigo_seguimiento_{{$compra->id_compra}}" value="{{$compra->codigo_seguimiento}}">
                </td>
            @endif
            <td>
                <a href="#" class="btn btn-sm btn-primary btn-actualizar-compra" id="actualizar-compra" data-id="{{$compra->id_compra}}">Actualizar</a>
                <a href="{{route("admin.verCompra", $compra->id_compra)}}" class="btn btn-sm btn-primary">Ver compra</a>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="4">No hay resultados para ese N° de compra y/o fecha seleccionados.</td>
    </tr>
@endif