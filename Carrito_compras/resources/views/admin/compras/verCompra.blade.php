@extends('layouts.layout_admin')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="{{ url()->previous() }}">Regresar</a>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Viendo la compra N° {{$compra->id_compra}}</h6>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card mb-4">
                            <div class="card-body m-0 p-0 pl-3 mt-1">
                                <h5><strong>Datos del cliente</strong></h5>
                                <p><strong>Nombre:</strong> {{$usuario->nombre}}</p>
                                <p><strong>Apellido:</strong> {{$usuario->apellido}}</p>
                                <p><strong>Email:</strong> {{$usuario->email}}</p>
                            
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="card mb-4">
                            <div class="card-body m-0 p-0 pl-3 mt-1">
                                <h5><strong>Datos de la compra</strong></h5>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?php
                                            $fecha_compra = date("d-m-Y", strtotime($compra->fecha_compra));
                                        ?>
                                        <p><strong>Fecha:</strong> {{$fecha_compra}}</p>
                                        <p><strong>Monto:</strong> ${{number_format($compra->monto_total, 2, ",", ".")}}</p>
                                        <p><strong>Estado:</strong> {{$compra->estado}}</p>
                                        <p><strong>Metodo de envio:</strong> @if($compra->metodo_envio) {{$compra->metodo_envio}} @else - @endif</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p><strong>Costo de envio:</strong> @if($compra->monto_envio) ${{number_format($compra->monto_envio, 2, ",", ".")}} @else - @endif</p>
                                        <p><strong>Codigo de seguimiento:</strong> @if($compra->codigo_seguimiento) {{$compra->codigo_seguimiento}} @else - @endif</p>
                                        <p><strong>El cliente obtuvo un beneficio de:</strong> @if($compra->beneficio) {{$compra->beneficio}} por puntaje acumulado @else - @endif</p>
                                        {{-- //TODO: este lo mostramos? <p><strong> Se aplico un descuento de:</strong> @if($compra->descuento) {{$compra->descuento}}% @else - @endif</p> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h5><strong>Items comprados</strong></h5>
                </div>

                @if(isset($compra->productos))
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($compra->productos as $producto)
                                    <tr>
                                        {{-- //TODO: aca deberiamos poner un link al producto comprado? --}}
                                        <td>{{$producto->nombre}}</td>
                                        <td>{{$producto->cantidad}}</td>
                                        <td>${{number_format($producto->precio, 2, ",", ".")}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                
                @if(isset($compra->servicios))
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>Servicio</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($compra->servicios as $servicio)
                                    <tr>
                                        {{-- //TODO: aca deberiamos poner un link al producto comprado? --}}
                                        <td>{{$servicio->titulo}}</td>
                                        <td>${{number_format($servicio->precio, 2, ",", ".")}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        
    </script>

@endsection
