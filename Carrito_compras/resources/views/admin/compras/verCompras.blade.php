@extends('layouts.layout_admin')

@section('content')

    <style>
        table > tbody > tr > td{
            padding-right: 0px !important;
        }
    </style>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-2 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de compras @if(isset($email_usuario)) del cliente {{$email_usuario}} @endif @if(Route::current()->getName() == "admin.verComprasProductos") de productos @elseif(Route::current()->getName() == "admin.verComprasServicios") de servicios @endif</h6>
                </div>
                @if(Route::current()->getName() == "admin.verComprasProductos" || Route::current()->getName() == "admin.verComprasServicios")
                    <h6 class="m-0 px-2 font-weight-bold text-secondary">Filtros:</h6>
                    <div class="row py-2 px-2">
                        <div class="input-group w-50 col-8">
                            <input type="search" class="form-control bg-light border-1 small" name="buscador_id_compra" id="buscador_id_compra" placeholder="Ingrese el N° de compra" style="border-color: #3f51b5;">
                            @if(Route::current()->getName() == "admin.verComprasProductos")
                                <input type="hidden" name="tipo_compra" id="tipo_compra" value="1">
                            @elseif(Route::current()->getName() == "admin.verComprasServicios")
                                <input type="hidden" name="tipo_compra" id="tipo_compra" value="0">
                            @endif
                        </div>
                        <div class="w-25 col-4">
                                <input type="date" class="form-control bg-light border-1 small" name="buscador_fecha_compra" id="buscador_fecha_compra" style="border-color: #3f51b5;">
                        </div>
                    </div>
                @endif
                
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>N° compra</th>
                                <th>Fecha de compra</th>
                                <th>Estado</th>
                                @if(Route::current()->getName() == "admin.verComprasProductos")
                                    <th>Codigo de seguimiento</th>
                                @endif
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @if(count($compras)>0)
                                @foreach($compras as $compra)
                                    <tr>
                                        <input type="hidden" class="form-control" id="tipo_compra_{{$compra->id_compra}}" value="{{$compra->tipo_compra}}" readonly>
                                        <td>
                                            <div style="width: 100px;">
                                                <input type="text" class="form-control" value="{{$compra->id_compra}}" readonly>
                                            </div>
                                        </td>
                                        <td>
                                            @php
                                                $compra->fecha_compra = date('d-m-Y', strtotime($compra->fecha_compra));
                                            @endphp
                                            
                                            <div style="width: 140px;"><input type="text" class="form-control" value="{{$compra->fecha_compra}}" readonly></div>
                                        </td>
                                        <td>
                                            <select class="form-control select_estado" id="estado_{{$compra->id_compra}}" data-id="{{$compra->estado}}">
                                                <option value="0" disabled>Seleccione un estado</option>
                                                <option value="Procesando" @if($compra->estado == "Procesando") selected @endif>Procesando</option>
                                                <option value="En camino" @if($compra->estado == "En camino") selected @endif>En camino</option>
                                                <option value="Finalizada" @if($compra->estado == "Finalizada") selected @endif>Finalizada</option>
                                            </select>
                                        </td>
                                        @if(Route::current()->getName() == "admin.verComprasProductos")
                                            <td>
                                                <input type="text" class="form-control input_codigo_seguimiento" id="codigo_seguimiento_{{$compra->id_compra}}" value="{{$compra->codigo_seguimiento}}" data-id="{{$compra->codigo_seguimiento}}">
                                            </td>
                                        @endif
                                        <td>
                                            <a href="#" class="btn btn-sm btn-primary disabled" id="actualizar-compra_{{$compra->id_compra}}" data-id="{{$compra->id_compra}}">Actualizar</a>
                                            <a href="{{route("admin.verCompra", $compra->id_compra)}}" class="btn btn-sm btn-primary">Ver compra</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">@if(isset($email_usuario)) Este cliente no realizo ninguna compra.@else Aun no se realizo ninguna compra @if(Route::current()->getName() == "admin.verComprasProductos") de productos. @elseif(Route::current()->getName() == "admin.verComprasServicios") de servicios. @endif @endif</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">    
                    <!-- Pagination links -->
                    {{ $compras->links() }}
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).on('click', '.btn-actualizar-compra', function() {
            var token = '{{csrf_token()}}';
            var id_compra = $(this).attr("data-id");
            var estado = $("#estado_"+id_compra).val();
            var tipo_compra = $("#tipo_compra_"+id_compra).val();
            var codigo_seguimiento;
            if(tipo_compra == 1){
                codigo_seguimiento = $("#codigo_seguimiento_"+id_compra).val();
            }

            $.ajax({
                url: '{{route("updateCompra")}}',
                type: 'post',
                data: {
                    id_compra: id_compra,
                    estado: estado,
                    codigo_seguimiento: codigo_seguimiento,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");

                        //Actualizo los atributos data-id del estado y codigo de seguimiento de la compra actualizada
                        $("#estado_"+id_compra).attr("data-id", estado);
                        if(tipo_compra == 1){
                            $("#codigo_seguimiento_"+id_compra).attr("data-id", codigo_seguimiento);
                        }

                        //Desactivo el boton de actualizar nuevamente.
                        $("#actualizar-compra_"+id_compra).removeClass("btn-actualizar-compra");
                        $("#actualizar-compra_"+id_compra).addClass("disabled");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        var id_compra;
        var fecha_compra;

        $("#buscador_id_compra").on("keyup",function(){
            id_compra = $(this).val();
            var tipo_compra = $("#tipo_compra").val();

            $.ajax({
                url: '{{route("admin.buscadorCompras")}}',
                type: 'get',
                data: {
                    id_compra: id_compra,
                    tipo_compra: tipo_compra,
                    fecha_compra: fecha_compra,
                },
                success: function(response){
                    $("#tbody").html(response);
                },
            });
        });

        $("#buscador_fecha_compra").on("change",function(){
            var tipo_compra = $("#tipo_compra").val();
            fecha_compra = $(this).val();

            $.ajax({
                url: '{{route("admin.buscadorCompras")}}',
                type: 'get',
                data: {
                    id_compra: id_compra,
                    tipo_compra: tipo_compra,
                    fecha_compra: fecha_compra,
                },
                success: function(response){
                    $("#tbody").html(response);
                },
            });
        });

        $(".select_estado").on("change", function() {
            checkCambios(this.id.substring(7), true);
        });

        $(".input_codigo_seguimiento").on("change", function() {
            checkCambios(this.id.substring(19), false);
        });

        function checkCambios(id_compra, select){
            let seleccionado;
            let guardado_en_bd;

            //Si el cambio se realizo en el SELECT
            if(select == true){
                seleccionado = $("#estado_"+id_compra).children("option:selected").val();
                guardado_en_bd = $("#estado_"+id_compra).attr("data-id");
            }
            else{ //Si el cambio se realizo en el INPUT

                seleccionado = $("#codigo_seguimiento_"+id_compra).val();
                guardado_en_bd = $("#codigo_seguimiento_"+id_compra).attr("data-id");
            }

            if(seleccionado === guardado_en_bd){

                //Si el cambio se realizo en el SELECT
                if(select == true){
                    //Me fijo si en el INPUT se realizo ningun cambio, si no se hizo ningun cambio desactivo el boton de actualizar.
                    if( $("#codigo_seguimiento_"+id_compra).val() === $("#codigo_seguimiento_"+id_compra).attr("data-id") ){
                        $("#actualizar-compra_"+id_compra).removeClass("btn-actualizar-compra");
                        $("#actualizar-compra_"+id_compra).addClass("disabled");
                    }
                }
                else{//Si el cambio se realizo en el INPUT

                    //Me fijo si en el SELECT se realizo algun cambio, si no se hizo ningun cambio desactivo el boton de actualizar.
                    if( $("#estado_"+id_compra).children("option:selected").val() === $("#estado_"+id_compra).attr("data-id") ){
                        $("#actualizar-compra_"+id_compra).removeClass("btn-actualizar-compra");
                        $("#actualizar-compra_"+id_compra).addClass("disabled");
                    }
                }
            }
            else{
                $("#actualizar-compra_"+id_compra).removeClass("disabled");
                $("#actualizar-compra_"+id_compra).addClass("btn-actualizar-compra");
            }
        }
    </script>

@endsection
