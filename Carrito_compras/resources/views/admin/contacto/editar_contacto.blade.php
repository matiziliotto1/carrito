@extends('layouts.layout_admin')

@section('content')

<div class="col-lg-12">
    <!-- General Element -->
    <div class="mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Editar informacion de contacto</h6>
        </div>
        <div class="card-body">
            <form class="row">
                @csrf

                <div class="col-sm-12">
                    <div class="form-group">
                        <h6 class="m-0 font-weight-bold text-primary">Teléfonos</h6>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Teléfono n°: 1</label>
                        <input type="text" class="form-control" name="telefono_1" id="telefono_1" value="{{$contacto->telefono_1}}" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Teléfono n°: 2</label>
                        <input type="text" class="form-control" name="telefono_2" id="telefono_2" value="{{$contacto->telefono_2}}" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Teléfono n°: 3</label>
                        <input type="text" class="form-control" name="telefono_3" id="telefono_3" value="{{$contacto->telefono_3}}" required>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <h6 class="m-0 font-weight-bold text-primary">Links de redes sociales</h6>
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Link de facebook</label>
                        <input type="text" class="form-control" name="facebook" id="facebook" value="{{$contacto->facebook}}" required>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Link de instagram</label>
                        <input type="text" class="form-control" name="instagram" id="instagram" value="{{$contacto->instagram}}" required>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Link de twitter</label>
                        <input type="text" class="form-control" name="twitter" id="twitter" value="{{$contacto->twitter}}" required>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Link de youtube</label>
                        <input type="text" class="form-control" name="youtube" id="youtube" value="{{$contacto->youtube}}" required>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <h6 class="m-0 font-weight-bold text-primary">Emails</h6>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email de informacion</label>
                        <input type="text" class="form-control" name="email_info" id="email_info" value="{{$contacto->email_info}}" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email de comprobantes</label>
                        <input type="text" class="form-control" name="email_comprobantes" id="email_comprobantes" value="{{$contacto->email_comprobantes}}" required>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Email de reclamos</label>
                        <input type="text" class="form-control" name="email_reclamos" id="email_reclamos" value="{{$contacto->email_reclamos}}" required>
                    </div>
                </div>

                <br>
                <div class="col-sm-12 text-center mt-3">
                    <a href="#" class="btn btn-primary btn-actualizar">Actualizar informacion de contacto</a>
                </div>
            </form>
        </div>
    </div>    

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">

        $(".btn-actualizar").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{route("editarContacto")}}',
                type: 'post',
                data: {
                    telefono_1: $('#telefono_1').val(),
                    telefono_2: $('#telefono_2').val(),
                    telefono_3: $('#telefono_3').val(),
                    facebook: $('#facebook').val(),
                    instagram: $('#instagram').val(),
                    twitter: $('#twitter').val(),
                    youtube: $('#youtube').val(),
                    email_info: $('#email_info').val(),
                    email_comprobantes: $('#email_comprobantes').val(),
                    email_reclamos: $('#email_reclamos').val(),
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
    </script>
@endsection
