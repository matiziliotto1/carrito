@if(count($usuarios)>0)
    @foreach ($usuarios as $usuario)
        <tr id="usuario{{$usuario->id}}" class="">
            <td>{{$usuario->id}}</td>
            <td>{{$usuario->nombre}}</td>
            <td>{{$usuario->apellido}}</td>
            <td>{{$usuario->email}}</td>
            
            <td class="td-actions">
                <a href="{{route("admin.getComprasXCliente", $usuario->id)}}" class="btn btn-sm btn-primary">Ver compras</a>
                @switch(Route::current()->getName())
                    @case("getUsuarios")
                        <a href="#" class="btn btn-sm btn-primary btn-deshabilitar" id="deshabilitar" data-id="{{$usuario->id}}">Deshabilitar</a>
                        <a href="#" class="btn btn-sm btn-primary btn-eliminar" id="eliminar" data-id="{{$usuario->id}}">Eliminar</a>
                    @break
                    @case("getUsuariosEliminados")
                        <a href="#" class="btn btn-sm btn-primary btn-restaurar" id="restaurar" data-id="{{$usuario->id}}">Restaurar</a>
                    @break
                    @case("getUsuariosDeshabilitados")
                        <a href="#" class="btn btn-sm btn-primary btn-habilitar" id="habilitar" data-id="{{$usuario->id}}">Habilitar</a>
                    @break
                @endswitch
                <a href="#" class="btn btn-sm btn-primary more-info" data-id="{{$usuario->id}}">+ Info</a>
            </td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="4">No hay resultados para ese usuario.</td>
    </tr>
@endif