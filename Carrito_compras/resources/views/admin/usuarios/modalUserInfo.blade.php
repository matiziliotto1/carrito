<div class="modal fade" id="userInfoModal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true" aria-labelledby="userInfoModalLabel" style="display:none">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form>
                <div class="modal-header">
                    <h5 class="modal-title" id="userInfoModalLabel">Viendo información del usuario:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body overlay overlay-block">
                    <div class="form-group mb-0">
                        <input class="form-control" type="hidden" id="id_usuario" name="id_usuario" required>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="nombre" name="nombre" required autofocus placeholder="Nombre/s">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="apellido" name="apellido" required placeholder="Apellido/s">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" id="tipo_documento" name="tipo_documento" required>
                                    <option disabled selected>Seleccione el tipo de documento</option>
                                    <option value="DNI">DNI</option>
                                    <option value="LE">LE</option>
                                    <option value="LC">LC</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="numero_documento"  name="numero_documento" required placeholder="Número de documento">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" id="provincia" name="provincia" required>
                                    <option disabled selected>Seleccione la provincia</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" id="ciudad" name="ciudad" required>
                                    <option disabled selected>Seleccione la ciudad</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="codigo_postal" name="codigo_postal" required placeholder="Codigo postal">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="domicilio_real" name="domicilio_real" required placeholder="Domicilio personal">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="telefono_fijo" name="telefono_fijo" required placeholder="Teléfono">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="telefono_celular" name="telefono_celular" required placeholder="Celular">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" id="provincia_recepcion" name="provincia_recepcion" required>
                                    <option disabled selected>Seleccione la provincia de recepción</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <select class="form-control" id="ciudad_recepcion" name="ciudad_recepcion" required>
                                    <option disabled selected>Seleccione la ciudad de recepción</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="codigo_postal_recepcion" name="codigo_postal_recepcion" required placeholder="Codigo postal de recepción">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="domicilio_recepcion" name="domicilio_recepcion" required placeholder="Domicilio de entrega">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="text" id="email" name="email" required placeholder="Email">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input class="form-control" type="password" id="password" name="password" placeholder="Nueva contraseña?">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Cancelar</button>
                    <a href="#" class="btn btn-primary font-weight-bold save-info">Guardar</a>
                </div>
            </form>
        </div>
    </div>
</div>