@extends('layouts.layout_admin')

@section('content')
<style>
    table > tbody > tr > td{
        padding-right: 0px !important;
    }
</style>
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de usuarios
                        @switch(Route::current()->getName())
                            @case("getUsuariosEliminados")
                                eliminados
                                @break
                            @case("getUsuariosDeshabilitados")
                                deshabilitados
                        @endswitch
                    </h6>
                </div>

                <h6 class="m-0 px-2 font-weight-bold text-secondary">Filtros:</h6>
                <div class="row py-2 px-2">
                    <div class="input-group w-50 col-4">
                        <input type="search" class="form-control bg-light border-1 small" name="nombre" id="buscador_nombre" placeholder="Ingrese el nombre del usuario" style="border-color: #3f51b5;">
                    </div>
                    <div class="input-group w-50 col-4">
                        <input type="search" class="form-control bg-light border-1 small" name="apellido" id="buscador_apellido" placeholder="Ingrese el apellido del usuario" style="border-color: #3f51b5;">
                    </div>
                    <div class="w-25 col-4">
                        <input type="email" class="form-control bg-light border-1 small" name="email" id="buscador_email" style="border-color: #3f51b5;" placeholder="Ingrese el correo del usuario">
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>N° cliente</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @if(count($usuarios)>0)
                                @foreach ($usuarios as $usuario)
                                    <tr id="usuario{{$usuario->id}}" class="">
                                        <td>{{$usuario->id}}</td>
                                        <td>{{$usuario->nombre}}</td>
                                        <td>{{$usuario->apellido}}</td>
                                        <td>{{$usuario->email}}</td>
                                        
                                        <td class="td-actions">
                                            <a href="{{route("admin.getComprasXCliente", $usuario->id)}}" class="btn btn-sm btn-primary">Ver compras</a>
                                            @switch(Route::current()->getName())
                                                @case("getUsuarios")
                                                    <a href="#" class="btn btn-sm btn-primary btn-deshabilitar" id="deshabilitar" data-id="{{$usuario->id}}">Deshabilitar</a>
                                                    <a href="#" class="btn btn-sm btn-primary btn-eliminar" id="eliminar" data-id="{{$usuario->id}}">Eliminar</a>
                                                @break
                                                @case("getUsuariosEliminados")
                                                    <a href="#" class="btn btn-sm btn-primary btn-restaurar" id="restaurar" data-id="{{$usuario->id}}">Restaurar</a>
                                                @break
                                                @case("getUsuariosDeshabilitados")
                                                    <a href="#" class="btn btn-sm btn-primary btn-habilitar" id="habilitar" data-id="{{$usuario->id}}">Habilitar</a>
                                                @break
                                            @endswitch
                                            <a href="#" class="btn btn-sm btn-primary more-info" data-id="{{$usuario->id}}">+ Info</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @switch(Route::current()->getName())
                                    @case("getUsuarios")
                                        <td colspan="5">Aun no hay usuarios registrados.</td>
                                    @break
                                    @case("getUsuariosEliminados")
                                        <td colspan="5">No hay usuarios eliminados.</td>
                                    @break
                                    @case("getUsuariosDeshabilitados")
                                        <td colspan="5">No hay usuarios deshabilitados.</td>
                                    @break
                                @endswitch
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">    
                    <!-- Pagination links -->
                    {{ $usuarios->links() }}
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    @include('admin.usuarios.modalUserInfo')

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $( document ).ready(function() {
            $.ajax({
                url: '{{route("getProvincias")}}',
                type: 'get',
                dataType: 'json',
                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['provincias'].length-1; i++) {
                            $("#provincia").append('<option value="'+response['provincias'][i].id_provincia+'">'+response['provincias'][i].nombre+'</option>');
                        }

                        for (var i =  0; i < response['provincias'].length-1; i++) {
                            $("#provincia_recepcion").append('<option value="'+response['provincias'][i].id_provincia+'">'+response['provincias'][i].nombre+'</option>');
                        }
                    }
                },
            });
        });

        $(".more-info").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_usuario = $(this).attr("data-id");

            $.ajax({
                url: '{{route("admin.users.info.ver")}}',
                method: "GET",
                data: {
                    id_user: id_usuario,
                    _token: token
                },
                success: function(response){
                    $("#id_usuario").val(response['id'])
                    $("#nombre").val(response['nombre']);
                    $("#apellido").val(response['apellido']);

                    let tipo_documento_element = document.getElementById('tipo_documento');
                    tipo_documento_element.value = response['tipo_documento'];

                    $("#numero_documento").val(response['numero_documento']);
                    $("#domicilio_real").val(response['domicilio_real']);
                    $("#codigo_postal").val(response['codigo_postal']);
                    $("#telefono_fijo").val(response['telefono_fijo']);
                    $("#telefono_celular").val(response['telefono_celular']);
                    $("#domicilio_recepcion").val(response['domicilio_recepcion']);
                    $("#codigo_postal_recepcion").val(response['codigo_postal_recepcion']);
                    $("#email").val(response['email']);
                    $("#password").val('');

                    let provincia_element = document.getElementById('provincia');
                    provincia_element.value = response['provincia'];
                    getLocalidades(response['provincia'], 'ciudad', response['ciudad']);

                    let provincia_recepcion_element = document.getElementById('provincia_recepcion');
                    provincia_recepcion_element.value = response['provincia_recepcion'];
                    getLocalidades(response['provincia_recepcion'], 'ciudad_recepcion', response['ciudad_recepcion']);

                    $('#userInfoModal').modal();
                    
                },
            });
        });

        $(document).on("change", "#provincia", function() {
            $("#ciudad option").remove();
            let id_provincia = $(this).children("option:selected").val();

            $('#provincia option[value="'+id_provincia+'"]').attr('selected', "selected");

            getLocalidades(id_provincia, 'ciudad');
        });

        $(document).on("change", "#provincia_recepcion", function() {
            $("#ciudad_recepcion option").remove();
            let id_provincia = $(this).children("option:selected").val();

            $('#provincia_recepcion option[value="'+id_provincia+'"]').attr('selected', "selected");

            getLocalidades(id_provincia, 'ciudad_recepcion');
        });

        function getLocalidades(id_provincia, select, id_ciudad=null){
            let url = "{{route('getLocalidades', ":id_provincia")}}";
            url = url.replace(':id_provincia', id_provincia);

            let ciudad = id_ciudad ? id_ciudad : "";
            $.ajax({
                url: url,
                type: 'get',
                dataType: 'json',
                success: function(response){
                    if(response['success']){
                        for (var i =  0; i < response['localidades'].length-1; i++) {
                            $('#'+select).append('<option value="'+response['localidades'][i].id_localidad+'">'+response['localidades'][i].nombre+'</option>');
                        }

                        if (ciudad !== "") {
                            let ciudad_element = document.getElementById(select);
                            ciudad_element.value = ciudad;
                        }
                    }
                },
            });
        }

        $(".save-info").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{ route('admin.users.info.save') }}',
                method: "POST",
                data: {
                    id_user: $("#id_usuario").val(),
                    nombre: $("#nombre").val(),
                    apellido: $("#apellido").val(),
                    tipo_documento: $("#tipo_documento").val(),
                    numero_documento: $("#numero_documento").val(),
                    domicilio_real: $("#domicilio_real").val(),
                    codigo_postal: $("#codigo_postal").val(),
                    telefono_fijo: $("#telefono_fijo").val(),
                    telefono_celular: $("#telefono_celular").val(),
                    domicilio_recepcion: $("#domicilio_recepcion").val(),
                    codigo_postal_recepcion: $("#codigo_postal_recepcion").val(),
                    email: $("#email").val(),
                    password: $("#password").val(),
                    provincia: $("#provincia").val(),
                    provincia_recepcion: $("#provincia_recepcion").val(),
                    ciudad: $("#ciudad").val(),
                    ciudad_recepcion: $("#ciudad_recepcion").val(),
                    _token: token
                },
                success: function(response){
                    $('#userInfoModal').modal('toggle');

                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
            });
        });

        $(".btn-eliminar").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_usuario = $(this).attr("data-id");

            bootbox.dialog({
                message: "¿Esta seguro que desea eliminar este cliente?",
                title: "<i class='glyphicon glyphicon-trash'></i> Eliminar cliente",
                buttons: {
                    success: {
                    label: "No",
                    className: "btn-success",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                    },
                    danger: {
                    label: "Si",
                    className: "btn-danger",
                    callback: function() {
                        $.ajax({
                            url: '{{route("deleteUsuario")}}',
                            type: 'post',
                            data: {
                                id_usuario: id_usuario,
                                _token: token
                            },
                            dataType: 'json',

                            beforeSend: function(){
                                // Show image container
                                $('#div_carga').fadeIn();
                            },
                            success: function(response){
                                $("#mensaje").empty();
                                $("#alert").removeClass("alert-success");
                                $("#alert").removeClass("alert-danger");

                                if(response['success']=="true"){
                                    $("#alert").addClass("alert-success");
                                }
                                else{
                                    $("#alert").addClass("alert-danger");
                                }
                                $("#alert").removeClass("d-none");
                                $("#mensaje").text(response['mensaje']);
                            },
                            complete:function(data){
                                $("#usuario"+id_usuario).remove();
                                // Hide image container
                                $('#div_carga').fadeOut();
                            }
                        });			  
                        }
                    }
                }
            });
        });

        $(".btn-deshabilitar").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_usuario = $(this).attr("data-id");

            $.ajax({
                url: '{{route("deshabilitarUsuario")}}',
                type: 'post',
                data: {
                    id_usuario: id_usuario,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $("#usuario"+id_usuario).remove();
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        $(".btn-restaurar").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_usuario = $(this).attr("data-id");

            $.ajax({
                url: '{{route("restaurarUsuario")}}',
                type: 'post',
                data: {
                    id_usuario: id_usuario,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $("#usuario"+id_usuario).remove();
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        $(".btn-habilitar").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_usuario = $(this).attr("data-id");

            $.ajax({
                url: '{{route("habilitarUsuario")}}',
                type: 'post',
                data: {
                    id_usuario: id_usuario,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $("#usuario"+id_usuario).remove();
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        var nombre;
        var apellido;
        var email;

        $("#buscador_nombre").on("keyup",function(){
            nombre = $(this).val();

            $.ajax({
                url: '{{route("admin.buscadorUsuario")}}',
                type: 'get',
                data: {
                    nombre: nombre,
                    apellido: apellido,
                    email: email,
                },
                success: function(response){
                    $("#tbody").html(response);
                },
            });
        });

        $("#buscador_apellido").on("keyup",function(){
            apellido = $(this).val();

            $.ajax({
                url: '{{route("admin.buscadorUsuario")}}',
                type: 'get',
                data: {
                    nombre: nombre,
                    apellido: apellido,
                    email: email,
                },
                success: function(response){
                    $("#tbody").html(response);
                },
            });
        });

        $("#buscador_email").on("keyup",function(){
            email = $(this).val();

            $.ajax({
                url: '{{route("admin.buscadorUsuario")}}',
                type: 'get',
                data: {
                    nombre: nombre,
                    apellido: apellido,
                    email: email,
                },
                success: function(response){
                    $("#tbody").html(response);
                },
            });
        });
    </script>
@endsection
