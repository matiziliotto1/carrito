@extends('layouts.layout_admin')

@section('content')

<div class="col-lg-12">
    <!-- General Element -->
    <div class="mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Mis datos</h6>
        </div>
        <div class="card-body">
            <form class="row">
                @csrf

                <div class="col-sm-12">
                    <div class="form-group">
                        <h6 class="m-0 font-weight-bold text-primary">Datos de la cuenta</h6>
                    </div>
                </div>
                
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" value="{{$administrador->nombre}}" required>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                        <input type="text" class="form-control" name="apellido" id="apellido" value="{{$administrador->apellido}}" required>
                    </div>
                </div>
                
                <div class="col-sm-9">
                    <div class="form-group">
                        <label for="nombre_usuario">Nombre de usuario</label>
                        <input type="text" class="form-control" name="nombre_usuario" id="nombre_usuario" value="{{$administrador->nombre_usuario}}" required readonly>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="editar_nombre_usuario">Editar nombre de usuario</label><br>
                        <a href="#" data-toggle="modal" data-target="#modal-password" id="editar_nombre_usuario" class="btn btn-primary btn-editar-nombre_usuario">Editar</a>
                    </div>
                </div>

                <div class="col-sm-9">
                    <div class="form-group">
                        <label for="password_view">Contraseña</label>
                        <input type="password" class="form-control" name="password_view" id="password_view" value="{{Str::limit($administrador->password, $limit=8)}}" required readonly>
                        <input type="hidden" class="form-control" name="password" id="password" value="{{$administrador->password}}" required>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="editar_password">Editar contraseña</label><br>
                        <a href="#" data-toggle="modal" data-target="#modal-password2" id="editar_password" class="btn btn-primary btn-editar-password">Editar</a>
                    </div>
                </div>
                
                <br>
                <div class="col-sm-12 text-center mt-3">
                    <a href="#" class="btn btn-primary btn-actualizar">Actualizar datos</a>
                </div>
            </form>
        </div>
    </div>    

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-password" tabindex="-1" role="dialog" aria-labelledby="titulo_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal">Ingrese la contraseña actual para continuar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-error">
                    <input class="form-control" id="validar_password" type="password" placeholder="Ingrese la contraseña" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-continuar" id="continuar-cambio-nombre_usuario">Continuar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-guardar-nombre_usuario" tabindex="-1" role="dialog" aria-labelledby="titulo_modal_nombre_usuario" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal_nombre_usuario">Ingrese el nuevo nombre de usuario y presione guardar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <input class="form-control" id="new_nombre_usuario" type="text" placeholder="Nuevo nombre de usuario" value="{{$administrador->nombre_usuario}}" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-guardar-nombre_usuario" id="guardar-nombre_usuario">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-password2" tabindex="-1" role="dialog" aria-labelledby="titulo_modal2" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal2">Ingrese la contraseña actual para continuar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modal-error2">
                    <input class="form-control" id="validar_password2" type="password" placeholder="Ingrese la contraseña" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-continuar-cambio-password" id="continuar-cambio-password">Continuar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-guardar-password" tabindex="-1" role="dialog" aria-labelledby="titulo_modal_password" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal_password">Ingrese la nueva contraseña y presione guardar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <input class="form-control" id="new_password" type="text" placeholder="Nueva contraseña" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-guardar-password" id="guardar-password">Guardar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">

        $(".btn-actualizar").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{route("admin.updateDatos")}}',
                type: 'post',
                data: {
                    nombre: $('#nombre').val(),
                    apellido: $('#apellido').val(),
                    nombre_usuario: $('#nombre_usuario').val(),
                    password: $('#password').val(),
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        $(".btn-continuar").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{route("admin.validarPassword")}}',
                type: 'post',
                data: {
                    validar_password: $('#validar_password').val(),
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    $('#modal-password').modal('toggle');
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){

                    if(response['validada']){
                        $('#modal-guardar-nombre_usuario').modal('toggle');

                        $(".btn-guardar-nombre_usuario").on("click",function(){
                             // Show image container
                            $('#div_carga').fadeIn();

                            $("#nombre_usuario").val($('#new_nombre_usuario').val());
                            $('#modal-guardar-nombre_usuario').modal('toggle');

                            // Hide image container
                            $('#div_carga').fadeOut();
                        });
                    }
                    else{
                        var html_error = '<span class="invalid-feedback" role="alert">'+
                                            '<strong>'+response['mensaje']+'</strong>'+
                                        '</span>';
                        $("#validar_password").addClass("is-invalid");
                        $('#modal-error').append(html_error);

                        $('#modal-password').modal('toggle');
                    }
                },
                complete:function(data){
                    $('#new_nombre_usuario').val("");
                    $('#validar_password').val("");
                   // Hide image container
                   $('#div_carga').fadeOut();
                }
            });
        });

        $(".btn-continuar-cambio-password").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{route("admin.validarPassword")}}',
                type: 'post',
                data: {
                    validar_password: $('#validar_password2').val(),
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    $('#modal-password2').modal('toggle');
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){

                    if(response['validada']){
                        $('#modal-guardar-password').modal('toggle');

                        $(".btn-guardar-password").on("click",function(){
                             // Show image container
                            $('#div_carga').fadeIn();

                            $("#password").val($('#new_password').val());
                            $('#modal-guardar-password').modal('toggle');

                            // Hide image container
                            $('#div_carga').fadeOut();
                        });
                    }
                    else{
                        var html_error = '<span class="invalid-feedback" role="alert">'+
                                            '<strong>'+response['mensaje']+'</strong>'+
                                        '</span>';
                        $("#validar_password2").addClass("is-invalid");
                        $('#modal-error2').append(html_error);

                        $('#modal-password2').modal('toggle');
                    }
                },
                complete:function(data){
                    $('#new_password').val("");
                    $('#validar_password2').val("");
                   // Hide image container
                   $('#div_carga').fadeOut();
                }
            });
        });

    </script>
@endsection
