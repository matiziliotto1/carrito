@extends('layouts.layout_admin')

@section('content')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="{{ url()->previous() }}">Regresar</a>
    </div>
    <a href="#" class="btn btn-primary m-1" data-toggle="modal" data-target="#modal-crear-variante">Crear nueva variante</a>
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de variantes</h6>
                </div>
                <div class="table-responsive">
                    <input type="hidden" value="{{$id_producto}}" id="id_producto">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th><div style="width: 100px;">Stock</div></th>
                                <th><div style="width: 130px;">Color</div></th>
                                <th><div style="width: 150px;">Talle de ropa</div></th>
                                <th><div style="width: 150px;">Talle de calzado</div></th>
                                <th><div style="width: 150px;">Talle de anzuelo</div></th>
                                <th><div style="width: 150px;">Left/Right</div></th>
                                <th><div style="width: 150px;">Longitud</div></th>
                                <th><div style="width: 150px;">Fuerza</div></th>
                                <th><div style="width: 150px;">Accion</div></th>
                                <th><div style="width: 150px;">Diametro</div></th>
                                <th><div style="width: 150px;">Mostrar variante</div></th>
                                <th><div style="width: 100px;">Acciones</div></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @if(count($variantes_producto)>0)
                                @foreach ($variantes_producto as $variante)
                                    <tr id="variante{{$variante->id_variante_producto}}" class="">
                                        <td>
                                            <input type="text" id="stock_{{$variante->id_variante_producto}}" class="form-control" value="{{$variante->stock}}">
                                        </td>
                                        <td>
                                            <input type="text" id="color_{{$variante->id_variante_producto}}" class="form-control" value="{{$variante->color}}">
                                        </td>

                                        <td>
                                            <select class="form-control" id="talle_ropa_{{$variante->id_variante_producto}}">
                                                <option value="null" @if(!$variante->talle_ropa) selected @endif disabled>Seleccione el talle de la ropa</option>
                                                <option value="S" @if($variante->talle_ropa == "S") selected @endif>S</option>
                                                <option value="M" @if($variante->talle_ropa == "M") selected @endif>M</option>
                                                <option value="L" @if($variante->talle_ropa == "L") selected @endif>L</option>
                                                <option value="XL" @if($variante->talle_ropa == "XL") selected @endif>XL</option>
                                                <option value="XXL" @if($variante->talle_ropa == "XXL") selected @endif>XXL</option>
                                                <option value="XXXL" @if($variante->talle_ropa == "XXXL") selected @endif>XXXL</option>
                                            </select>
                                        </td>

                                        <td>
                                            <select class="form-control" id="talle_calzado_{{$variante->id_variante_producto}}">
                                                <option value="null" @if(!$variante->talle_calzado) selected @endif disabled>Seleccione el talle del calzado</option>
                                                @for ($i = 37; $i <= 45; $i++)
                                                    <option @if($variante->talle_calzado == $i) selected @endif value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </td>

                                        <td>
                                            <select class="form-control" id="talle_anzuelo_{{$variante->id_variante_producto}}">
                                                <option value="null" @if(!$variante->talle_anzuelo) selected @endif disabled>Seleccione el talle del calzado</option>
                                                
                                                @for ($i = 1; $i <= 30; $i++)
                                                    <option @if($variante->talle_anzuelo == $i) selected @endif value="{{$i}}">{{$i}}</option>
                                                @endfor

                                                @for ($i = 1; $i <= 12; $i++)
                                                    <option @if($variante->talle_anzuelo == $i."/0") selected @endif value="{{$i}}/0">{{$i}}/0</option>
                                                @endfor
                                            </select>
                                        </td>

                                        <td>
                                            <select class="form-control" id="mano_habil_{{$variante->id_variante_producto}}">
                                                <option @if(!$variante->mano_habil) selected @endif value="null" disabled>Seleccione Left/Right</option>
                                                <option @if($variante->mano_habil==1) selected @endif value="1">Derecha</option>
                                                <option @if($variante->mano_habil==2) selected @endif value="2">Izquierda</option>
                                            </select>
                                        </td>

                                        <td>
                                            <input type="text" id="longitud_{{$variante->id_variante_producto}}" class="form-control" value="{{$variante->longitud}}">
                                        </td>

                                        <td>
                                            <input type="text" id="fuerza_{{$variante->id_variante_producto}}" class="form-control" value="{{$variante->fuerza}}">
                                        </td>

                                        <td>
                                            <select class="form-control" id="accion_{{$variante->id_variante_producto}}">
                                                <option value="0" @if(!$variante->accion) selected @endif disabled>Seleccione la accion</option>
                                                <option value="Baja" @if($variante->accion == "Baja") selected @endif>Baja</option>
                                                <option value="Media" @if($variante->accion == "Media") selected @endif>Media</option>
                                                <option value="Alta" @if($variante->accion == "Alta") selected @endif>Alta</option>
                                            </select>
                                        </td>

                                        <td>
                                            <input type="text" id="diametro_{{$variante->id_variante_producto}}" class="form-control" value="{{$variante->diametro}}">
                                        </td>

                                        <td>
                                            <select class="form-control" id="habilitado_{{$variante->id_variante_producto}}">
                                                    <option disabled>Seleccione una opción</option>
                                                @if($variante->habilitado==1)
                                                    <option selected value="1">Si</option>
                                                    <option value="0">No</option>
                                                @else
                                                    <option value="1">Si</option>
                                                    <option value="0" selected>No</option>
                                                @endif
                                            </select>
                                        </td>
                                        <td class="td-actions">
                                            <a href="#" class="btn btn-sm btn-primary btn-actualizar" id="actualizar" data-id="{{$variante->id_variante_producto}}">Actualizar</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td colspan="4" id="variantes-vacio">Aun no hay variantes asociadas al producto.</td>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-crear-variante" tabindex="-1" role="dialog" aria-labelledby="titulo_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <form id="nueva_variante_form">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal">Complete los campos y presione crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body" id="div_stock">
                    <input class="form-control" id="stock" name="stock" type="text" placeholder="Ingrese el stock" required>
                </div>

                <div class="modal-body" id="div_color">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="color_checkbox" id="color_checkbox">
                        <label class="custom-control-label" for="color_checkbox">Color?</label>
                    </div>
                    
                        <input class="form-control d-none" id="color" name="color" type="text" placeholder="Ingrese el color">
                    
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="mano_habil_checkbox" id="mano_habil_checkbox">
                        <label class="custom-control-label" for="mano_habil_checkbox">Left/Right?</label>
                    </div>
                    <select class="form-control d-none" id="mano_habil" name="mano_habil">
                        <option value="null" selected disabled>Seleccione Left/Right</option>
                        <option value="1">Derecha</option>
                        <option value="2">Izquierda</option>
                    </select>
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="talle_ropa_checkbox" id="talle_ropa_checkbox">
                        <label class="custom-control-label" for="talle_ropa_checkbox">Ropa?</label>
                    </div>
                    <select class="form-control d-none" id="talle_ropa" name="talle_ropa">
                        <option value="null" selected disabled>Seleccione el talle de la ropa</option>
                        <option value="S">S</option>
                        <option value="M">M</option>
                        <option value="L">L</option>
                        <option value="XL">XL</option>
                        <option value="XXL">XXL</option>
                        <option value="XXXL">XXXL</option>
                    </select>
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="talle_calzado_checkbox" id="talle_calzado_checkbox">
                        <label class="custom-control-label" for="talle_calzado_checkbox">Calzado?</label>
                    </div>
                    <select class="form-control d-none" id="talle_calzado" name="talle_calzado">
                        <option value="null" selected disabled>Seleccione el talle del calzado</option>
                        @for ($i = 37; $i <= 45; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="talle_anzuelo_checkbox" id="talle_anzuelo_checkbox">
                        <label class="custom-control-label" for="talle_anzuelo_checkbox">Anzuelo?</label>
                    </div>
                    <select class="form-control d-none" id="talle_anzuelo" name="talle_anzuelo">
                        <option value="null" selected disabled>Seleccione el talle del anzuelo</option>
                        @for ($i = 1; $i <= 30; $i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                        @for ($i = 1; $i <= 12; $i++)
                            <option value="{{$i}}/0">{{$i}}/0</option>
                        @endfor
                    </select>
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="longitud_checkbox" id="longitud_checkbox">
                        <label class="custom-control-label" for="longitud_checkbox">Longitud?</label>
                    </div>
                    <input class="form-control d-none" id="longitud" name="longitud" type="text" placeholder="Ingrese la longitud">
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="fuerza_checkbox" id="fuerza_checkbox">
                        <label class="custom-control-label" for="fuerza_checkbox">Fuerza?</label>
                    </div>
                    <input class="form-control d-none" id="fuerza" name="fuerza" type="text" placeholder="Ingrese la fuerza">
                </div>
                
                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="accion_checkbox" id="accion_checkbox">
                        <label class="custom-control-label" for="accion_checkbox">Acción?</label>
                    </div>
                    <select class="form-control d-none" id="accion" name="accion">
                        <option value="null" selected disabled>Seleccione el talle del anzuelo</option>
                        <option value="Baja">Baja</option>
                        <option value="Media">Media</option>
                        <option value="Alta">Alta</option>
                    </select>
                </div>

                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="diametro_checkbox" id="diametro_checkbox">
                        <label class="custom-control-label" for="diametro_checkbox">Diametro?</label>
                    </div>
                    <input class="form-control d-none" id="diametro" name="diametro" type="text" placeholder="Ingrese el diametro">
                </div>

                <div class="modal-body">
                    <div id="alert" class="alert alert-dismissible alert-danger d-none" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div id="mensaje_error">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <span class="text-danger d-none" id="span_error_campos">
                        <strong> *Debes completar todos los campos seleccionados</strong>
                    </span>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-crear" id="crear-variante">Crear</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <style>
        .modal-body{
            padding: 10px 16px 10px 16px !important;
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>

    <script type="text/javascript">

        //Si lo uso de esta forma, se reconocen los nuevos botones agregados dinamicamente.
        $(document).on("click", ".btn-actualizar", function() {
            var token = '{{csrf_token()}}';
            var id_variante_producto = $(this).attr("data-id");
            var stock = $("#stock_"+id_variante_producto).val();
            var color = $("#color_"+id_variante_producto).val();

            var talle_ropa;
            if($("#talle_ropa_"+id_variante_producto).children("option:selected").val() != "null"){
                talle_ropa = $("#talle_ropa_"+id_variante_producto).children("option:selected").val();
            }
            else{
                talle_ropa = null;
            }

            var talle_calzado;
            if($("#talle_calzado_"+id_variante_producto).children("option:selected").val() != "null"){
                talle_calzado = $("#talle_calzado_"+id_variante_producto).children("option:selected").val();
            }
            else{
                talle_calzado = null;
            }

            var talle_anzuelo;
            if($("#talle_anzuelo_"+id_variante_producto).children("option:selected").val() != "null"){
                talle_anzuelo = $("#talle_anzuelo_"+id_variante_producto).children("option:selected").val();
            }
            else{
                talle_anzuelo = null;
            }

            var mano_habil;
            if($("#mano_habil_"+id_variante_producto).children("option:selected").val() != "null"){
                mano_habil = $("#mano_habil_"+id_variante_producto).children("option:selected").val();
            }
            else{
                mano_habil = null;
            }

            var longitud = $("#longitud_"+id_variante_producto).val();
            var fuerza = $("#fuerza_"+id_variante_producto).val();
            var accion = $("#accion_"+id_variante_producto).val();
            var diametro = $("#diametro_"+id_variante_producto).val();
            var habilitado = $("#habilitado_"+id_variante_producto).children("option:selected").val();

            $.ajax({
                url: '{{route("editVarianteProducto")}}',
                type: 'post',
                data: {
                    id_variante_producto: id_variante_producto,
                    stock: stock,
                    color: color,
                    talle_ropa: talle_ropa,
                    talle_calzado: talle_calzado,
                    talle_anzuelo: talle_anzuelo,
                    mano_habil: mano_habil,
                    longitud: longitud,
                    fuerza: fuerza,
                    accion: accion,
                    diametro: diametro,
                    habilitado: habilitado,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        $(".btn-crear").on("click",function(){
            //ME QUEDE ACA, CREA EL PRODUCTO PERO NO MUESTRA EL MENSAJE DE QUE SE CREO
            //INTENTO IMPRIMIR EL NUEVO ID O CUALQUIER COSA EN LA FUNCION SUCCESS Y NO LO HACE

            //Para crear una variante al menos debe tener seleccionado el stock
            let flag=true;
            var input_error;
            $('#nueva_variante_form').find('input').each(function(){
                if($(this).prop('required') && !$(this).val()){
                    flag=false;
                    input_error=$(this).attr('id');
                }
            });
            $('#nueva_variente_form').find('select').each(function(){
                if($(this).prop('required') && $(this).children("option:selected").val() == 'null'){
                    flag=false;
                    input_error=$(this).attr('id');
                }
            });
            if(flag && $("#stock").val()){
                var token_aux = '{{csrf_token()}}';
                var stock_aux = $("#stock").val();
                var color_aux = $("#color").val();

                var talle_ropa_aux;
                if($("#talle_ropa").children("option:selected").val() != "null"){
                    talle_ropa_aux = $("#talle_ropa").children("option:selected").val();
                }
                else{
                    talle_ropa_aux = null;
                }

                var talle_calzado_aux;
                if($("#talle_calzado").children("option:selected").val() != "null"){
                    talle_calzado_aux = $("#talle_calzado").children("option:selected").val();
                }
                else{
                    talle_calzado_aux = null;
                }

                var talle_anzuelo_aux;
                if($("#talle_anzuelo").children("option:selected").val() != "null"){
                    talle_anzuelo_aux = $("#talle_anzuelo").children("option:selected").val();
                }
                else{
                    talle_anzuelo_aux = null;
                }

                var mano_habil_aux;
                if($("#mano_habil").children("option:selected").val() != "null"){
                    mano_habil_aux = $("#mano_habil").children("option:selected").val();
                }
                else{
                    mano_habil_aux = null;
                }
                
                var longitud_aux = $("#longitud").val();
                var fuerza_aux = $("#fuerza").val();
                var accion_aux = $("#accion").val();
                var diametro_aux = $("#diametro").val();

                $.ajax({
                    url: '{{route("createVarianteProducto")}}',
                    type: 'post',
                    data: {
                        stock: stock_aux,
                        color: color_aux,
                        talle_ropa: talle_ropa_aux,
                        talle_calzado: talle_calzado_aux,
                        talle_anzuelo: talle_anzuelo_aux,
                        mano_habil: mano_habil_aux,
                        longitud: longitud_aux,
                        fuerza: fuerza_aux,
                        accion: accion_aux,
                        diametro: diametro_aux,
                        id_producto: $("#id_producto").val(),
                        _token: token_aux
                    },
                    dataType: 'json',

                    beforeSend: function(){
                        // Show image container
                        $('#div_carga').fadeIn();
                    },
                    success: function(response){
                        $("#mensaje").empty();
                        $("#alert").removeClass("alert-success");
                        $("#alert").removeClass("alert-danger");

                        if(response['success']=="true"){
                            var nuevo_id = response['id_variante_producto'];
                            var nuevaVariante = 
                            '<tr id="variante'+nuevo_id+'" class="">'+
                                '<td><input type="text" id="stock_'+nuevo_id+'" class="form-control" value="'+stock_aux+'"></td>'+
                                '<td><input type="text" id="color_'+nuevo_id+'" class="form-control" value="'+color_aux+'"></td>'+
                                '<td>'+
                                    '<select class="form-control" id="talle_ropa_'+nuevo_id+'">'+
                                        '<option value="null" disabled>Seleccione el talle de la ropa</option>'+
                                        '<option value="S">S</option>'+
                                        '<option value="M">M</option>'+
                                        '<option value="L">L</option>'+
                                        '<option value="XL">XL</option>'+
                                        '<option value="XXL">XXL</option>'+
                                        '<option value="XXXL">XXXL</option>'+
                                    '</select>'+
                                '</td>'+
                                
                                '<td>' + 
                                    '<select class="form-control" id="talle_calzado_'+nuevo_id+'">'+
                                        '<option value="null" disabled>Seleccione el talle del calzado</option>';
                            for(let i=37; i<=45; i++){
                                nuevaVariante += '<option value="'+i+'">'+i+'</option>';
                            }

                            nuevaVariante += '</select>'+
                                '</td>'+
                                '<td>' + 
                                    '<select class="form-control" id="talle_anzuelo_'+nuevo_id+'">'+
                                        '<option value="null" disabled>Seleccione el talle del anzuelo</option>';
                            for(let i=1; i<=30; i++){
                                nuevaVariante += '<option value="'+i+'">'+i+'</option>';
                            }
                            for(let i=1; i<=12; i++){
                                nuevaVariante += '<option value="'+i+'/0">'+i+'/0</option>';
                            }

                            nuevaVariante += '</select>'+
                                '</td>'+

                                '<td>' + 
                                    '<select class="form-control" id="mano_habil_'+nuevo_id+'">'+
                                        '<option value="null" disabled>Seleccione Left/Right</option>'+
                                        '<option value="1">Derecha</option>'+
                                        '<option value="2">Izquierda</option>'+
                                    '</select>'+
                                '</td>'+
                                '<td><input type="text" id="longitud_'+nuevo_id+'" class="form-control" value="'+longitud_aux+'"></td>'+
                                '<td><input type="text" id="fuerza_'+nuevo_id+'" class="form-control" value="'+fuerza_aux+'"></td>'+
                                '<td>' + 
                                    '<select class="form-control" id="accion_'+nuevo_id+'">'+
                                        '<option value="null" disabled>Seleccione la accion</option>'+
                                        '<option value="Baja">Baja</option>'+
                                        '<option value="Media">Media</option>'+
                                        '<option value="Alta">Alta</option>'+
                                    '</select>'+
                                '</td>'+
                                '<td><input type="text" id="diametro_'+nuevo_id+'" class="form-control" value="'+diametro_aux+'"></td>'+
                                '<td>' + 
                                    '<select class="form-control" id="habilitado_'+nuevo_id+'">'+
                                        '<option disabled>Seleccione una opción</option>'+
                                        '<option value="0">No</option>'+
                                        '<option value="1" selected>Si</option>'+
                                    '</select>'+
                                '</td>'+
                                '<td class="td-actions">'+
                                    '<a href="#" class="btn btn-sm btn-primary btn-actualizar mr-1" id="actualizar" data-id="'+nuevo_id+'">Actualizar</a>'+
                                '</td>'+
                            '</tr>';
                            
                            $('#tbody').append(nuevaVariante);

                            //Recorro las opciones del select de los talles de ropa para dejar seleccionada la que se cargo.
                            if(talle_ropa_aux != null){
                                $("#talle_ropa_"+nuevo_id+" option").each(function(){
                                    if ($(this).val() == talle_ropa_aux ){        
                                        $(this).attr("selected","selected");
                                    }
                                });
                            }
                            else{
                                $("#talle_ropa_"+nuevo_id+" option[value='null']").attr("selected", "selected");
                            }

                            //Recorro las opciones del select de los talles del calzado para dejar seleccionada la que se cargo.
                            if(talle_calzado_aux != null){
                                $("#talle_calzado_"+nuevo_id+" option").each(function(){
                                    if ($(this).val() == talle_calzado_aux ){        
                                        $(this).attr("selected","selected");
                                    }
                                });
                            }
                            else{
                                $("#talle_calzado_"+nuevo_id+" option[value='null']").attr("selected", "selected");
                            }

                            //Recorro las opciones del select de los talles del anzuelo para dejar seleccionada la que se cargo.
                            if(talle_anzuelo_aux != null){
                                $("#talle_anzuelo_"+nuevo_id+" option").each(function(){
                                    if ($(this).val() == talle_anzuelo_aux ){        
                                        $(this).attr("selected","selected");
                                    }
                                });
                            }
                            else{
                                $("#talle_anzuelo_"+nuevo_id+" option[value='null']").attr("selected", "selected");
                            }

                            //Recorro las opciones del select de la mano habil para dejar seleccionada la que se cargo.
                            if(mano_habil_aux != null){
                                $("#mano_habil_"+nuevo_id+" option").each(function(){
                                    if ($(this).val() == mano_habil_aux ){        
                                        $(this).attr("selected","selected");
                                    }
                                });
                            }
                            else{
                                $("#mano_habil_"+nuevo_id+" option[value='null']").attr("selected", "selected");
                            }

                            //Recorro las opciones del select de la accion para dejar seleccionada la que se cargo.
                            if(accion_aux != null){
                                $("#accion_"+nuevo_id+" option").each(function(){
                                    if ($(this).val() == accion_aux ){        
                                        $(this).attr("selected","selected");
                                    }
                                });
                            }
                            else{
                                $("#accion_"+nuevo_id+" option[value='null']").attr("selected", "selected");
                            }

                            $("#alert").addClass("alert-success");
                        }
                        else{
                            $("#alert").addClass("alert-danger");
                        }
                        $("#alert").removeClass("d-none");
                        $("#mensaje").text(response['mensaje']);
                    },
                    complete:function(data){
                        if(!$("#variantes-vacio").hasClass("d-none")){
                            $("#variantes-vacio").addClass("d-none");
                        }

                        if(!$("#color").hasClass("d-none")){
                            $("#color").addClass("d-none");
                        }

                        if(!$("#mano_habil").hasClass("d-none")){
                            $("#mano_habil").addClass("d-none");
                        }

                        if(!$("#talle_ropa").hasClass("d-none")){
                            $("#talle_ropa").addClass("d-none");
                        }

                        if(!$("#talle_calzado").hasClass("d-none")){
                            $("#talle_calzado").addClass("d-none");
                        }

                        if(!$("#talle_anzuelo").hasClass("d-none")){
                            $("#talle_anzuelo").addClass("d-none");
                        }

                        if(!$("#longitud").hasClass("d-none")){
                            $("#longitud").addClass("d-none");
                        }

                        if(!$("#fuerza").hasClass("d-none")){
                            $("#fuerza").addClass("d-none");
                        }

                        if(!$("#accion").hasClass("d-none")){
                            $("#accion").addClass("d-none");
                        }

                        if(!$("#diametro").hasClass("d-none")){
                            $("#diametro").addClass("d-none");
                        }

                        $('#nueva_variante_form').find('input:checkbox').each(function(){
                            $(this).prop('checked', false);
                        });
                        $('#nueva_variante_form').find('input:text').each(function(){
                            $(this).prop('required', false);
                            $(this).val('');
                        });
                        $('#nueva_variante_form').find('select').each(function(){
                            $(this).prop('required', false);
                            $(this).prop('selectedIndex',0);
                        });

                        $('#modal-crear-variante').modal('toggle');
                        // Hide image container
                        $('#div_carga').fadeOut();
                    }
                });
            } else{
                if(!$("#stock").val()){
                    var html_error = '<span class="invalid-feedback" role="alert">'+
                                                '<strong>Debe ingresar el stock</strong>'+
                                            '</span>';
                    $("#stock").addClass("is-invalid");
                    $('#div_stock').append(html_error);
                    $("#stock").focus();
                } else {
                    $("#span_error_campos").removeClass("d-none");
                }
            }
        });

        $("#color_checkbox").on("change",function(){
            if($("#color").hasClass("d-none")){
                $("#color").removeClass("d-none");
                $("#color").prop('required', true);
            }
            else{
                $("#color").addClass("d-none");
                $("#color").prop('required', false);
            }
        });

        $("#mano_habil_checkbox").on("change",function(){
            if($("#mano_habil").hasClass("d-none")){
                $("#mano_habil").removeClass("d-none");
                $("#mano_habil").prop('required', true);
            }
            else{
                $("#mano_habil").addClass("d-none");
                $("#mano_habil").prop('required', false);
            }
        });

        $("#talle_ropa_checkbox").on("change",function(){
            if($("#talle_ropa").hasClass("d-none")){
                $("#talle_ropa").removeClass("d-none");
                $("#talle_ropa").prop('required', true);
            }
            else{
                $("#talle_ropa").addClass("d-none");
                $("#talle_ropa").prop('required', false);
            }
        });

        $("#talle_calzado_checkbox").on("change",function(){
            if($("#talle_calzado").hasClass("d-none")){
                $("#talle_calzado").removeClass("d-none");
                $("#talle_calzado").prop('required', true);
            }
            else{
                $("#talle_calzado").addClass("d-none");
                $("#talle_calzado").prop('required', false);
            }
        });

        $("#talle_anzuelo_checkbox").on("change",function(){
            if($("#talle_anzuelo").hasClass("d-none")){
                $("#talle_anzuelo").removeClass("d-none");
                $("#talle_anzuelo").prop('required', true);
            }
            else{
                $("#talle_anzuelo").addClass("d-none");
                $("#talle_anzuelo").prop('required', false);
            }
        });

        $("#longitud_checkbox").on("change",function(){
            if($("#longitud").hasClass("d-none")){
                $("#longitud").removeClass("d-none");
                $("#longitud").prop('required', true);
            }
            else{
                $("#longitud").addClass("d-none");
                $("#longitud").prop('required', false);
            }
        });

        $("#fuerza_checkbox").on("change",function(){
            if($("#fuerza").hasClass("d-none")){
                $("#fuerza").removeClass("d-none");
                $("#fuerza").prop('required', true);
            }
            else{
                $("#fuerza").addClass("d-none");
                $("#fuerza").prop('required', false);
            }
        });

        $("#accion_checkbox").on("change",function(){
            if($("#accion").hasClass("d-none")){
                $("#accion").removeClass("d-none");
                $("#accion").prop('required', true);
            }
            else{
                $("#accion").addClass("d-none");
                $("#accion").prop('required', false);
            }
        });

        $("#diametro_checkbox").on("change",function(){
            if($("#diametro").hasClass("d-none")){
                $("#diametro").removeClass("d-none");
                $("#diametro").prop('required', true);
            }
            else{
                $("#diametro").addClass("d-none");
                $("#diametro").prop('required', false);
            }
        });

    </script>
@endsection
