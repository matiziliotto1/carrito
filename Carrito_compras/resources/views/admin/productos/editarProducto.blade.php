@extends('layouts.layout_admin')

@section('content')
<style>
    input[type=number]::-webkit-outer-spin-button,
    input[type=number]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
        -moz-appearance:textfield;
    }    
</style>

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <a href="{{ route('admin.getProductos') }}">Regresar</a>
</div>
<div class="col-lg-12">
    <!-- General Element -->
    <div class="mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Editar producto</h6>
        </div>
        <div class="card-body">
            <form class="row">
                @csrf

                <input type="hidden" name="id_producto" id="id_producto" value="{{$producto->id_producto}}">

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" value="{{$producto->nombre}}" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Marca</label>
                        <input type="text" class="form-control" name="marca" id="marca" value="{{$producto->marca}}" required>
                    </div>
                </div>
                
                <div class="col-sm-4">
                    <label for="exampleFormControlInput1">Precio</label>
                        
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">$</span>
                        </div>

                        <input type="number" class="form-control" name="precio" id="precio" value="{{$producto->precio}}" required aria-label="Precio"
                            aria-describedby="basic-addon1">
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="id_subcategoria">Subcategoria</label>
                        <select name="id_subcategoria" id="id_subcategoria" required class="form-control">
                            <option disabled>Elija la subcategoria del producto</option>
                            @foreach ($subcategorias as $subcategoria)
                                <option value="{{$subcategoria->id_subcategoria}}" @if($subcategoria->id_subcategoria == $producto->id_subcategoria) selected @endif>{{$subcategoria->nombre_subcategoria}}</option>   
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="descripcion">Descripción resumida</label>
                        <textarea type="text" class="form-control" name="descripcion_resumida" id="descripcion_resumida" required>{{$producto->descripcion_resumida}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="texto_respuesta">Descripción</label>
                        <textarea type="text" class="form-control" rows="5" name="descripcion" id="descripcion" required>{{$producto->descripcion}}</textarea>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="exampleFormControlInput1">Alto</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" name="alto" id="alto" value="{{$producto->alto}}" required aria-label="Alto"
                            aria-describedby="basic-addon1">

                            <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                            </div>
                    </div>
                </div>
                
                <div class="col-sm-3">
                    <label for="exampleFormControlInput1">Ancho</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" name="ancho" id="ancho" value="{{$producto->ancho}}" required aria-label="Ancho"
                            aria-describedby="basic-addon1">

                            <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                            </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="exampleFormControlInput1">Largo</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" name="largo" id="largo" value="{{$producto->largo}}" required aria-label="Largo"
                            aria-describedby="basic-addon1">

                            <div class="input-group-append">
                                <span class="input-group-text">cm</span>
                            </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <label for="exampleFormControlInput1">Peso</label>
                    <div class="input-group mb-3">
                        <input type="number" class="form-control" name="peso" id="peso" value="{{$producto->peso}}" required aria-label="Peso"
                            aria-describedby="basic-addon1">

                            <div class="input-group-append">
                                <span class="input-group-text">gr</span>
                            </div>
                    </div>
                </div>
                
            

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="destacado" id="destacado" @if($producto->destacado==1) checked @endif>
                            <label class="custom-control-label" for="destacado">Destacar producto?</label>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" name="habilitado" id="habilitado" @if($producto->habilitado==1) checked @endif>
                            <label class="custom-control-label" for="habilitado">Mostrar producto?</label>
                        </div>
                    </div>
                </div>

                
                <br>
                <div class="col-sm-12 mt-3">
                    <a href="#" class="btn btn-primary btn-actualizar">Actualizar producto</a>
                        <a href="{{route('getVariantesProducto',$producto->id_producto)}}" class="btn btn-primary float-right">Ver variantes</a>
                        <a href="{{route('verImagenesProductoView',$producto->id_producto)}}" class="btn btn-primary float-right mr-2">Ver imagenes</a>
                </div>
                
            </form>
        </div>
    </div>    

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')

<script type="text/javascript">

    $(".btn-actualizar").on("click",function(){
        var token = '{{csrf_token()}}';

        var destacado;
        if($("#destacado").prop('checked')){
            destacado = 1;
        }else{
            destacado = 0;
        }

        var habilitado;
        if($("#habilitado").prop('checked')){
            habilitado = 1;
        }else{
            habilitado = 0;
        }

        $.ajax({
            url: '{{route("admin.updateProducto")}}',
            type: 'post',
            data: {
                id_producto: $('#id_producto').val(),
                id_subcategoria: $('#id_subcategoria').val(),
                nombre: $('#nombre').val(),
                precio: $('#precio').val(),
                marca: $('#marca').val(),
                descripcion: $('#descripcion').val(),
                descripcion_resumida: $('#descripcion_resumida').val(),
                alto: $("#alto").val(),
                ancho: $("#ancho").val(),
                largo: $("#largo").val(),
                peso: $("#peso").val(),
                destacado: destacado,
                habilitado: habilitado,
                _token: token
            },
            dataType: 'json',

            beforeSend: function(){
                // Show image container
                $('#div_carga').fadeIn();
            },
            success: function(response){
                $("#mensaje").empty();
                $("#alert").removeClass("alert-success");
                $("#alert").removeClass("alert-danger");

                if(response['success']=="true"){
                    $("#alert").addClass("alert-success");
                }
                else{
                    $("#alert").addClass("alert-danger");
                }
                $("#alert").removeClass("d-none");
                $("#mensaje").text(response['mensaje']);
            },
            complete:function(data){
                // Hide image container
                $('#div_carga').fadeOut();
            }
        });
    });
    </script>
@endsection
