@extends('layouts.layout_admin')

@section('content')

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Productos</h6>
            </div>
        
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal-crear-producto">Crear producto</a>
                <div>
                    <form class="navbar-search" method="get" action="{{route('admin.buscadorAdminProductos')}}">
                        <div class="input-group">
                            <input type="search" class="form-control bg-light border-1 small" name="texto" placeholder="Buscar productos..." value="@if(request()->get('texto')){{request()->get('texto')}}@endif"
                            aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de productos
                        @switch(Route::current()->getName())
                            @case("admin.getProductosDeshabilitados")
                                deshabilitados
                                @break
                            @case("admin.verProductosXCategoria")
                                de la categoria: {{$nombre_categoria}}
                                @break
                            @case("admin.verProductosXSubcategoria")
                                de la categoria: {{$nombre_categoria}} / {{$nombre_subcategoria}}
                                @break
                        @endswitch
                    </h6>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>Nombre</th>
                                <th>Marca</th>
                                <th>Precio</th>
                                <th>Destacado?</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="productos">
                            @if(count($productos)>0)
                                @foreach ($productos as $producto)
                                    <tr id="producto{{$producto->id_producto}}" class="">
                                        <td>{{$producto->nombre}}</td>
                                        <td>{{$producto->marca}}</td>
                                        <td>{{$producto->precio}}</td>
                                        <td>@if($producto->destacado == 1) Si @else No @endif </td>
                                        <td class="td-actions">
                                            <a href="{{route('admin.editarProducto',$producto->id_producto)}}" class="btn btn-sm btn-primary">Editar</a>
                                            @if(Route::current()->getName() == "admin.getProductos" || Route::current()->getName() == "admin.verProductosXCategoria" || Route::current()->getName() == "admin.verProductosXSubcategoria" || Route::current()->getName() == "admin.buscadorAdminProductos")
                                                    <a href="#" class="btn btn-sm btn-primary btn-deshabilitar" id="deshabilitar" data-id="{{$producto->id_producto}}">Deshabilitar</a>
                                            @elseif("admin.getProductosDeshabilitados")
                                                <a href="#" class="btn btn-sm btn-primary btn-habilitar" id="habilitar" data-id="{{$producto->id_producto}}">Habilitar</a>
                                            @endif
                                            <a href="#" class="btn btn-sm btn-primary btn-eliminar" data-id="{{$producto->id_producto}}">Eliminar</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @switch(Route::current()->getName())
                                    @case("admin.getProductos")
                                        <td colspan="5">Aun no hay productos cargados.</td>
                                    @case("admin.getProductosDeshabilitados")
                                        <td colspan="5">Aun no hay productos deshabilitados.</td>
                                @endswitch 
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">    
                    <!-- Pagination links -->
                    {{ $productos->links() }}
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-crear-producto" tabindex="-1" role="dialog" aria-labelledby="titulo_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal">Complete los campos y presione crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input class="form-control" id="nombre" name="nombre" type="text" placeholder="Ingrese el nombre" required>
                </div>
                <div class="modal-body">
                    <input class="form-control" id="marca" name="marca" type="text" placeholder="Ingrese la marca" required>
                </div>
                <div class="modal-body">
                    <input class="form-control" id="precio" name="precio" type="number" placeholder="Ingrese el precio" required>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" rows="2" id="descripcion_resumida" name="descripcion_resumida" type="text" placeholder="Ingrese la descripcion resumida"></textarea>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" rows="5" id="descripcion" name="descripcion" type="text" placeholder="Ingrese la descripcion"></textarea>
                </div>
                <div class="modal-body">
                    <label for="alto">Alto (en centímetros)</label>
                    <input class="form-control" id="alto" name="alto" type="number" value="0.01" placeholder="Ingrese la altura en centimetros" required min="0.01" step="0.01">
                </div>
                <div class="modal-body">
                    <label for="alto">Ancho (en centímetros)</label>
                    <input class="form-control" id="ancho" name="ancho" type="number" value="0.01" placeholder="Ingrese el ancho en centimetros" required min="0.01" step="0.01">
                </div>
                <div class="modal-body">
                    <label for="alto">Largo (en centímetros)</label>
                    <input class="form-control" id="largo" name="largo" type="number" value="0.01" placeholder="Ingrese el largo en centimetros" required min="0.01" step="0.01">
                </div>
                <div class="modal-body">
                    <label for="alto">Peso (en gramos)</label>
                    <input class="form-control" id="peso" name="peso" type="number" value="0.01" placeholder="Ingrese el peso en gramos" required min="0.01" step="0.01">
                </div>
                <div class="modal-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="destacado" id="destacado">
                        <label class="custom-control-label" for="destacado">Destacar producto?</label>
                    </div>
                </div>
                <div class="modal-body">
                    <select class="form-control" id="id_categoria" name="id_categoria">
                        <option value="" selected disabled>Seleccione la categoria</option>
                        @foreach($categorias as $categoria)
                            <option value="{{$categoria->id_categoria}}">{{$categoria->nombre_categoria}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="modal-body d-none" id="select-subcategorias">
                    <select class="form-control" id="id_subcategoria" name="id_subcategoria">
                        <option value="" selected disabled>Seleccione la sub categoria</option>
                    </select>
                </div>

                <div class="modal-body">
                    <div id="alert-subcategorias" class="alert alert-dismissible alert-danger d-none" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div id="mensaje-subcategorias">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-crear" id="crear-producto">Crear</button>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>

    <script type="text/javascript">

        $(document).on("click", ".btn-eliminar", function() {
            var token = '{{csrf_token()}}';
            var id_producto = $(this).attr("data-id");

            bootbox.dialog({
                message: "¿Esta seguro que desea eliminar este producto?",
                title: "<i class='glyphicon glyphicon-trash'></i> Eliminar producto",
                buttons: {
                    success: {
                    label: "No",
                    className: "btn-success",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                    },
                    danger: {
                    label: "Si",
                    className: "btn-danger",
                    callback: function() {
                        $.ajax({
                            url: '{{route("admin.deleteProducto")}}',
                            type: 'delete',
                            data: {
                                id_producto: id_producto,
                                _token: token
                            },
                            dataType: 'json',

                            beforeSend: function(){
                                // Show image container
                                $('#div_carga').fadeIn();
                            },
                            success: function(response){
                                $("#mensaje").empty();
                                $("#alert").removeClass("alert-success");
                                $("#alert").removeClass("alert-danger");

                                if(response['success']=="true"){
                                    $("#alert").addClass("alert-success");
                                }
                                else{
                                    $("#alert").addClass("alert-danger");
                                }
                                $("#alert").removeClass("d-none");
                                $("#mensaje").text(response['mensaje']);
                            },
                            complete:function(data){
                                $("#producto"+id_producto).remove();
                                // Hide image container
                                $('#div_carga').fadeOut();
                            }
                        })			  
                        }
                    }
                }
            });
        });

        $(".btn-deshabilitar").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_producto = $(this).attr("data-id");

            $.ajax({
                url: '{{route("admin.deshabilitarProducto")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $("#producto"+id_producto).remove();
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        $(".btn-habilitar").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_producto = $(this).attr("data-id");

            $.ajax({
                url: '{{route("admin.habilitarProducto")}}',
                type: 'post',
                data: {
                    id_producto: id_producto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $("#producto"+id_producto).remove();
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        $("#id_categoria").on("change",function(){
            var id_categoria =$("#id_categoria").val();
            var url = "{{ route('getSubcategorias', ":id") }}";
            url = url.replace(':id', id_categoria);
            $.ajax({
                url: url,
                type: 'get',
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}',
                },
                dataType: 'json',

                success: function(response){
                    if(response['success']=="true"){
                        $.each( response['subcategorias'], function( key, value ) {
                            $('#id_subcategoria').append('<option value="'+value["id_subcategoria"]+'">'+value["nombre_subcategoria"]+'</option>');
                        });
                        $("#select-subcategorias").removeClass("d-none");
                    }
                    else{
                        $("#alert-subcategorias").removeClass("d-none");
                        $("#mensaje-subcategorias").text(response['mensaje']);
                    }
                },
            });

        });

        $(".btn-crear").on("click",function(){
            var token = '{{csrf_token()}}';

            var destacado;
            var destacado_texto="";
            if($("#destacado").prop('checked')){
                destacado = 1;
                destacado_texto = "Si";
            }else{
                destacado = 0;
                destacado_texto = "No";
            }

            $.ajax({
                url: '{{route("admin.createProducto")}}',
                type: 'post',
                data: {
                    nombre: $("#nombre").val(),
                    marca: $("#marca").val(),
                    precio: $("#precio").val(),
                    descripcion_resumida: $("#descripcion_resumida").val(),
                    descripcion: $("#descripcion").val(),
                    alto: $("#alto").val(),
                    ancho: $("#ancho").val(),
                    largo: $("#largo").val(),
                    peso: $("#peso").val(),
                    id_subcategoria : $("#id_subcategoria").val(),
                    destacado: destacado,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                        
                        var id_producto = response['id_producto'];
                        var url = "{{ route('admin.editarProducto', ":id") }}";
                        url = url.replace(':id', id_producto);
                        var nuevo_producto = '<tr id="producto'+id_producto+'" class="">'+
                                        '<td>'+$("#nombre").val()+'</td>'+
                                        '<td>'+$("#marca").val()+'</td>'+
                                        '<td>'+$("#precio").val()+'</td>'+
                                        '<td>'+destacado_texto+'</td>'+
                                        '<td class="td-actions">'+
                                            '<a href="'+url+'" class="btn btn-sm btn-primary mr-1">Editar</a>'+
                                            '<a href="#" class="btn btn-sm btn-primary btn-deshabilitar mr-1" id="deshabilitar" data-id="'+id_producto+'">Deshabilitar</a>'+
                                            '<a href="#" class="btn btn-sm btn-primary btn-eliminar mr-1" data-id="'+id_producto+'">Eliminar</a>'+
                                        '</td>'+
                                    '</tr>';

                        $("#productos").append(nuevo_producto);
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $('#nombre').val(""),
                    $('#marca').val(""),
                    $('#precio').val(""),
                    $('#descripcion').val(""),
                    $('#descripcion_resumida').val(""),
                    $('#alto').val(""),
                    $('#ancho').val(""),
                    $('#largo').val(""),
                    $('#peso').val(""),
                    $("#id_categoria option[value='']").prop('selected', true);

                    $("#select-subcategorias").addClass("d-none");
                    $("#id_subcategoria option[value='']").prop('selected', true);

                    $('#modal-crear-producto').modal('toggle');

                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
    </script>
@endsection
