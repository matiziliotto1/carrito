@extends('layouts.layout_admin')

@section('content')

    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de imagenes</h6>
                </div>
                
                <div class="d-inline">
                    @foreach ($sliders as $slider)
                        <div class="ml-4 d-inline-flex" style="width: 30%" id="imagen{{$slider->id_imagen}}">
                            <div class="card shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Imagen N° {{$slider->orden}}</h6>
                                </div>
                                <div class="card-body bg-blue">
                                    <div class="chart-pie">
                                        <img src="{{asset($slider->url)}}" alt="Error al cargar imagen" width="100%" height="100%"/>
                                    </div>
                                    <hr>
                                        <h6 class="m-0 font-weight-bold text-primary">
                                            Mostrar imagen: @if ($slider->mostrar==1) Si @else No @endif
                                        </h6>
                                    <hr>
                                    <a href="{{route('editarSliderView', $slider->id_imagen)}}" class="btn btn-sm btn-primary">Editar</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $('.custom-file-input').on('change', function() { 
            let fileName = $(this).val().split('\\').pop();
            $(this).siblings('.custom-file-label').addClass("selected").html(fileName);
        });
        $('#archivo_imagen').change(function(e) {
            var allowedExtensions = /(.jpg|.jpeg|.png)$/i;
            if ( !allowedExtensions.exec($('#archivo_imagen').val())) {
                $('#archivo_imagen').val('');
                document.getElementById("div-alert").style.display = "block";
            } else {
                addImage(e); 
            }
            });

            function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
            
            if (!file.type.match(imageType))
            return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
            }
        
            function fileOnload(e) {
            var result=e.target.result;
            $('#imgSalida').attr("src",result);
            }
        });
    </script>
@endsection
