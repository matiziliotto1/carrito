@extends('layouts.layout_admin')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <a href="{{ url()->previous() }}">Regresar</a>
    </div>
    <div class="row w-90 mx-auto">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Agregar subcategoria</h6>
                </div>
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead>
                            <tr>
                                <th>
                                    <input type="text" name="nombre_subcategoria_nueva" id="nombre_subcategoria_nueva" class="form-control" placeholder="Ingrese nombre de la subcategoria" required>
                                </th>
                                <th>
                                    <input type="hidden" name="tipo_categoria_nueva" id="tipo_categoria_nueva" value="{{$categoria->tipo}}">
                                    <button class="btn btn-primary btn-crear" data-id="{{$categoria->id_categoria}}">Crear Subcategoria</button>
                                </th>
                            </tr>
                        </thead>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>

    <div id="alert_nueva" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje_nueva">
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 mb-4">
            <!-- Simple Tables -->
            <div class="card">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Lista de subcategorias</h6>
                </div>
                
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>Nombre</th>
                                <th>Tipo</th>
                                <th>Mostrar subcategoria</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            @if(count($subcategorias)>0)
                                @foreach ($subcategorias as $subcategoria)
                                    <tr id="subcategoria{{$subcategoria->id_subcategoria}}" class="">
                                        <td>
                                            <input type="text" id="nombre_subcategoria_{{$subcategoria->id_subcategoria}}" class="form-control" value="{{$subcategoria->nombre_subcategoria}}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" value="@if($subcategoria->tipo==1) Producto @else Servicio @endif" disabled>
                                        </td>
                                        <td>
                                            <select class="form-control" id="habilitado_{{$subcategoria->id_categoria}}">
                                                    <option disabled>Mostrar subcategoria</option>
                                                    <option @if($subcategoria->habilitado==1) selected @endif value="1">Si</option>
                                                    <option @if($subcategoria->habilitado==0) selected @endif value="0">No</option>
                                            </select>
                                        </td>
                                        <td class="td-actions">
                                            <a href="#" class="btn btn-sm btn-primary btn-actualizar text-white" id="actualizar" data-id="{{$subcategoria->id_subcategoria}}">Actualizar</a>
                                            @if($subcategoria->tipo == 0)
                                                <a href="{{route('admin.verServiciosXSubcategoria', $subcategoria->id_subcategoria)}}" class="btn btn-sm btn-primary text-white">Ver servicios</a>
                                            @elseif($subcategoria->tipo == 1)
                                                <a href="{{route('admin.verProductosXSubcategoria', $subcategoria->id_subcategoria)}}" class="btn btn-sm btn-primary text-white">Ver productos</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td colspan="4">Aun no hay subcategorias cargadas.</td>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">    
                    <!-- Pagination links -->
                    {{ $subcategorias->links() }}
                </div>
            </div>
        </div>
    </div>

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>

    <script type="text/javascript">
    
        //Si lo uso de esta forma, se reconocen los nuevos botones agregados dinamicamente.
        $(document).on("click", ".btn-actualizar", function() {
            var token = '{{csrf_token()}}';
            var id_subcategoria = $(this).attr("data-id");
            var nombre_subcategoria = $("#nombre_subcategoria_"+id_subcategoria).val();
            var habilitado = $("#habilitado_"+id_subcategoria).children("option:selected").val();
            var tipo = $("#tipo_categoria_nueva").val();

            $.ajax({
                url: '{{route("updateSubcategoria")}}',
                type: 'post',
                data: {
                    id_subcategoria: id_subcategoria,
                    nombre_subcategoria: nombre_subcategoria,
                    tipo: tipo,
                    habilitado: habilitado,
                    _token: token,
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

        
        $(".btn-crear").on("click",function(){
            var token = '{{csrf_token()}}';
            var id_categoria = $(this).attr("data-id");
            var nombre_subcategoria = $("#nombre_subcategoria_nueva").val();
            var tipo = $("#tipo_categoria_nueva").val();

            $.ajax({
                url: '{{route("createSubcategoria")}}',
                type: 'post',
                data: {
                    id_categoria: id_categoria,
                    nombre_subcategoria: nombre_subcategoria,
                    tipo: tipo,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje_nueva").empty();
                    $("#alert_nueva").removeClass("alert-success");
                    $("#alert_nueva").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert_nueva").addClass("alert-success");
                        var id_subcategoria = response['id_subcategoria'];
                        let texto_tipo;
                        if (tipo==1) {
                            texto_tipo='Producto';
                        } else {
                            texto_tipo='Servicio';
                        }
                        var nueva_subcategoria ='<tr id="subcategoria'+id_subcategoria+'" class="">'+
                                                '<td>'+
                                                    '<input type="text" id="nombre_subcategoria_'+id_subcategoria+'" class="form-control" value="'+nombre_subcategoria+'">'+
                                                '</td>'+
                                                '<td>'+
                                                    '<input type="text" class="form-control" value="'+texto_tipo+'" disabled>'+
                                                '</td>'+
                                                '<td>'+
                                                    '<select class="form-control" id="habilitado_'+id_subcategoria+'">'+
                                                            '<option disabled>Mostrar subcategoria</option>'+
                                                            '<option selected value="1">Si</option>'+
                                                            '<option value="0">No</option>'+
                                                    '</select>'+
                                                '</td>'+
                                                '<td class="td-actions">'+
                                                    '<a href="#" class="btn btn-sm btn-primary btn-actualizar text-white" id="actualizar" data-id="'+id_subcategoria+'">Actualizar</a>'+
                                                '</td>'+
                                            '</tr>';

                        $("#tbody").append(nueva_subcategoria);
                    }
                    else{
                        $("#alert_nueva").addClass("alert-danger");
                    }
                    $("#alert_nueva").removeClass("d-none");
                    $("#mensaje_nueva").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

    </script>
@endsection