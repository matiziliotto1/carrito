@extends('layouts.layout_admin')

@section('content')
<div class="col-lg-12">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">¿Quienes somos?</h6>
    </div>
    <div class="card mb-4 p-3">
        <div class="card-body p-0">
            <label class="m-4 font-weight-bold text-primary m-0 p-0" for="review">Reseña</label>
            <textarea class="form-control text-center" name="review" id="review" cols="30" rows="10">{{$textos->review}}</textarea>
        </div>
        <div class="card-body p-0">
            <label class="m-4 font-weight-bold text-primary m-0 p-0" for="objetivos">Objetivos</label>
            <textarea class="form-control text-center" name="objetivos" id="objetivos" cols="30" rows="10">{{$textos->objetivos}}</textarea>
        </div>
        <div class="col-sm-12 m-3 text-right">
            <a class="btn btn-sm btn-primary btn-actualizar text-white" id="actualizar">Actualizar</a>
        </div>
    </div>
  </div>

  <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(".btn-actualizar").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{route("admin.updateQuienesSomos")}}',
                type: 'post',
                data: {
                    review: $('#review').val(),
                    objetivos: $('#objetivos').val(),
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });

    </script>
@endsection