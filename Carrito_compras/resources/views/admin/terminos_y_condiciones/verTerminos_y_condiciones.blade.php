@extends('layouts.layout_admin')

@section('content')
<div class="col-lg-12">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">Terminos y condiciones</h6>
    </div>

    <div class="col-sm-12 mb-3">
        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal-agregar-termino">Agregar terminos y condiciones</a>
    </div>

    <div id="terminos_y_condiciones">
        @foreach ($terminos as $termino)
            <div class="card mb-4" id="termino{{$termino->id_termino_condiciones}}">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary m-0 p-0">{{$termino->titulo}}</h6>
                </div>
                <div class="card-body m-0 p-0 pl-3">
                    <p>{{$termino->texto}}</p>
                </div>
                <div class="col-sm-12 mb-3">
                    <a href="{{route('admin.editarTerminosYCondicionesView', $termino->id_termino_condiciones)}}" class="btn btn-primary">Editar</a>
                    <a href="#" class="btn btn-primary btn-eliminar text-white" data-id="{{$termino->id_termino_condiciones}}">Eliminar</a>
                </div>
            </div>
        @endforeach
    </div>

  </div>

  <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <!-- Modal Center -->
    <div class="modal fade" id="modal-agregar-termino" tabindex="-1" role="dialog" aria-labelledby="titulo_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titulo_modal">Complete los campos y presione crear</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input class="form-control" id="titulo" name="titulo" type="text" placeholder="Ingrese el titulo" required>
                </div>
                <div class="modal-body">
                    <textarea class="form-control" name="texto" id="texto" cols="30" rows="10" placeholder="Ingrese el texto" required></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary btn-agregar" id="agregar-termino">Agregar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js" integrity="sha256-sfG8c9ILUB8EXQ5muswfjZsKICbRIJUG/kBogvvV5sY=" crossorigin="anonymous"></script>
    <script type="text/javascript">

        $(document).on("click", ".btn-eliminar", function() {
            var token = '{{csrf_token()}}';
            var id_termino_condiciones = $(this).attr("data-id");

            bootbox.dialog({
                message: "¿Esta seguro que desea eliminar estos terminos y condiciones?",
                title: "<i class='glyphicon glyphicon-trash'></i> Eliminar",
                buttons: {
                    success: {
                    label: "No",
                    className: "btn-success",
                    callback: function() {
                        $('.bootbox').modal('hide');
                    }
                    },
                    danger: {
                    label: "Si",
                    className: "btn-danger",
                    callback: function() {
                        $.ajax({
                            url: '{{route("admin.deleteTerminosYCondiciones")}}',
                            type: 'post',
                            data: {
                                id_termino_condiciones: id_termino_condiciones,
                                _token: token
                            },
                            dataType: 'json',

                            beforeSend: function(){
                                // Show image container
                                $('#div_carga').fadeIn();
                            },
                            success: function(response){
                                $("#mensaje").empty();
                                $("#alert").removeClass("alert-success");
                                $("#alert").removeClass("alert-danger");

                                if(response['success']=="true"){
                                    $("#alert").addClass("alert-success");
                                }
                                else{
                                    $("#alert").addClass("alert-danger");
                                }
                                $("#alert").removeClass("d-none");
                                $("#mensaje").text(response['mensaje']);
                            },
                            complete:function(data){
                                $("#termino"+id_termino_condiciones).remove();
                                // Hide image container
                                $('#div_carga').fadeOut();
                            }
                        })			  
                        }
                    }
                }
            });
        });

        $(".btn-agregar").on("click",function(){
            var token = '{{csrf_token()}}';
            var titulo = $('#titulo').val();
            var texto = $('#texto').val();

            $.ajax({
                url: '{{route("admin.addTerminosYCondiciones")}}',
                type: 'post',
                data: {
                    titulo: titulo,
                    texto: texto,
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                        
                        var id_termino_condiciones = response['id_termino_condiciones'];
                        var url = "{{ route('admin.editarServicio', ":id") }}";
                        url = url.replace(':id', id_termino_condiciones);
                        var nuevo_termino = '<div class="card mb-4" id="termino'+id_termino_condiciones+'">'+
                                                '<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">'+
                                                    '<h6 class="m-0 font-weight-bold text-primary m-0 p-0">'+titulo+'</h6>'+
                                                '</div>'+
                                                '<div class="card-body m-0 p-0 pl-3">'+
                                                    '<p>'+texto+'</p>'+
                                                '</div>'+
                                                '<div class="col-sm-12 mb-3">'+
                                                    '<a href="'+url+'" class="btn btn-primary mr-1">Editar</a>'+
                                                    '<a href="#" class="btn btn-primary btn-eliminar"  data-id="'+id_termino_condiciones+'">Eliminar</a>'+
                                                '</div>'+
                                            '</div>';

                        $("#terminos_y_condiciones").append(nuevo_termino);
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    $('#titulo').val(""),
                    $('#texto').val(""),
                    $('#modal-agregar-termino').modal('toggle');

                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
    </script>
@endsection