@extends('layouts.layout_admin')

@section('content')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <a href="{{ url()->previous() }}">Regresar</a>
</div>
<div class="col-lg-12">
    <!-- General Element -->
    <div class="mb-4">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Editar terminos y condiciones</h6>
        </div>
        <div class="card-body">
            <form class="row">
                @csrf

                <input type="hidden" name="id_termino_condiciones" id="id_termino_condiciones" value="{{$termino->id_termino_condiciones}}">
                
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="titulo">Titulo</label>
                        <textarea type="text" class="form-control" name="titulo" id="titulo" required>{{$termino->titulo}}</textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="texto">Texto</label>
                        <textarea type="text" class="form-control" rows="5" name="texto" id="texto" required>{{$termino->texto}}</textarea>
                    </div>
                </div>
                <br>
                <div class="col-sm-12 mt-3">
                    <a href="#" class="btn btn-primary btn-actualizar">Actualizar terminos y condiciones</a>
                </div>
            </form>
        </div>
    </div>    

    <div id="alert" class="alert alert-dismissible d-none" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <div id="mensaje">
        </div>
    </div>

    <div id="div_carga">
        <div class="spinner-border" role="status" style="color:#161718e5;" id="loader">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
@endsection

@section('scripts')

    <script type="text/javascript">

        $(".btn-actualizar").on("click",function(){
            var token = '{{csrf_token()}}';

            $.ajax({
                url: '{{route("admin.updateTerminosYCondiciones")}}',
                type: 'post',
                data: {
                    id_termino_condiciones: $('#id_termino_condiciones').val(),
                    titulo: $('#titulo').val(),
                    texto: $('#texto').val(),
                    _token: token
                },
                dataType: 'json',

                beforeSend: function(){
                    // Show image container
                    $('#div_carga').fadeIn();
                },
                success: function(response){
                    $("#mensaje").empty();
                    $("#alert").removeClass("alert-success");
                    $("#alert").removeClass("alert-danger");

                    if(response['success']=="true"){
                        $("#alert").addClass("alert-success");
                    }
                    else{
                        $("#alert").addClass("alert-danger");
                    }
                    $("#alert").removeClass("d-none");
                    $("#mensaje").text(response['mensaje']);
                },
                complete:function(data){
                    // Hide image container
                    $('#div_carga').fadeOut();
                }
            });
        });
    </script>
@endsection
