@extends('layouts.layout_admin')

@section('content')
    <div class="row">
        <div class="card mx-auto">
            <div class="d-sm-flex align-items-center justify-content-between p-2">
                <h1 class="h3 mb-0 text-gray-800">Bienvenido al panel de administración</h1>
            </div>
        </div>
    </div>
@endsection