@extends('layouts.layout')

@section('content')
    <form>
        <input type="text" name="peso_total" id="peso_total" placeholder="Peso">
        <br>
        <input type="text" name="volumen_total" id="volumen_total" placeholder="Volumen">
        <br>
        <input type="text" name="codigo_postal_destino" id="codigo_postal_destino" placeholder="Codigo postal destino">
        <br>
        <input type="text" name="cant_paquetes" id="cant_paquetes" placeholder="Cantidad de paquetes">
        <br>

        <a href="#" class="send_form">Consultar tarifa</a>

        <br>
        <span id="resultado"></span>
    </form>
@endsection

@section('scripts')
    <script>
        $(".send_form").on('click', function(){
            let url = "{{route('cotizarEnvio2')}}";
            var token = '{{csrf_token()}}';

            $.ajax({
                url: url,
                type: 'post',
                data: {
                    peso_total: $("#peso_total").val(),
                    volumen_total: $("#volumen_total").val(),
                    codigo_postal_destino: $("#codigo_postal_destino").val(),
                    cant_paquetes: $("#cant_paquetes").val(),
                    _token: token
                },

                success: function(response){
                    $("#resultado").text(JSON.stringify(response));
                },
            });
        });
    </script>
@endsection

