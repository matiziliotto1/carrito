<?php

return [
    'oca' => [
        'CodigoPostalOrigen' => env('CODIGO_POSTAL_ORIGEN'),
        'Cuit' => env('CUIT'),
        'Operativa' => env('OPERATIVA'),
    ],
];