<?php

return [
    'beneficios' => [
        'nivel_1' => 150,
        'nivel_2' => 200,
        'nivel_3' => 400,
        'nivel_4' => 700,
        'nivel_5' => 1001,
    ],

    'monto_compra_para_beneficios' => [
        'nivel_1' => 3500,
        'nivel_2' => 3500,
        'nivel_3' => 4500,
        'nivel_4' => 5000,
        'nivel_5' => 0,
    ],

    'puntaje_compra' => [
        'cada' => 100,
        'gana' => 1,
    ],
];