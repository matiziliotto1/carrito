<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprasXProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras_x_productos', function (Blueprint $table) {
            $table->unsignedBigInteger('id_compra');
            $table->unsignedBigInteger('id_variante_producto');
            $table->integer('cantidad');
            $table->float('precio');
            $table->foreign('id_compra')->references('id_compra')->on('compras')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('id_variante_producto')->references('id_variante_producto')->on('variantes_producto')->onDelete('restrict')->onUpdate('restrict');
            $table->primary(array('id_compra','id_variante_producto'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras_x_productos');
    }
}
