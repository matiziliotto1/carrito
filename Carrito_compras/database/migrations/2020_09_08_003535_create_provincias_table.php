<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProvinciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provincias', function (Blueprint $table) {
            $table->id('id_provincia');
            $table->string('nombre');
        });

        // =====================
        // Al crear la tabla, le inserto las provincias
        // =====================
        $data = [
            ['nombre'=> 'Buenos Aires'],
            ['nombre'=> 'Capital Federal'],
            ['nombre'=> 'Catamarca'],
            ['nombre'=> 'Chaco'],
            ['nombre'=> 'Chubut'],
            ['nombre'=> 'Córdoba'],
            ['nombre'=> 'Corrientes'],
            ['nombre'=> 'Entre Ríos'],
            ['nombre'=> 'Formosa'],
            ['nombre'=> 'Jujuy'],
            ['nombre'=> 'La Pampa'],
            ['nombre'=> 'La Rioja'],
            ['nombre'=> 'Mendoza'],
            ['nombre'=> 'Misiones'],
            ['nombre'=> 'Neuquén'],
            ['nombre'=> 'Río Negro'],
            ['nombre'=> 'Salta'],
            ['nombre'=> 'San Juan'],
            ['nombre'=> 'San Luis'],
            ['nombre'=> 'Santa Cruz'],
            ['nombre'=> 'Santa Fé'],
            ['nombre'=> 'Santiago del Estero'],
            ['nombre'=> 'Tierra del Fuego'],
            ['nombre'=> 'Tucumán'],
        ];
        DB::table('provincias')->insert( $data );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provincias');
    }
}
