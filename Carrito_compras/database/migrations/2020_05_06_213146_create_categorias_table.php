<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->id("id_categoria");
            $table->unsignedBigInteger("id_menu")->default(1);
            $table->string('nombre_categoria');
            $table->string('link');
            $table->boolean('habilitado')->default(true);
            $table->boolean('tipo');
            $table->foreign('id_menu')->references('id_menu')->on('menu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
