<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('tipo_documento');
            $table->integer('numero_documento');
            $table->string('provincia');
            $table->string('ciudad');
            $table->string('domicilio_real');
            $table->integer('codigo_postal');
            $table->string('telefono_fijo');
            $table->string('telefono_celular');
            $table->string('provincia_recepcion');
            $table->string('ciudad_recepcion');
            $table->string('domicilio_recepcion');
            $table->integer('codigo_postal_recepcion');
            $table->unsignedBigInteger('puntaje_acumulado')->default(15);
            $table->boolean('habilitado')->default(1);
            $table->boolean('eliminado')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
