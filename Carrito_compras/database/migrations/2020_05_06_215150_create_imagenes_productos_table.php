<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagenesProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imagenes_productos', function (Blueprint $table) {
            $table->id('id_imagen');
            $table->string('url')->nullable();
            $table->integer('orden');
            $table->boolean('mostrar')->default(true);
            $table->unsignedBigInteger('id_producto')->nullable();
            $table->unsignedBigInteger('id_servicio')->nullable();
            $table->foreign('id_producto')->references('id_producto')->on('Productos')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('id_servicio')->references('id_servicio')->on('Servicios')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imagenes_productos');
    }
}
