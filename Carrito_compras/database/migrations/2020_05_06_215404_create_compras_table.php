<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras', function (Blueprint $table) {
            $table->id('id_compra');
            $table->unsignedBigInteger('id_usuario');
            $table->date('fecha_compra');
            $table->unsignedBigInteger('puntaje_compra');
            $table->unsignedBigInteger('monto_total');
            $table->string('codigo_seguimiento')->nullable();
            $table->unsignedBigInteger('monto_envio')->nullable();
            $table->string('metodo_envio')->nullable();
            $table->string('estado');
            $table->string('estado_pago')->default('Pendiente');
            $table->boolean('tipo_compra');
            $table->string('beneficio')->nullable();
            $table->unsignedBigInteger('descuento')->nullable();
            $table->foreign('id_usuario')->references('id')->on('users')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras');
    }
}
