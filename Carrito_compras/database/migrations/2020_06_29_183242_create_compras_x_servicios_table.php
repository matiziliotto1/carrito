<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComprasXServiciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras_x_servicios', function (Blueprint $table) {
            $table->unsignedBigInteger('id_compra');
            $table->unsignedBigInteger('id_servicio');
            $table->float('precio');
            $table->foreign('id_compra')->references('id_compra')->on('compras')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('id_servicio')->references('id_servicio')->on('servicios')->onDelete('restrict')->onUpdate('restrict');
            $table->primary(array('id_compra','id_servicio'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras_x_servicios');
    }
}
