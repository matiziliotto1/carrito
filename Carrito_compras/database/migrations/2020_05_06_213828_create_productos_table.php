<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id('id_producto');
            $table->string('nombre');
            $table->text('descripcion');
            $table->float('precio');
            $table->unsignedBigInteger('id_subcategoria');
            $table->text('descripcion_resumida');
            $table->string('marca');
            $table->float('alto');
            $table->float('ancho');
            $table->float('largo');
            $table->float('peso');
            $table->boolean('destacado')->default(false);
            $table->boolean('habilitado')->default(true);
            $table->boolean('eliminado')->default(false);
            $table->foreign('id_subcategoria')->references('id_subcategoria')->on('subcategorias')->onDelete('restrict')->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
