<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosXCarritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos_x_carritos', function (Blueprint $table) {
            $table->unsignedBigInteger('id_variante_producto');
            $table->unsignedBigInteger('id_carrito');
            $table->integer('cantidad');
            $table->foreign('id_variante_producto')->references('id_variante_producto')->on('variantes_producto')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('id_carrito')->references('id_carrito')->on('carrito_compras')->onDelete('restrict')->onUpdate('restrict');
            $table->primary(array('id_variante_producto','id_carrito'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos_x_carritos');
    }
}
