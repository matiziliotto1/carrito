<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantesProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variantes_producto', function (Blueprint $table) {
            $table->id('id_variante_producto');
            $table->string('color')->nullable();
            $table->string('talle_ropa')->nullable();
            $table->string('talle_calzado')->nullable();
            $table->string('talle_anzuelo')->nullable();
            $table->smallInteger('mano_habil')->nullable();
            $table->string('longitud')->nullable();
            $table->string('fuerza')->nullable();
            $table->string('accion')->nullable();
            $table->string('diametro')->nullable();
            $table->integer('stock')->nullable();
            $table->boolean('habilitado')->default(true);
            $table->unsignedBigInteger('id_producto');
            $table->foreign('id_producto')->references('id_producto')->on('Productos')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variantes_producto');
    }
}
