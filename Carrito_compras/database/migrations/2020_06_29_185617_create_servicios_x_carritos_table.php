<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiciosXCarritosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servicios_x_carritos', function (Blueprint $table) {
            $table->unsignedBigInteger('id_servicio');
            $table->unsignedBigInteger('id_carrito');
            $table->foreign('id_servicio')->references('id_servicio')->on('servicios')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('id_carrito')->references('id_carrito')->on('carrito_compras')->onDelete('restrict')->onUpdate('restrict');
            $table->primary(array('id_servicio','id_carrito'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servicios_x_carritos');
    }
}
