<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Variantes_producto;
use Faker\Generator as Faker;

$factory->define(Variantes_producto::class, function (Faker $faker) {
    return [
        'color' => $faker->safeColorName,
        'mano_habil' => $faker->numberBetween($min = 0, $max = 2),
        'stock' => $faker->numberBetween($min = 1, $max = 100),
        'id_producto' => App\Productos::all()->random()->id_producto,
    ];
});
