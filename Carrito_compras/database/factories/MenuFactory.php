<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Menu;
use Faker\Generator as Faker;

$factory->define(Menu::class, function (Faker $faker) {
    static $orden = 1;
    return [
        'etiqueta' => $faker->sentence(1, false),
        'link' => $faker->url,
        'orden' => $orden++,
    ];
});
