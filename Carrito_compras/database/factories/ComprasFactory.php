<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Compras;
use Faker\Generator as Faker;

$factory->define(Compras::class, function (Faker $faker) {
    return [
        'id_usuario' => App\User::all()->random()->id,
        'fecha_compra' => now(),
        'puntaje_compra' => $faker->numberBetween($min = 2500, $max = 10000),
        'monto_total' => $faker->numberBetween($min = 500, $max = 50000),
        'codigo_seguimiento' => $faker->uuid,
        'metodo_pago' => $faker->creditCardType,
        'metodo_envio' => $faker->company,
    ];
});
