<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Recibos;
use Faker\Generator as Faker;

$factory->define(Recibos::class, function (Faker $faker) {
    return [
        'id_compra' => App\Compras::all()->random()->id_compra,
        'fecha_recibo' => now(),
        'gastos_envio' => $faker->numberBetween($min = 250, $max = 1000),
        'monto_pagado' => $faker->numberBetween($min = 500, $max = 100000),
    ];
});
