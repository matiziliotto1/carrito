<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Imagenes_producto;
use Faker\Generator as Faker;

$factory->define(Imagenes_producto::class, function (Faker $faker) {
    static $orden = 1;
    return [
        'url' => $faker->url,
        'orden' => $orden++,
        'mostrar' => $faker->boolean,
        'id_producto' => App\Productos::all()->random()->id_producto,
    ];
});
