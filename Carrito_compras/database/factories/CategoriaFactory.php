<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Categoria;
use Faker\Generator as Faker;

$factory->define(Categoria::class, function (Faker $faker) {
    static $orden = 1;
    return [
        'id_menu' => App\Menu::all()->random()->id_menu,
        'nombre_categoria' => $faker->sentence(1, false),
        'link' => $faker->url,
        'habilitado' => $faker->boolean,
    ];
});
