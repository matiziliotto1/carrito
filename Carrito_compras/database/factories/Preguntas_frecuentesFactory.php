<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Preguntas_frecuentes;
use Faker\Generator as Faker;

$factory->define(Preguntas_frecuentes::class, function (Faker $faker) {
    return [
        'texto_pregunta' => $faker->text($maxNbChars = 200),
        'texto_respuesta' => $faker->text($maxNbChars = 200),
        'mostrar' => $faker->boolean,
    ];
});
