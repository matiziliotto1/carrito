<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Productos;
use Faker\Generator as Faker;

$factory->define(Productos::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'descripcion' => $faker->text,
        'precio' => $faker->numberBetween($min = 1000, $max = 9000),
        'id_subcategoria' => App\Subcategoria::all()->random()->id_subcategoria,
        'descripcion_resumida' => $faker->sentence(10),
        'marca' => $faker->sentence(1, false),
        'habilitado' => $faker->boolean,
    ];
});
