<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Productos_x_carrito;
use Faker\Generator as Faker;

$factory->define(Productos_x_carrito::class, function (Faker $faker) {
    return [
        'id_carrito' => App\Carrito_compra::all()->random()->id_carrito,
        'id_producto' => App\Productos::all()->random()->id_producto,
        'cantidad' => $faker->numberBetween($min = 1, $max = 100),
    ];
});
