<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Slider;
use Faker\Generator as Faker;

$factory->define(Slider::class, function (Faker $faker) {
    static $orden = 1;
    return [
        'url' => $faker->url,
        'orden' => $orden++,
        'mostrar' => $faker->boolean,
    ];
});
