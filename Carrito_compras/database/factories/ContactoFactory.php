<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contacto;
use Faker\Generator as Faker;

$factory->define(Contacto::class, function (Faker $faker) {
    return [
        'telefono_1' => $faker->e164PhoneNumber,
        'telefono_2' => $faker->e164PhoneNumber,
        'telefono_3' => $faker->e164PhoneNumber,
        'facebook' => $faker->userName,
        'instagram' => $faker->userName,
        'twitter' => $faker->userName,
        'youtube' => $faker->userName,
        'email_info' => $faker->email,
        'email_comprobantes' => $faker->email,
        'email_reclamos' => $faker->email,
    ];
});
