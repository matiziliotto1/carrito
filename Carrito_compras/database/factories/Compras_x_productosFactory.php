<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Compras_x_productos;
use Faker\Generator as Faker;

$factory->define(Compras_x_productos::class, function (Faker $faker) {
    return [
        'id_compra' => App\Compras::all()->random()->id_compra,
        'id_producto' => App\Productos::all()->random()->id_producto,
        'cantidad' => $faker->numberBetween($min = 1, $max = 100),
        'precio' => $faker->numberBetween($min = 100, $max = 1000),
    ];
});
