<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'tipo_documento' => $faker->randomElement($array = array ('DNI','LC','LE', 'CI')),
        'numero_documento' => $faker->ean8,
        'codigo_postal' => $faker->numberBetween($min = 6300, $max = 6400),
        'domicilio_real' => $faker->streetAddress,
        'domicilio_recepcion' => $faker->streetAddress,
        'telefono_fijo' => $faker->e164PhoneNumber,
        'telefono_celular' => $faker->e164PhoneNumber,
        'puntaje_acumulado' => $faker->numberBetween($min = 10000, $max = 25000),
        'habilitado' => $faker->boolean,
        'eliminado' => $faker->boolean,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => $faker->password,
        'remember_token' => Str::random(10),
    ];
});
