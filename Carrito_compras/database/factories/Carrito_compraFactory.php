<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Carrito_compra;
use Faker\Generator as Faker;

$factory->define(Carrito_compra::class, function (Faker $faker) {
    return [
        'id_usuario' => App\User::all()->random()->id,
    ];
});
