<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subcategoria;
use Faker\Generator as Faker;

$factory->define(Subcategoria::class, function (Faker $faker) {
    static $orden = 1;
    return [
        'id_categoria' => App\Categoria::all()->random()->id_categoria,
        'nombre_subcategoria' => $faker->sentence(1, false),
        'link' => $faker->url,
        'habilitado' => $faker->boolean,
    ];
});
