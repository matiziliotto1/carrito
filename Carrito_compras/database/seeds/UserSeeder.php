<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nombre' => 'admin',
            'apellido' => 'admin',
            'tipo_documento' => 'DNI',
            'numero_documento' => '123456789',
            'codigo_postal' => '6360',
            'domicilio_real' => 'calle admin 123',
            'domicilio_recepcion' => 'calle admin 123',
            'telefono_fijo' => '12345',
            'telefono_celular' => '12345',
            'puntaje_acumulado' => '15',
            'habilitado' => '1',
            'eliminado' => '0',
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ]);
        factory(App\User::class, 5)->create();
    }
}
