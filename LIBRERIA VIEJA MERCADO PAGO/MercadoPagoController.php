<?php
    require_once "mercadopago/lib/mercadopago.php";
    // require_once "conexionDB.php";

    class MercadoPagoController{

        function getMetodosYCostosDeEnvio($dimensiones, $peso, $zip, $item_price){
            $mp = new MP('6485925283344635', 'GUM3CGr9wGPdDfEwJzwe2GRFQUHsNrIy');

            $params = array(
                "dimensions" =>     $dimensiones.",".$peso,
                "zip_code" =>       $zip,
                "item_price"=>      floatval($item_price),
                // "free_method" =>    "73328" // optional
            );

            $response = $mp->get("/shipping_options", $params);

            $shipping_options = $response['response']['options'];

            return $shipping_options;

            // print_r ($shipping_options);
            // echo "<br>";

            // $data[3] = $shipping_options[0]['cost']; 

            // echo "Costo envio ".$data[3];
        }

        //La funcion crea la preferencia y retorna el url al que se debe redireccionar para realizar el pago.
        function crearPreferencia($data, $metodo_envio, $costo_envio, $usuario){
            $conectar=new ConexionDB();
            $conexion=$conectar->inicializar();

            $mp = new MP('6485925283344635', 'GUM3CGr9wGPdDfEwJzwe2GRFQUHsNrIy');

            $items =  array();

            foreach($data->items as $item){
                $sql = "select * from variantes_productos where id_variante_producto = ".$item->id_variante_producto.";";
                $resultado = $conexion->query($sql);
                $variante_producto = mysqli_fetch_assoc($resultado);
                // $variante_producto = Variantes_producto::where('id_variante_producto', $item->id_variante_producto)->first();

                $sql2 = "select nombre, descripcion_resumida from productos where id_producto = ".$item->id_producto.";";
                $resultado2 = $conexion->query($sql2);
                $producto = mysqli_fetch_assoc($resultado2);
                // $producto = Productos::where('id_producto', $variante_producto->id_producto)->select('nombre','descripcion_resumida')->first();

                $item = array(
                    "id" => $item->id_variante_producto,
                    "title" => $producto['nombre'],
                    "quantity" => $item->cantidad,
                    "currency_id" => "ARS",
                    "unit_price" => floatval($item->precio),
                    "description" => $producto['descripcion_resumida']
                    //Faltaria la imagen
                    // "picture_url" =>"https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
                    // if (Imagenes_producto::where('id_producto', $variante_producto['id_producto'])->exists()) {
                    // $item_aux->picture_url = $imagenes_controller->getPrimeraImagenDeProducto($variante_producto['id_producto']);
                    // } else {
                    //     $item_aux->picture_url = '/img/imagenes_productos/producto_sin_imagen.jpg';
                    // }
                )
                array_push($items,$item_aux);
            }

            $preference_data = array(
                "items" => $items,
                "payer" => array(
                                "name" =>       $usuario->nombre,
                                "surname" =>    $usuario->apellido,
                                "email" =>      $usuario->email,
                                // "address" => array(
                                //                     "street_name" => "Street",
                                //                     "street_number" => 123,
                                //                     "zip_code" => "1430"
                                // )
                ),
                "back_urls" => array(
                    "success" => "127.0.0.1:8000/mercadopago/pago/success",
                    "failure" => "127.0.0.1:8000/mercadopago/pago/failure",
                    "pending" => "127.0.0.1:8000/mercadopago/pago/pending"
                ),
                "auto_return" => "approved",
                "shipments" => array(
                    "mode" => "me2",
                    "dimensions" => $dimensiones.",".$peso,
                    "local_pickup" => false,
                    "zip_code" => $usuario->codigo_postal,
                    "receiver_address" => array(
                        "zip_code" => $usuario->codigo_postal,
                        "street_number"=> 123,
                        "street_name"=> "Street",
                        "floor"=> 4,
                        "apartment"=> "C"
                    )
                ),
                // "notification_url" => "https://www.your-site.com/ipn",
                "external_reference" => "Reference"."id_compra",
            );

            $preference = $mp->create_preference($preference_data);

            //Es el link al que hay que redireccionarlo en el boton de pago.
            $url = $preference['response']['init_point'];

            return $url;
        }
    }
?>