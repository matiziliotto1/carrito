<?php ini_set('display_errors', 1); ?> 
<?php
require_once "lib/mercadopago.php";
require_once "../negocio/controlador.php";

$c = new Controlador();

$conf = $c->getConfiguracion(1);

$myfile = fopen("log_acreditaciones.txt", "a") or die("Unable to open file!");
		fwrite($myfile, "Acreditacion: ".date("d-m-Y H:i:s") . PHP_EOL);
		fclose($myfile);


$mp = new MP('7604074344436435', 'aArnyPk05OcZkfiZZxRrNgyNjr97Eorv');

// $mp->sandbox_mode(true);


// $params = ["access_token" => $mp->get_access_token()];
// Check mandatory parameters
if (!isset($_GET["id"], $_GET["topic"]) || !ctype_digit($_GET["id"])) {
	http_response_code(400);
	return;
}

$myfile = fopen("log.txt", "a") or die("Unable to open file!");
		fwrite($myfile, "Topic: ".$_GET["topic"] . PHP_EOL);
		fclose($myfile);
// Get the payment reported by the IPN. Glossary of attributes response in https://developers.mercadopago.com
if($_REQUEST["topic"] == 'payment'){
	$params = array("access_token" => $mp->get_access_token());
	$payment_info = $mp->get("/collections/notifications/" . $_GET["id"], $params, false);
	$merchant_order_info = $mp->get("/merchant_orders/" . $payment_info["response"]["collection"]["merchant_order_id"], $params, false);


	if($payment_info['status']==200&&$payment_info['response']['collection']['status']=='approved') 
	{
		list($sistema,$id_cliente,$id_pedido)=explode("_",$payment_info['response']['collection']['external_reference']);
		$valor=$payment_info['response']['collection']['transaction_amount'];
		$ref=$payment_info['response']['collection']['id'];
		$fecha=$payment_info['response']['collection']['date_created'];
		$id_medio_pago = 0;

		$cliente = $c->getCliente($id_cliente);

		
		$ok = $c->acreditarPagoPedido($id_cliente,$id_pedido,$valor,$fecha,$ref,"En proceso");
		

		$myfile = fopen("log.txt", "a") or die("Unable to open file!");
		fwrite($myfile, " ". PHP_EOL);
		fwrite($myfile, "Fecha: ".$fecha. PHP_EOL);
		fwrite($myfile, "ID Pedido: ".$id_pedido. PHP_EOL);
		fwrite($myfile, "Cliente: ".$id_cliente. PHP_EOL);
		fwrite($myfile, "Valor: ".$valor. PHP_EOL);
		fwrite($myfile, "Codigo: ".$ref. PHP_EOL);
		
		fclose($myfile);

		if($ok)
		{
			$mensaje = "<p>
                      Su pago ha sido acreditado correctamente. En caso de haber seleccionado el retiro del pedido en sucursal, en las <b>próximas 24 hs</b> estará recibiendo un correo indicando en que fecha puede acercarse a sucursal retirar el pedido. Si seleccionó la opción de envío a domicilio, estaremos despachando el pedido y enviandole el código de seguimiento.<br/><br/>
                      ";
			$mensaje .=       "Muchas gracias por su compra!
                      <br><br>
                      <img src='https://multiespaciocuentaconmigo.com/nuevo/img/logo.png' />
                      <!--<a href='http://sweetdesignweb.com.ar/admin'>http://sweetdesignweb.com.ar/admin</a> <br><br>-->
                      <!--<span style='font-style:italic'>¡Muchas gracias por formar parte de todo esto!</span>--></p>";

		   $ok = $f->enviarCorreoSinTemplate("no-reply@multiespaciocuentaconmigo.com",$cliente->getEmail(),"Gracias por su compra en Multiespacio CuentaConmigo!",$mensaje);

		   //Envia correo a admin
		   $mensaje = "<p>Se ha acreditado un pago a través de MercadoPago.<br><br>
		                      
		                      Detalles del pago:<br/>";

		    $mensaje .=       "<br>Valor acreditado: $".$valor."<br/>
		                      <br>Fecha de acreditación: ".$fecha."<br/>
		                      <br>ID del Cliente: #".$id_cliente."<br/>
		                      <br>ID del Pedido: ".$id_pedido."<br/>
		                      <br>Cod. de Referencia: ".$ref."<br/>
		                      <a href='http://multiespaciocuentaconmigo.com/admin'>http://multiespaciocuentaconmigo.com/admin</a> <br><br>
		                      <img src='https://multiespaciocuentaconmigo.com/nuevo/img/logo.png' />
		                      <!--<span style='font-style:italic'>¡Muchas gracias por formar parte de todo esto!</span>--></p>";

		     $f->enviarCorreoSinTemplate("no-reply@multiespaciocuentaconmigo.com","info@multiespaciocuentaconmigo.com","Nuevo pedido recibido en Multiespacio CuentaConmigo!",$mensaje);
		}
	}
			

// Get the merchant_order reported by the IPN. Glossary of attributes response in https://developers.mercadopago.com	
}else if($_GET["topic"] == 'merchant_order'){
	$merchant_order_info = $mp->get("/merchant_orders/" . $_GET["id"], $params, false);
}


//If the payment's transaction amount is equal (or bigger) than the merchant order's amount you can release your items 
if ($merchant_order_info["status"] == 200) {
	$transaction_amount_payments= 0;
	$transaction_amount_order = $merchant_order_info["response"]["total_amount"];
    $payments=$merchant_order_info["response"]["payments"];
    foreach ($payments as  $payment) {
    	if($payment['status'] == 'approved'){
	    	$transaction_amount_payments += $payment['transaction_amount'];
	    }	
    }
    if($transaction_amount_payments >= $transaction_amount_order){
    	//echo "release your items";
    	$myfile = fopen("log.txt", "a") or die("Unable to open file!");
		$txt = "Liberar.\n";
		fwrite($myfile, $txt);
		fclose($myfile);
    }
    else{
		//echo "dont release your items";
		$myfile = fopen("log.txt", "a") or die("Unable to open file!");
		$txt = "NO Liberar.\n";
		fwrite($myfile, $txt);
		fclose($myfile);
	}
}
?>