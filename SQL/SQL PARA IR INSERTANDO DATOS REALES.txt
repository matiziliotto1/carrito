#Usuarios
INSERT INTO `users` (`id`, `nombre`, `apellido`, `tipo_documento`, `numero_documento`, `codigo_postal`, `domicilio_real`, `domicilio_recepcion`, `telefono_fijo`, `telefono_celular`, `puntaje_acumulado`, `habilitado`, `eliminado`, `email`, `password`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Matias', 'Ziliotto', 'DNI', 40610993, 6360, 'Calle 30 n� 964', 'Calle 30 n� 964', '332380', '2302209165', 0, 1, 0, 'mati_97_ziliotto@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-10 03:00:00', '2020-05-10 03:00:00'),
(2, 'Jorge', 'Mi�o', 'DNI', 1, 6360, '123', '123123', '1', '1', 0, 1, 0, 'jormino78@gmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-10 03:00:00', '2020-06-04 06:14:27'),
(3, 'Juan', 'Perez', 'DNI', 40610113, 6360, 'Calle 20 n� 123', 'Calle 20 n� 123', '332380', '2302209165', 0, 1, 0, 'juan@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-10 03:00:00', '2020-05-10 03:00:00'),
(4, 'Maria', 'Dominguez', 'DNI', 45630993, 6360, 'Calle 20 n� 123', 'Calle 20 n� 123', '332380', '2302209165', 0, 1, 0, 'maria@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-10 03:00:00', '2020-05-10 03:00:00'),
(5, 'Pedro', 'Garcia', 'DNI', 41610693, 6360, 'Calle 20 n� 123', 'Calle 20 n� 123', '332380', '2302209165', 0, 1, 0, 'pedro@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-10 03:00:00', '2020-05-10 03:00:00'),
(6, 'Lucia', 'Martinez', 'DNI', 30610293, 6360, 'Calle 20 n� 123', 'Calle 20 n� 123', '332380', '2302209165', 0, 1, 0, 'lucia@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-10 03:00:00', '2020-05-10 03:00:00'),
(7, 'Test', 'Test', 'DNI', '11111111', '1111', 'Avenida San martin', 'Avenida Sarmiento', '2302-209165', '2302-209165', '10000', '1', '0', 'test@gmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-06-05 00:00:00', NULL, '2020-06-05 00:00:00', NULL);

#Carritos
INSERT INTO `carrito_compras` (`id_carrito`, `id_usuario`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(2, 2, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(3, 3, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(4, 4, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(5, 5, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(6, 6, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(7, 7, '2020-05-21 03:00:00', '2020-05-21 03:00:00');

#Administrador
INSERT INTO `administradores` (`id_administrador`, `nombre_usuario`, `nombre`, `apellido`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'matias', 'Matias', 'Ziliotto', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', NULL, NULL, NULL),
(2, 'jorge', 'Jorge', 'Mi�o', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', NULL, NULL, NULL);

#Menues
INSERT INTO `menu` (`id_menu`, `etiqueta`, `link`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'Quienes Somos', 'quienesSomos', 1, NULL, NULL),
(2, 'Contacto', 'contacto', 2, NULL, NULL),
(3, 'Ayuda', 'ayuda', 3, NULL, NULL);
#(4, 'Terminos y condiciones', 'terminosYCondiciones', 4, NULL, NULL);

#Categorias
INSERT INTO `categorias` (`id_categoria`, `id_menu`, `nombre_categoria`, `link`, `habilitado`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ca�as', '/categoria/1', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(2, 1, 'Anzuelos', '/categoria/2', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(4, 1, 'Termos', '/categoria/4', 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(5, 1, 'Reeles', '/categoria/verCategoria/5', 1, 1, '2020-06-05 08:15:51', '2020-06-05 08:16:16'),
(6, 1, 'Se�uelos', '/categoria/verCategoria/6', 1, 1, '2020-06-05 08:30:10', '2020-06-05 08:30:10'),
(7, 1, 'Hospedamiento', '/categoria/verCategoria/7', 1, 0, '2020-07-05 02:54:43', '2020-07-05 02:54:43');

#Subcategorias
INSERT INTO `subcategorias` (`id_subcategoria`, `id_categoria`, `nombre_subcategoria`, `link`, `habilitado`, `tipo`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cl�sica', 'subcategoria/verSubcategoria/1', 1, 1, '2020-05-15 06:00:00', '2020-06-05 07:59:12'),
(2, 1, 'R�gida', 'subcategoria/verSubcategoria/2', 1, 1, '2020-05-15 06:00:00', '2020-06-05 07:59:16'),
(3, 1, 'Surfcasting', 'subcategoria/verSubcategoria/3', 0, 1, '2020-05-15 06:00:00', '2020-06-05 07:59:20'),
(4, 2, 'Rectos', '/subcategoria/4', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(5, 2, 'Anilla', '/subcategoria/5', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(6, 2, 'Carbono', '/subcategoria/6', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(10, 4, '750 ml', '/subcategoria/10', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(11, 4, '1 lt', '/subcategoria/11', 1, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(12, 5, 'Frontal', 'subcategoria/verSubategoria/12', 1, 1, '2020-06-05 08:16:43', '2020-06-05 08:16:43'),
(13, 5, 'Rotativo', 'subcategoria/verSubategoria/13', 1, 1, '2020-06-05 08:16:48', '2020-06-05 08:16:48'),
(14, 6, 'Peces de superficie', 'subcategoria/verSubategoria/14', 1, 1, '2020-06-05 08:30:43', '2020-06-05 08:30:43'),
(15, 6, 'Jibioneras', 'subcategoria/verSubategoria/15', 1, 1, '2020-06-05 08:31:01', '2020-06-05 08:31:01'),
(16, 6, 'Jigs', 'subcategoria/verSubategoria/16', 1, 1, '2020-06-05 08:31:17', '2020-06-05 08:31:17'),
(17, 7, 'Hotel', 'subcategoria/verSubategoria/17', 1, 0, '2020-07-05 02:56:34', '2020-07-05 02:56:34'),
(18, 7, 'Camping', 'subcategoria/verSubategoria/18', 1, 0, '2020-07-05 02:56:40', '2020-07-05 02:56:40');

#Contacto
INSERT INTO `contacto` (`id_contacto`, `telefono_1`, `telefono_2`, `telefono_3`, `facebook`, `instagram`, `twitter`, `youtube`, `email_info`, `email_comprobantes`, `email_reclamos`) VALUES
(1, '556971', '2302659713', '0800-456-9778', 'https://www.facebook.com', 'https://www.instagram.com', 'https://www.twitter.com', 'https://www.youtube.com', 'info@gmail.com', 'comprobantes@gmail.com', 'reclamos@gmail.com');

#Preguntas frecuentes
INSERT INTO `preguntas_frecuentes` (`id_pregunta`, `texto_pregunta`, `texto_respuesta`, `mostrar`, `created_at`, `updated_at`) VALUES
(1, '�Como hago para comprar?', 'En el siguiente link www.mercadofishing.com y alli tienes que ingresar y comenzar a agregar productos al carrito .', 1, NULL, NULL),
(2, '�Como hago para agregar productos al carrito?', 'Al lado de cada producto tendras un boton que te permite agregar un nuevo producto al carrito de compras.', 1, NULL, NULL),
(3, '�Como puedo pagar?', 'Los metodos de pago que ofrecemos son todos los disponibles en mercado pago.', 1, NULL, NULL);

#Productos
INSERT INTO `productos` (`id_producto`, `nombre`, `descripcion`, `precio`, `id_subcategoria`, `descripcion_resumida`, `marca`, `alto`, `ancho`, `largo`, `peso`, `destacado`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 'Shimano Solara', 'Ca�a SHIMANO modelo SOLARA\r\nIdeal para la pesca de taruchas-truchas y dorados con artificiales .\r\nCa�a de 2 tramos  fibra de vidrio\r\nMango de corcho\r\nPasahilos reforzados de oxido de aluminio \r\nPortareel a rosca de grafito.', 2200.00, 1, 'Baitcast 2 Tramos 1,98 Mts', 'Shimano', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(2, 'Ca�a Telescopica Pejerrey Caster', '� Modelo: IRONFORCE TL-4004\r\n\r\n� Medida: 4m en 4 secciones\r\n\r\n� Composici�n 100% carbono IM7\r\n\r\n� 4 + 1 pasahilos SiC de titanio de 2 patas\r\n\r\n� Acci�n: Liviana\r\n\r\n� Cast: 15 a 40 gramos\r\n\r\n� Peso: 245 gramos\r\n\r\n� Medida cerrada: 129 cm\r\n\r\n� Diametro (mm): Top 2 / Butt 20.8\r\n\r\n� Empu�adura de pol�mero anti-deslizante\r\n\r\n� Portareel a cremallera de aluminio con topes de goma\r\n\r\n� Incluye capuch�n protector y funda de tela estampada', 2900.00, 1, 'Ca�a Telescopica Iron Force 4m Carbono Im7', 'Caster', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(3, 'Ca�a Waterdog Ribere�a', 'Ca�a Waterdog Ribere�a 1.95mts 2 Tramos 30-50lbs\r\n-Composici�n HIGH MODULUS GRAPHITE IM7\r\n-2 Tramos\r\n-Pasahilos de TITANIUM OXIDE ALUMINIUM\r\n-Porta reel de GRAFITO A ROSCA CON GATILLO.\r\n-Empu�adura de CORCHO DE ALTA DENSIDAD.\r\n-Resistencia: 30-50lbs\r\n-Incluye Funda', 3000.00, 1, '1.95mt 2tramos 30-50lbs 100% Gr�fito', 'Waterdog', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(4, 'Anzuelos Sasame Akitakitsune', 'BLISTER DE ANZUELOS\nSASAME AKITAKITSUNE BLACK NIKEL CON OJAL.\nMADE IN JAPAN\n(LA MEJOR CALIDAD EN ANZUELOS PARA PEJERREY)\nCLAVAN SOLOS!!!', 200.00, 4, 'Pejerrey Japones Varias Medidas', 'Waterdog', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-06-05 08:03:57'),
(5, 'Anzuelos Sasame Kamakura Pata Larga ', 'Anzuelo Japon�s de ojal, construido en acero inoxidable y recubiertos en niquel negro.\r\nDureza extrema y Filo extremo - Clavada 100% efectiva.\r\nConstruido y pensado para peces como el Dorado y la Tararira.', 260.00, 5, 'Pesca Dorado Japoneses', 'Sasame', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(6, 'Ca�a Mitchell Tanager Feeder', 'Ca�a de pejerrey Mitchell Tanager\r\n\r\n-3 Tramos + 2 punteras \r\n-Ca�a de grafito\r\n-Para plomo de 80 grs \r\n-Pasahilos aptos para multifilamento\r\n-Porta reel a rosca con empu�adura de corcho y goma eva \r\n-Largo 300 cm\r\n-Peso de la ca�a: 205 grs', 3200.00, 2, 'Ca�a Mitchell Tanager Feeder Pejerrey 3 Tramos Grafito 3 mts', 'Mitchell', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(7, 'Ca�a Albatros Champion', '- 1.80 mts en dos tramos.\r\n- Maciza de fibra de vidrio\r\n- Muy resistente\r\n- Pasahilos aptos para Multifilamento\r\n- Portarrel a rosca con topes plasticos para no rayar la pata del reel\r\n- Monta anzuelo\r\n- Incluye funda orignal\r\n- Ideal embarcado o variada de rio', 1100.00, 3, 'Ca�a Albatros Champion 1.80mt 2 Tramos Maciza Pesca Variada', 'Albatros', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-05-15 06:00:00', '2020-05-15 06:00:00'),
(8, 'Ca�a Albatros Scout', '- 1.80 mts en dos tramos\n- Maciza de fibra\n- Pasahilos de tres patas\n- Variedad de colores\n- Ideal variada liviana / ni�os', 750.00, 1, 'Ca�a Albatros Scout Maciza 1.80 M Dos Tramos', 'Albatros', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 07:41:00', '2020-06-05 07:41:52'),
(9, 'Ca�a Pejerrey Shimano Stimula', '� Largo: 4.20 mts.\n� 4 Secciones .\n� 100% Grafito .\n� 6 + 1 Pasahilos en oxido de aluminio resistentes al mono y multifilamento.\n� Mango con portareel a cremallera.\n� Peso: 250 gramos.\n� Plomada: Hasta 80 gramos.\n� Incluye Protector de Pasahilos.', 6000.00, 1, 'Ca�a Pejerrey Shimano Stimula 420 Telescopica', 'Shimano', 1.00, 1.00, 1.00, 1.00, 1, 1, '2020-06-05 08:08:02', '2020-06-05 08:08:02'),
(10, 'Ca�a De Mosca Patagonia', 'CA�A ALBATROS #6\n- Este modelo es ideal para truchas en r�os grandes, tarariras y dorados.\n- Construida en grafito (acci�n medium/fast).\n� 4 tramos.\n� Mango en corcho tipo cigaret.\n� Porta-reel de aluminio anodizado y madera.\n- mide 2,70m. (9\' pies)\n- Incluye tuvo r�gido para transporte y funda.', 7300.00, 1, 'Ca�a De Mosca Albatros Patagonia Fly #6 / 4 Tramos /con Tubo', 'Albatros', 1.00, 1.00, 1.00, 1.00, 1, 1, '2020-06-05 08:09:53', '2020-07-30 19:14:07'),
(11, 'Anzuelos Pejerrey Eagle', 'Anzuelo de bronce, ideal para pejerrey, permite acomodar muy bien una buena carnada.\n\nMedidas:\n\nN�4/0 - 5 unidades. Largo: 60 mm Abertura: 22 mm\nN�3/0 - 5 unidades. Largo: 58 mm Abertura: 22 mm\nN�2/0 - 6 unidades. Largo: 52 mm Abertura: 20mm\nN�1/0 - 6 unidades. Largo: 47 mm Abertura: 18 mm\nN�1 - 6 unidades. Largo: 40 mm Abertura: 15mm\nN�2 - 6 unidades. Largo: 35 mm Abertura: 13 mm\nN�4 - 6 unidades. Largo: 32 mm Abertura: 11 mm\nN�6 - 6 unidades. Largo: 28 mm Abertura: 8 mm\nN�8 - 6 unidades. Largo: 26 mm Abertura: 7 mm', 300.00, 4, 'Anzuelos Pejerrey Eagle Claw L044 Rio Laguna Pesca Carnada', 'Eagle Claw', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:12:16', '2020-06-05 08:12:16'),
(12, 'Anzuelos Pesca Waterdog', 'ANZUELOS PESCA WATERDOG X100 UNIDADES BOGA CARPA N� 1\n\n� Anzuelo Waterdog n� 1\n� Para Boga o Carpa\n� x100 Unidades\n� Modelo: 92553\n� Medida largo: 2,3 cm', 400.00, 6, 'Anzuelos Pesca Waterdog X100 Unidades Boga Carpa N�1 2.3cm', 'Waterdog', 1.00, 1.00, 1.00, 1.00, 1, 1, '2020-06-05 08:14:32', '2020-06-05 08:14:32'),
(13, 'Reel Albatros Aep 60', 'REEL ALBATROS AEP 60 / FRONTAL\n- 2 RULEMANES\n- IDEAL PESCA VARIADA, PEJE, ETC.\n- CARGA 140m. / 0.45mm. - 115m. / 0,50mm.\n- CUERPO Y CARRETE DE GRAFITO\n- SISTEMA DE BALANCEO POR COMPUTADORA\n- RELACI�N 5.2:1', 2000.00, 12, 'Reel Albatros Aep 60 - Frontal - Ideal Variada', 'Albatros', 1.00, 1.00, 1.00, 1.00, 1, 1, '2020-06-05 08:17:43', '2020-06-05 08:17:43'),
(14, 'Reel Huevito Colony', 'reel colony challenger\n5+1 Rulemanes.\nCuerpo y laterales de grafito.\nCarretel de aluminio.\nmanivela de grafito\nSistema de freno centrifugo\ncapacidad 140m /0.30mm', 5500.00, 13, 'Reel Huevito Colony Challenger 6 Rulmanes Baicasting', 'Colony', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:19:35', '2020-06-05 08:19:35'),
(15, 'Reel Frontal Albatros Mz 60', 'Reel Frontal 3BB MZ60 Albatros Con Carretel extra\n\nCaracteristicas:\n- 3 Rulemanes Con Carretel Extra\n- Cuerpo Grafito\n- Manija Grafito one touch\n- Rotor Balanceado\n- Relacion de giro: 5:1.1\n- Capacidad de nylon: 0,40mm/ 140Mts', 2800.00, 12, 'Reel Frontal Albatros Mz 60 2 Rulemanes Variada Rio', 'Albatros', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:20:50', '2020-06-05 08:20:50'),
(16, 'Reel Rotativo Spinit', '* Capacidad de Linea: 0.40/380 mt. / 0.60/150 mt.\n* Recuperaci�n: 4.2:1.\n* Engranaje anti-reverse con \"One Way Clutch\".\n* Armaz�n de bronce.\n* Carretel de aluminio.\n* Engranaje principal silencioso con freno estrella.\n* Chicharra con selector.\n* Freno dual centrifugo y mec�nico.\n* 4 Rulemanes.', 5000.00, 13, 'Reel Rotativo Spinit Off Shore', 'Spinit', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:22:57', '2020-06-05 08:22:57'),
(17, 'Reel Shimano Fx 2500', '- Reel Shimano Fx 2500\n- 3 Rulemanes De Acero Inox.\n- Antireverse Infinito\n- Rotor Balanceado Anti Vibraci�n.\n- Carretel De Aluminio.\n- Manija Met�lica Intercambiable.\n- Devanador Apto Multifilamento\n- Recuperaci�n Por Vuelta 71 Cm\n- Construido En Grafito Alta Resistencia\n- Relacion: 5:0.1\n- Peso 250grs.\n- Capacidad: Aproxi 160 Del 0.25mm\n- Freno: 4 kgs', 2000.00, 12, 'Reel Shimano Fx 2500 Spinning Pejerrey Modelo Nuevo', 'Shimano', 1.00, 1.00, 1.00, 1.00, 1, 1, '2020-06-05 08:24:33', '2020-06-05 08:24:33'),
(18, 'Reel Frontal Albatros Tifon 2000', 'Reel Tifon\n6+1BB\nCuerpo de carbon\nCarretel de aluminio anodizado doble\nManija CNC de aluminio intercambiable\nAnti-reverse infinito\nSistema de balanceo S-curve\nRelaci�n: 5.5:1\nCapacidad de l�nea (mm-m): 0.25 - 215 / 0.30 - 150 / 0.35 -110', 3000.00, 12, 'Reel Frontal Albatros Tifon 2000 7 Rulemanes Spool Aluminio', 'Albatros', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:26:05', '2020-06-05 08:26:05'),
(19, 'Reel Spinit Triumph', '3 Rulemanes.\nManivela balanceada ambidiestra.\nSistema Pick Up.\nRotor balanceado.\nAnti-reverse silencioso.\nMulti Stop.\nFreno delantero progresivo\nCapacidad: 0.30 / 195-0.35 / 145-0.40 / 110\nRelacion: 5.2:1', 2400.00, 12, 'Reel Spinit Triumph Se4000 Pesca Frontal Agente Oficial', 'Spinit', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:28:03', '2020-06-05 08:28:03'),
(20, 'Se�uelo Para Bait Cast', '-9cm\n-14.5 gramos\n-2 anzuelos\n-Profundidad maxima 0.6m', 700.00, 14, 'Se�uelo Para Bait Cast Spinning De Tararira Strikepro', 'Strikepro', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:33:37', '2020-06-05 08:33:37'),
(21, 'Se�uelo Cuchara Antienganche', 'SE�UELO CUCHARA ANTIENGANCHE\n\n* La medida de la cuchara es 8,5cm\n* El total de largo es 12.5cm', 200.00, 15, 'Se�uelo Cuchara Antienganche Tarariras Dorado Truchas Salmon', 'X- Fish', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:35:06', '2020-06-05 08:35:06'),
(22, 'Se�uelos Banana Donkb', '-2 ganchos\n-30 gramos', 200.00, 16, 'Se�uelos Banana Donkb Magnet Bad Junior Banana Classic', 'Donkb', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:37:29', '2020-06-05 08:37:29'),
(23, 'Se�uelo Rapala Twitchin', 'Acci�n: paseante subsuperficial (acci�n walking the dog).\nLargo: 8 cm.\nPeso: 12,5 gr.\nDensidad: slow sinking.\nProfundidad de nataci�n: entre 5 y 60 cm.\nAnzuelos: simples VMC In-line n�mero 2/0. Lastiman menos a los peces, con un mejor punto de anclaje para la pelea.\nAplicaci�n: cazadores de agua dulce y salada. Tarariras, doradillos, lenguados, anchoas, y peces del caribe.', 1500.00, 15, 'Se�uelo Rapala Twitchin Mullet Sxrtm08 Paseante Subsuperfici', 'Rapala', 1.00, 1.00, 1.00, 1.00, 1, 1, '2020-06-05 08:39:01', '2020-06-05 08:39:01'),
(24, 'Se�uelos Antienganche Gozio', 'Ficha T�cnica:\n- Blister por 2 unidades.\n-Antienganche con f�rmula mejorada ultra resistente a las mordidas con lo cual tienen mayor duraci�n.\n-Ideales para usar con anzuelo offset N�4\n- Hecho en Argentina', 300.00, 14, 'Se�uelos Antienganche Gozio Soft Lure Tarucha', 'GOZIO', 1.00, 1.00, 1.00, 1.00, 0, 1, '2020-06-05 08:40:50', '2020-06-05 08:41:04');

#Variantes
INSERT INTO `variantes_producto` (`id_variante_producto`, `color`, `talle_ropa`, `talle_calzado`, `talle_anzuelo`, `mano_habil`, `longitud`, `fuerza`, `accion`, `diametro`, `stock`, `habilitado`, `id_producto`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, '3 metros', '7', 'Alta', NULL, 0, 1, 1),
(3, NULL, NULL, NULL, NULL, NULL, '5 mts', '10', 'Media', NULL, 21, 1, 2),
(5, NULL, NULL, NULL, NULL, NULL, '2.5 metros', '12', 'Alta', NULL, 50, 1, 6),
(10, NULL, NULL, NULL, NULL, NULL, '3 mts', '3', 'Baja', NULL, 20, 1, 7),
(11, NULL, NULL, NULL, NULL, NULL, '2.5 metros', '15', 'Baja', NULL, 50, 1, 1),
(12, NULL, NULL, NULL, NULL, NULL, '2 metros', '5', 'Alta', NULL, 20, 1, 3),
(13, NULL, NULL, NULL, '14', NULL, NULL, NULL, NULL, NULL, 100, 1, 4),
(14, NULL, NULL, NULL, '1/0', NULL, NULL, NULL, NULL, NULL, 150, 1, 5),
(15, NULL, NULL, NULL, '4/0', NULL, NULL, NULL, NULL, NULL, 200, 1, 5),
(16, NULL, NULL, NULL, NULL, NULL, '2.5 metros', '10', 'Alta', NULL, 100, 1, 8),
(17, NULL, NULL, NULL, NULL, NULL, '5.5 metros', '2', 'Baja', NULL, 40, 1, 8),
(18, NULL, NULL, NULL, NULL, NULL, '2.5 metros', '3', 'Media', NULL, 150, 1, 9),
(19, NULL, NULL, NULL, NULL, NULL, '2.5 metros', '5', 'Media', NULL, 200, 1, 9),
(20, NULL, NULL, NULL, NULL, NULL, '1.5 metros', '15', 'Baja', NULL, 100, 1, 10),
(21, NULL, NULL, NULL, NULL, NULL, '1.8 metros', '15', 'Media', NULL, 20, 1, 10),
(22, NULL, NULL, NULL, '14', NULL, NULL, NULL, NULL, NULL, 100, 1, 11),
(23, NULL, NULL, NULL, '8', NULL, NULL, NULL, NULL, NULL, 200, 1, 11),
(24, NULL, NULL, NULL, '5', NULL, NULL, NULL, NULL, NULL, 200, 1, 12),
(25, NULL, NULL, NULL, '4', NULL, NULL, NULL, NULL, NULL, 100, 1, 12),
(26, 'Rojo', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 50, 1, 13),
(27, 'Rojo', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 20, 1, 14),
(28, 'Negro', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 15, 1, 15),
(29, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 20, 1, 16),
(30, 'Azul', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 40, 1, 17),
(31, 'Rojo', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 20, 1, 18),
(32, 'Rojo', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 10, 1, 18),
(33, 'Verde', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 10, 1, 19),
(34, NULL, NULL, NULL, '15', NULL, NULL, NULL, NULL, NULL, 40, 1, 20),
(35, NULL, NULL, NULL, '15', NULL, NULL, NULL, NULL, NULL, 20, 1, 21),
(36, NULL, NULL, NULL, '10/0', NULL, NULL, NULL, NULL, NULL, 10, 1, 21),
(37, NULL, NULL, NULL, '13', NULL, NULL, NULL, NULL, NULL, 30, 1, 21),
(38, NULL, NULL, NULL, '16', NULL, NULL, NULL, NULL, NULL, 10, 1, 22),
(39, NULL, NULL, NULL, '17', NULL, NULL, NULL, NULL, NULL, 10, 1, 23),
(40, NULL, NULL, NULL, '20', NULL, NULL, NULL, NULL, NULL, 20, 1, 23),
(41, NULL, NULL, NULL, '19', NULL, NULL, NULL, NULL, NULL, 30, 1, 23),
(42, NULL, NULL, NULL, '19', NULL, NULL, NULL, NULL, NULL, 40, 1, 23),
(43, NULL, NULL, NULL, '14', NULL, NULL, NULL, NULL, NULL, 30, 1, 24),
(44, NULL, NULL, NULL, '18', NULL, NULL, NULL, NULL, NULL, 10, 1, 24),
(45, NULL, NULL, NULL, '16', NULL, NULL, NULL, NULL, NULL, 20, 1, 24),
(46, NULL, NULL, NULL, '6/0', NULL, NULL, NULL, NULL, NULL, 0, 1, 4),
(47, NULL, NULL, NULL, NULL, NULL, '1.9 metros', '6', 'Media', NULL, 5, 1, 1),
(48, NULL, NULL, NULL, NULL, NULL, '1.5 metros', '17', 'Baja', NULL, 5, 1, 1),
(49, NULL, NULL, NULL, NULL, NULL, '3.5 metros', '2', 'Alta', NULL, 0, 1, 1),
(50, NULL, NULL, NULL, NULL, NULL, '4 metros', '12', 'Baja', NULL, 0, 1, 1);

#Servicios
INSERT INTO `servicios` (`id_servicio`, `titulo`, `descripcion`, `precio`, `id_subcategoria`, `descripcion_resumida`, `destacado`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 'Bog Atardeceres del lago', 'Contamos con 7 departamentos dise�ados para disfrutar de una de las mejores vistas al lago Nahuel Huapi en un entorno confortable, ambientados con muebles de dise�o. Ubicados frente a la bajada a la playa, mirando a la cordillera. Como su nombre lo indica, su balc�n ofrece atardeceres inolvidables.\nCada departamento es independiente y est� equipado con: cocina con horno, tostadora, pava el�ctrica, cafetera, parrilla,heladera, tv led de 42�� con Direc TV, wifi y caja de seguridad.\nAloj�ndote en este departamento tendr�s estacionamiento privado y acceso al club house con sala de juegos y TV, desde donde se accede a la pileta climatizada de temporada', 18514, 17, 'Villa La Angostura, a 3.0 km de: Centro de la ciudad', 0, 1, '2020-07-05 03:09:30', '2020-07-05 03:09:30'),
(2, 'Llao Llao Resort, Golf-Spa', 'Este hotel est� rodeado por el cerro L�pez, el cerro Tronador y los lagos Moreno y Nahuel Huapi y cuenta con spa y club de bienestar, campo de golf y piscinas cubiertas y al aire libre.\n\nTodas las habitaciones del Llao Llao Hotel & Resort, Golf-Spa incluyen TV y minibar y gozan de vistas al lago o a las monta�as de los alrededores. Solo hay aire acondicionado en algunas habitaciones. Se proporciona room service todos los d�as, las 24 horas.\n\nEl Llao Llao Hotel alberga un campo de golf de 18 hoyos y organiza diversas actividades al aire libre, como senderismo, ciclismo de monta�a y esqu�. El alojamiento ofrece varias terapias de bienestar, como tratamientos de relajaci�n, hidroterapia y masajes. Tambi�n est� equipado con sauna.\n\nEl restaurante del hotel elabora platos de las cocinas local (como cordero patag�nico) e internacional. Adem�s, el hotel ofrece infusiones especiales y happy hour por las tardes.\n\nEl Llao Llao se halla a 20 km de la estaci�n de esqu� Catedral, a 25 km del centro de Bariloche y a 35 km del aeropuerto de San Carlos de Bariloche.\n\nA las parejas les gusta la ubicaci�n. Le pusieron un puntaje de 9,5 para un viaje de a dos.\n\n�Hablamos tu idioma!', 15360, 17, 'Av. Bustillo Km 25, R8401ALN San Carlos de Bariloche, Argentina', 0, 1, '2020-07-05 03:19:12', '2020-07-05 03:19:12'),
(3, 'Camping La Encantada', 'El Camping La Encantada est� situado a 8 km de la bah�a Encerrada y ofrece alojamiento con sal�n compartido, jard�n y cocina compartida. Hay WiFi y estacionamiento privado gratuitos en el camping.\n\nEl Camping La Encantada sirve un desayuno continental y buffet a diario.\n\nEl alojamiento alberga un parque infantil.\n\nEn los alrededores se puede esquiar. El Camping La Encantada ofrece servicio de alquiler de autos.\n\nEl tren del Fin del Mundo se encuentra a 16 km del camping, mientras que el parque nacional Tierra del Fuego est� a 18 km. El aeropuerto m�s cercano es el aeropuerto internacional de Ushuaia-Malvinas Argentinas, ubicado a 12 km del Camping La Encantada. El alojamiento ofrece un servicio de link con el aeropuerto por un adicional.\n\n�Hablamos tu idioma!', 5800, 18, 'Seccion O masizo A grilla 15, 9410 Ushuaia, Argentina', 0, 1, '2020-07-05 03:25:44', '2020-07-05 03:25:44');

INSERT INTO `imagenes_productos` (`id_imagen`, `url`, `orden`, `mostrar`, `id_producto`, `id_servicio`) VALUES
(1, 'img/imagenes_productos/producto1/1_Cana_1.jpg', 1, 1, 1, NULL),
(2, 'img/imagenes_productos/producto1/2_Cana_2.jpg', 2, 1, 1, NULL),
(3, 'img/imagenes_productos/producto2/3_Cana_1.jpg', 1, 1, 2, NULL),
(4, 'img/imagenes_productos/producto5/4_anzuelos_1.jpg', 1, 1, 5, NULL),
(5, 'img/imagenes_productos/producto5/5_anzuelos_2.jpg', 2, 1, 5, NULL),
(12, 'img/imagenes_productos/producto8/12_Cana-Albatros (dorado).jpg', 1, 1, 8, NULL),
(13, 'img/imagenes_productos/producto8/13_Cana-Albatros (negro).jpg', 2, 1, 8, NULL),
(14, 'img/imagenes_productos/producto6/14_Cana- (negro-celeste).jpg', 1, 1, 6, NULL),
(15, 'img/imagenes_productos/producto7/15_Cana-champion (verde).jpg', 1, 1, 7, NULL),
(16, 'img/imagenes_productos/producto3/16_Cana-Kay (negro).jpeg', 1, 1, 3, NULL),
(17, 'img/imagenes_productos/producto4/17_Anzuelo-Limerick (dorado).jpg', 1, 1, 4, NULL),
(18, 'img/imagenes_productos/producto9/18_Cana-Shimano (marron claro-negro).jpg', 1, 1, 9, NULL),
(19, 'img/imagenes_productos/producto9/19_Cana-Shimano (negro).jpeg', 2, 1, 9, NULL),
(20, 'img/imagenes_productos/producto/20_Cana-Patagonia (violeta).jpg', 1, 1, 10, NULL),
(21, 'img/imagenes_productos/producto11/21_Anzuelos-Eagle (dorado).jpg', 1, 1, 11, NULL),
(22, 'img/imagenes_productos/producto11/22_Anzuelos-Eagle (plateado).jpg', 2, 1, 11, NULL),
(23, 'img/imagenes_productos/producto12/23_Anzuelos-Mustad (negro).jpg', 1, 1, 12, NULL),
(24, 'img/imagenes_productos/producto12/24_Anzuelos-Mustad (plateado).jpg', 2, 1, 12, NULL),
(25, 'img/imagenes_productos/producto13/25_Reel-AEP (amarillo).jpg', 1, 1, 13, NULL),
(26, 'img/imagenes_productos/producto14/26_Reel-Colony (verde oscuro).jpg', 1, 1, 14, NULL),
(27, 'img/imagenes_productos/producto15/27_Reel-MZ (gris-naranja).jpg', 1, 1, 15, NULL),
(28, 'img/imagenes_productos/producto16/28_Reel-Safari (plateado-brillante).jpg', 1, 1, 16, NULL),
(29, 'img/imagenes_productos/producto17/29_Reel-Shimano (plateado-dorado).jpg', 1, 1, 17, NULL),
(30, 'img/imagenes_productos/producto18/30_Reel-tifon (dorado).jpg', 1, 1, 18, NULL),
(31, 'img/imagenes_productos/producto18/31_Reel-tifon (verde).jpg', 2, 1, 18, NULL),
(32, 'img/imagenes_productos/producto19/32_Reel-triumph (negro-rojo).jpg', 1, 1, 19, NULL),
(33, 'img/imagenes_productos/producto20/33_Senuelo-Baitcasting.png', 1, 1, 20, NULL),
(34, 'img/imagenes_productos/producto21/34_Senuelo-Cucharita.png', 1, 1, 21, NULL),
(35, 'img/imagenes_productos/producto/35_Senuelo-Kb.jpg', 1, 1, 22, NULL),
(36, 'img/imagenes_productos/producto23/36_Senuelo-Rapala.jpg', 1, 1, 23, NULL),
(37, 'img/imagenes_productos/producto23/37_Senuelo-Samurai.jpg', 2, 1, 23, NULL),
(38, 'img/imagenes_productos/producto24/38_Senuelo-Tech.jpg', 1, 1, 24, NULL),
(39, 'img/imagenes_servicios/servicio1/39_imagen_39.jpg', 1, 1, NULL, 1),
(40, 'img/imagenes_servicios/servicio1/40_imagen_40.jpg', 2, 1, NULL, 1),
(41, 'img/imagenes_servicios/servicio1/41_imagen_41.jpg', 3, 1, NULL, 1),
(42, 'img/imagenes_servicios/servicio2/42_imagen_42.jpg', 1, 1, NULL, 2),
(43, 'img/imagenes_servicios/servicio2/43_imagen_43.jpg', 2, 1, NULL, 2),
(44, 'img/imagenes_servicios/servicio2/44_imagen_44.jpg', 3, 1, NULL, 2),
(45, 'img/imagenes_servicios/servicio2/45_imagen_45.jpg', 4, 1, NULL, 2),
(46, 'img/imagenes_servicios/servicio3/46_imagen_46.jpg', 1, 1, NULL, 3),
(47, 'img/imagenes_servicios/servicio3/47_imagen_47.jpg', 2, 1, NULL, 3),
(48, 'img/imagenes_servicios/servicio3/48_imagen_48.jpg', 3, 1, NULL, 3);

#Terminos y condiciones
INSERT INTO `terminos_y_condiciones` (`id_termino_condiciones`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL),
(2, 'Lorem ipsum 2', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?', NULL, NULL),
(3, 'Lorem ipsum 3', 'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?', NULL, NULL),
(4, 'Lorem ipsum 4', 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', NULL, NULL);

#Slider
INSERT INTO `slider` (`id_imagen`, `url`, `orden`, `texto`, `mostrar`, `created_at`, `updated_at`) VALUES
(1, 'img/slider/1_imagen.jpg', '1', 'Ca�a gris de 2.3 mts de longitud flexible', 1, '2020-05-15 03:00:00', '2020-06-05 20:19:59'),
(2, 'img/slider/2_imagen.jpg', '2', 'Venta de todo tipo de anzuelos para un mejor agarre', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(3, 'img/slider/3_imagen.jpg', '3', 'Reeles de todos los colores para zurdos y derechos', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(4, 'img/slider/4_imagen.jpg', '4', '', 0, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(5, 'img/slider/5_imagen.jpg', '5', '', 0, '2020-05-15 03:00:00', '2020-05-15 03:00:00');

#Quienes somos
INSERT INTO `quienes_somos` (`id_quienes_somos`, `review`, `objetivos`, `created_at`, `updated_at`) VALUES (NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', NULL, NULL);