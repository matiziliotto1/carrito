//Para crear datos de prueba en la Base de Datos
php artisan db:seed

//Reiniciar la Base de Datos y cargarla con datos de prueba
php artisan migrate:fresh --seed

//Cargar datos de prueba de una clase determinada
php artisan db:seed --class=UserSeeder

Para importar la libreria se pone en la carpeta app, que composer la carga automaticamente en 
composer.json en la parte de autoload->psr-4
Despues en los archivos de la libreria hay que poner namespace y la ruta de la carpeta ej:
namespace App\mercadopago\lib;
Despues donde queramos usar la libreria tenemos que poner use y la ruta del namespace ej:
use App\mercadopago\lib\MP;

----------------------------------------------------------------------------------------------
Si quiero guardar en otra carpeta distinta a App, tendria que agregar la ruta en composer.json
donde dice
"autoload": {
        "psr-4": {
            "App\\": "app/"
		<-------- aca agregar, por ejemplo: "MisLibrerias\\":"mis_lib/"
        },
----------------------------OJO al tejo que esto no lo probe-----------------------------------
