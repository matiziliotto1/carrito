-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-05-2020 a las 18:18:20
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `carrito_compras`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administradores`
--

CREATE TABLE `administradores` (
  `id_administrador` bigint(20) UNSIGNED NOT NULL,
  `nombre_usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `administradores`
--

INSERT INTO `administradores` (`id_administrador`, `nombre_usuario`, `nombre`, `apellido`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'matias', 'Matias', 'Ziliotto', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', NULL, NULL, NULL),
(2, 'jorge', 'Jorge', 'Miño', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_compras`
--

CREATE TABLE `carrito_compras` (
  `id_carrito` bigint(20) UNSIGNED NOT NULL,
  `id_usuario` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `carrito_compras`
--

INSERT INTO `carrito_compras` (`id_carrito`, `id_usuario`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(2, 2, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(3, 3, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(4, 4, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(5, 5, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(6, 6, '2020-05-21 03:00:00', '2020-05-21 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id_categoria` bigint(20) UNSIGNED NOT NULL,
  `id_menu` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `nombre_categoria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `habilitado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id_categoria`, `id_menu`, `nombre_categoria`, `link`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cañas', '/categoria/1', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(2, 1, 'Anzuelos', '/categoria/2', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(3, 1, 'Indumentaria', '/categorias/3', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(4, 1, 'Termos', '/categoria/4', 0, '2020-05-15 03:00:00', '2020-05-15 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras`
--

CREATE TABLE `compras` (
  `id_compra` bigint(20) UNSIGNED NOT NULL,
  `id_usuario` bigint(20) UNSIGNED NOT NULL,
  `fecha_compra` date NOT NULL,
  `puntaje_compra` bigint(20) UNSIGNED NOT NULL,
  `monto_total` bigint(20) UNSIGNED NOT NULL,
  `codigo_seguimiento` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metodo_pago` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metodo_envio` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_x_productos`
--

CREATE TABLE `compras_x_productos` (
  `id_compra` bigint(20) UNSIGNED NOT NULL,
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE `contacto` (
  `id_contacto` bigint(20) UNSIGNED NOT NULL,
  `telefono_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_info` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_comprobantes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_reclamos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id_contacto`, `telefono_1`, `telefono_2`, `telefono_3`, `facebook`, `instagram`, `twitter`, `youtube`, `email_info`, `email_comprobantes`, `email_reclamos`) VALUES
(1, '556971', '2302659713', '0800-456-9778', 'https://www.facebook.com', 'https://www.instagram.com', 'https://www.twitter.com', 'https://www.youtube.com', 'info@gmail.com', 'comprobantes@gmail.com', 'reclamos@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes_productos`
--

CREATE TABLE `imagenes_productos` (
  `id_imagen` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orden` int(11) NOT NULL,
  `mostrar` tinyint(1) NOT NULL DEFAULT '1',
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `imagenes_productos`
--

INSERT INTO `imagenes_productos` (`id_imagen`, `url`, `orden`, `mostrar`, `id_producto`, `created_at`, `updated_at`) VALUES
(1, '/img/imagenes_productos/producto1/1_Caña_1.jpg', 1, 1, 1, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(2, '/img/imagenes_productos/producto1/2_Caña_2.jpg', 2, 1, 1, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(3, '/img/imagenes_productos/producto2/3_Caña_1.jpg', 1, 1, 2, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(4, '/img/imagenes_productos/producto5/4_anzuelos_1.jpg', 1, 1, 5, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(5, '/img/imagenes_productos/producto5/5_anzuelos_2.jpg', 2, 1, 5, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(6, '/img/imagenes_productos/producto6/6_remera_1.jpg', 1, 1, 6, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(7, '/img/imagenes_productos/producto6/7_remera_2.jpg', 2, 1, 6, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(8, '/img/imagenes_productos/producto6/8_remera_3.jpeg', 3, 1, 6, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(9, '/img/imagenes_productos/producto7/9_botas_1.jpg', 1, 1, 7, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(10, '/img/imagenes_productos/producto7/10_botas_2.jpg', 2, 1, 7, '2020-05-21 03:00:00', '2020-05-21 03:00:00'),
(11, '/img/imagenes_productos/producto7/11_botas_3.png', 3, 1, 7, '2020-05-21 03:00:00', '2020-05-21 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `id_menu` bigint(20) UNSIGNED NOT NULL,
  `etiqueta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`id_menu`, `etiqueta`, `link`, `orden`, `created_at`, `updated_at`) VALUES
(1, 'Categorias', '#', 1, NULL, NULL),
(2, 'Terminos y condiciones', '#', 1, NULL, NULL),
(3, 'Contacto', '#', 1, NULL, NULL),
(4, 'Ayuda', 'ayuda', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(91, '2014_10_12_000000_create_users_table', 1),
(92, '2014_10_12_100000_create_password_resets_table', 1),
(93, '2019_08_19_000000_create_failed_jobs_table', 1),
(94, '2020_05_06_212654_create_carrito_compras_table', 1),
(95, '2020_05_06_212710_create_menu_table', 1),
(96, '2020_05_06_213146_create_categorias_table', 1),
(97, '2020_05_06_213446_create_subcategorias_table', 1),
(98, '2020_05_06_213556_create_contacto_table', 1),
(99, '2020_05_06_213827_create_slider_table', 1),
(100, '2020_05_06_213828_create_productos_table', 1),
(101, '2020_05_06_213929_create_productos_x_carritos_table', 1),
(102, '2020_05_06_214717_create_variantes_producto_table', 1),
(103, '2020_05_06_215150_create_imagenes_productos_table', 1),
(104, '2020_05_06_215404_create_compras_table', 1),
(105, '2020_05_06_215428_create_compras_x_productos_table', 1),
(106, '2020_05_06_215924_create_recibos_table', 1),
(107, '2020_05_07_000519_create_preguntas_frecuentes_table', 1),
(108, '2020_05_09_023242_create_administradores_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas_frecuentes`
--

CREATE TABLE `preguntas_frecuentes` (
  `id_pregunta` bigint(20) UNSIGNED NOT NULL,
  `texto_pregunta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto_respuesta` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mostrar` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `preguntas_frecuentes`
--

INSERT INTO `preguntas_frecuentes` (`id_pregunta`, `texto_pregunta`, `texto_respuesta`, `mostrar`, `created_at`, `updated_at`) VALUES
(1, '¿Como hago para comprar?', 'En el siguiente link www.mercadofishing.com y alli tienes que ingresar y comenzar a agregar productos al carrito .', 1, NULL, NULL),
(2, '¿Como hago para agregar productos al carrito?', 'Al lado de cada producto tendras un boton que te permite agregar un nuevo producto al carrito de compras.', 1, NULL, NULL),
(3, '¿Como puedo pagar?', 'Los metodos de pago que ofrecemos son todos los disponibles en mercado pago.', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` bigint(20) UNSIGNED NOT NULL,
  `id_subcategoria` bigint(20) UNSIGNED NOT NULL,
  `descripcion_resumida` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `marca` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `habilitado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id_producto`, `nombre`, `descripcion`, `precio`, `id_subcategoria`, `descripcion_resumida`, `marca`, `destacado`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 'Shimano Solara', 'Caña SHIMANO modelo SOLARA\nIdeal para la pesca de taruchas-truchas y dorados con artificiales .\nCaña de 2 tramos  fibra de vidrio\nMango de corcho\nPasahilos reforzados de oxido de aluminio \nPortareel a rosca de grafito.', 2200, 1, 'Baitcast 2 Tramos 1,98 Mts', 'Shimano', 1, 1, '2020-05-15 03:00:00', '2020-05-24 02:46:42'),
(2, 'Caña Telescopica Pejerrey Caster', '• Modelo: IRONFORCE TL-4004\n\n• Medida: 4m en 4 secciones\n\n• Composición 100% carbono IM7\n\n• 4 + 1 pasahilos SiC de titanio de 2 patas\n\n• Acción: Liviana\n\n• Cast: 15 a 40 gramos\n\n• Peso: 245 gramos\n\n• Medida cerrada: 129 cm\n\n• Diametro (mm): Top 2 / Butt 20.8\n\n• Empuñadura de polímero anti-deslizante\n\n• Portareel a cremallera de aluminio con topes de goma\n\n• Incluye capuchón protector y funda de tela estampada', 2900, 1, 'Caña Telescopica Iron Force 4m Carbono Im7', 'Caster', 1, 1, '2020-05-15 03:00:00', '2020-05-24 02:46:50'),
(3, 'Caña Waterdog Ribereña', 'Caña Waterdog Ribereña 1.95mts 2 Tramos 30-50lbs\n-Composición HIGH MODULUS GRAPHITE IM7\n-2 Tramos\n-Pasahilos de TITANIUM OXIDE ALUMINIUM\n-Porta reel de GRAFITO A ROSCA CON GATILLO.\n-Empuñadura de CORCHO DE ALTA DENSIDAD.\n-Resistencia: 30-50lbs\n-Incluye Funda', 3000, 1, '1.95mt 2tramos 30-50lbs 100% Gràfito', 'Waterdog', 1, 1, '2020-05-15 03:00:00', '2020-05-24 02:46:56'),
(4, 'Anzuelos Sasame Akitakitsune', 'BLISTER DE ANZUELOS\r\nSASAME AKITAKITSUNE BLACK NIKEL CON OJAL.\r\nMADE IN JAPAN\r\n(LA MEJOR CALIDAD EN ANZUELOS PARA PEJERREY)\r\nCLAVAN SOLOS!!!', 200, 2, 'Pejerrey Japones Varias Medidas', 'Waterdog', 0, 1, '2020-05-15 03:00:00', '2020-05-24 02:40:29'),
(5, 'Anzuelos Sasame Kamakura Pata Larga ', 'Anzuelo Japonés de ojal, construido en acero inoxidable y recubiertos en niquel negro.\r\nDureza extrema y Filo extremo - Clavada 100% efectiva.\r\nConstruido y pensado para peces como el Dorado y la Tararira.', 260, 5, 'Pesca Dorado Japoneses', 'Sasame', 0, 1, '2020-05-15 03:00:00', '2020-05-24 02:40:30'),
(6, 'Remera Pesca Secado Rapido', 'COMBO REMERA + BUFF GO FISH\r\n» Diseños únicos y exclusivos de GO FISH\r\n» Anti arrugas\r\n» Talles disponibles: S, M, L, XL, XXL, XXXL\r\n» Reducción de manchas\r\n» Fácil de cuidar\r\n» Larga duración al lavado\r\n» Suavidad al tacto', 3200, 7, 'Cuello Go Fish Surubi Seca Rapido Uv + Llavero Filtro Uv Secado Rapido Anti Arrugas', 'Go Fish', 0, 1, '2020-05-15 03:00:00', '2020-05-24 02:40:31'),
(7, 'Botas De Neoprene', '-Modelo DV-40B\r\n-Espesor  5mm\r\n-Con suela de goma \r\n-Puntera reforzada \r\n-Tiene cierre lateral \r\n-Consulte talles disponible', 4500, 8, 'Waterdog Dv40b 5mm Todos Los Talles', 'Waterdog', 0, 1, '2020-05-15 03:00:00', '2020-05-24 02:40:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_x_carritos`
--

CREATE TABLE `productos_x_carritos` (
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `id_carrito` bigint(20) UNSIGNED NOT NULL,
  `cantidad` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `productos_x_carritos`
--

INSERT INTO `productos_x_carritos` (`id_producto`, `id_carrito`, `cantidad`, `created_at`, `updated_at`) VALUES
(6, 1, 1, '2020-05-23 22:40:49', '2020-05-23 22:40:49'),
(7, 1, 1, '2020-05-24 23:43:17', '2020-05-24 23:43:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibos`
--

CREATE TABLE `recibos` (
  `id_recibo` bigint(20) UNSIGNED NOT NULL,
  `id_compra` bigint(20) UNSIGNED NOT NULL,
  `fecha_recibo` date NOT NULL,
  `gastos_envio` bigint(20) UNSIGNED NOT NULL,
  `monto_pagado` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id_imagen` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orden` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mostrar` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id_imagen`, `url`, `orden`, `mostrar`, `created_at`, `updated_at`) VALUES
(1, '/img/slider/1_imagen.jpg', '1', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(2, '/img/slider/2_imagen.jpg', '2', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(3, '/img/slider/3_imagen.jpg', '3', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(4, '/img/slider/4_imagen.jpg', '4', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(5, '/img/slider/5_imagen.jpg', '5', 0, '2020-05-15 03:00:00', '2020-05-15 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategorias`
--

CREATE TABLE `subcategorias` (
  `id_subcategoria` bigint(20) UNSIGNED NOT NULL,
  `id_categoria` bigint(20) UNSIGNED NOT NULL,
  `nombre_subcategoria` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `habilitado` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `subcategorias`
--

INSERT INTO `subcategorias` (`id_subcategoria`, `id_categoria`, `nombre_subcategoria`, `link`, `habilitado`, `created_at`, `updated_at`) VALUES
(1, 1, 'clásica', '/subcategoria/1', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(2, 1, 'rígida', '/subcategoria/2', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(3, 1, 'surfcasting', '/subcategoria/3', 0, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(4, 2, 'Rectos', '/subcategoria/4', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(5, 2, 'Anilla', '/subcategoria/5', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(6, 2, 'Carbono', '/subcategoria/6', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(7, 3, 'Remeras', '/subcategoria/7', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(8, 3, 'Botas', '/subcategoria/8', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(9, 3, 'Guantes', '/subcategoria/9', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(10, 4, '750 ml', '/subcategoria/10', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00'),
(11, 4, '1 lt', '/subcategoria/11', 1, '2020-05-15 03:00:00', '2020-05-15 03:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_documento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `numero_documento` int(11) NOT NULL,
  `codigo_postal` int(11) NOT NULL,
  `domicilio_real` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `domicilio_recepcion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_fijo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_celular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `puntaje_acumulado` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `habilitado` tinyint(1) NOT NULL DEFAULT '1',
  `eliminado` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `nombre`, `apellido`, `tipo_documento`, `numero_documento`, `codigo_postal`, `domicilio_real`, `domicilio_recepcion`, `telefono_fijo`, `telefono_celular`, `puntaje_acumulado`, `habilitado`, `eliminado`, `email`, `password`, `email_verified_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Matias', 'Ziliotto', 'DNI', 40610993, 6360, 'Calle 30 n° 964', 'Calle 30 n° 964', '332380', '2302209165', 0, 1, 0, 'mati_97_ziliotto@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, '2020-05-22 03:00:00', NULL),
(2, 'Jorge', 'Miño', 'DNI', 39931241, 6360, 'Calle 105 bis  n° 34', 'Calle 105 bis n° 34', '332380', '2302209165', 0, 1, 0, 'jormino78@gmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, NULL, NULL),
(3, 'Juan', 'Perez', 'DNI', 40610113, 6360, 'Calle 20 n° 123', 'Calle 20 n° 123', '332380', '2302209165', 0, 1, 0, 'juan@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, NULL, NULL),
(4, 'Maria', 'Dominguez', 'DNI', 45630993, 6360, 'Calle 20 n° 123', 'Calle 20 n° 123', '332380', '2302209165', 0, 1, 0, 'maria@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, NULL, NULL),
(5, 'Pedro', 'Garcia', 'DNI', 41610693, 6360, 'Calle 20 n° 123', 'Calle 20 n° 123', '332380', '2302209165', 0, 1, 0, 'pedro@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, NULL, NULL),
(6, 'Lucia', 'Martinez', 'DNI', 30610293, 6360, 'Calle 20 n° 123', 'Calle 20 n° 123', '332380', '2302209165', 0, 1, 0, 'lucia@hotmail.com', '$2y$10$EGYbqJtrCjAurosOq4XeTe/9FJkuIaxggdGZJSm1xKLRR2SfzG9HC', '2020-05-10 03:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `variantes_producto`
--

CREATE TABLE `variantes_producto` (
  `id_variante_producto` bigint(20) UNSIGNED NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diestro` tinyint(1) NOT NULL DEFAULT '1',
  `stock` int(11) NOT NULL,
  `id_producto` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `variantes_producto`
--

INSERT INTO `variantes_producto` (`id_variante_producto`, `color`, `diestro`, `stock`, `id_producto`, `created_at`, `updated_at`) VALUES
(1, 'Negro', 1, 50, 1, NULL, NULL),
(2, 'Blanco', 1, 50, 2, NULL, NULL),
(3, 'Negro', 1, 21, 2, NULL, NULL),
(4, 'Rojo', 1, 5, 2, NULL, NULL),
(5, 'Blanco', 1, 50, 6, NULL, NULL),
(6, 'Negro', 1, 21, 6, NULL, NULL),
(7, 'Rojo', 1, 5, 6, NULL, NULL),
(8, 'Gris', 1, 50, 6, NULL, NULL),
(9, 'Marron', 1, 21, 6, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`id_administrador`);

--
-- Indices de la tabla `carrito_compras`
--
ALTER TABLE `carrito_compras`
  ADD PRIMARY KEY (`id_carrito`),
  ADD KEY `carrito_compras_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id_categoria`),
  ADD KEY `categorias_id_menu_foreign` (`id_menu`);

--
-- Indices de la tabla `compras`
--
ALTER TABLE `compras`
  ADD PRIMARY KEY (`id_compra`),
  ADD KEY `compras_id_usuario_foreign` (`id_usuario`);

--
-- Indices de la tabla `compras_x_productos`
--
ALTER TABLE `compras_x_productos`
  ADD PRIMARY KEY (`id_compra`,`id_producto`),
  ADD KEY `compras_x_productos_id_producto_foreign` (`id_producto`);

--
-- Indices de la tabla `contacto`
--
ALTER TABLE `contacto`
  ADD PRIMARY KEY (`id_contacto`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `imagenes_productos`
--
ALTER TABLE `imagenes_productos`
  ADD PRIMARY KEY (`id_imagen`),
  ADD KEY `imagenes_productos_id_producto_foreign` (`id_producto`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id_producto`),
  ADD KEY `productos_id_subcategoria_foreign` (`id_subcategoria`);

--
-- Indices de la tabla `productos_x_carritos`
--
ALTER TABLE `productos_x_carritos`
  ADD PRIMARY KEY (`id_producto`,`id_carrito`),
  ADD KEY `productos_x_carritos_id_carrito_foreign` (`id_carrito`);

--
-- Indices de la tabla `recibos`
--
ALTER TABLE `recibos`
  ADD PRIMARY KEY (`id_recibo`),
  ADD KEY `recibos_id_compra_foreign` (`id_compra`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id_imagen`);

--
-- Indices de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `subcategorias_id_categoria_foreign` (`id_categoria`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `variantes_producto`
--
ALTER TABLE `variantes_producto`
  ADD PRIMARY KEY (`id_variante_producto`),
  ADD KEY `variantes_producto_id_producto_foreign` (`id_producto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administradores`
--
ALTER TABLE `administradores`
  MODIFY `id_administrador` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `carrito_compras`
--
ALTER TABLE `carrito_compras`
  MODIFY `id_carrito` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id_categoria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `compras`
--
ALTER TABLE `compras`
  MODIFY `id_compra` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `contacto`
--
ALTER TABLE `contacto`
  MODIFY `id_contacto` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `imagenes_productos`
--
ALTER TABLE `imagenes_productos`
  MODIFY `id_imagen` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de la tabla `preguntas_frecuentes`
--
ALTER TABLE `preguntas_frecuentes`
  MODIFY `id_pregunta` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id_producto` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `recibos`
--
ALTER TABLE `recibos`
  MODIFY `id_recibo` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `id_imagen` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  MODIFY `id_subcategoria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `variantes_producto`
--
ALTER TABLE `variantes_producto`
  MODIFY `id_variante_producto` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito_compras`
--
ALTER TABLE `carrito_compras`
  ADD CONSTRAINT `carrito_compras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD CONSTRAINT `categorias_id_menu_foreign` FOREIGN KEY (`id_menu`) REFERENCES `menu` (`id_menu`);

--
-- Filtros para la tabla `compras`
--
ALTER TABLE `compras`
  ADD CONSTRAINT `compras_id_usuario_foreign` FOREIGN KEY (`id_usuario`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `compras_x_productos`
--
ALTER TABLE `compras_x_productos`
  ADD CONSTRAINT `compras_x_productos_id_compra_foreign` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `compras_x_productos_id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `imagenes_productos`
--
ALTER TABLE `imagenes_productos`
  ADD CONSTRAINT `imagenes_productos_id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_id_subcategoria_foreign` FOREIGN KEY (`id_subcategoria`) REFERENCES `subcategorias` (`id_subcategoria`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `productos_x_carritos`
--
ALTER TABLE `productos_x_carritos`
  ADD CONSTRAINT `productos_x_carritos_id_carrito_foreign` FOREIGN KEY (`id_carrito`) REFERENCES `carrito_compras` (`id_carrito`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `productos_x_carritos_id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `recibos`
--
ALTER TABLE `recibos`
  ADD CONSTRAINT `recibos_id_compra_foreign` FOREIGN KEY (`id_compra`) REFERENCES `compras` (`id_compra`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Filtros para la tabla `subcategorias`
--
ALTER TABLE `subcategorias`
  ADD CONSTRAINT `subcategorias_id_categoria_foreign` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`);

--
-- Filtros para la tabla `variantes_producto`
--
ALTER TABLE `variantes_producto`
  ADD CONSTRAINT `variantes_producto_id_producto_foreign` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
